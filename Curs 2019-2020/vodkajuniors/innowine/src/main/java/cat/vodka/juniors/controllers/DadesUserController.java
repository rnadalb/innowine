package cat.vodka.juniors.controllers;

import cat.vodka.juniors.Main;
import cat.vodka.juniors.classes.DAOHelper;
import cat.vodka.juniors.model.Config;
import cat.vodka.juniors.model.Usuari;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DadesUserController extends DefaultControllerDiferit implements Initializable {

    @FXML
    public TextField tfLogin,tfIntents,tfCaracters,tfNom,tfCognoms,tfCorreu,tfTelefon,tfAdress,tfCP,tfPoblacio,tfPais,tfPassword;

    @FXML
    public TextArea taCos,taAssumpte;

    @FXML
    public TabPane tabPanell;

    @FXML
    public Button btActualitza;

    @FXML
    public Label lbNoti,lbNotiAdmin;

    @FXML
    public Tab tabAdmin,tabDades;


    public void Restriccio(Usuari user) {
        this.getUsuari().isAdmin();
        if (!user.isAdmin()) {
            tabPanell.getTabs().remove(tabAdmin);
        }
    }

    public void btActualitzaOnMouseClicked(MouseEvent mouseEvent) {

        if (tfLogin.getText().isEmpty()) {
            lbNoti.setText("No has escrit al camp login");
        } else if(tfNom.getText().isEmpty()) {
            lbNoti.setText("No has escrit al camp nom");
        } else if(tfCognoms.getText().isEmpty()) {
            lbNoti.setText("No has escrit al camp cognom");
        } else if(tfCorreu.getText().isEmpty()) {
            lbNoti.setText("No has escrit al camp correu");
        }
        else {
            DAOHelper<Usuari> helper = new DAOHelper<>(getEmf(), Usuari.class);
            getUsuari().setLogin(tfLogin.getText());
            getUsuari().setNom(tfNom.getText());
            getUsuari().setCognom(tfCognoms.getText());
            getUsuari().setCorreu(tfCorreu.getText());
            getUsuari().setTelefon(tfTelefon.getText());
            getUsuari().setCodiPostal(tfCP.getText());
            getUsuari().setPoblacio(tfPoblacio.getText());
            getUsuari().setPais(tfPais.getText());
            getUsuari().setAdreca(tfAdress.getText());
            helper.update(getUsuari());
            lbNoti.setText("Dades actualitzades!");
        }

    }

    public void btActualitzaConfigOnMsouseClicked(MouseEvent mouseEvent) {

        if (tfIntents.getText().isEmpty()) {
            lbNotiAdmin.setText("Número d'intents buit");
        } else if(tfCaracters.getText().isEmpty()) {
            lbNotiAdmin.setText("Número de caracters buit");
        }
        else {
            DAOHelper<Config> hConfig = new DAOHelper<>(getEmf(), Config.class);
            Config.getInstancia().setMaxIntents(Integer.valueOf(tfIntents.getText()));
            Config.getInstancia().setPassLong(Integer.valueOf(tfCaracters.getText()));
            Config.getInstancia().setDefaultMessage(taCos.getText());
            Config.getInstancia().setSubjectMessage(taAssumpte.getText());
            hConfig.update(Config.getInstancia());
            lbNotiAdmin.setText("Dades actualitzades!");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //String login;

    }

    public void btCanviarContrasenyaOnMouseClicked() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CanviarPasswd.fxml"));
        Parent root = null;
        Stage stage = new Stage();
        try {
            root = loader.load();
            Scene scene = new Scene(root);
            stage.setAlwaysOnTop(true);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.initStyle(StageStyle.DECORATED);
            stage.initModality(Modality.APPLICATION_MODAL); // No permet usar la finestra que hi ha per sota la finestra mostrada
            CanviarPasswdController controller = loader.getController();
            controller.setEmf(getEmf()); //Li passem l'objecte Emf.
            controller.setUsuari(this.getUsuari());
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }

    @Override
    public void iniciDiferit() {
        Restriccio(getUsuari());
        System.out.println(getUsuari());

        //Usuari
        tfLogin.setText(getUsuari().getLogin());
        tfNom.setText(getUsuari().getNom());
        tfCognoms.setText(getUsuari().getCognom());
        tfCorreu.setText(getUsuari().getCorreu());
        tfTelefon.setText(getUsuari().getTelefon());
        //tfPassword.setText(getUsuari().getPassword());
        tfAdress.setText(getUsuari().getAdreca());
        tfCP.setText(getUsuari().getCodiPostal());
        tfPoblacio.setText(getUsuari().getPoblacio());
        tfPais.setText(getUsuari().getPais());

        //Config
        tfIntents.setText(String.valueOf(Config.getInstancia().getMaxIntents()));
        tfCaracters.setText(String.valueOf(Config.getInstancia().getPassLong()));
        taCos.setText(Config.getInstancia().getDefaultMessage());
        taAssumpte.setText(Config.getInstancia().getSubjectMessage());
    }
}
