package cat.vodka.juniors;

import cat.vodka.juniors.classes.AppPropertiesHelper;
import cat.vodka.juniors.classes.DAOHelper;
import cat.vodka.juniors.controllers.LoginFormController;
import cat.vodka.juniors.exceptions.PropertiesHelperException;
import cat.vodka.juniors.model.Config;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.naming.ConfigurationException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application {
    private EntityManagerFactory emf;
    private LoginFormController loginForm;

    @FXML
    private Label lbErrors;

    @Override
    public void init() throws Exception{
        super.init();
        Map<String, String> properties = new HashMap<>();
        String passwd = getDecryptedPropsFilePassword("app.properties", "password", "enc");
        properties.put("javax.persistence.jdbc.user","vj");
        properties.put("javax.persistence.jdbc.password", passwd);

        emf = Persistence.createEntityManagerFactory("mariaDB", properties);
        actualitzaSingleton();
    }

    private void actualitzaSingleton() {
        DAOHelper<Config> helper = new DAOHelper(emf, Config.class);
        Config c = helper.getFirst();

        //Singleton
        Config.getInstancia().setId(c.getId());
        Config.getInstancia().setMaxIntents(c.getMaxIntents());
        Config.getInstancia().setPassLong(c.getPassLong());
        Config.getInstancia().setDefaultMessage(c.getDefaultMessage());
        Config.getInstancia().setSubjectMessage(c.getSubjectMessage());
        Config.getInstancia().setDefaultMessageAdministrador(c.getDefaultMessageAdministrador());
    }

    public static String getDecryptedPropsFilePassword(String fitxer, String clauAEncriptar, String clauTestEncriptacio) throws PropertiesHelperException, ConfigurationException, org.apache.commons.configuration.ConfigurationException {
        AppPropertiesHelper helper = new AppPropertiesHelper(fitxer, clauAEncriptar, clauTestEncriptacio);
        return helper.getDecryptedUserPassword();

    }

    @Override
    public void stop() throws Exception{
        if (emf != null && emf.isOpen())
            emf.close();

        super.stop();
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/LoginForm.fxml"));
        Parent root = null;
        try {
            root = loader.load();
            LoginFormController controller = loader.getController();
            controller.setEmf(emf);
            Scene scene = new Scene(root);

            stage.setResizable(false);

            stage.setTitle("Login");
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setScene(scene);

            //TODO: ajuda sobre esdeveniments
            /*
            stage.setOnHiding(event -> {
                System.out.println("setOnHiding");
                System.out.println(controller.getStates());
            });

            stage.setOnHidden(event -> {
                System.out.println("setOnHidden");
                System.out.println(controller.getStates());
            });*/

            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
