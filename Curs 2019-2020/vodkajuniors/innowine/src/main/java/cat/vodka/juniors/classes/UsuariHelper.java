package cat.vodka.juniors.classes;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


public class UsuariHelper<Usuari> extends DAOHelper<Usuari> {


    public UsuariHelper(EntityManagerFactory emf, Class<Usuari> parameterClass) {
        super(emf, parameterClass);
    }

    /**
     * Retorna un objecte Usuari si aquest existeia a la BBDD
     * @param login Nom d'usuari
     * @return Objecte Usuari si existeix a la BBDD
     */
    public Usuari getUsuariByLogin(String login){
        EntityManager manager = this.emf.createEntityManager();
        CriteriaBuilder cb = emf.getCriteriaBuilder();

        CriteriaQuery<Usuari> cbQuery = cb.createQuery(parameterClass);
        Root<Usuari> c = cbQuery.from(parameterClass);

        cbQuery.select(c).where(cb.equal(c.get("login"), login));

        Query query = manager.createQuery(cbQuery);
        return (Usuari) query.getSingleResult();

    }
}