package cat.vodka.juniors.classes;

import cat.vodka.juniors.model.Usuari;

public class UsuariStates {
    private Usuari usuari;
    private LoginStates state;

    public Usuari getUsuari() {
        return usuari;
    }

    public void setUsuari(Usuari usuari) {
        this.usuari = usuari;
    }

    public LoginStates getState() {
        return state;
    }

    public void setState(LoginStates state) {
        this.state = state;
    }
}
