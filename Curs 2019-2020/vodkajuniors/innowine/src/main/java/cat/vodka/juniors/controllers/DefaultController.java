package cat.vodka.juniors.controllers;

import cat.vodka.juniors.model.Config;
import cat.vodka.juniors.model.Usuari;

import javax.persistence.EntityManagerFactory;

public abstract class DefaultController {
    private Usuari usuari;

    private Config config;
    private EntityManagerFactory emf;

    public EntityManagerFactory getEmf() {
        return emf;
    }
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public Usuari getUsuari() {
        return usuari;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public void setUsuari(Usuari usuari) {
        this.usuari = usuari;
    }
}