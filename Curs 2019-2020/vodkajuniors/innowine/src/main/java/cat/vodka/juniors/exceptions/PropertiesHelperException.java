package cat.vodka.juniors.exceptions;

public class PropertiesHelperException extends Exception {
    public PropertiesHelperException(String message, Throwable cause) {
        super(message, cause);
    }
}
