package cat.vodka.juniors.classes;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


public class DAOHelper<T> {
    protected EntityManagerFactory emf;
    protected final Class<T> parameterClass;

    public DAOHelper(EntityManagerFactory emf, Class<T> parameterClass) {
        this.emf = emf;
        this.parameterClass = parameterClass;
    }

    public void insert(T t) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.persist(t);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    public void update(T t) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.merge(t);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();
        T t = em.find((Class<T>) parameterClass, id);

        try {
            if (t != null) {
                em.getTransaction().begin();
                em.remove(t);
                em.getTransaction().commit();
            }
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    public T getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        try {
            return em.find((Class<T>) parameterClass, id);
        } finally {
            em.close();
        }
    }

    public T getFirst() {
        CriteriaBuilder cb = this.emf.getCriteriaBuilder();
        EntityManager manager = this.emf.createEntityManager();

        CriteriaQuery<T> cbQuery = cb.createQuery((Class<T>) parameterClass);
        Root<T> c = cbQuery.from((Class<T>) parameterClass);
        cbQuery.select(c);

        Query query = manager.createQuery(cbQuery);

        return query.getResultList().isEmpty() ? null: (T) query.getResultList().get(0);
    }

    public List<T> getAll() {
        CriteriaBuilder cb = this.emf.getCriteriaBuilder();
        EntityManager manager = this.emf.createEntityManager();

        CriteriaQuery<T> cbQuery = cb.createQuery((Class<T>) parameterClass);
        Root<T> c = cbQuery.from((Class<T>) parameterClass);
        cbQuery.select(c);

        Query query = manager.createQuery(cbQuery);

        return query.getResultList();
    }

    public void printAll() {
        List<T> list = getAll();
        list.forEach(System.out::println);
    }
}
