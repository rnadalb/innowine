package cat.vodka.juniors.controllers;

import cat.vodka.juniors.Main;
import cat.vodka.juniors.classes.DAOHelper;
import cat.vodka.juniors.interfaces.IniciDiferit;
import cat.vodka.juniors.model.Usuari;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


public class AdministracioUsersController extends DefaultController implements Initializable, IniciDiferit {

    private static final int DOBLE_CLICK = 2;
    private static final int FIRST_ITEM = 0;

    @FXML
    private TextField tfCerca;

    @FXML
    private TableView<Usuari> tvUsuari;
    @FXML
    private TableColumn<Usuari, String> clLogin;
    @FXML
    private TableColumn<Usuari, String> clNom;
    @FXML
    private TableColumn<Usuari, String> clCognoms;
    @FXML
    private TableColumn<Usuari, String> clCorreu;
    @FXML
    private TableColumn<Usuari, String> clTelefon;
    @FXML
    private TableColumn<Usuari, Boolean> colBloquejat;

    private DAOHelper<Usuari> helper;
    private ObservableList<Usuari> llistaUsuaris;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ContextMenu cm = new ContextMenu();
        MenuItem mil = new MenuItem("Modifica");
        cm.getItems().add(mil);
        cm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                modificaNom(tvUsuari.getSelectionModel().getSelectedItem());
            }
        });

        tvUsuari.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getButton() == MouseButton.SECONDARY) {
                    cm.show(tvUsuari, event.getScreenX(), event.getScreenY());
                }
            }
        });
        //doble click
        tvUsuari.setRowFactory(tv -> {
            TableRow<Usuari> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == DOBLE_CLICK && (!row.isEmpty())) {
                    Usuari u = tvUsuari.getSelectionModel().getSelectedItem();
                    u.setBloquejat(!u.isBloquejat());
                    helper.update(u);
                    tvUsuari.refresh();
                }
            });
            return row;
        });
        setInfoColumnes();
    }

    private void modificaNom(Usuari usuari) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/DadesUser.fxml"));
        Parent root;
        int indexActual = tvUsuari.getSelectionModel().getSelectedIndex();
        try {
            root = loader.load();
            DadesUserController controller = loader.getController();
            controller.setEmf(getEmf());
            controller.setUsuari(usuari);
            controller.iniciDiferit();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.initStyle(StageStyle.DECORATED);
            stage.setAlwaysOnTop(true);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.setOnHidden(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Usuari u = controller.getUsuari();
                    llistaUsuaris.get(indexActual).setNom(u.getNom());
                    actualitzaTaula(false);
                    goTableItem(indexActual);
                }
            });
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void iniciDiferit() {
        helper = new DAOHelper<>(getEmf(), Usuari.class);
        actualitzaTaula(true);
        goTableItem(FIRST_ITEM);
    }

   /* @FXML
    protected void mnuSurtOnAction (ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Filtratge de taules amb/sense BBDD Demo");
        alert.setHeaderText("Segur que vols sortir de l'aplicació ?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            Platform.exit();
        }
    }*/
   /* @FXML
    protected void mnuSobreOnAction (ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/SobreFXML.fxml"));
            Parent root = loader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.DECORATED);
            stage.setScene(new Scene(root));
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/

    private void actualitzaTaula(boolean update) {
        if (update) {
            llistaUsuaris = getUsuaris();
            tvUsuari.setItems(llistaUsuaris);
        }
        filtraTaula();
    }


    private void filtraTaula() {
        if (!tvUsuari.getItems().isEmpty())
            tvUsuari.getItems().removeAll();

        FilteredList<Usuari> filteredData = new FilteredList<>(tvUsuari.getItems(), p -> true);
        tfCerca.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(usuari -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();
                if (usuari.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true;
                } else if ((String.valueOf(usuari.getCodi()).indexOf(lowerCaseFilter) != -1)) {
                    return true;
                }
                return false;
            });
        });

        SortedList<Usuari> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(tvUsuari.comparatorProperty());
        tvUsuari.setItems(sortedData);
    }

    private void setInfoColumnes() {

        clLogin.setCellValueFactory(new PropertyValueFactory<>("login"));
        clNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        clCognoms.setCellValueFactory(new PropertyValueFactory<>("cognom"));
        clCorreu.setCellValueFactory(new PropertyValueFactory<>("correu"));
        clTelefon.setCellValueFactory(new PropertyValueFactory<>("telefon"));

        colBloquejat.setCellValueFactory(new PropertyValueFactory<>("bloquejat"));
        colBloquejat.setCellFactory(column -> new CheckBoxTableCell<>());
        colBloquejat.setCellValueFactory(cellData -> cellData.getValue().getBloquejatProperty());

    }

    private ObservableList<Usuari> getUsuaris() {
        return FXCollections.observableArrayList(helper.getAll());
    }

    private void goTableItem(int row) {
        tvUsuari.requestFocus();
        tvUsuari.scrollTo(row);
        tvUsuari.getSelectionModel().select(row);
        tvUsuari.getFocusModel().focus(row);
    }

}



