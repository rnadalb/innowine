package cat.vodka.juniors.model;


import javafx.beans.property.SimpleBooleanProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tbl_usuari")
public class Usuari {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer codi;

  @Column(name = "bloquejat")
  private boolean bloquejat;

  @Column(name = "intents")
  private Integer intents;

  @Column(name = "data")
  private Date data;

  @Column(name = "login", length = 100)
  private String login;

  @Column(name = "nom")
  private String nom;

  @Column(name = "cognom")
  private String cognom;

  @Column(name = "password", length = 64)
  private String password;

  @Column(name = "admin")
  private boolean admin;

  @Column(name = "correu", length = 100)
  private String correu;

  @Column(name = "telefon", length = 12)
  private String telefon;

  @Column(name = "adreca", length = 80)
  private String adreca;

  @Column(name = "codiPostal", length = 5)
  private String codiPostal;

  @Column(name = "poblacio", length = 100)
  private String poblacio;

  @Column(name = "pais",length = 58)
  private String pais;

  public Usuari() {
  }

  public Integer getCodi() { return codi; }

  public void setCodi(Integer codi) { this.codi = codi; }

  public boolean isBloquejat() { return bloquejat; }

  public void setBloquejat(boolean bloquejat) { this.bloquejat = bloquejat; }

  // es necessita per omplir la casella propi el formulari de cerca
  public SimpleBooleanProperty getBloquejatProperty() {
    return new SimpleBooleanProperty(bloquejat);
  }


  public Date getData() { return data; }

  public void setData(Date data) { this.data = data; }

  public Integer getIntents() { return intents; }

  public void setIntents(Integer intents) { this.intents = intents; }

  public String getLogin() { return login; }

  public void setLogin(String login) { this.login = login; }

  public String getNom() { return nom; }

  public void setNom(String nom) { this.nom = nom; }

  public String getCognom() {return cognom; }

  public void setCognom(String cognom) { this.cognom = cognom; }

  public String getPassword() { return password; }

  public void setPassword(String password) { this.password = password; }

  public boolean isAdmin() { return admin; }

  public void setAdmin(boolean admin) { this.admin = admin; }

  public String getCorreu() { return correu; }

  public void setCorreu(String correu) { this.correu = correu; }

  public String getTelefon() { return telefon; }

  public void setTelefon(String telefon) { this.telefon = telefon; }

  public String getAdreca() { return adreca; }

  public void setAdreca(String adreca) { this.adreca = adreca; }

  public String getCodiPostal() { return codiPostal; }

  public void setCodiPostal(String codiPostal) { this.codiPostal = codiPostal; }

  public String getPoblacio() { return poblacio; }

  public void setPoblacio(String poblacio) { this.poblacio = poblacio; }

  public String getPais() { return pais; }

  public void setPais(String pais) { this.pais = pais; }

  @Override
  public String toString() {
    return "Usuari{" +
            "codi=" + codi +
            ", bloquejat=" + bloquejat +
            ", data=" + data +
            ", intents=" + intents +
            ", login='" + login + '\'' +
            ", nom='" + nom + '\'' +
            ", password='" + password + '\'' +
            ", admin=" + admin +
            ", correu='" + correu + '\'' +
            ", telefon='" + telefon + '\'' +
            ", adreca='" + adreca + '\'' +
            ", codiPostal='" + codiPostal + '\'' +
            ", poblacio='" + poblacio + '\'' +
            ", pais='" + pais + '\'' +
            '}';
  }
}