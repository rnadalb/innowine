package cat.vodka.juniors.controllers;

import cat.vodka.juniors.Main;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainController extends DefaultController {
    @FXML
    private Button btSignIn;

    /**
     * Acció del botó Sortir dins el menú principal - Obre Alerta per escollir si sortir o no de l'aplicació
     *
     * @param actionEvent - ActionEvent
     */
    @FXML
    public void miSurtOnAction(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Dialeg de Confirmaci\u00f3");
        alert.setHeaderText("Segur que vols sortir?");

        ButtonType buttonSi = new ButtonType("Si");
        ButtonType buttonNo = new ButtonType("No");

        alert.getButtonTypes().setAll(buttonSi, buttonNo);

        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == buttonSi) {
            Platform.exit();
        } else if (result.get() == buttonNo) {
            //https://code.makery.ch/blog/javafx-dialogs-official/
        }
    }

    /**
     * Acció del botó Sobre dins el menú principal - Obre el fxml de Sobre
     *
     * @param actionEvent -
     */
    @FXML
    public void miSobreOnAction(ActionEvent actionEvent) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Sobre.fxml"));
        try {
            Parent root = loader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setAlwaysOnTop(true);
            stage.setScene(scene);
            stage.setTitle("Sobre");
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL); // No permet usar la finestra que hi ha per sota la finestra mostrada
            stage.showAndWait();
        } catch (IOException e) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, "ERROR ??????", e);
        }
    }

    /**
     * Acció del botó Personalizació dins el menú principal - Obre el fxml de DadesUser
     *
     * @param actionEvent -
     */
    @FXML
    public void miPersonalitzacioOnAction(ActionEvent actionEvent) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/DadesUser.fxml"));
        Parent root = null;
        Stage stage = new Stage();
        try {
            root = loader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setAlwaysOnTop(true);
            stage.setResizable(false);
            stage.setTitle("Personalitzaci\u00f3");
            stage.initStyle(StageStyle.DECORATED);
            stage.initModality(Modality.APPLICATION_MODAL); // No permet usar la finestra que hi ha per sota la finestra mostrada
            DefaultControllerDiferit controller = loader.getController();
            controller.setEmf(getEmf()); //Li passem l'objecte Emf.
            controller.setUsuari(this.getUsuari());
            controller.iniciDiferit();
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }

    /**
     * Acció del botó Canviar contrasenya dins el menú principal - Obre el fxml de CanviarPasswd
     *
     * @param actionEvent -
     */
    @FXML
    public void miCanviarPasswdOnAction(ActionEvent actionEvent) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CanviarPasswd.fxml"));
        Parent root = null;
        Stage stage = new Stage();
        try {
            root = loader.load();
            Scene scene = new Scene(root);
            stage.setAlwaysOnTop(true);
            stage.setScene(scene);
            stage.setTitle("Canviar Contrasenya");
            stage.setResizable(false);
            stage.initStyle(StageStyle.DECORATED);
            stage.initModality(Modality.APPLICATION_MODAL); // No permet usar la finestra que hi ha per sota la finestra mostrada
            CanviarPasswdController controller = loader.getController();
            controller.setEmf(getEmf()); //Li passem l'objecte Emf.
            controller.setUsuari(this.getUsuari());
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }

    /**
     * Acció del botó Administració dins el menú principal - Obre el fxml de AdministracioUsuaris
     *
     * @param actionEvent - ActionEvent
     */
    @FXML
    public void miAdministracioOnAction(ActionEvent actionEvent) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AdministracioUsuaris.fxml"));
        Parent root = null;
        Stage stage = new Stage();
        try {
            root = loader.load();
            Scene scene = new Scene(root);
            stage.setAlwaysOnTop(true);
            stage.setScene(scene);
            stage.setTitle("Administraci\u00f3");
            stage.setResizable(false);
            stage.initStyle(StageStyle.DECORATED);
            stage.initModality(Modality.APPLICATION_MODAL);
            AdministracioUsersController controller = loader.getController();
            controller.setEmf(this.getEmf());
            controller.iniciDiferit();
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }

    public void miAdministrarDevOnAction(ActionEvent actionEvent) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AdministracioDispositius.fxml"));
        Parent root = null;
        Stage stage = new Stage();
        try {
            root = loader.load();
            Scene scene = new Scene(root);
            stage.setAlwaysOnTop(true);
            stage.setScene(scene);
            //stage.setMaximized(true); // Fer pantalla completa
            stage.setTitle("Administrar dispositius");
            stage.setResizable(false);
            stage.initStyle(StageStyle.DECORATED);
            stage.initModality(Modality.APPLICATION_MODAL); // No permet usar la finestra que hi ha per sota la finestra mostrada
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }
}

