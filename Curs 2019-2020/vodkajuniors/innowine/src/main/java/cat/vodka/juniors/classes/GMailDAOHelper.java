package cat.vodka.juniors.classes;


import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import javax.persistence.EntityManagerFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

public class GMailDAOHelper {
    private EntityManagerFactory emf;


    public static boolean enviarMail(String destinatari, String asupte, String misatge) {
        try {
            Email email = new SimpleEmail();
            email.setHostName("smtp.googlemail.com");
            email.setSmtpPort(465);
            email.setAuthenticator(new DefaultAuthenticator("vkjuniorsopel", "vkjuniors123456**"));
            email.setSSLOnConnect(true);
            email.setFrom("vkjuniorsopel@gmail.com");
            email.setSubject(asupte);
            email.setMsg(misatge);
            email.addTo(destinatari);
            email.send();
        } catch (EmailException e) {
            return false;
        }
        return true;
    }

    public static String crearMisatgeAdmin() throws UnknownHostException {
        InetAddress address = InetAddress.getLocalHost();
        java.util.Date data = new Date();
        String dia = String.valueOf(data);
        String adrec = String.valueOf("IP Local :"+address);
        String misatgeAdmin= (dia+"\n"+adrec);

        return misatgeAdmin;
    }
}