package cat.vodka.juniors.classes;

import cat.vodka.juniors.model.Config;
import cat.vodka.juniors.model.Usuari;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.persistence.EntityManagerFactory;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static cat.vodka.juniors.classes.EncriptarPswd.encriptar;

public class UtilsDB {

    //public static final Config conf = new Config();
    private EntityManagerFactory emf;

    public UtilsDB(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public UsuariStates checkPassword(String usuari, String password)  {
        UsuariStates result = new UsuariStates();
        //Helper per la taula/class Usuari
        UsuariHelper helper = new UsuariHelper(this.emf, Usuari.class);

        //Helper per la taula/class Config
        DAOHelper<Config> confHelper = new DAOHelper(emf, Config.class);

        Usuari u = (Usuari) helper.getUsuariByLogin(usuari);
        result.setUsuari(u);

        try {
            password = encriptar(password);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }

        //Crea un objecte conf de la taula Config
        Config con = confHelper.getFirst();
        //con.getMaxIntents();

        if (password.equals(u.getPassword()) && !u.isBloquejat()) {
            result.setState(LoginStates.GRANTED);
        } else {
            if (u.getIntents() < con.getMaxIntents() && !u.isBloquejat()) {
                u.setIntents(u.getIntents() + 1);
                result.setState(LoginStates.FAILED);
            } else {
                u.setBloquejat(true);
                if (u.isAdmin()) {
                    result.setState(LoginStates.FAILED);
                }
                else {
                    result.setState(LoginStates.LOCKED);
                }
            }
            if (u.isAdmin()){
                u.setBloquejat(false);
            }
            helper.update(u);
        }
        return result;
    }
    
    public int intents(String usuari) {
        LoginStates result;
        UsuariHelper helper = new UsuariHelper(this.emf, Usuari.class);

        Usuari u = (Usuari) helper.getUsuariByLogin(usuari);

        return u.getIntents();
    }

    public String correuUser(String usuari) {
        LoginStates result;
        UsuariHelper helper = new UsuariHelper(this.emf, Usuari.class);

        Usuari u = (Usuari) helper.getUsuariByLogin(usuari);

        return u.getCorreu();
    }

    public String getUsername(String usuari){

        UsuariHelper helper = new UsuariHelper(this.emf, Usuari.class);

        Usuari u = (Usuari) helper.getUsuariByLogin(usuari);

        return u.getLogin();
    }
    public String getNomUser(String usuari){

        UsuariHelper helper = new UsuariHelper(this.emf, Usuari.class);

        Usuari u = (Usuari) helper.getUsuariByLogin(usuari);

        return u.getNom();
    }

    public String getPassword(String usuari){

        UsuariHelper helper = new UsuariHelper(this.emf, Usuari.class);

        Usuari u = (Usuari) helper.getUsuariByLogin(usuari);

        return u.getPassword();
    }
}
