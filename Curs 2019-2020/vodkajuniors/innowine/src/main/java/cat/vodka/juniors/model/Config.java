package cat.vodka.juniors.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tbl_config")
public class Config {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "passLong")
    private  Integer passLong;

    @Column(name = "defaultMessage")
    private String defaultMessage;

    @Column(name = "maxIntents")
    private Integer maxIntents;

    @Column(name = "subjectMessage")
    private String subjectMessage;

    @Column(name = "defaultMessageAdministrador")
    private String defaultMessageAdministrador;


    private static Config instancia;

    private Config() {

    }

    private Config(int passLong, int maxIntents, String defaultMessage, String subjectMessage) {
        this.passLong = passLong;
        this.maxIntents = maxIntents;
        this.defaultMessage = defaultMessage;
        this.subjectMessage = subjectMessage;

    }

    public static Config getInstancia () {
        if (instancia == null){
            instancia = new Config();
        }
        else{
            System.out.println("No es pot crear l'objete perquè ja existeix");
        }

        return instancia;
    }


    //Geters & Setters

    public Integer getId() {return id;}

    public void setId(Integer id) { this.id = id;}

    public Integer getPassLong() {return passLong; }

    public void setPassLong(Integer passLong) { this.passLong = passLong; }

    public String getDefaultMessage() { return defaultMessage; }

    public void setDefaultMessage(String defaultMessage) { this.defaultMessage = defaultMessage; }

    public String getSubjectMessage() {
        return subjectMessage;
    }

    public void setSubjectMessage(String subjectMessage) {
        this.subjectMessage = subjectMessage;
    }

    public Integer getMaxIntents() {
        return maxIntents;
    }

    public void setMaxIntents(Integer maxIntents) {
        this.maxIntents = maxIntents;
    }

    public String getDefaultMessageAdministrador() {
        return defaultMessageAdministrador;
    }

    public void setDefaultMessageAdministrador(String defaultMessageAdministrador) {
        this.defaultMessageAdministrador = defaultMessageAdministrador;
    }

    @Override
    public String toString() {
        return "Config{" +
                "id=" + id +
                ", passLong=" + passLong +
                ", defaultMessage='" + defaultMessage + '\'' +
                ", maxIntents=" + maxIntents +
                ", subjectMessage='" + subjectMessage + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Config)) return false;
        Config config = (Config) o;
        return Objects.equals(id, config.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, passLong, defaultMessage, maxIntents, subjectMessage,defaultMessageAdministrador);
    }
}
