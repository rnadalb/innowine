package cat.vodka.juniors.controllers;

import cat.vodka.juniors.Main;
import cat.vodka.juniors.classes.DAOHelper;
import cat.vodka.juniors.classes.UsuariStates;
import cat.vodka.juniors.classes.UtilsDB;
import cat.vodka.juniors.exceptions.PropertiesHelperException;
import cat.vodka.juniors.model.Config;
import cat.vodka.juniors.model.Usuari;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.naming.ConfigurationException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cat.vodka.juniors.Main.getDecryptedPropsFilePassword;
import static cat.vodka.juniors.classes.GMailDAOHelper.crearMisatgeAdmin;
import static cat.vodka.juniors.classes.GMailDAOHelper.enviarMail;

public class LoginFormController extends DefaultController implements Initializable {
    private EntityManagerFactory emf;

    @FXML
    private Label lbErrors;

    @FXML
    private Label lbIntentsRestants;

    @FXML
    private TextField tfUsuari;

    @FXML
    private TextField tfPassword;

    @FXML
    private Button btSignIn;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tfUsuari.clear();
        tfPassword.clear();
        tfUsuari.requestFocus();
    }

    /**
     * Acció del botó X del fxml del LoginForm
     * @param actionEvent- ActionEvent
     */
    public void btTancarOnAction(ActionEvent actionEvent) {
        Platform.exit();
    }

    @FXML
    public void btSignInOnMouseClicked() {
        funcio_click();
    }

    private void funcio_click() {
        Map<String, String> properties = new HashMap<>();
        String passwd = null;
        try {
            passwd = getDecryptedPropsFilePassword("app.properties", "password", "enc");
        } catch (PropertiesHelperException e) {
            e.printStackTrace();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        } catch (org.apache.commons.configuration.ConfigurationException e) {
            e.printStackTrace();
        }
        properties.put("javax.persistence.jdbc.user", "vj");
        properties.put("javax.persistence.jdbc.password", passwd);

        emf = Persistence.createEntityManagerFactory("mariaDB", properties);
        DAOHelper<Config> helper = new DAOHelper(emf, Config.class);
        Config c = helper.getFirst();
        //String nomUser;
        UsuariStates states;
        try {
            if (tfUsuari.getText().isEmpty()) {
                //TODO: Mostrar alerta
                lbErrors.setText("Escriu en nom d'usuari");
                tfUsuari.requestFocus();
                lbIntentsRestants.setText("");

            } else {
                if (tfPassword.getText().isEmpty()) {
                    //TODO: Mostrar alerta
                    lbErrors.setText("Escriu la contrasenya");
                    tfPassword.requestFocus();
                    lbIntentsRestants.setText("");
                } else {
                    UtilsDB u = new UtilsDB(this.getEmf()); //Base de Dades Usuari
                    states = u.checkPassword(tfUsuari.getText(), tfPassword.getText());
                    lbErrors.setText(states.getState().getMessage());
                    String username = u.getUsername(tfUsuari.getText());

                    switch (states.getState()) {
                        case GRANTED:
                            ((Stage) tfUsuari.getScene().getWindow()).close();
                            obreMain(tfUsuari.getText(), states.getUsuari());
                            break;
                        case FAILED:
                            String restants;
                            lbIntentsRestants.setText("Intents: " + (u.intents(tfUsuari.getText())) + "/" + c.getMaxIntents());
                            break;
                        case LOCKED:
                            miInformacio();
                            lbIntentsRestants.setText("");
                            try {
                                String misatgeAdmin = c.getDefaultMessageAdministrador() + " \n " + username + "\n" + crearMisatgeAdmin();
                                if (!enviarMail("vkjuniorsopel@gmail.com", c.getSubjectMessage(), misatgeAdmin))
                                    //TODO: Mostrar alerta
                                    ;
                                String misatgeUser = username + "\n" + c.getDefaultMessage();
                                enviarMail(u.correuUser(tfUsuari.getText()), c.getSubjectMessage(), misatgeUser);
                            } catch (UnknownHostException e) {
                                e.printStackTrace();
                            }
                            break;
                    }
                }
            }
        }catch (Exception exe){
            lbErrors.setText("El Usuari o contrasenya no es correcte");
        }
    }

    public void obreMain(String nomUser, Usuari usuari) {
        if (usuari.isAdmin()) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/MainAdmin.fxml"));
            Parent root = null;
            Stage stage = new Stage();
            try {
                root = loader.load();
                Scene scene = new Scene(root);
                stage.setTitle("Innowine: Admin " + nomUser);
                stage.setMaximized(true);
                stage.setScene(scene);
                MainController mCtr = loader.getController();
                mCtr.setEmf(getEmf());
                mCtr.setUsuari(usuari);
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                Platform.exit();
            }
        } else {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/MainUser.fxml"));
            Parent root = null;
            Stage stage = new Stage();
            try {
                root = loader.load();
                Scene scene = new Scene(root);
                stage.setTitle("Innowine: User " + nomUser);
                stage.setMaximized(true);
                stage.setScene(scene);
                MainController mCtr = loader.getController();
                mCtr.setEmf(getEmf());
                mCtr.setUsuari(usuari);
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                Platform.exit();
            }
        }
    }

    public void miInformacio() {
        Alert alertInfo = new Alert(Alert.AlertType.CONFIRMATION);
        Text text = new Text("El seu usuari es troba bloquejat, per favor contacti amb l'administrador del sistema, perquè siguin restablerts els drets d'accés.");
        text.setWrappingWidth(250);

        alertInfo.setTitle("Informació Important");
        alertInfo.setHeaderText("Compte Bloquejat");
        alertInfo.getDialogPane().setContent(text);


        alertInfo.initModality(Modality.APPLICATION_MODAL);
        Stage stage = (Stage) alertInfo.getDialogPane().getScene().getWindow();
        stage.setAlwaysOnTop(true);
        alertInfo.setResizable(false);

        ButtonType buttonTanca = new ButtonType("Tanca");

        alertInfo.getButtonTypes().setAll(buttonTanca);

        Optional<ButtonType> result = alertInfo.showAndWait();


        if (result.get() == buttonTanca) {
            stage.setAlwaysOnTop(false);
            alertInfo.close();
        }

    }


    @FXML
    public void btForgotOnMouseClicked(MouseEvent mouseEvent) {
        //TODO:
    }

    @FXML
    public void btSurtOnMouseClicked(MouseEvent mouseEvent) {
        //TODO:
    }

    @FXML
    public void btSignUpMouseClicked(MouseEvent mouseEvent) {
        //TODO:
    }

    @FXML
    public void tfUsuariOnKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            if (!tfUsuari.getText().isEmpty()) {
                tfPassword.requestFocus();
            }
        }
    }

    @FXML
    public void tfPasswordOnKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            if (!tfPassword.getText().isEmpty()) {
                funcio_click();
            }
        }
    }


}
