package cat.vodka.juniors.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class AdministracioDevController {

    @FXML
    public TextField tfUser;
    @FXML
    public TextField tfId;
    @FXML
    public TextField tfDescDisp;
    @FXML
    public TextField tfLat;
    @FXML
    public TextField tfLon;
    @FXML
    public ComboBox cbComarca;



}
