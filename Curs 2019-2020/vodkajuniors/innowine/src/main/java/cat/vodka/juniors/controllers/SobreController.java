package cat.vodka.juniors.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class SobreController {
    @FXML

    /**
     * Acció del botó Tanca - Tancar la finestra Sobre
     */
    public void btTancaOnAction(ActionEvent actionEvent) {
        ((Stage)((Button)actionEvent.getSource()).getScene().getWindow()).close();
    }
}