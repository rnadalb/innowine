package cat.vodka.juniors.classes;

import cat.vodka.juniors.model.Usuari;

import javax.persistence.EntityManagerFactory;

public abstract class DefaultController {
    private Usuari usuari;
    private EntityManagerFactory emf;

    public EntityManagerFactory getEmf() {
        return emf;
    }
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public Usuari getUsuari() {
        return usuari;
    }

    public void setUsuari(Usuari usuari) {
        this.usuari = usuari;
    }
}