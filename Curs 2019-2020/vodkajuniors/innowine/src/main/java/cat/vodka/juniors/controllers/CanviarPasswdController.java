package cat.vodka.juniors.controllers;

import cat.vodka.juniors.classes.DAOHelper;
import cat.vodka.juniors.exceptions.PropertiesHelperException;
import cat.vodka.juniors.model.Config;
import cat.vodka.juniors.model.Usuari;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.naming.ConfigurationException;
import java.io.IOException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;

import static cat.vodka.juniors.classes.EncriptarPswd.encriptar;

public class CanviarPasswdController extends DefaultController implements Initializable {
    @FXML
    public PasswordField tfPsswdActual;
    @FXML
    public PasswordField tfPsswdNova;
    @FXML
    public PasswordField tfPsswdConfirm;
    @FXML
    public Button btAccept;
    @FXML
    public Label lbErrors;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    /**
     * Acció del botó Accepta
     *
     * @param mouseEvent -
     * @throws IOException -
     * @throws IllegalBlockSizeException -
     * @throws NoSuchPaddingException -
     * @throws BadPaddingException -
     * @throws NoSuchAlgorithmException -
     * @throws InvalidKeyException -
     */
    public void btAcceptOnMouseClicked(MouseEvent mouseEvent) throws IOException, IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException, PropertiesHelperException, ConfigurationException, ConfigurationException {

        String password = tfPsswdActual.getText();
        String newPsswd = tfPsswdNova.getText();
        String confirmPsswd = tfPsswdConfirm.getText();
        DAOHelper<Config> hConfig = new DAOHelper<>(getEmf(), Config.class); //Agafem el Helper de la classe Config.
        Config c = hConfig.getFirst();

        if (password.isEmpty()) {
            lbErrors.setText("No has escrit la contrasenya actual.");
        } else {
            if (encriptar(password).equals(getUsuari().getPassword())) {
                if (!newPsswd.equals(password)) {
                    if (newPsswd.equals(confirmPsswd)) {
                        if (newPsswd.length() > c.getPassLong()) {   // Agafem el camp PassLong de la taula Config
                            getUsuari().setPassword(encriptar(newPsswd)); // Canviem contrasenya dins la BD, encriptada
                            DAOHelper<Usuari> helper = new DAOHelper<>(getEmf(), Usuari.class);
                            helper.update(getUsuari()); // Fem un update a la BD, canviant-li la contrasenya
                            ((Stage)((Button)mouseEvent.getSource()).getScene().getWindow()).close(); // Quan es canvia correctament, tanquem la finestra.

                        } else {lbErrors.setText("La contrasenya introduida no compleix els requisits.");}
                    } else { lbErrors.setText("La nova contrasenya i la contrasenya confirmada no coincideixen."); }
                } else { lbErrors.setText("Has d'introduir una contrasenya diferent a l'actual."); }
            } else {lbErrors.setText("La contrasenya escrita no és correcte."); }
        }
    }
}
