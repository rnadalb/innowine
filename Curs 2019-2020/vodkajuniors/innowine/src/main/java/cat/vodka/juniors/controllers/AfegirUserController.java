package cat.vodka.juniors.controllers;

import cat.vodka.juniors.classes.DAOHelper;
import cat.vodka.juniors.model.Usuari;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class AfegirUserController extends DefaultController {

    @FXML
    public TextField tfNom;

    @FXML
    public TextField tfCognom;

    @FXML
    public TextField tfLogin;

    @FXML
    public PasswordField tfPsswd;

    @FXML
    public TextField tfCorreu;

    @FXML
    public TextField tfTelefon;

    @FXML
    public Button btGuardar;


//reverted commit 2wrarwawra
    //reverted commit 2wrarwawra
    //reverted commit 2wrarwawra

    public void GuardarOnAction(ActionEvent actionEvent) {
        DAOHelper<Usuari> helper = new DAOHelper<>(getEmf(), Usuari.class);
        getUsuari().setLogin(tfLogin.getText());
        getUsuari().setNom(tfNom.getText());
        getUsuari().setCognom(tfCognom.getText());
        getUsuari().setPassword(tfPsswd.getText());
        getUsuari().setCorreu(tfCorreu.getText());
        getUsuari().setTelefon(tfTelefon.getText());
        helper.insert(getUsuari());

        buidaCamps();

    }

    public void buidaCamps() {
        tfLogin.setText(" ");
        tfNom.setText(" ");
        tfCognom.setText(" ");
        tfPsswd.setText(" ");
        tfCorreu.setText(" ");
        tfTelefon.setText(" ");

    }
}
