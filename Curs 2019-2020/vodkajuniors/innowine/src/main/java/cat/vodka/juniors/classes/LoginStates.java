package cat.vodka.juniors.classes;

public enum LoginStates {
    GRANTED(0, "Accés garantit"),
    FAILED(1, "El usuari o contrasenya no es correcte"),
    LOCKED(2, "Compte bloquejat");
   // WRONG (3,"El usuari no existeix");

    private int errorCode;
    private String message;

    LoginStates(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}