package cat.vodka.juniors.model;

import javax.persistence.*;


@Entity
@Table(name = "tbl_dispositius")
public class Dispositius {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "usuarip", length = 100)
    private String usuari_p;

    @Column(name = "iddispositiu")
    private Integer iddispositiu;

    @Column(name = "descripcio")
    private String descripcio;

    @Column(name = "latitud", length = 100)
    private String latitud;

    @Column(name = "longitud", length = 100)
    private String longitud;

    @Column(name = "comarca", length = 150)
    private String comarca;

    //Getters i Setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsuari_p() {
        return usuari_p;
    }

    public void setUsuari_p(String usuari_p) {
        this.usuari_p = usuari_p;
    }

    public Integer getIddispositiu() {
        return iddispositiu;
    }

    public void setIddispositiu(Integer iddispositiu) {
        this.iddispositiu = iddispositiu;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getComarca() {
        return comarca;
    }

    public void setComarca(String comarca) {
        this.comarca = comarca;
    }

    @Override
    public String toString() {
        return "Dispositius{" +
                "id=" + id +
                ", usuari_p='" + usuari_p + '\'' +
                ", iddispositiu=" + iddispositiu +
                ", descripcio='" + descripcio + '\'' +
                ", latitud='" + latitud + '\'' +
                ", longitud='" + longitud + '\'' +
                ", comarca='" + comarca + '\'' +
                '}';
    }


}