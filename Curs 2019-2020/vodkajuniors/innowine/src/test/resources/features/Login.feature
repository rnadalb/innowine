Feature: Avaluador de Login
  Com usuari del sistema
  Necessito accedir d'una manera segura
  Per poder treballar

Scenario Outline: Entrada correcte al sistema
  Given Donat l'avaluador de login
  When Quan l'usuari introdueix les dades correctament <usuari> <password>
  Then L'usuari accedeix al sistema

  Examples:
    | usuari  | password  |
    | bgates  | pass  |
    | pnorton | pass  |
    | emusk   | pass  |