package cat.dam2.m15.tdd.login.testfx;


import cat.vodka.juniors.Main;
import cat.vodka.juniors.classes.LoginStates;
import cat.vodka.juniors.controllers.DadesUserController;
import cat.vodka.juniors.controllers.LoginFormController;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;
import org.testfx.api.FxAssert;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@DisplayName("Test: Login Test GUI")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class LoginTestFX extends ApplicationTest {

    @Start
    public void start(Stage stage) throws IOException {
        Map<String, String> properties = new HashMap<>();
        properties.put("javax.persistence.jdbc.user", "vj");
        properties.put("javax.persistence.jdbc.password", "vodkajuniors");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mariaDB", properties);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/LoginForm.fxml"));
        Parent root = null;

        try {
            root = loader.load();
            LoginFormController controller = loader.getController();
            controller.setEmf(emf);
            Scene scene = new Scene(root);
            //stage.initStyle(StageStyle.DECORATED);
            //stage.setMaximized(true);
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }

    @Order(1)
    @DisplayName("Test d'usuaris")
    @ParameterizedTest
    @CsvSource({"carlos,pass", "ferran,password", "emusk,password"})
    void introdueix_dades_correctament(ArgumentsAccessor argumentsAccesor) {
        String usuari = argumentsAccesor.getString(0);
        String password = argumentsAccesor.getString(1);


        clickOn("#tfUsuari");
        write(usuari);

        clickOn("#tfPassword");
        write(password);

        clickOn("#btSignIn");

        try {
            FxAssert.verifyThat("#lbErrors", LabeledMatchers.hasText(LoginStates.GRANTED.getMessage()));
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}