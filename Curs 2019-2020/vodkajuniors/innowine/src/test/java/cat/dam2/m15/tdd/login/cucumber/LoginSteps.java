package cat.dam2.m15.tdd.login.cucumber;


import cat.vodka.juniors.classes.LoginStates;
import cat.vodka.juniors.classes.UsuariStates;
import cat.vodka.juniors.classes.UtilsDB;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class LoginSteps {

    private UsuariStates result;
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("mariaDB");
    private static UtilsDB dbUtils;

    @Given("^Donat l'avaluador de login$")
    public void donatLAvaluadorDeLogin() {
        dbUtils = new UtilsDB(emf);
    }

    @When("^Quan l'usuari introdueix les dades correctament (.+) (.+)$")
    public void quanLUsuariIntrodueixLesDadesCorrectamentUsuariPassword(String usuari, String password) {
        result = dbUtils.checkPassword(usuari, password);
    }

    @Then("^L'usuari accedeix al sistema$")
    public void lUsuariAccedeixAlSistema() {
    LoginStates expected = LoginStates.GRANTED;
    Assertions.assertEquals(expected,result,"accessUtils.check_passwd() fal");
    }
}
