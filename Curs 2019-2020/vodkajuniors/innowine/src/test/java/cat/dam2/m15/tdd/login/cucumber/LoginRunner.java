package cat.dam2.m15.tdd.login.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = "pretty", monochrome = false, strict = false,
        features = "src/test/resources/features", glue = "cat.dam2.m15.tdd.login")
public class LoginRunner {

}