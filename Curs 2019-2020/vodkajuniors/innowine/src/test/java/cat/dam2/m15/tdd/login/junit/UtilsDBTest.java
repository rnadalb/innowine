package cat.dam2.m15.tdd.login.junit;


import cat.vodka.juniors.classes.LoginStates;
import cat.vodka.juniors.classes.UsuariStates;
import cat.vodka.juniors.classes.UtilsDB;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;
@DisplayName("Test: Avaluador de Login")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UtilsDBTest {
    private EntityManagerFactory emf;
    private UtilsDB utilsDB;

    @BeforeAll
    public void beforeAll(){
        emf = Persistence.createEntityManagerFactory("mariaDB");
        utilsDB = new UtilsDB(emf);
    }
    @AfterAll
    public void afterAll(){
        if (emf.isOpen())
            emf.close();
        emf=null;
        utilsDB =null;
    }

    @ParameterizedTest
    @DisplayName("L'usuari introdueix les dades correctament")
    @Order(1)
    @CsvSource({"bgates,pass", "pnorton,pass", "emusk,pass"})
    void usuari_usuari_introdueix_les_dades_correctament(ArgumentsAccessor argumentsAccessor){
        //Sabem que entra
        String usuari = argumentsAccessor.getString(0);
        String password = argumentsAccessor.getString(1);

        LoginStates esperat = LoginStates.GRANTED;

        UtilsDB utilsDB = new UtilsDB(emf);
        UsuariStates resultat = utilsDB.checkPassword(usuari, password);

        assertEquals(esperat, resultat.getState(), "");
    }

    @ParameterizedTest
    @DisplayName("L'usuari introdueix les dades incorrectament")
    @Order(2)
    @CsvSource({"bgates,fail", "pnorton,fail", "emusk,fail"})
    void usuari_usuari_introdueix_les_dades_incorrectament(ArgumentsAccessor argumentsAccessor){
        //Sabem que entra
        String usuari = argumentsAccessor.getString(0);
        String password = argumentsAccessor.getString(1);

        LoginStates esperat = LoginStates.FAILED;

        UtilsDB utilsDB = new UtilsDB(emf);
        UsuariStates resultat = utilsDB.checkPassword(usuari, password);

        assertEquals(esperat, resultat.getState(), "");
    }
    @ParameterizedTest
    @DisplayName("L'usuari introdueix les dades incorrectament i es bloqueja el compte")
    @Order(3)
    @CsvSource({"bgates,fail", "pnorton,fail", "emusk,fail"})
    void usuari_usuari_introdueix_les_dades_incorrectament_i_bloquejat(ArgumentsAccessor argumentsAccessor){
        //Sabem que entra
        String usuari = argumentsAccessor.getString(0);
        String password = argumentsAccessor.getString(1);

        LoginStates esperat = LoginStates.LOCKED;

        UtilsDB utilsDB = new UtilsDB(emf);
        UsuariStates resultat = utilsDB.checkPassword(usuari, password);

        assertEquals(esperat, resultat.getState(), "");
    }
}