package cat.dam2.m15.tdd.login.testfx;

import cat.vodka.juniors.Main;
import cat.vodka.juniors.classes.LoginStates;
import cat.vodka.juniors.controllers.DadesUserController;
import cat.vodka.juniors.controllers.DefaultControllerDiferit;
import cat.vodka.juniors.model.Usuari;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;
import org.testfx.api.FxAssert;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;



@DisplayName("Test: Login Test GUI")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DadesUserTestFX extends ApplicationTest {

    @Start
    public void start(Stage stage) throws IOException {
        Map<String, String> properties = new HashMap<>();
        properties.put("javax.persistence.jdbc.user", "vj");
        properties.put("javax.persistence.jdbc.password", "vodkajuniors");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mariaDB", properties);

        Usuari user = new Usuari();
        user.setLogin("carlos");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/DadesUser.fxml"));
        Parent root = null;
        stage = new Stage();
        try {
            root = loader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setAlwaysOnTop(true);
            stage.setResizable(false);
            stage.setTitle("Personalitzaci\u00f3");
            stage.initStyle(StageStyle.DECORATED);
            stage.initModality(Modality.APPLICATION_MODAL); // No permet usar la finestra que hi ha per sota la finestra mostrada
            DefaultControllerDiferit controller = loader.getController();
            controller.setEmf(emf); //Li passem l'objecte Emf.
            controller.setUsuari(user);
            controller.iniciDiferit();

            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }

    }


    @Order(1)
    @DisplayName("Test d'usuaris")
    @ParameterizedTest
    @CsvSource({"Carlos,Algar,carlos.algar2000@gmail.com,674744592,08775,Torrelavit,Espanya,C/Inventaoo"})
    void introdueix_dades_correctament(ArgumentsAccessor argumentsAccesor) {
        String nom = argumentsAccesor.getString(0);
        String cognom = argumentsAccesor.getString(1);
        String correu = argumentsAccesor.getString(2);
        String telefon = argumentsAccesor.getString(3);
        String cp = argumentsAccesor.getString(4);
        String poblacio = argumentsAccesor.getString(5);
        String pais = argumentsAccesor.getString(6);
        String adress = argumentsAccesor.getString(7);

        clickOn("#tfNom");
        write(nom);

        clickOn("#tfCognoms");
        write(cognom);

        clickOn("#tfCorreu");
        write(correu);

        clickOn("#tfTelefon");
        write(telefon);

        clickOn("#tfCP");
        write(cp);

        clickOn("#tfPoblacio");
        write(poblacio);

        clickOn("#tfPais");
        write(pais);

        clickOn("#tfAdress");
        write(adress);


        clickOn("#btActualitza");

        try {
            FxAssert.verifyThat("#lbNoti", LabeledMatchers.hasText("Dades actualitzades!"));
        }catch (Exception e){
            e.printStackTrace();
        }


    }
}
