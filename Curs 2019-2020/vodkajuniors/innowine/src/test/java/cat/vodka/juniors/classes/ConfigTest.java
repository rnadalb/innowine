package cat.vodka.juniors.classes;

import cat.vodka.juniors.model.Config;
import org.junit.jupiter.api.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test Case: PrimeFactors")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ConfigTest {
    private static EntityManagerFactory
            emf;
    private static Config esperat;

    @BeforeAll
    public static void beforeAll() {
        Map<String, String> properties = new HashMap<>();
        properties.put("javax.persistence.jdbc.user", "vj");
        properties.put("javax.persistence.jdbc.password", "vodkajuniors");
        emf = Persistence.createEntityManagerFactory("mariaDB", properties);
        esperat = new Config();
        esperat.setId(6);
        /*
        esperat.setDefaultMessage("555");
        esperat.setMaxIntents(5);
        esperat.setPassLong(5);
        esperat.setSubjectMessage("555");*/
    }

    @AfterAll
    public static void afterAll() {
        if (emf.isOpen())
            emf.close();
        emf = null;
    }

    @Test
    @DisplayName("A la taula no hi ha cap registre")
    @Order(1)
    void la_taula_no_te_registres() {
        DAOHelper<Config> helper = new DAOHelper<>(emf, Config.class);
        Config resultat = helper.getFirst();

        assertEquals(null, resultat, "missatge d'error");
    }

    @Test
    @DisplayName("La taula te 1 sol registre")
    @Order(2)
    void la_taula_te_un_registre() {
        DAOHelper<Config> helper = new DAOHelper<>(emf, Config.class);
        List<Config> listConfig = helper.getAll();
        //Config resultat = helper.getFirst();

        try {
            if (listConfig.equals(listConfig.get(1))) {
            }
        } catch (Exception e) {
            System.out.println("A la taula no hi ha mes de 1 registre");
        }

        assertEquals(esperat, listConfig.get(0), "missatge d'error");
    }
}