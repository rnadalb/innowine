package cat.vodka.juniors.classes;

import cat.vodka.juniors.model.Dispositius;
import org.junit.jupiter.api.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DispositiusTest {
    private static EntityManagerFactory emf;
    private static Dispositius esperat;

    @BeforeAll
    public static void beforeAll() {
        Map<String, String> properties = new HashMap<>();
        properties.put("javax.persistence.jdbc.user", "vj");
        properties.put("javax.persistence.jdbc.password", "vodkajuniors");
        emf = Persistence.createEntityManagerFactory("mariaDB", properties);
        esperat = new Dispositius();

        esperat.setId(1);

    }

    @AfterAll
    public static void afterAll() {
        if (emf.isOpen())
            emf.close();
        emf = null;
    }

    @Test
    @DisplayName("A la taula no hi ha cap registre")
    @Order(1)
    void la_taula_no_te_registres() {
        DAOHelper<Dispositius> helper = new DAOHelper<>(emf, Dispositius.class);
        List<Dispositius> llistaDispositius = new ArrayList();

        llistaDispositius = helper.getAll();

        System.out.println(llistaDispositius);

        assertEquals(null, llistaDispositius, "missatge d'error");
    }

}