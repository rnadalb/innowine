package cat.innowine.desktop.model;
/*

> select * from dummyData order by time desc limit 10;
        name: dummyData
        time                           Hamb Pamb Tamb airtimeT1 bootC counterT1 humiditatT1 humiditatT2 humiditatT3 id  portT1 temperaturaT1 temperaturaT2 temperaturaT3 vBattery
        ----                           ---- ---- ---- --------- ----- --------- ----------- ----------- ----------- --  ------ ------------- ------------- ------------- --------
        2020-12-07T10:04:48.968441571Z 49   1000 12   82.176    275   1         1808.7      1809.57     1802.82     100 1      12.7          13.1          12.8          3410.3567399999997
        2020-12-07T10:04:22.216976006Z 50   1000 12   82.176    275   0         1810.56     1812.28     1805.64     100 1      12.7          13.1          12.8          3405.8973
        2020-12-07T09:49:02.739693558Z 50   997  12   82.176    274   1         1807.72     1807.72     1802.04     100 1      12.1          12.4          12.1          3512.92386
        2020-12-07T09:48:36.006596066Z 50   1002 12   82.176    274   0         1808.36     1811.79     1804.46     100 1      12            12.3          12.1          3508.46442
        2020-12-07T09:33:15.944921188Z 50   1015 11   82.176    273   1         1806.53     1807.72     1801.1      100 1      10.8          11            10.8          3385.82982
        2020-12-07T09:32:49.215562403Z 51   1015 10   82.176    273   0         1807.51     1810.09     1802.21     100 1      10.8          11            10.8          3384.71496
        2020-12-07T09:17:29.852465358Z 51   1013 10   82.176    272   1         1805.95     1806.35     1800.09     100 1      10.3          10.4          10.3          3408.12702
        2020-12-07T09:17:03.117157637Z 51   1017 10   82.176    272   0         1806.05     1809        1800.93     100 1      10.3          10.4          10.2          3407.0121599999998
        2020-12-07T09:01:43.512602491Z 51   1019 10   82.176    271   1         1805.86     1805.02     1799.74     100 1      10.1          10.1          10            3361.3028999999997
        2020-12-07T09:01:16.774941604Z 51   1017 10   82.176    271   0         1808.34     1808.56     1800.81     100 1      10.1          10.1          10            3370.22178

*/

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

import java.io.Serializable;
import java.time.Instant;

//@Measurement(name = "dummyData", database = "iwinedb", timeUnit = TimeUnit.SECONDS)
@Measurement(name = "dummyData", database = "iwinedb")
public class DummyData implements Serializable {
    private static final long serialVersionUID = -1497034550418278038L;
    @Column(name = "time")
    private Instant time;

    @Column(name = "Hamb")
    private Double hAmb;

    @Column(name = "Pamb")
    private Double pAmb;

    @Column(name = "Tamb")
    private Double tAmb;

    @Column(name = "airtimeT1")
    private Double airtimeT1;

    @Column(name = "bootC")
    private Double bootC;

    @Column(name = "counterT1")
    private Long comptadorT1;

    @Column(name = "humiditatT1")
    private Double humitatT1;

    @Column(name = "humiditatT2")
    private Double humitatT2;

    @Column(name = "humiditatT3")
    private Double humitatT3;

    @Column(name = "id")
    private Integer id;

    @Column(name = "portT1")
    private Double portT1;

    @Column(name = "temperaturaT1")
    private Double temperaturaT1;

    @Column(name = "temperaturaT2")
    private Double temperaturaT2;

    @Column(name = "temperaturaT3")
    private Double temperaturaT3;

    @Column(name = "vBattery")
    private Double vBattery;

    public Instant getTime() {
        return time;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public Double gethAmb() {
        return hAmb;
    }

    public void sethAmb(Double hAmb) {
        this.hAmb = hAmb;
    }

    public Double getpAmb() {
        return pAmb;
    }

    public void setpAmb(Double pAmb) {
        this.pAmb = pAmb;
    }

    public Double gettAmb() {
        return tAmb;
    }

    public void settAmb(Double tAmb) {
        this.tAmb = tAmb;
    }

    public Double getAirtimeT1() {
        return airtimeT1;
    }

    public void setAirtimeT1(Double airtimeT1) {
        this.airtimeT1 = airtimeT1;
    }

    public Double getBootC() {
        return bootC;
    }

    public void setBootC(Double bootC) {
        this.bootC = bootC;
    }

    public Long getComptadorT1() {
        return comptadorT1;
    }

    public void setComptadorT1(Long comptadorT1) {
        this.comptadorT1 = comptadorT1;
    }

    public Double getHumitatT1() {
        return humitatT1;
    }

    public void setHumitatT1(Double humitatT1) {
        this.humitatT1 = humitatT1;
    }

    public Double getHumitatT2() {
        return humitatT2;
    }

    public void setHumitatT2(Double humitatT2) {
        this.humitatT2 = humitatT2;
    }

    public Double getHumitatT3() {
        return humitatT3;
    }

    public void setHumitatT3(Double humitatT3) {
        this.humitatT3 = humitatT3;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPortT1() {
        return portT1;
    }

    public void setPortT1(Double portT1) {
        this.portT1 = portT1;
    }

    public Double getTemperaturaT1() {
        return temperaturaT1;
    }

    public void setTemperaturaT1(Double temperaturaT1) {
        this.temperaturaT1 = temperaturaT1;
    }

    public Double getTemperaturaT2() {
        return temperaturaT2;
    }

    public void setTemperaturaT2(Double temperaturaT2) {
        this.temperaturaT2 = temperaturaT2;
    }

    public Double getTemperaturaT3() {
        return temperaturaT3;
    }

    public void setTemperaturaT3(Double temperaturaT3) {
        this.temperaturaT3 = temperaturaT3;
    }

    public Double getvBattery() {
        return vBattery;
    }

    public void setvBattery(Double vBattery) {
        this.vBattery = vBattery;
    }
}