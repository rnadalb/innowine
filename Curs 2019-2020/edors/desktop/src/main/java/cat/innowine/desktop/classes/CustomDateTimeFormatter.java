package cat.innowine.desktop.classes;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

public class CustomDateTimeFormatter {

    private static final DateTimeFormatter DATE_TIME_FORMATTER;

    private CustomDateTimeFormatter() {
    }

    static {
        final DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder()
                .appendPattern("yyyy-MM-dd")
                .appendLiteral('T')
                .appendPattern("HH:mm:ss")
                .optionalStart()
                .appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true).parseDefaulting(ChronoField.NANO_OF_SECOND, 0)
                .optionalEnd()
                .appendOffset("+HH:mm", "Z");
        DATE_TIME_FORMATTER = builder.toFormatter();
    }

    public static DateTimeFormatter getRFC3339Formatter() {
        return DATE_TIME_FORMATTER;
    }
}
