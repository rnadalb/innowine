package cat.innowine.desktop.utils;


import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class GMailHelper {
    private final String smtpHost;
    private final Integer smtpPort;
    private final String smtpUser;
    private final String smtpPasswd;
    private final boolean smtpTls;
    private final boolean smtpAuth;

    private static final String SMTP_PROTO = "smtp";

    public GMailHelper(String smtpHost, Integer smtpPort, String smtpUser, String smtpPasswd, boolean smtpTls, boolean smtpAuth) {
        this.smtpHost = smtpHost;
        this.smtpPort = smtpPort;
        this.smtpUser = smtpUser;
        this.smtpPasswd = smtpPasswd;
        this.smtpTls = smtpTls;
        this.smtpAuth = smtpAuth;
    }

    /**
     * Envia un coreu utilitzant el servidor de correu smtp de Google
     * El format del missatge és text pla.
     *
     * @param recipient Destinatari del missatge
     * @param subject Assumte del correu
     * @param body Cos del missatge en format text pla
     */
    public void send(String recipient, String subject, String body) {
        Properties props = System.getProperties();
        props.put("mail.smtp.host", smtpHost);  //El servidor SMTP de Google
        props.put("mail.smtp.port", smtpPort); // El port SMTP segur de Google
        props.put("mail.smtp.user", smtpUser);
        props.put("mail.smtp.clave", smtpPasswd);    //La contrasenya del compte
        props.put("mail.smtp.auth", smtpAuth);    // Utilitzar autenticació mitjançant usuari / contrasenya
        props.put("mail.smtp.starttls.enable", smtpTls); // Connectar de manera segura al servidor SMTP

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(smtpUser)); // Remitent
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));   //Destinatari
            message.setSubject(subject); // Assumpte
            message.setText(body); // Cos del missatge
            Transport transport = session.getTransport(SMTP_PROTO);
            transport.connect(smtpHost, smtpUser, smtpPasswd);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (MessagingException me) {
            me.printStackTrace();   // Si es produeix algun error
        }
    }
}