package cat.innowine.desktop.classes;

public enum SQLState {
    SQL_OK(0, "Operació realitzada correctament."),
    SQL_DUPLICATED(0, "Error. Clau duplicada."),
    SQL_ERROR(0, "Error general SQL.");

    private int errorCode;
    private String message;

    SQLState(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
