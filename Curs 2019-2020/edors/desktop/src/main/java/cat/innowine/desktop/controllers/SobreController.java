package cat.innowine.desktop.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class SobreController {
    @FXML
    public void lbTancaOnMouseClicked(MouseEvent mouseEvent) {
        if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
            ((Stage) ((Label)mouseEvent.getSource()).getScene().getWindow()).close();
        }
    }
}