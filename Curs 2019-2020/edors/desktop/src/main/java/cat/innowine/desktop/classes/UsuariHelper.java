package cat.innowine.desktop.classes;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class UsuariHelper<Usuari> extends DAOHelper<Usuari> {

    public UsuariHelper(EntityManagerFactory emf, Class<Usuari> parameterClass) {
        super(emf, parameterClass);
    }

    public Usuari getUsuariByLogin(String login) {
        try {
            EntityManager em = this.emf.createEntityManager();
            CriteriaBuilder cb = em.getCriteriaBuilder();

            CriteriaQuery<Usuari> cbQuery = cb.createQuery(parameterClass);
            Root<Usuari> c = cbQuery.from(parameterClass);
            cbQuery.select(c).where(cb.equal(c.get("login"), login));

            Query query = em.createQuery(cbQuery);
            return (Usuari) query.getSingleResult();
        } catch (NoResultException nre) {
                return null;
        }
    }
}