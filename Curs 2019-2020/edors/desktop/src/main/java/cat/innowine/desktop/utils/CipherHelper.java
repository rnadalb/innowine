package cat.innowine.desktop.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.MessageDigestAlgorithms;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

public class CipherHelper {

    private static final String DEFAULT_MESSAGEDIGEST_ALGORITHM = MessageDigestAlgorithms.SHA_256;
    /**
     * Genera una clau apta per encriptar utilitzant la transforamció AES
     *
     * @param password Contrasenya a adaptar
     * @return Objkecte SecretKey per ser utilitzat al procés d'encriptació
     */
    private static SecretKey getKey(String password) {
        SecretKeyFactory factory = null;
        try {
            factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        byte[] salt = new byte[16];

        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 256);
        SecretKey tmp = null;
        try {
            tmp = factory.generateSecret(spec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return new SecretKeySpec(tmp.getEncoded(), "AES");
    }

    /**
     * Encripta un fitxer de text utilitzant la clau @password i emmagatzema el resultat al fitxer @encFile
     * @param clearFile Ruta del fitxer a encriptar
     * @param encFile Ruta del fitxer resultat de l'encriptació
     * @param password Contrasenya encapsulada en un objecte SecretKey
     */
    public static void fileEncrypt(String clearFile, String encFile, String password) {
        try {
            FileInputStream fileInput = new FileInputStream(clearFile);
            final SecretKey secretKey = getKey(password);
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            StringBuilder resultStringBuilder = new StringBuilder();

            BufferedReader br = new BufferedReader(new InputStreamReader(fileInput));
            String clearLine;
            while ((clearLine = br.readLine()) != null) {
                resultStringBuilder.append(clearLine).append("\n");
            }

            try (FileOutputStream fileOut = new FileOutputStream(encFile);
                 CipherOutputStream cipherOut = new CipherOutputStream(fileOut, cipher)) {
                cipherOut.write(resultStringBuilder.toString().getBytes());
            }
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | IOException | InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    /**
     * Desencripta un fitxer de text encriptat mitjançant la transformació AES amb la contrasenya @password
     * @param encFile Ruta del fitxer encriptat
     * @param password Contrasenya encapsulada en un objecte SecretKey
     * @return String amb el contingut del fitxer, separat per caràcters \n
     */
    public static String fileDecrypt(String encFile, String password) {
        String content = null;
        try {
            FileInputStream fileIn = new FileInputStream(encFile);
            final SecretKey secretKey = getKey(password);
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            CipherInputStream cipherIn = new CipherInputStream(fileIn, cipher);
            InputStreamReader inputReader = new InputStreamReader(cipherIn);
            BufferedReader reader = new BufferedReader(inputReader);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            content = sb.toString();
        } catch (IOException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
        return content;
    }

    public static String encryptPassword(String password) {
        return encryptPassword(password, DEFAULT_MESSAGEDIGEST_ALGORITHM);
    }

    private static String encryptPassword(String password, String algorithm) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(password.getBytes());
        byte[] digest = md.digest();

        // Escriu el base 64 utilitzant la llibreria
        // commons-codec-x.x.x.jar de Apache
        byte[] encoded = Base64.encodeBase64(digest);
        return new String(encoded);
    }
}