package cat.innowine.desktop.classes;

import org.hibernate.exception.ConstraintViolationException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.List;

public class DAOHelper<T> {
    protected EntityManagerFactory emf;
    protected final Class<T> parameterClass;

    public DAOHelper(EntityManagerFactory emf, Class<T> parameterClass) {
        this.emf = emf;
        this.parameterClass = parameterClass;
    }

    public SQLState insert(T t) {
        EntityManager em = emf.createEntityManager();
        SQLState state = SQLState.SQL_OK;
        em.getTransaction().begin();
        try {
            em.persist(t);
            em.getTransaction().commit();
        } catch (Exception e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                state = SQLState.SQL_DUPLICATED;
            }
            else {
                state = SQLState.SQL_ERROR;
            }
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return state;
    }

    public SQLState update(T t) {
        EntityManager em = emf.createEntityManager();
        SQLState state = SQLState.SQL_OK;
        em.getTransaction().begin();
        try {
            em.merge(t);
            em.getTransaction().commit();
        } catch (Exception e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                state = SQLState.SQL_DUPLICATED;
            }
            else {
                state = SQLState.SQL_ERROR;
            }
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return state;
    }

    public SQLState update (String where, String pattern, String col, String value) {
        SQLState state = SQLState.SQL_OK;
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaUpdate<T> update = builder.createCriteriaUpdate(parameterClass);
        Root<T> root = update.from(parameterClass);
        Expression<Boolean> filterPredicate = builder.like(builder.lower(root.get(where)), pattern);
        try {
            update.set(root.get(col), value).where(filterPredicate);
            em.getTransaction().begin();
            em.createQuery(update).executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                state = SQLState.SQL_DUPLICATED;
            }
            else {
                state = SQLState.SQL_ERROR;
            }
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return state;
    }

    public SQLState bulkUpdate(String col, String value) {
        SQLState state = SQLState.SQL_OK;
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaUpdate<T> update = builder.createCriteriaUpdate(parameterClass);
        Root<T> root = update.from(parameterClass);
        try {
            update.set(root.get(col), value);
            em.getTransaction().begin();
            em.createQuery(update).executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                state = SQLState.SQL_DUPLICATED;
            }
            else {
                state = SQLState.SQL_ERROR;
            }
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return state;
    }

    public SQLState delete(Integer id) {
        SQLState state = SQLState.SQL_OK;
        EntityManager em = emf.createEntityManager();
        T t = em.find(parameterClass, id);

        try {
            if (t != null) {
                em.getTransaction().begin();
                em.remove(t);
                em.getTransaction().commit();
            }
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return state;
    }

    public T read(Integer id) {
        EntityManager em = emf.createEntityManager();
        try {
            return em.find(parameterClass, id);
        } finally {
            em.close();
        }
    }

    public T read(String id) {
        EntityManager em = emf.createEntityManager();
        try {
            return em.find(parameterClass, id);
        } finally {
            em.close();
        }
    }

    public List<T> getAll() {
        CriteriaBuilder cb = this.emf.getCriteriaBuilder();
        EntityManager manager = this.emf.createEntityManager();

        CriteriaQuery<T> cbQuery = cb.createQuery(parameterClass);
        Root<T> c = cbQuery.from(parameterClass);
        cbQuery.select(c);

        Query query = manager.createQuery(cbQuery);

        return query.getResultList();
    }

    public T getFirst() {
        List<T> list = getAll();
        return getAll().isEmpty() ? null : list.iterator().next();
    }

    public void printAll() {
        List<T> list = getAll();
        list.forEach(System.out::println);
    }
}