package cat.innowine.desktop.model;

import javax.persistence.*;
import java.io.Serializable;

@SuppressWarnings ("JpaObjectClassSignatureInspection")
@Entity
@Table (name = "tbl_config")
public class Config implements Serializable {
  private static final long serialVersionUID = 1860724276025173560L;

  private static Config instancia;

  private Config() {}

  public static Config getInstancia(){
    if(instancia == null){
      instancia = new Config();
    }
    return instancia;
  }

  @Id
  @GeneratedValue (strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "max_intents", nullable = false)
  private Integer maxIntents;

  @Column(name = "min_caracters", nullable = false)
  private Integer minCaracters;

  @Column(name = "email_subject", nullable = false)
  private String emailSubject;

  @Column(name = "email_body", nullable = false)
  private String emailBody;

  @Column(name = "map_projection", nullable = false)
  private Integer mapProjection;

  @Column(name = "email_smtp")
  private String emailSmtp;

  @Column(name = "email_port")
  private Integer emailPort;

  @Column(name = "email_user", nullable = false)
  private String emailUser;

  @Column(name = "email_passwd", nullable = false)
  private String emailPasswd;

  @Column(name = "email_tls")
  private boolean emailTls;

  @Column(name = "email_auth")
  private boolean emailAuth;

  @Column(name = "influxdb_host", nullable = false)
  private String influxdbHost;

  @Column(name = "influxdb_port", nullable = false)
  private Integer influxdbPort;

  @Column(name = "influxdb_database", length = 100, nullable = false)
  private String influxdbDatabase;

  @Column(name = "influxdb_user", length = 20, nullable = false)
  private String influxdbUser;

  @Column(name = "influxdb_passwd", nullable = false)
  private String influxdbPassword;

  @Column(name = "influxdb_ssl")
  private boolean influxdbSSL;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getMaxIntents() {
    return maxIntents;
  }

  public void setMaxIntents(Integer maxIntents) {
    this.maxIntents = maxIntents;
  }

  public Integer getMinCaracters() {
    return minCaracters;
  }

  public void setMinCaracters(Integer minCaracters) {
    this.minCaracters = minCaracters;
  }

  public String getEmailSubject() {
    return emailSubject;
  }

  public void setEmailSubject(String emailSubject) {
    this.emailSubject = emailSubject;
  }

  public String getEmailBody() {
    return emailBody;
  }

  public void setEmailBody(String emailBody) {
    this.emailBody = emailBody;
  }

  public Integer getMapProjection() {
    return mapProjection;
  }

  public void setMapProjection(Integer mapProjection) {
    this.mapProjection = mapProjection;
  }

  public String getEmailSmtp() {
    return emailSmtp;
  }

  public void setEmailSmtp(String emailSmtp) {
    this.emailSmtp = emailSmtp;
  }

  public Integer getEmailPort() {
    return emailPort;
  }

  public void setEmailPort(Integer emailPort) {
    this.emailPort = emailPort;
  }

  public String getEmailUser() {
    return emailUser;
  }

  public void setEmailUser(String emailUser) {
    this.emailUser = emailUser;
  }

  public String getEmailPasswd() {
    return emailPasswd;
  }

  public void setEmailPasswd(String emailPasswd) {
    this.emailPasswd = emailPasswd;
  }

  public boolean isEmailTls() {
    return emailTls;
  }

  public void setEmailTls(boolean emailTls) {
    this.emailTls = emailTls;
  }

  public boolean isEmailAuth() {
    return emailAuth;
  }

  public void setEmailAuth(boolean emailAuth) {
    this.emailAuth = emailAuth;
  }

  public String getInfluxdbHost() {
    return influxdbHost;
  }

  public void setInfluxdbHost(String influxdbHost) {
    this.influxdbHost = influxdbHost;
  }

  public Integer getInfluxdbPort() {
    return influxdbPort;
  }

  public void setInfluxdbPort(Integer influxdbPort) {
    this.influxdbPort = influxdbPort;
  }

  public String getInfluxdbDatabase() {
    return influxdbDatabase;
  }

  public void setInfluxdbDatabase(String influxdbDatabase) {
    this.influxdbDatabase = influxdbDatabase;
  }

  public String getInfluxdbUser() {
    return influxdbUser;
  }

  public void setInfluxdbUser(String influxdbUser) {
    this.influxdbUser = influxdbUser;
  }

  public String getInfluxdbPassword() {
    return influxdbPassword;
  }

  public void setInfluxdbPassword(String influxdbPassword) {
    this.influxdbPassword = influxdbPassword;
  }

  public boolean isInfluxdbSSL() {
    return influxdbSSL;
  }

  public void setInfluxdbSSL(boolean influxdbSSL) {
    this.influxdbSSL = influxdbSSL;
  }
}
