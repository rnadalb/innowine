package cat.innowine.desktop.report;

import cat.innowine.desktop.classes.Constants;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

public class ReportFX {
    private static ReportFX instancia;

    public ReportFX(){

    }

    public static ReportFX getInstancia() {
        if (instancia == null) {
            instancia = new ReportFX();
        }

        return instancia;
    }

    /**
     * Informe bàsic, sense paràmetres
     * @param con Conenxió de la BBDD
     * @param informe Ruta a l'informe
     */
    public void preview(Connection con, String informe) {
        try {
            InputStream inputStream = getClass().getResourceAsStream(informe);
            JasperDesign jdesign = JRXmlLoader.load(inputStream);
            JasperReport jreport = JasperCompileManager.compileReport(jdesign);
            JasperPrint jprint = JasperFillManager.fillReport(jreport, new HashMap<>(), con);
            new JasperViewerFX().viewReport(Constants.HEADER_PREVIEW, jprint);
            con.close();
            System.out.println(con.isClosed());
        } catch (JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Informe bàsic amb paràmetres
     * @param con Conenxió de la BBDD
     * @param informe Ruta a l'informe
     * @param parametres Paràmetres a passar a l'informe
     */
    public void preview(Connection con, String informe, HashMap parametres) {
        try {
            InputStream inputStream = getClass().getResourceAsStream(informe);
            JasperDesign jdesign = JRXmlLoader.load(inputStream);
            JasperReport jreport = JasperCompileManager.compileReport(jdesign);
            JasperPrint jprint = JasperFillManager.fillReport(jreport, parametres, con);
            new JasperViewerFX().viewReport(Constants.HEADER_PREVIEW, jprint);
            con.close();
        } catch (JRException | SQLException e) {
            e.printStackTrace();
        }
    }
}
