package cat.innowine.desktop.classes;

/**
 * Java Simplified Encryption
 *
 * http://www.jasypt.org/
 *
 * https://stackoverflow.com/questions/10306673/securing-a-password-in-a-properties-file
 *
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppPropertiesHelper {
    private static AppProperties readPropertiesFile(String fileName) {
        InputStream fitxer = null;
        Properties credencials = new Properties();
        AppProperties propertiesFile = new AppProperties();

        try {
            fitxer = AppPropertiesHelper.class.getClassLoader().getResourceAsStream(fileName);

            if (fitxer == null) {
                System.err.println("No puc llegir " + fileName);
            } else {
                credencials.load(fitxer);

                propertiesFile.setHost(credencials.getProperty("host"));
                propertiesFile.setPort(credencials.getProperty("port"));
                propertiesFile.setBd(credencials.getProperty("bd"));
                propertiesFile.setUser(credencials.getProperty("user"));
                propertiesFile.setPassword(credencials.getProperty("password"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fitxer != null) {
                try {
                    fitxer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return propertiesFile;
    }
}
