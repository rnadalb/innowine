package cat.innowine.desktop.controllers;

import javax.persistence.EntityManagerFactory;

public abstract class DefaultController  {
    private EntityManagerFactory emf;

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }
    public EntityManagerFactory getEmf() {
        return emf;
    }

}
