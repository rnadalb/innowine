package cat.innowine.desktop.controllers;

import cat.innowine.desktop.Main;
import cat.innowine.desktop.classes.Constants;
import cat.innowine.desktop.classes.LoginState;
import cat.innowine.desktop.classes.UsuariHelper;
import cat.innowine.desktop.model.Usuari;
import cat.innowine.desktop.utils.AlertHelper;
import cat.innowine.desktop.utils.CipherHelper;
import cat.innowine.desktop.utils.LoginUtils;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginController extends DefaultController implements Initializable {

    @FXML
    private Label lbErrors;

    @FXML
    private TextField tfUsuari;

    @FXML
    private TextField tfPassword;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tfUsuari.clear();
        tfPassword.clear();
        tfUsuari.requestFocus();
    }

    @FXML
    public void btSignInOnMouseClicked(MouseEvent mouseEvent) {
        logIn();
    }

    private void logIn() {
        if (!tfUsuari.getText().isEmpty()) {
            if (!tfPassword.getText().isEmpty()){
                UsuariHelper usuariHelper = new UsuariHelper(this.getEmf(), Usuari.class);
                Usuari usuari = (Usuari) usuariHelper.getUsuariByLogin(tfUsuari.getText());
                LoginState loginState = LoginUtils.checkPassword(usuari, CipherHelper.encryptPassword(tfPassword.getText()));
                lbErrors.setText(loginState.getMessage());
                switch (loginState) {
                    case GRANTED:
                        openMainForm(usuari);
                        ((Stage)tfPassword.getParent().getScene().getWindow()).close(); // Tanca la finestra actual
                        break;
                    case FAILED:
                        AlertHelper.alertInfo("Usuari/Contrasenya no vàlids");
                        if (!usuari.isAdmin()) {
                            usuari.setIntents(usuari.getIntents() + 1);
                            if (usuari.getIntents() >= Constants.MAX_INTENTS)
                                usuari.setBloquejat(true);
                            usuariHelper.update(usuari);
                        }
                        break;
                    case LOCKED:
                        AlertHelper.alertInfo("Compte bloquejat");
                        break;
                }
            } else {
                AlertHelper.alertInfo("El camp contrasenya està buït");
                tfPassword.selectAll();
                tfPassword.requestFocus();
            }
        } else {
            AlertHelper.alertInfo("El camp nom d'usuari està buït");
            tfUsuari.selectAll();
            tfUsuari.requestFocus();
        }
    }

    private void openMainForm(Usuari usuari) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/DashBoard.fxml"));
        Parent root = null;

        try {
            root = loader.load();
            DefaultControllerDiferit controller = loader.getController();
            controller.setEmf(this.getEmf());
            controller.setUsuari(usuari);
            controller.iniciDiferit();
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.initStyle(StageStyle.DECORATED);
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }

    @FXML
    public void tfOnKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            if (keyEvent.getSource().equals(tfUsuari) & !tfUsuari.getText().isEmpty()) {
                tfPassword.requestFocus();
                tfPassword.selectAll();
            } else {
                if (!tfPassword.getText().isEmpty()) {
                    logIn();
                }
            }
        }
    }

    @FXML
    public void btSurtOnMouseClicked(MouseEvent mouseEvent) {
        if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
            Platform.exit();
        }
    }
}
