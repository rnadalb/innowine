package cat.innowine.desktop.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtils {
    public static Date localDateToDate(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    public static Instant toInstant(LocalDate localDate) {
        return localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
    }

    public static String toInfluxDBDate(LocalDateTime date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return date.format(formatter);
    }

    //public static String toInfluxDBDate(String date) {
    //    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    //    LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
    //}

/*
    public static String formatToInfluxTime(Date date) {
        try {
            //influxDate = new SimpleDateFormat("yyyy-MM-ddT00:00:00.00000000Z").parse(date.toString());
            influxDate = new SimpleDateFormat("yyyy-MM-dd").parse(date.toString());
        }
        catch (ParseException e) {
            // Show error message to enduser about the wrong format and forward back to the JSP with the form.
            request.setAttribute("error", "Invalid format, please enter yyyy-MM-dd");
            request.getRequestDispatcher("search.jsp").forward(request, response);
            return;
        }
    }

 */
}
