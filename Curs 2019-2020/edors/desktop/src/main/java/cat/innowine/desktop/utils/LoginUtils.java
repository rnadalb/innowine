package cat.innowine.desktop.utils;

import cat.innowine.desktop.classes.Constants;
import cat.innowine.desktop.classes.LoginState;
import cat.innowine.desktop.model.Usuari;

public class LoginUtils {

    public static LoginState checkPassword(Usuari usuari, String password) {
        LoginState loginState;
        if (usuari != null) {
            if ( usuari.getPassword().equals(password))
                loginState = LoginState.GRANTED;
            else {
                if (usuari.getIntents() < Constants.MAX_INTENTS && !usuari.isBloquejat()) {
                    loginState = LoginState.FAILED;
                } else {
                    loginState = LoginState.LOCKED;
                }
            }
        } else
            loginState = LoginState.NOEXISTS;
        return loginState;
    }
}