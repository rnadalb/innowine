package cat.innowine.desktop.controllers;

import cat.dam2.m15.controls.doubletextfield.DoubleTextField;
import cat.dam2.m15.controls.uppercasetextfield.UpperCaseTextField;
import cat.innowine.desktop.Main;
import cat.innowine.desktop.classes.*;
import cat.innowine.desktop.exceptions.AlertHelperExcepcion;
import cat.innowine.desktop.exceptions.AlertHelperExcepcionSelectedYes;
import cat.innowine.desktop.model.Config;
import cat.innowine.desktop.model.Dispositiu;
import cat.innowine.desktop.model.DummyData;
import cat.innowine.desktop.model.TipusSol;
import cat.innowine.desktop.utils.AlertHelper;
import com.sothawo.mapjfx.*;
import com.sothawo.mapjfx.event.MapViewEvent;
import com.sothawo.mapjfx.offline.OfflineCache;
import eu.hansolo.medusa.Gauge;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.influxdb.dto.BoundParameterQuery;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class DadesDispositiuController extends DefaultControllerDiferit implements Initializable {
    @FXML
    private SplitPane spMain, spDades;

    @FXML
    private Gauge gBateria;

    @FXML
    private Accordion  mapControls;

    @FXML
    private TitledPane optionsMapType;

    @FXML
    private Label lbInfoLatLong, lblHumiAmb, lblPresAmb, lblTempAmb, lblError;

    @FXML
    private MapView mapView;

    @FXML
    private UpperCaseTextField tfDescripcio;

    @FXML
    private TextField tfIdDispositiu;

    @FXML
    private DoubleTextField tfLatitud, tfLongitud;

    @FXML
    private RadioButton radioMsOSM; // RadioButton prr estil de mapa OSM

    @FXML
    private RadioButton radioMsWMS; // RadioButton prr estil de mapa WMS

    @FXML
    private RadioButton radioMsXYZ; // RadioButton prr estil de mapa XYZ

    @FXML
    private ToggleGroup mapTypeGroup; // ToggleGroup per gestionar els botons de radio dels estils de mapa

    @FXML
    private Slider sdZoom;

    @FXML
    private Button btZoomPlus, btZoomMinus;

    @FXML
    private ComboBox cboTipusSol;

    private Dispositiu dispositiu;
    private boolean newRecord = false;
    private Marker currentMarker;

    /** zoom per defecte **/
    private static final int ZOOM_DEFAULT = 14;
    private static final int ZOOM_LOCATED = 17;
    private static final Marker.Provided DEFAULT_MARKER_COLOR = Marker.Provided.BLUE;
    private static final IntegerProperty zoom_value = new SimpleIntegerProperty(ZOOM_DEFAULT);

    /** coordinades per defecte **/
    private static final double DEFAULT_LAT= 41.3461265;
    private static final double DEFAULT_LONG= 1.697939799999972;
    private static final Coordinate defaultLocation = new Coordinate(DEFAULT_LAT, DEFAULT_LONG);

    /** Posició fixada dels dividers dels SplitPane */
    private static final double DIVIDER_MAIN = 0.4673913043478261d;
    private static final double DIVIDER_DADES = 0.43219076005961254d;

    /** Paràmetres per al WMS server. */
    private final WMSParam wmsParam = new WMSParam()
            .setUrl("http://ows.terrestris.de/osm/service?")
            .addParam("layers", "OSM-WMS");

    private final XYZParam xyzParams = new XYZParam()
            .withUrl("https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x})")
            .withAttributions("'Tiles &copy; <a href=\"https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer\">ArcGIS</a>'");

    Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.35), evt -> lblError.setVisible(false)),
            new KeyFrame(Duration.seconds(0.6), evt -> lblError.setVisible(true)));

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Evitar que el TitledPane es colapsi
        mapControls.setExpandedPane(optionsMapType);
        mapControls.getExpandedPane().setExpanded(true);
        mapControls.getExpandedPane().setCollapsible(false);

        // No hi ha altra manera --> stackoverflow
        SplitPane.Divider mainDivider = spMain.getDividers().get(Constants.FIRST_ITEM);
        mainDivider.positionProperty().addListener((observable, oldvalue, newvalue) -> {
            mainDivider.setPosition(DIVIDER_MAIN);
        });

        SplitPane.Divider dadesDivider = spDades.getDividers().get(Constants.FIRST_ITEM);
        dadesDivider.positionProperty().addListener((observable, oldvalue, newvalue) -> {
            dadesDivider.setPosition(DIVIDER_DADES);
        });
    }

    @Override
    public void iniciDiferit() {
        ompleTipusSol();
        if (dispositiu != null) {
            cboTipusSol.getSelectionModel().select(dispositiu.getTipusSol());
            tfIdDispositiu.setText(dispositiu.getIdDispositiu().toString());
            tfDescripcio.setText(dispositiu.getDescripcio());
            tfDescripcio.requestFocus();
            tfDescripcio.selectAll();
            if (dispositiu.getLatitud() != null)
                tfLatitud.setText(dispositiu.getLatitud().toString());
            if (dispositiu.getLongitud() != null)
                tfLongitud.setText(dispositiu.getLongitud().toString());
            updateDeviceData(dispositiu.getIdDispositiu());
        } else { // Nou registre
            newRecordActions();
            updateDeviceData(Constants.NEW_RECORD);
        }

        MapProjection mp = MapProjection.values()[Config.getInstancia().getMapProjection()];
        if (mp.toString().contains(MapProjection.WGS_84.getProjectionDescription()))
            initMapAndControls(Projection.WGS_84);
        else
            initMapAndControls(Projection.WEB_MERCATOR);
    }

    /**
     * Aquest mètode es cridat desprès de carregar el fxml i tots els objectes són creats. No tornarà acridar-se,
     * perquè necessitem passar la projecció abans d'inicialitzar.
     * @param projection
     *      la projecció a utitlizar al mapa.
     */
    private void initMapAndControls(Projection projection) {
        // inicialitza MapView-Cache
        final OfflineCache offlineCache = mapView.getOfflineCache();
        final String cacheDir = System.getProperty("java.io.tmpdir") + "/mapjfx-cache";

        // estil CSS del mapa
        mapView.setCustomMapviewCssURL(getClass().getResource("/view/css/mapview.css"));

        // escolta (monitortiza) la propietat que indica que el mapa ha finalitzat la seva inicialització
        mapView.initializedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                afterMapIsInitialized();
            }
        });

        // controla els radio btuuons que gestionen els tipus de mapes
        mapTypeGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            MapType mapType = MapType.OSM;
            if (newValue == radioMsOSM) {
                mapType = MapType.OSM;
            } else if (newValue == radioMsWMS) {
                mapView.setWMSParam(wmsParam);
                mapType = MapType.WMS;
            } else if (newValue == radioMsXYZ) {
                mapView.setXYZParam(xyzParams);
                mapType = MapType.XYZ;
            }
            mapView.setMapType(mapType);
        });
        mapTypeGroup.selectToggle(radioMsOSM);

        // Associament de la propietat del zoom als controls slider i button corresponents
        sdZoom.valueProperty().bindBidirectional(mapView.zoomProperty());
        zoom_value.bindBidirectional(mapView.zoomProperty());

        setupEventHandlers();

        mapView.initialize(Configuration.builder()
                .projection(projection)
                .showZoomControls(false)
                .build());
    }

    /**
     * Inicialitza els listeners de gestió del mapa i la GUI
     */
    private void setupEventHandlers() {
        // Gestió del zoom pels botons
        btZoomPlus.setOnAction(event -> {
            event.consume();
            mapView.setZoom(mapView.getZoom() + 1);
        });

        btZoomMinus.setOnAction(event -> {
            event.consume();
            mapView.setZoom(mapView.getZoom() - 1);
        });

        mapView.addEventHandler(MapViewEvent.MAP_POINTER_MOVED, event -> {
            event.consume();
            Coordinate c = new Coordinate(event.getCoordinate().getLatitude(), event.getCoordinate().getLongitude());
            DecimalFormat format = new DecimalFormat(Patterns.DECIMAL_FORMAT);
            try {
                Number lat = format.parse(c.getLatitude().toString());
                Number lng = format.parse(c.getLongitude().toString());
                lbInfoLatLong.setText(String.format("[%f, %f]", lat.doubleValue(), lng.doubleValue()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        // Gestiona l'esdeveniment de fer clic amb el botó dret en el mapa
        mapView.addEventHandler(MapViewEvent.MAP_RIGHTCLICKED, event -> {
            event.consume();
            Coordinate c = new Coordinate(event.getCoordinate().getLatitude(), event.getCoordinate().getLongitude());
            DecimalFormat format = new DecimalFormat(Patterns.DECIMAL_FORMAT);
            Number lat = null;
            Number lng = null;
            try {
                lat = format.parse(c.getLatitude().toString());
                lng = format.parse(c.getLongitude().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            try {
                String msg = String.format("Modificar Latitud i Longitud ?\n\n[lat,long] = [%f %f]", lat.doubleValue(), lng.doubleValue());
                AlertHelper.alertaYesNo(Constants.APPNAME, msg);
            } catch (AlertHelperExcepcion excepcion) {
                if (excepcion instanceof AlertHelperExcepcionSelectedYes) {
                    tfLatitud.setText(String.format("%f", lat.doubleValue()));
                    tfLongitud.setText(String.format("%f", lng.doubleValue()));
                    if (currentMarker != null)
                        mapView.removeMarker(currentMarker);
                    currentMarker = addMarker(c, DEFAULT_MARKER_COLOR, true);
                }
            }
        });
    }

    /**
     * Finalitza la configuració desprès que el mapa estigui inicialitzat
     */
    private void afterMapIsInitialized() {
        if (dispositiu != null) {
            if (dispositiu.getLatitud() != null && this.dispositiu.getLongitud() != null) {
                updateMap(dispositiu.getLatitud(), dispositiu.getLongitud());
            }
        } else { // Si és null, es un nou registre
            updateMap(defaultLocation.getLatitude(), defaultLocation.getLongitude());
        }
    }

    private void updateMap(Double lat, Double lng){
        //TODO: si lat i lng estan, cal marcar i localitar l'ubicació amb un PIN
        mapView.setZoom(ZOOM_LOCATED);
        mapView.setCenter(new Coordinate(lat, lng));
        currentMarker = addMarker(true);
    }

    /**
     * Afegeix un marcador
     *
     * @param center si és true centra en el mapa la nova posició
     * @return
     */
    private Marker addMarker (boolean center) {
        Marker result = null;
        if (!tfLatitud.getText().isEmpty() && !tfLongitud.getText().isEmpty()) {
            if (Pattern.matches(Patterns.fpRegex, tfLatitud.getText())) {
                Double lat = Double.valueOf(tfLatitud.getText());
                if (Pattern.matches(Patterns.fpRegex, tfLongitud.getText())) {
                    Double lng = Double.valueOf(tfLongitud.getText());
                    Coordinate c = new Coordinate(lat,lng);
                    result = addMarker(c, DEFAULT_MARKER_COLOR, center);
                } else {
                    AlertHelper.alertInfo("El valor de la <longitud> no és correcte");
                }
            } else {
                AlertHelper.alertInfo("El valor de la <latitud> no és correcte");
            }
        }
        return result;
    }

    private Marker addMarker (Coordinate coordinate, Marker.Provided color, boolean center) {
        Marker marker = Marker.createProvided(color).setPosition(coordinate).setVisible(true);
        String info = String.format("Posició [%s,%s]", coordinate.getLatitude().toString(), coordinate.getLongitude().toString());
        MapLabel lblInfo = new MapLabel(info, 10, -10).setVisible(false).setCssClass("orange-label");
        marker.attachLabel(lblInfo);
        mapView.addMarker(marker);
        if (center)
            mapView.setCenter(coordinate);
        mapView.setZoom(ZOOM_LOCATED);

        return marker;
    }

    private void ompleTipusSol() {
        DAOHelper<TipusSol> helper = new DAOHelper<>(this.getEmf(), TipusSol.class);
        cboTipusSol.getItems().addAll(helper.getAll());
    }

    private Dispositiu obteDadesFormulari(Dispositiu d) {
        d.setIdDispositiu(Integer.parseInt(tfIdDispositiu.getText()));
        d.setLatitud(Double.valueOf(tfLatitud.getText()));
        d.setLongitud(Double.valueOf(tfLongitud.getText()));
        d.setDescripcio(tfDescripcio.getText());

        if (this.getUsuari().isAdmin())
            if (d.getUsuari() == null)
                d.setUsuari(this.getUsuari());
            else
                d.setUsuari(d.getUsuari());
        else
            d.setUsuari(this.getUsuari());
        d.setTipusSol((TipusSol) cboTipusSol.getSelectionModel().getSelectedItem());
        return d;
    }

    private void newRecordActions() {
        newRecord = true;
        tfIdDispositiu.clear();
        tfDescripcio.clear();
        tfLatitud.clear();
        tfLongitud.clear();
        cboTipusSol.getSelectionModel().selectFirst();
        tfIdDispositiu.requestFocus();
    }

/*    private Dispositiu guardar (Dispositiu dispositiu) {
        DAOHelper<Dispositiu> helper = new DAOHelper<>(this.getEmf(), Dispositiu.class);
        if (newRecord) {
            if (helper.insert(dispositiu) == SQLState.SQL_DUPLICATED) {
                AlertHelper.alertaWarning(String.format("L'identificador de dispositiu [%d] ja existeix", dispositiu.getIdDispositiu()));
                tfIdDispositiu.requestFocus();
                tfIdDispositiu.selectAll();
                newRecord = false; // TODO: Comprovar que funciona el flag
            }
        } else {
            helper.update(dispositiu);
        }
        return dispositiu;
    }*/

    private boolean guardar (Dispositiu dispositiu) {
        boolean result = true;
        DAOHelper<Dispositiu> helper = new DAOHelper<>(this.getEmf(), Dispositiu.class);
        if (newRecord) {
            if (helper.insert(dispositiu) == SQLState.SQL_DUPLICATED) {
                result = false;
            }
        } else {
            helper.update(dispositiu);
        }
        return result;
    }

    @FXML
    public void btnGuardarOnAction(ActionEvent actionEvent) {
        if (!tfLatitud.getText().isEmpty() && Pattern.matches(Patterns.fpRegex, tfLatitud.getText())) {
            if (!tfLongitud.getText().isEmpty() && Pattern.matches(Patterns.fpRegex, tfLongitud.getText())) {
                if (!tfDescripcio.getText().isEmpty()) {
                    if (dispositiu == null) {
                        newRecord = true; // Mantenir la posició ja que guardar modifica el flag
                        dispositiu = obteDadesFormulari(new Dispositiu());
                        if (guardar(dispositiu)) {
                            updateDeviceData(dispositiu.getIdDispositiu());
                        } else {
                            AlertHelper.alertaWarning(String.format("L'identificador de dispositiu [%s] ja existeix", tfIdDispositiu.getText()));
                            tfIdDispositiu.requestFocus();
                            tfIdDispositiu.selectAll();
                        }
                    } else {
                        if (guardar(obteDadesFormulari(dispositiu)))
                            updateDeviceData(dispositiu.getIdDispositiu());
                    }
                } else {
                    AlertHelper.alertaError("Error. El camp [descripció] no pot quedar buit");
                }
            } else {
                AlertHelper.alertaError("Error. El format del camp [longitud] no és correcte");
            }
        } else {
            AlertHelper.alertaError("Error. El format del camp [latitud] no és correcte");
        }
    }

    @FXML
    public void btSurtOnAction(ActionEvent actionEvent) {
        ((Stage)((Button)actionEvent.getSource()).getScene().getWindow()).close();
    }

    @FXML
    public void btSearchOnAction(ActionEvent actionEvent) {
        if (!tfLongitud.getText().isEmpty() && !tfLatitud.getText().isEmpty()) {
            Coordinate coordinate = new Coordinate(Double.parseDouble(tfLatitud.getText()),
                                                   Double.parseDouble(tfLongitud.getText()));
            if (currentMarker != null)
                mapView.removeMarker(currentMarker);
            currentMarker = addMarker(coordinate, DEFAULT_MARKER_COLOR, true);
        }
    }

    @FXML
    private void btnCancelarOnAction(ActionEvent actionEvent) {
        ((Stage) ((Button) actionEvent.getSource()).getScene().getWindow()).close();
    }

    @FXML
    private void btnMonitorOnAction(ActionEvent actionEvent) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Chart.fxml"));
        Parent root = null;

        try {
            root = loader.load();
            ChartController controller = loader.getController();
            controller.setEmf(this.getEmf());
            controller.setUsuari(this.getUsuari());
            controller.setDispositiu(this.getDispositiu());
            controller.iniciDiferit();
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.initStyle(StageStyle.DECORATED);
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            AlertHelper.alertaError("No ha estat possible mostrar la pantalla del gràfics de dispositiu\nAvisi a l'administrador/a.");
        }
    }

    /**
     * Obté les últimes dades de temperetura, Humitat, Pressió i Nivell de bateria del dispositiu
     * @param id Identificador del dispositiu
     */
    private void updateDeviceData(Integer id) {
        InfluxDbHelper influxDbHelper = new InfluxDbHelper(Config.getInstancia().getInfluxdbHost(),
                                                           Config.getInstancia().getInfluxdbPort(),
                                                           Config.getInstancia().getInfluxdbUser(),
                                                           Config.getInstancia().getInfluxdbPassword(),
                                                           Config.getInstancia().isInfluxdbSSL());
        Query query = BoundParameterQuery.QueryBuilder.newQuery("select id, Hamb, Pamb, Tamb, vBattery from dummyData where id = $id order by time desc limit 1;")
                .forDatabase("iwinedb")
                .bind("id", id)
                .create();

        QueryResult queryResult = influxDbHelper.query(query);
        InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
        List<DummyData> resultat = resultMapper.toPOJO(queryResult, DummyData.class);
        lblError.setVisible(resultat.isEmpty());
        if (resultat != null && resultat.size() > Constants.FIRST_ITEM) {
            gBateria.setValue(resultat.get(Constants.FIRST_ITEM).getvBattery()/1000);
            lblHumiAmb.setText(resultat.get(Constants.FIRST_ITEM).gethAmb().toString() + " %");
            lblPresAmb.setText(resultat.get(Constants.FIRST_ITEM).getpAmb().toString() + " mbars");
            lblTempAmb.setText(resultat.get(Constants.FIRST_ITEM).gettAmb().toString() + " ºC");
            timeline.stop();
            lblError.setVisible(false);
        } else {
            lblError.setText("ERROR. No hi han dades del dispositiu");
            lblHumiAmb.setText("######");
            lblPresAmb.setText("######");
            lblTempAmb.setText("######");
            gBateria.setValue(0.0d);
            timeline.setCycleCount(Animation.INDEFINITE);
            timeline.play();
        }
        influxDbHelper.closeClient();
    }

    public Dispositiu getDispositiu() {
        return dispositiu;
    }

    public void setDispositiu(Dispositiu dispositiu) {
        this.dispositiu = dispositiu;
    }

    public boolean isNewRecord() {
        return newRecord;
    }
}
