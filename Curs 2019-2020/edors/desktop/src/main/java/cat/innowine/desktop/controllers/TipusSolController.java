package cat.innowine.desktop.controllers;

import cat.innowine.desktop.classes.DAOHelper;
import cat.innowine.desktop.model.TipusSol;
import cat.innowine.desktop.utils.AlertHelper;
import cat.innowine.desktop.utils.MissatgesApp;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class TipusSolController extends DefaultControllerDiferit implements Initializable {

    @FXML private TableView<TipusSol> tvTipusSol;
    @FXML private TableColumn<TipusSol, Integer> colCodi;
    @FXML private TableColumn<TipusSol, String> colDescripcio;
    @FXML private TextField tfCodi;
    @FXML private TextField tfDescripcio;

    @FXML private Button btnNou, btnGuardar, btnEliminar, btnCancelar;

    private static final int FIRST = 0;

    private DAOHelper<TipusSol> helper;

    private boolean mode_insercio = false;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        configuraColumnes();
        tvTipusSol.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends TipusSol> observable, TipusSol oldValue, TipusSol newValue) -> {
            if (newValue != null) {
                tfCodi.setText(String.valueOf(newValue.getId()));
                tfDescripcio.setText(newValue.getDescripcio());
            }
        });

        tvTipusSol.requestFocus();
    }

    @Override
    public void iniciDiferit() {
        helper = new DAOHelper<TipusSol>(this.getEmf(), TipusSol.class);
        refrescaTaula(FIRST);
        if (tvTipusSol.getItems().isEmpty()) {
            btnNou.setDisable(false);
            btnGuardar.setDisable(true);
            btnEliminar.setDisable(true);
            btnCancelar.setDisable(true);
        } else {
            btnNou.setDisable(false);
            btnGuardar.setDisable(false);
            btnEliminar.setDisable(false);
            btnCancelar.setDisable(true);
        }
    }

    private void configuraColumnes() {
        colCodi.setCellValueFactory(new PropertyValueFactory<>("id"));
        colDescripcio.setCellValueFactory(new PropertyValueFactory<>("descripcio"));
    }

    private void refrescaTaula() {
        tvTipusSol.getItems().removeAll();
        tvTipusSol.getItems().setAll(helper.getAll());
        if (tvTipusSol.getItems().isEmpty()) {
            tfCodi.clear();
            tfDescripcio.clear();
            btnEliminar.setDisable(true);
            btnGuardar.setDisable(true);
        }
    }

    private void refrescaTaula(int index) {
        refrescaTaula();
        tvTipusSol.requestFocus();
        tvTipusSol.getSelectionModel().select(index);
        tvTipusSol.getFocusModel().focus(index);
    }

    private void refrescaTaula(boolean last) {
        refrescaTaula();
        tvTipusSol.getSelectionModel().selectLast();
    }

    @FXML
    private void btnNouOnAction (ActionEvent event) {
        inserir();
    }

    @FXML
    private void btnCancelarOnAction (ActionEvent event) {
        cancelar();
    }

    @FXML
    private void btnEliminarOnAction (ActionEvent event) {
        eliminar();
    }

    @FXML
    private void btnGuardarOnAction (ActionEvent event) {
        guardar();
    }

    public void inserir() {
        tfCodi.clear();
        tfDescripcio.clear();
        tfDescripcio.requestFocus();
        btnNou.setDisable(true);
        btnGuardar.setDisable(false);
        btnEliminar.setDisable(true);
        btnCancelar.setDisable(false);
        mode_insercio = true;
    }

    public void cancelar() {
        if (!tvTipusSol.getItems().isEmpty()) {
            TipusSol item = tvTipusSol.getSelectionModel().getSelectedItem();
            tfCodi.setText(String.valueOf(item.getId()));
            tfDescripcio.setText(item.getDescripcio());
            btnNou.setDisable(false);
            btnGuardar.setDisable(false);
            btnEliminar.setDisable(false);
        } else {
            tfCodi.clear();
            tfDescripcio.clear();
            btnNou.setDisable(false);
            btnGuardar.setDisable(true);
            btnEliminar.setDisable(true);
        }
        btnCancelar.setDisable(true);
        mode_insercio = false;
    }

    public void eliminar () {
        if (!tvTipusSol.getItems().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(MissatgesApp.INF_QUESTION);
            alert.setContentText(MissatgesApp.WRN_DELETE_ITEM);

            ButtonType btnSi = new ButtonType(MissatgesApp.OPT_YES);
            ButtonType btnNo = new ButtonType(MissatgesApp.OPT_NO);
            alert.getButtonTypes().setAll(btnSi, btnNo);

            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == btnSi) {
                TipusSol t = tvTipusSol.getSelectionModel().getSelectedItem();
                if (t != null) {
                    int indexActual = tvTipusSol.getItems().indexOf(t);
                    int nouIndex = indexActual - 1;
                    if (indexActual == FIRST)
                        nouIndex = FIRST;
                    helper.delete(t.getId());
                    refrescaTaula(nouIndex);
                }
            }
        }
    }

    public void guardar () {
        if (!tfDescripcio.getText().isEmpty()) {
            boolean last = false;
            boolean duplicat = true;
            TipusSol itemDuplicat = null;
            int index = tvTipusSol.getSelectionModel().getSelectedIndex();

            if (mode_insercio) {
                TipusSol t = new TipusSol();
                t.setDescripcio(tfDescripcio.getText());
                helper.insert(t);
                mode_insercio = false;
                last = true;
            } else {
                TipusSol t = tvTipusSol.getSelectionModel().getSelectedItem();
                index = tvTipusSol.getSelectionModel().getSelectedIndex();
                t.setDescripcio(tfDescripcio.getText());
                helper.update(t);
            }

            if (last)
                refrescaTaula(last);
            else
                refrescaTaula(index);

            btnNou.setDisable(false);
            btnGuardar.setDisable(false);
            btnEliminar.setDisable(false);
            btnCancelar.setDisable(true);

        } else {
            AlertHelper.alertaWarning("El camp <nom> és obligatori");
        }
    }

}
