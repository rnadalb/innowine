package cat.innowine.desktop.controllers;

import cat.innowine.desktop.interfaces.IniciDiferit;
import cat.innowine.desktop.model.Usuari;

public abstract class DefaultControllerDiferit extends DefaultController implements IniciDiferit {
    private Usuari usuari;

    public Usuari getUsuari() {
        return usuari;
    }

    public void setUsuari(Usuari usuari) {
        this.usuari = usuari;
    }
}
