package cat.innowine.desktop.controllers;

import cat.innowine.desktop.classes.DAOHelper;
import cat.innowine.desktop.classes.Mode;
import cat.innowine.desktop.model.Usuari;
import cat.innowine.desktop.utils.AlertHelper;
import cat.innowine.desktop.utils.CipherHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CanviaPasswordController extends DefaultControllerDiferit {
    @FXML private VBox vbCanviaPassword;
    @FXML private AnchorPane apUsuari, apAdmin;
    @FXML private PasswordField pfOldPassword, pfNewPassword, pfConfirmationPassword, pfAdminPassword;

    private Mode mode;
    private String newPassword;

    @Override
    public void iniciDiferit() {
        switch (mode){
            case USER_MODE: vbCanviaPassword.getChildren().remove(apAdmin); break;
            case ADMIN_MODE: vbCanviaPassword.getChildren().remove(apUsuari); break;
            case ONLY_VALUE_MODE: vbCanviaPassword.getChildren().remove(apUsuari); break;
        }
    }

    @FXML
    public void pfOnKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            if (!this.getUsuari().isAdmin()) {
                if (keyEvent.getSource().equals(pfOldPassword) & !pfOldPassword.getText().isEmpty()) {
                    pfNewPassword.requestFocus();
                    pfNewPassword.selectAll();
                } else if (keyEvent.getSource().equals(pfNewPassword) & !pfNewPassword.getText().isEmpty()) {
                    pfConfirmationPassword.requestFocus();
                    pfConfirmationPassword.selectAll();
                } else if (keyEvent.getSource().equals(pfConfirmationPassword) & !pfConfirmationPassword.getText().isEmpty()) {
                    guardaCanvis(mode);
                }
            } else if (!pfAdminPassword.getText().isEmpty())
                guardaCanvis(mode);
        }
    }

    @FXML
    public void btAcceptarOnAction(ActionEvent actionEvent) {
        guardaCanvis(mode);
    }

    private void guardaCanvis(Mode mode) {
        TextField t = null;

        switch (mode) {
            case ADMIN_MODE:  t = pfAdminPassword;
                break;
            case USER_MODE: t = this.getUsuari().isAdmin() ? pfAdminPassword : pfConfirmationPassword;
                break;
            case ONLY_VALUE_MODE: t = pfAdminPassword;
                break;
        }

        if (!t.getText().isEmpty()) {
            getUsuari().setPassword(CipherHelper.encryptPassword(t.getText()));
            //getUsuari().setPassword(SecurityHelper.getInstancia().passwordEncrypt(t.getText()));
            if (mode != Mode.ONLY_VALUE_MODE) {
                DAOHelper<Usuari> helper = new DAOHelper<>(this.getEmf(), Usuari.class);
                helper.update(this.getUsuari());
                ((Stage) (vbCanviaPassword.getScene().getWindow())).close();
            } else {
                ((Stage) (vbCanviaPassword.getScene().getWindow())).close();
            }
        } else {
            AlertHelper.alertInfo("Ha d'introduir una contrasenya obligatòriament.");
            t.requestFocus();
        }
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}