package cat.innowine.desktop.exceptions;

public class PropertiesHelperException extends Exception {
    public PropertiesHelperException(String message, Throwable cause) {
        super(message, cause);
    }
}
