package cat.innowine.desktop.controllers;

import cat.innowine.desktop.classes.Constants;
import cat.innowine.desktop.classes.DAOHelper;
import cat.innowine.desktop.classes.Mode;
import cat.innowine.desktop.model.Usuari;
import cat.innowine.desktop.utils.AlertHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class AltaUsuariController extends DefaultControllerDiferit implements Initializable {

    @FXML
    private TextField tfCerca;

    @FXML
    private TableView<Usuari> tvUsuaris;

    @FXML
    private TableColumn<Usuari, String> colLogin;

    @FXML
    private TableColumn<Usuari, String>  colNom;

    @FXML
    private TableColumn<Usuari, String> colCognoms;

    @FXML
    private TableColumn<Usuari, String> colEmail;

    @FXML
    private TableColumn<Usuari, Boolean> colBloquejat;

    private DAOHelper<Usuari> helper;
    private ObservableList<Usuari> llistaUsuaris;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ContextMenu cm = new ContextMenu();
        MenuItem mi1 = new MenuItem("Modifica");
        MenuItem mi2 = new MenuItem("Canvia contrassenya");
        MenuItem mi3 = new SeparatorMenuItem();
        MenuItem mi4 = new MenuItem("Elimina");

        cm.getItems().addAll(mi1, mi2, mi3, mi4);
        cm.setOnAction(event -> {
            if (event.getTarget().equals(mi1))
                newRecord(tvUsuaris.getSelectionModel().getSelectedItem());
            else if (event.getTarget().equals(mi2))
                if (this.getUsuari().isAdmin())
                    changePassword(tvUsuaris.getSelectionModel().getSelectedItem(), Mode.ADMIN_MODE);
                else
                    changePassword(tvUsuaris.getSelectionModel().getSelectedItem(), Mode.USER_MODE);
            else if (event.getTarget().equals(mi4)) {
                // L'usuari admin no pot ser eliminat per tant no és possible que quede buida la taula
                deleteActions();
            }
        });

        tvUsuaris.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getButton() == MouseButton.SECONDARY) {
                cm.show(tvUsuaris, event.getScreenX(), event.getScreenY());
            }
        });

        // doble click a la taula
        tvUsuaris.setRowFactory( tv -> {
            TableRow<Usuari> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == Constants.DOUBLE_CLICK && (! row.isEmpty())) {
                    // Al fer dos clicks dona el camp Bloquejat (swap)
                    Usuari u = tvUsuaris.getSelectionModel().getSelectedItem();
                    if (!u.isAdmin()) {
                        u.setBloquejat(!u.isBloquejat()); //Obté el valor booleà contrari.
                        helper.update(u);
                        tvUsuaris.refresh();
                    }
                }
            });
            return row ;
        });
        configureColumns();
    }

    @Override
    public void iniciDiferit() {
        helper = new DAOHelper<>(this.getEmf(), Usuari.class);
        refreshTable(true);
        gotoTableItem(Constants.FIRST_ITEM);
    }

    private void deleteActions() {
        int index = tvUsuaris.getSelectionModel().getSelectedIndex();
        deleteRecord(tvUsuaris.getSelectionModel().getSelectedItem());
        refreshTable(false);
        gotoTableItem(index - 1);
    }

    private void deleteRecord(Usuari selectedItem) {
        if (!selectedItem.isAdmin()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(Constants.APPNAME);
            alert.setHeaderText("Estàs segur d'eliminar a l'usuari <" + selectedItem.getNomComplet() + ">");
            alert.setContentText("Tria una opció");
            ButtonType btSi = new ButtonType("Si");
            ButtonType btNo = new ButtonType("No");

            alert.getButtonTypes().setAll(btSi, btNo);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == btSi) {
                DAOHelper<Usuari> helper = new DAOHelper<>(this.getEmf(), Usuari.class);
                helper.delete(selectedItem.getId());
                llistaUsuaris.remove(selectedItem);
            }
        } else {
            AlertHelper.alertInfo("L'usuari <administrador> no pot ser eliminat!!!");
        }
    }

    private void changePassword(Usuari usuari, Mode mode) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CanviaPassword.fxml"));
        Parent root = null;
        int indexActual = tvUsuaris.getSelectionModel().getSelectedIndex();
        try {
            root = loader.load();
            CanviaPasswordController controller = loader.getController();
            controller.setEmf(this.getEmf());
            controller.setUsuari(usuari);
            controller.setMode(mode);
            controller.iniciDiferit();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.initStyle(StageStyle.DECORATED);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.setOnHidden(event -> {
                Usuari u = controller.getUsuari();
                llistaUsuaris.set(indexActual, u);
                refreshTable(false);
                gotoTableItem(indexActual);
            });
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void newRecord(Usuari usuari) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/DadesUsuari.fxml"));
        Parent root = null;
        int indexActual = tvUsuaris.getSelectionModel().getSelectedIndex();
        try {
            root = loader.load();
            DadesUsuariController controller = loader.getController();
            controller.setEmf(this.getEmf());
            controller.setUsuari(usuari);
            controller.setNewRecord(usuari == null);
            controller.iniciDiferit();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.initStyle(StageStyle.DECORATED);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.setOnHidden(event -> {
                Usuari u = null;
                if (controller.isNewRecord()) {
                    u = controller.getNewUser();
                    llistaUsuaris.add(u);
                } else {
                    u = controller.getUsuari();
                    llistaUsuaris.set(indexActual, u);
                }
                refreshTable(false);
                gotoTableItem(indexActual);
            });
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void configureColumns() {
        // Alerta que els valors --------------------------------<>
        // Identifiquen el noms de les VARIABLES de la classe
        colLogin.setCellValueFactory(new PropertyValueFactory<>("login"));
        colNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        colCognoms.setCellValueFactory(new PropertyValueFactory<>("cognoms"));
        colEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        colBloquejat.setCellValueFactory(new PropertyValueFactory<>("bloquejat"));
        colBloquejat.setCellFactory(column -> new CheckBoxTableCell<>());
        colBloquejat.setCellValueFactory(cellData -> cellData.getValue().getBloquejatProperty());
    }

    private void refreshTable(boolean update) {
        if (update) {
            if (llistaUsuaris != null)
                llistaUsuaris.clear();
            llistaUsuaris = FXCollections.observableArrayList(helper.getAll());
            tvUsuaris.getItems().clear();
            tvUsuaris.setItems(llistaUsuaris);
        } else
            tvUsuaris.refresh();
        filterTable();
    }

    /**
     * Adaptació del filtratge per a la taula de Productes, pero en BBDD. És identic !!!
     *
     * Vigila el filtres que apliques, sobre tot el tipus (String, Integer, Float, ...)
     *
     */
    private void filterTable() {
        if (!tvUsuaris.getItems().isEmpty())
            tvUsuaris.getItems().removeAll();

        // 1.  Ajustar el ObservableList en una llista filtrada (inicialment mostra totes les dades).
        FilteredList<Usuari> filteredData = new FilteredList<>(tvUsuaris.getItems(), p -> true);

        // 2. Estableix el Predicat del filtre cada vegada que camvia en filtre
        tfCerca.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(usuari -> {
                // Si el text del filtre està buit (null o empty), mostrar-ho tot
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (usuari.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true; // El filtre coincideix amb el nom del producte
                } else return String.valueOf(usuari.getLogin()).indexOf(lowerCaseFilter) != -1; // El filtre conincideix amb el codi del producte
// No hi ha coincidència
            });
        });

        // 3. Embolica el FilteredList en una SortedList.
        SortedList<Usuari> sortedData = new SortedList<>(filteredData);

        // 4. Lliga el comparador de la SortedList cal del TableView
        // 	  En qualsevol altre cas, l'ordenació de la TableView no tindrà efecte
        sortedData.comparatorProperty().bind(tvUsuaris.comparatorProperty());

        // 5. Afegeix la informació ordenada (i filtrada) a la taula
        tvUsuaris.setItems(sortedData);
    }

    private void gotoTableItem(int row) {
        tvUsuaris.requestFocus();
        tvUsuaris.scrollTo(row);
        tvUsuaris.getSelectionModel().select(row);
        tvUsuaris.getFocusModel().focus(row);
    }

    @FXML
    public void btNewOnAction(ActionEvent actionEvent) {
        // Si l'usuari és null, és un nou registre
        newRecord(null);
    }

    @FXML
    public void btUpdateOnAction(ActionEvent actionEvent) {
        newRecord(tvUsuaris.getSelectionModel().getSelectedItem());
    }

    @FXML
    public void btDeleteOnAction(ActionEvent actionEvent) {
        deleteActions();
    }
}