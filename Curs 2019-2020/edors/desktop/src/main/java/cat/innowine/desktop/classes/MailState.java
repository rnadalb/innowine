package cat.innowine.desktop.classes;

public enum MailState {
    MAIL_SENDED(0, "Correu enviat correctament"),
    ERROR_GENERAL (-1, "Error general en l'enviament de correu electrònic\nAvisi a l'administrador/a"),
    ERR_AUTH (-2, "Error d'autenticació. Usuari i/o contrasenya no vàlids."),
    ERR_SENDING_MAIL (-3, "Error en l'enviament de correu electrònic.");

    private int errorCode;
    private String message;

    MailState(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}