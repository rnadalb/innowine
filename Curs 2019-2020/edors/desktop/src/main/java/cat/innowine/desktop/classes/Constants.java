package cat.innowine.desktop.classes;

public class Constants {
    public static final int MAX_INTENTS = 3;
    public static final String APPNAME = "iWine desktop App";
    public static final String ENCRYPTOR_PASSWORD = "CoNtRaSeNyA_dE_xIfRaTgE";
    public static final int FIRST_ITEM = 0;
    public static final int DOUBLE_CLICK = 2;
    public static final String HEADER_PREVIEW = "Vista prèvia";
    public static final String ERR_NO_CONNECTION = "No és possible connectar amb el servidor de la BBDD";
    public static final String SI = "Sí";
    public static final String NO = "No";
    public static final String MSG_CONFIRMAR = "Confirmar acció";
    public static final String EMAIL_TEST_SUBJECT = "Assumpte de prova";
    public static final String EMAIL_TEST_BODY = "Missatge de prova";
    public static final Integer NEW_RECORD = -1;
}
