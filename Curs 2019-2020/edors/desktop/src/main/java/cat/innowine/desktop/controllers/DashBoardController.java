package cat.innowine.desktop.controllers;

import cat.innowine.desktop.Main;
import cat.innowine.desktop.classes.Constants;
import cat.innowine.desktop.model.Usuari;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DashBoardController extends DefaultControllerDiferit implements Initializable {

    @FXML
    private Label lbUser;

    @FXML
    private ImageView ivLogo, ivUsuaris;

    @FXML
    private AnchorPane scenePane;

    @FXML
    private VBox vbMenuButtons;

    @FXML
    private Button btDispositius, btUsuaris;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem miSurt = new MenuItem("Sobre ...");
        contextMenu.getItems().setAll(miSurt);
        ivLogo.setOnContextMenuRequested(event -> {
            contextMenu.show(ivLogo, event.getScreenX(), event.getScreenY());
        });

        miSurt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Sobre.fxml"));
                    Parent root = loader.load();
                    Stage stage = new Stage();
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.initStyle(StageStyle.UNDECORATED);
                    stage.setScene(new Scene(root));
                    stage.show();
                } catch (IOException ex) {
                    Logger.getLogger(DashBoardController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    @Override
    public void iniciDiferit() {
        btUsuaris.setVisible(this.getUsuari().isAdmin());

        if (this.getUsuari().isAdmin()) {
            lbUser.setText(this.getUsuari().getNomComplet() + " is admin");
        } else {
            //FIXME: Revisar i modificar perquè ho faci independenment de la posició
            vbMenuButtons.getChildren().remove(1);
            lbUser.setText(this.getUsuari().getNomComplet());
        }

        // Per defecte, la primera pantalla són els dispositius d'un usuari
        nestForm(this.getUsuari(), "/view/AltaDispositiu.fxml", btDispositius);
    }

    @FXML
    public void btSurtOnAction(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(Constants.APPNAME);
        alert.setContentText("Segur que vol sortir de l'aplicació?");
        ButtonType btSi = new ButtonType("Si");
        ButtonType btNo = new ButtonType("No");
        alert.getButtonTypes().setAll(btSi, btNo);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == btSi) {
            Platform.exit();
        }
    }

    @FXML
    public void btSistemaOnAction(ActionEvent actionEvent) {
        nestForm(this.getUsuari(), "/view/DadesUsuari.fxml", (Button) actionEvent.getSource());
    }

    @FXML
    public void btUsuarisOnAction(ActionEvent actionEvent) {
        nestForm(this.getUsuari(), "/view/AltaUsuari.fxml", (Button) actionEvent.getSource());
    }

    @FXML
    public void btDispositiusOnAction(ActionEvent actionEvent) {
        nestForm(this.getUsuari(), "/view/AltaDispositiu.fxml", (Button) actionEvent.getSource());
    }

    @FXML
    public void btTipusSolOnAction(ActionEvent actionEvent) {
        nestForm(this.getUsuari(), "/view/TipusSol.fxml", (Button) actionEvent.getSource());
    }

    private void nestForm(Usuari usuari, String formPath, Button button) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(formPath));
        Parent root = null;

        try {
            root = loader.load();
            DefaultControllerDiferit controller = loader.getController();
            controller.setEmf(this.getEmf());
            controller.setUsuari(usuari);
            controller.iniciDiferit();
            scenePane.getChildren().clear();
            scenePane.getChildren().add(root);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }

        updateButtonsStyle(button);
    }

    private void openForm(Usuari usuari, String formPath, boolean onCloseRequest, boolean modal) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(formPath));
        Parent root = null;

        try {
            root = loader.load();
            DefaultControllerDiferit controller = loader.getController();
            controller.setEmf(this.getEmf());
            controller.setUsuari(usuari);
            controller.iniciDiferit();
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.initStyle(StageStyle.DECORATED);
            stage.setScene(scene);
            if (modal)
                stage.initModality(Modality.APPLICATION_MODAL);
            if (onCloseRequest)
                stage.setOnCloseRequest(event -> event.consume());
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }

    private void updateButtonsStyle(Button button) {
        for (Node n : vbMenuButtons.getChildren()) {
            if (n instanceof Button) {
                n.getStyleClass().remove("menu_button_clicked");
            }
        }
        button.getStyleClass().add("menu_button_clicked");
    }
}
