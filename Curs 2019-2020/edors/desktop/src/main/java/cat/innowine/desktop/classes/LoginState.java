package cat.innowine.desktop.classes;

public enum LoginState {
    GRANTED(0, "Accés garantit !!!"),
    FAILED(-1,  "Accés erroni"),
    LOCKED(1,  "Compte bloquejat"),
    NOEXISTS(2,  "Usuari inexistent"),;

    private int errorCode;
    private String message;

    LoginState(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
