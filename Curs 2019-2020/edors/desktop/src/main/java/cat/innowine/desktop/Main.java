package cat.innowine.desktop;

import cat.innowine.desktop.classes.*;
import cat.innowine.desktop.controllers.DefaultControllerDiferit;
import cat.innowine.desktop.controllers.LoginController;
import cat.innowine.desktop.exceptions.PropertiesHelperException;
import cat.innowine.desktop.model.Config;
import cat.innowine.desktop.model.Usuari;
import cat.innowine.desktop.utils.AlertHelper;
import cat.innowine.desktop.utils.SecurityHelper;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.commons.configuration.ConfigurationException;
import org.hibernate.exception.JDBCConnectionException;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.sql.SQLInvalidAuthorizationSpecException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application {

    private EntityManagerFactory emf;
    private static final String APP_PROPERTIES = "app.properties";
    private static final String ENC_WORD = "password";
    private static final String ENC_KEY = "enc";
    //DELETE <19/03/2020> [RNB]: Cal eliminar-ho abans de passar-ho a producció
    private static final boolean byPass = false;
    private static final String BYPASS_USER = "emusk";
    private boolean initMain = true;

    @Override
    public void init() throws Exception {
        super.init();
        String passwd = "";
        try {
            passwd = getDecryptedPropsFilePassword(APP_PROPERTIES, ENC_WORD, ENC_KEY);
        } catch (PropertiesHelperException e) {
            System.err.println("Error en el procés d'encriptació");
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }

        HashMap<String, String> properties = new HashMap<>();
        properties.put("javax.persistence.jdbc.password", passwd);

        try {
            emf = Persistence.createEntityManagerFactory( "mariaDB", properties );
        } catch (Exception e) {
            initMain = false;
            if (e.getCause() instanceof JDBCConnectionException) {
                if (e.getCause().getCause() instanceof SQLInvalidAuthorizationSpecException)
                    System.out.println("Usuari/Contrasenya de la BBDD erronis");
                //Utils.logErrGreu(Constants.ERR_NO_CONNECTION, this.getClass().getName());
                System.out.println("No hi ha connexió a Internet");
            }
            e.printStackTrace();
        }

        initSecurityManager();

        if (emf != null) {
            //InfluxDbProperties influxDbProperties = InfluxDbPropertiesHelper.readPropertiesFile("influxdb.properties");
            updateSingletonConfig();
            //updateSingletonInflux();

        } else {
            //AlertHelper.alertaError("No s'ha pogt connectar a la BBDD");
            Platform.exit();
        }
    }

    private void initSecurityManager() {
        // Simplement inicialitza el motor criptogràfic
        SecurityHelper.getInstancia();
    }

    private void updateSingletonConfig() {
        DAOHelper<Config> helper = new DAOHelper<>(emf, Config.class);
        Config c = helper.getFirst();
        if (c != null) {
            Config.getInstancia().setId(c.getId());
            Config.getInstancia().setMaxIntents(c.getMaxIntents());
            Config.getInstancia().setMinCaracters(c.getMinCaracters());
            Config.getInstancia().setEmailSubject(c.getEmailSubject());
            Config.getInstancia().setEmailBody(c.getEmailBody());
            Config.getInstancia().setMapProjection(c.getMapProjection());
            Config.getInstancia().setEmailSmtp(c.getEmailSmtp());
            Config.getInstancia().setEmailPort(c.getEmailPort());
            Config.getInstancia().setEmailUser(c.getEmailUser());
            Config.getInstancia().setEmailPasswd(c.getEmailPasswd());
            Config.getInstancia().setEmailTls(c.isEmailTls());
            Config.getInstancia().setEmailAuth(c.isEmailAuth());
            Config.getInstancia().setInfluxdbHost(c.getInfluxdbHost());
            Config.getInstancia().setInfluxdbPort(c.getInfluxdbPort());
            Config.getInstancia().setInfluxdbDatabase(c.getInfluxdbDatabase());
            Config.getInstancia().setInfluxdbUser(c.getInfluxdbUser());
            Config.getInstancia().setInfluxdbPassword(c.getInfluxdbPassword());
            Config.getInstancia().setInfluxdbSSL(c.isInfluxdbSSL());
        } else {
            Config.getInstancia().setMaxIntents(3);
            Config.getInstancia().setMinCaracters(6);
            Config.getInstancia().setEmailSubject("Assumpte per defecte");
            Config.getInstancia().setEmailBody("Cos per defecte");
            Config.getInstancia().setMapProjection(MapProjection.MERCATOR.getProjectionCode());
            Config.getInstancia().setEmailSmtp("smtp.gmail.com");
            Config.getInstancia().setEmailPort(587);
            Config.getInstancia().setEmailUser("rnadalb@gmail.com");
            Config.getInstancia().setEmailPasswd(SecurityHelper.getInstancia().encrypt("mnlM028@2ms.sH"));
            Config.getInstancia().setEmailTls(true);
            Config.getInstancia().setEmailAuth(true);
            Config.getInstancia().setInfluxdbHost("iwine.ddns.net");
            Config.getInstancia().setInfluxdbPort(8086);
            Config.getInstancia().setInfluxdbDatabase("iwinedb");
            Config.getInstancia().setInfluxdbUser("iwine");
            Config.getInstancia().setInfluxdbPassword(SecurityHelper.getInstancia().encrypt("CuPoFwInE2020"));
            Config.getInstancia().setInfluxdbSSL(true);
            helper.insert(Config.getInstancia());
        }
    }
/*
    private void updateSingletonInflux() {
        DAOHelper<InfluxConfig> helper = new DAOHelper<>(emf, InfluxConfig.class);
        InfluxConfig c = helper.getFirst();
        if (c != null) {
            InfluxConfig.getInstancia().setId(c.getId());
            InfluxConfig.getInstancia().setHost(c.getHost());
            InfluxConfig.getInstancia().setPort(c.getPort());
            InfluxConfig.getInstancia().setUser(c.getUser());
            InfluxConfig.getInstancia().setPassword(c.getPassword());
            InfluxConfig.getInstancia().setSsl(c.isSsl());
            InfluxConfig.getInstancia().setDatabase(c.getDatabase());
        } else {
            InfluxConfig.getInstancia().setHost("iwine.ddns.net");
            InfluxConfig.getInstancia().setPort(8086);
            InfluxConfig.getInstancia().setDatabase("iwinedb");
            InfluxConfig.getInstancia().setUser("iwine");
            InfluxConfig.getInstancia().setPassword("CuPoFwInE2020");
            InfluxConfig.getInstancia().setSsl(true);
            helper.insert(InfluxConfig.getInstancia());
        }
    }
*/
    @Override
    public void stop() throws Exception {
        if (emf != null && emf.isOpen())
            emf.close();

        super.stop();
    }

    @Override
    public void start(Stage stage) {
        if (initMain) {
            //DELETE <19/03/2020> [RNB]: Eliminar el byPass abans de producció
            if (!byPass) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Login.fxml"));
                Parent root = null;
                try {
                    root = loader.load();
                    LoginController controller = loader.getController();
                    controller.setEmf(emf);

                    Scene scene = new Scene(root);
                    stage.initStyle(StageStyle.UNDECORATED);
                    stage.setScene(scene);
                    stage.show();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    Platform.exit();
                }
            } else {
                // REVIEW. RNB Canvis a la GUI
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/DashBoard.fxml"));
                //FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Main.fxml"));
                Parent root = null;

                try {
                    UsuariHelper<Usuari> usuariHelper = new UsuariHelper<>(emf, Usuari.class);
                    Usuari usuari = usuariHelper.getUsuariByLogin(BYPASS_USER);
                    root = loader.load();
                    DefaultControllerDiferit controller = loader.getController();
                    controller.setEmf(emf);
                    controller.setUsuari(usuari);
                    controller.iniciDiferit();
                    Scene scene = new Scene(root);
                    stage.initStyle(StageStyle.DECORATED);
                    stage.setScene(scene);
                    stage.show();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    Platform.exit();
                }
            }
        } else {
            AlertHelper.alertaError(Constants.ERR_NO_CONNECTION);
            Platform.exit();
        }
    }

    private static String getDecryptedPropsFilePassword(String fitxer, String clauAEncriptar, String clauTestEncriptacio) throws PropertiesHelperException, ConfigurationException {
        PropertiesFile helper = new PropertiesFile(fitxer, clauAEncriptar, clauTestEncriptacio);
        return helper.getDecryptedUserPassword();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}