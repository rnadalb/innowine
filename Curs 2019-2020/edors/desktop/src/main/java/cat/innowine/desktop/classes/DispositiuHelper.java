package cat.innowine.desktop.classes;


import cat.innowine.desktop.model.Usuari;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class DispositiuHelper<Dispositiu> extends DAOHelper<Dispositiu> {
    public DispositiuHelper(EntityManagerFactory emf, Class<Dispositiu> parameterClass) {
        super(emf, parameterClass);
    }
    public List<Dispositiu> getDevices(Usuari usuari) {
        try {
            EntityManager em = this.emf.createEntityManager();
            CriteriaBuilder cb = em.getCriteriaBuilder();

            CriteriaQuery<Dispositiu> cbQuery = cb.createQuery(parameterClass);
            Root<Dispositiu> c = cbQuery.from(parameterClass);
            cbQuery.select(c).where(cb.equal(c.get("usuari"), usuari));

            Query query = em.createQuery(cbQuery);
            return query.getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public void delete(Integer idDispositiu, Integer idUsuari) {
        EntityManager em = this.emf.createEntityManager();
        Query query = em.createNativeQuery("delete from tbl_dispositiu where id = :id and id_usuari = :id_usuari", parameterClass);
        query.setParameter("id", idDispositiu);
        query.setParameter("id_usuari", idUsuari);
        em.getTransaction().begin();
        query.executeUpdate();
        em.getTransaction().commit();
    }
}