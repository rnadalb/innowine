package cat.innowine.desktop.utils;

public class MissatgesApp {
    public final static String ERR_DEFAULT_ITEM_DELETED = "L'element està marcat com 'per defecte'\n No és possible eliminar aquest element.";
    public final static String WRN_DELETE_ITEM = "Està segur d'eliminar l'element?";
    public final static String INF_QUESTION = "Pregunta";
    public final static String OPT_YES = "Si";
    public final static String OPT_NO = "NO";
}
