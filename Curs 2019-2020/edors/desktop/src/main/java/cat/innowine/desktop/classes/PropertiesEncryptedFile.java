package cat.innowine.desktop.classes;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public abstract class PropertiesEncryptedFile {
    protected static String decryptPropsFilePassword (String fileName, String clauAEncriptar) throws ConfigurationException {
        PropertiesConfiguration config = new PropertiesConfiguration(fileName);
        String encryptedPropertyValue = config.getString(clauAEncriptar);
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(Constants.ENCRYPTOR_PASSWORD);
        return encryptor.decrypt(encryptedPropertyValue);
    }
}
