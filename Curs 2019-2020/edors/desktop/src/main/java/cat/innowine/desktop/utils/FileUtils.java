package cat.innowine.desktop.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileUtils {
    /**
     * Retorna el contigut d'un fitxer de text en un String separat per \n
     *
     * @param filePath Ruta al fitxer de text
     * @return Contigut d'un fitxer de text en un StringBuilder separat per \n
     */
    public static StringBuilder readFile(String filePath) {
        StringBuilder sb = new StringBuilder();
        try {
            FileInputStream fileIn = new FileInputStream(filePath);
            InputStreamReader inputReader = new InputStreamReader(fileIn);
            BufferedReader reader = new BufferedReader(inputReader);
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb;
    }
}