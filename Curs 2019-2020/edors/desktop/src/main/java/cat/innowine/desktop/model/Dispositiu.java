package cat.innowine.desktop.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table (name = "tbl_dispositiu")
public class Dispositiu implements Serializable {

  private static final long serialVersionUID = 2891765770844231638L;

  @Id
  @Column(name = "id")
  @GeneratedValue (strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "id_dispositiu")
  @GeneratedValue (strategy = GenerationType.IDENTITY)
  private Integer idDispositiu;

  @Column(name = "descripcio", length = 100, nullable = false)
  private String descripcio;

  @Column(name = "latitud")
  private Double latitud;

  @Column(name = "longitud")
  private Double longitud;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="id_usuari", nullable = false)
  private Usuari usuari;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="id_tipus_sol", nullable = false)
  private TipusSol tipusSol;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getIdDispositiu() {
    return idDispositiu;
  }

  public void setIdDispositiu(Integer idDispositiu) {
    this.idDispositiu = idDispositiu;
  }

  public String getDescripcio() {
    return descripcio;
  }

  public void setDescripcio(String descripcio) {
    this.descripcio = descripcio;
  }

  public Double getLatitud() {
    return latitud;
  }

  public void setLatitud(Double latitud) {
    this.latitud = latitud;
  }

  public Double getLongitud() {
    return longitud;
  }

  public void setLongitud(Double longitud) {
    this.longitud = longitud;
  }

  public Usuari getUsuari() {
    return usuari;
  }

  public void setUsuari(Usuari usuari) {
    this.usuari = usuari;
  }

  public TipusSol getTipusSol() {
    return tipusSol;
  }

  public void setTipusSol(TipusSol tipusSol) {
    this.tipusSol = tipusSol;
  }
}
