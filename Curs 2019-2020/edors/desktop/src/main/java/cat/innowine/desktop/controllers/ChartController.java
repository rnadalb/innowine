package cat.innowine.desktop.controllers;

import cat.innowine.desktop.classes.InfluxDbHelper;
import cat.innowine.desktop.model.Config;
import cat.innowine.desktop.model.Dispositiu;
import cat.innowine.desktop.model.DummyData;
import cat.innowine.desktop.utils.DateUtils;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.influxdb.dto.BoundParameterQuery;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class ChartController extends DefaultControllerDiferit implements Initializable {
    @FXML
    AnchorPane apTempChart, apHumiChart;

    @FXML
    VBox vbFormulari;

    @FXML
    DatePicker dpDesde, dpFinsA;

    @FXML
    CheckBox chkTemperatura, chkHumitat, chkMostrarPunts;

    @FXML
    CheckBox chkT1, chkT2, chkT3, chkH1, chkH2, chkH3;

    @FXML
    Slider sRegistres;

    @FXML
    Label lbNRegistres;

    private Dispositiu dispositiu;
    private static final long DAYS_DIFFERENCE = 1;
    private static final int DEFAULT_MAX_ROWS = 250;
    private static final String ORDER_BY = "ORDER by time ASC";

    private final int WINDOW_SIZE = 10;
    private final CategoryAxis xAxisTempChart = new CategoryAxis();
    private final CategoryAxis xAxisHumiChart = new CategoryAxis();
    private final NumberAxis yAxisTempChart = new NumberAxis("Temperatura", -10.00f, 50.00f, 0.1f);
    private final NumberAxis yAxisHumiChart = new NumberAxis("Humitat", 1750.000f, 1900.00f, 1.00f);
    private final XYChart.Series<String, Number> serieHumiS1 = new XYChart.Series<>();
    private final XYChart.Series<String, Number> serieHumiS2 = new XYChart.Series<>();
    private final XYChart.Series<String, Number> serieHumiS3 = new XYChart.Series<>();
    private final XYChart.Series<String, Number> serieTempS1 = new XYChart.Series<>();
    private final XYChart.Series<String, Number> serieTempS2 = new XYChart.Series<>();
    private final XYChart.Series<String, Number> serieTempS3 = new XYChart.Series<>();

    List<XYChart.Series<String, Number>> tempSeries = new ArrayList<>();
    List<XYChart.Series<String, Number>> humiSeries = new ArrayList<>();

    private AreaChart<String, Number> tempChart;
    private AreaChart<String, Number> humiChart;

    private static InfluxDbHelper influxDbHelper;// = new InfluxDbHelper(InfluxConfig.getInstancia());

    //private InfluxDB influxDB = InfluxDbHelper.createClient();

    @Override
    public void iniciDiferit() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sRegistres.setValue(DEFAULT_MAX_ROWS);
        lbNRegistres.setText(String.valueOf(DEFAULT_MAX_ROWS));
        dpDesde.setValue(LocalDate.now().minusDays(DAYS_DIFFERENCE));
        dpFinsA.setValue(LocalDate.now());
        chkMostrarPunts.setSelected(false);
        chkTemperatura.setSelected(false);
        chkT1.setSelected(false);
        chkT2.setSelected(false);
        chkT3.setSelected(false);
        chkHumitat.setSelected(false);
        chkH1.setSelected(false);
        chkH2.setSelected(false);
        chkH3.setSelected(false);

        setupEventHandlers();
        setupTempChart();
        setupHumiChart();
    }

    private void setupEventHandlers() {

    // FIXME: Comrpovar i esbrinar perquè no funciona !
    /*
        chkTemperatura.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (chkTemperatura.isSelected()) {
                    chkT1.setSelected(false);
                    chkT2.setSelected(false);
                    chkT3.setSelected(false);
                    chkTemperatura.setSelected(false);
                } else {
                    chkT1.setSelected(true);
                    chkT2.setSelected(true);
                    chkT3.setSelected(true);
                    chkTemperatura.setSelected(true);
                }
            }
        });

    */
        sRegistres.valueProperty().addListener((observable, oldValue, newValue) -> {
            lbNRegistres.setText(String.valueOf(newValue.intValue()));
        });

        // Mostrar o no els punts
        chkMostrarPunts.selectedProperty().addListener((observable, oldValue, newValue) -> {
            tempChart.setCreateSymbols(newValue);
        });

        // Mostrar o no els punts
        chkMostrarPunts.selectedProperty().addListener((observable, oldValue, newValue) -> {
            tempChart.setCreateSymbols(newValue);
        });
    }

    private void setupChart() {
        if (chkT1.isSelected()) tempSeries.add(serieTempS1);
        if (chkT2.isSelected()) tempSeries.add(serieTempS2);
        if (chkT3.isSelected()) tempSeries.add(serieTempS3);

        if (chkH1.isSelected()) humiSeries.add(serieHumiS1);
        if (chkH2.isSelected()) humiSeries.add(serieHumiS2);
        if (chkH3.isSelected()) humiSeries.add(serieHumiS3);
    }

    private void setupTempChart() {
        // Definició dels eixos
        //CategoryAxis xAxisTempChart = new CategoryAxis(); // Gràcfic amb eix temporal (basat en temps)
        xAxisTempChart.setLabel("Data");
        xAxisTempChart.setAnimated(false); // Desactivar les animacions
        // nom de l'eix, valor mínim, valor màxim, unitat mínima
        //NumberAxis yAxisTempChart = new NumberAxis("Temperatura", -20.00f, 60.00f, 0.01f);

        //NumberAxis yAxisTempChart = new NumberAxis();
        yAxisTempChart.setLabel("Valors");
        yAxisTempChart.setAnimated(false); // Desactivar les animacions

        // Creem un gràfic de línies amb els 2 eixos creats anteriorment
        tempChart = new AreaChart<>(xAxisTempChart, yAxisTempChart);
        tempChart.setTitle("Temperatura ( ºC )");
        tempChart.setAnimated(false); // Desactivar les animacions
        tempChart.setCreateSymbols(chkMostrarPunts.isSelected());

        // Definició de les sèries a mostrar
        serieTempS1.setName("Temp. S1");
        serieTempS2.setName("Temp. S2");
        serieTempS3.setName("Temp. S3");

        // Afegim les sèries al gràfic
        tempChart.getData().addAll(serieTempS1, serieTempS2, serieTempS3);

        // Afegim el gràfic al AnchorPane
        apTempChart.getChildren().add(tempChart);
    }

    private void setupHumiChart() {
        // Definició dels eixos
        //CategoryAxis xAxisHumiChart = new CategoryAxis(); // Gràcfic amb eix temporal (basat en temps)
        xAxisHumiChart.setLabel("Data");
        xAxisHumiChart.setAnimated(false); // Desactivar les animacions
        // nom de l'eix, valor mínim, valor màxim, unitat mínima
        //NumberAxis yAxisHumiChart = new NumberAxis("Humitat", 1750.000f, 1900.00f, 1.00f);


        yAxisHumiChart.setLabel("Valors");
        yAxisHumiChart.setAnimated(false); // Desactivar les animacions

        // Creem un gràfic de línies amb els 2 eixos creats anteriorment
        humiChart = new AreaChart<>(xAxisHumiChart, yAxisHumiChart);
        humiChart.setTitle("Humitat");
        humiChart.setAnimated(false); // Desactivar les animacions
        humiChart.setCreateSymbols(chkMostrarPunts.isSelected());

        // Definició de les sèies a mostrar
        serieHumiS1.setName("Humi. S1");
        serieHumiS2.setName("Humi. S2");
        serieHumiS3.setName("Humi. S3");

        // Afegim les sèries al gràfic
        humiChart.getData().addAll(serieHumiS1, serieHumiS2, serieHumiS3);

        // Afegim el gràfic al AnchorPane
        apHumiChart.getChildren().add(humiChart);
    }

    @FXML
    public void btnSetupChartOnAction (ActionEvent actionEvent) {
        setupChart();
        doChart(tempSeries, humiSeries);
    }

    @FXML
    public void btnClearChartOnAction (ActionEvent actionEvent) {
        // JavaFX bug
        tempChart.setAnimated(false);
        humiChart.setAnimated(false);
        tempChart.getData().clear();
        humiChart.getData().clear();
        tempSeries.clear();
        humiSeries.clear();
        serieTempS1.getData().clear();
        serieTempS2.getData().clear();
        serieTempS3.getData().clear();
        serieHumiS1.getData().clear();
        serieHumiS2.getData().clear();
        serieHumiS3.getData().clear();
        setupTempChart();
        setupHumiChart();
        setupChart();
    }

    @FXML
    public void btnPrintChartOnAction(ActionEvent actionEvent) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Report.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ReportController controller = loader.getController();
        controller.setEmf(getEmf());
        controller.iniciDiferit();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        Scene scene = new Scene(root);
        controller.setEmf(getEmf());
        //controller.setScene(scene); // ÉS necessatri=

        stage.setScene(scene);
        stage.showAndWait();
    }

    @FXML
    public void btnSurtOnAction(ActionEvent actionEvent) {
        //RNB. Tancar la connexió amb influxdb
        influxDbHelper.closeClient();
        ((Stage) ((Button) actionEvent.getSource()).getScene().getWindow()).close();
    }

    private void doChart(List<XYChart.Series<String, Number>> tempSeries, List<XYChart.Series<String, Number>> humiSeries) {
        List<DummyData> dummyData = getDades(dpDesde.getValue(), dpFinsA.getValue(), (int)sRegistres.getValue());

        // Actualitzar el gràfic
        Platform.runLater(() -> {
            //final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
            // inserim el valor entero amb la data actual
            //FIXME: extreure els valors double
            SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy HH:mm:ss");
            for (DummyData d : dummyData) {
                Date date = Date.from(d.getTime());
                String formattedDate = formatter.format(date);
                for (XYChart.Series<String, Number> temp: tempSeries) {
                    if (chkT1.isSelected())
                        temp.getData().add(new XYChart.Data(formattedDate, d.getTemperaturaT1().doubleValue()));
                    if (chkT2.isSelected())
                        temp.getData().add(new XYChart.Data(formattedDate, d.getTemperaturaT2().doubleValue()));
                    if (chkT3.isSelected())
                        temp.getData().add(new XYChart.Data(formattedDate, d.getTemperaturaT3().doubleValue()));
                }

                for (XYChart.Series<String, Number> humi: humiSeries) {
                    if (chkH1.isSelected())
                        humi.getData().add(new XYChart.Data(formattedDate, d.getHumitatT1().doubleValue()));
                    if (chkH2.isSelected())
                        humi.getData().add(new XYChart.Data(formattedDate, d.getHumitatT2().doubleValue()));
                    if (chkH3.isSelected())
                        humi.getData().add(new XYChart.Data(formattedDate, d.getHumitatT3().doubleValue()));
                }
            }
        });
    }

    //private List<DummyData> getDades(LocalDateTime desde, LocalDateTime finsA, int maxRows) {
    private List<DummyData> getDades(LocalDate desde, LocalDate finsA, int maxRows) {
        InfluxDbHelper influxDbHelper = new InfluxDbHelper(Config.getInstancia().getInfluxdbHost(),
                Config.getInstancia().getInfluxdbPort(),
                Config.getInstancia().getInfluxdbUser(),
                Config.getInstancia().getInfluxdbPassword(),
                Config.getInstancia().isInfluxdbSSL());

        Query query = BoundParameterQuery.QueryBuilder.newQuery("SELECT * FROM dummyData WHERE time >= $desde AND time <= $finsa " +  ORDER_BY + " LIMIT " + maxRows)
                .forDatabase("iwinedb")
                .bind("desde", DateUtils.toInstant(desde))
                .bind("finsa", DateUtils.toInstant(finsA))
                //.bind("desde", dpDesde.getEditor().getText() + ":00")
                //.bind("finsa", dpFinsA.getEditor().getText() + ":00")
                //.bind("desde", DateUtils.toInfluxDBDate(desde))
                //.bind("finsa", DateUtils.toInfluxDBDate(finsA))
                .create();

        System.out.println(query.toString());
        //QueryResult queryResult = InfluxDbHelper.getInstancia().query(query);
        QueryResult queryResult = influxDbHelper.query(query);
        InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
        List<DummyData> resultat = resultMapper.toPOJO(queryResult, DummyData.class);
        influxDbHelper.closeClient();
        return resultat;
        //return resultMapper.toPOJO(queryResult, DummyData.class);
    }

    // -- Getter's && Setter's -- \\

    public Dispositiu getDispositiu() {
        return dispositiu;
    }

    public void setDispositiu(Dispositiu dispositiu) {
        this.dispositiu = dispositiu;
    }

    public void btSurtOnAction(ActionEvent actionEvent) {
    }
}