package cat.innowine.desktop.controllers;

import cat.dam2.m15.controls.uppercasetextfield.UpperCaseTextField;
import cat.innowine.desktop.Main;
import cat.innowine.desktop.classes.Constants;
import cat.innowine.desktop.classes.DAOHelper;
import cat.innowine.desktop.classes.MapProjection;
import cat.innowine.desktop.classes.Mode;
import cat.innowine.desktop.model.Config;
import cat.innowine.desktop.model.Usuari;
import cat.innowine.desktop.utils.AlertHelper;
import cat.innowine.desktop.utils.GMailHelper;
import cat.innowine.desktop.utils.SecurityHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DadesUsuariController extends DefaultControllerDiferit implements Initializable {

    @FXML
    private ComboBox cbProjeccioMapa;

    @FXML
    private AnchorPane apDadesPersonals, apDadesAdmin, apDadesInflux, apDadesEmail;

    @FXML
    private CheckBox chkSSL, chkAuth, chkTls;

    @FXML
    private TextField tfDataBase,tfUser, tfPort, tfHost,
                      tfLogin, tfEmail, tfCodiPostal, tfMaxIntents, tfMinCaracters,
                      tfAssumpteBloqueig, tfEmailBloqueig, tfEmailSmtp, tfEmailPort, tfEmailUser;

    @FXML
    private PasswordField tfPassword, tfEmailPasswd;

    @FXML
    private UpperCaseTextField tfNom, tfCognoms, tfAdreca, tfPoblacio, tfPais;

    @FXML
    TabPane tpDades;

    @FXML
    Tab tbDadesPersonals, tbAdministracio, tbInflux, tbEmail;

    @FXML
    Button btPassword;

    private boolean newRecord;
    private Usuari newUser;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Arrays.asList(MapProjection.values()).forEach(p -> cbProjeccioMapa.getItems().add(p.getProjectionDescription()));
        cbProjeccioMapa.getSelectionModel().selectFirst();
    }

    @Override
    public void iniciDiferit() {
        if (!newRecord) {
            if (!this.getUsuari().isAdmin())
                tpDades.getTabs().removeAll(tbAdministracio, tbInflux, tbEmail);
            fillFormData(this.getUsuari().isAdmin());
        } else {
            tpDades.getTabs().removeAll(tbAdministracio, tbInflux, tbEmail);
            btPassword.setVisible(false);
            tfLogin.setEditable(true);
            tfLogin.requestFocus();
            newUser = new Usuari();
        }
    }

    private void fillFormData(boolean admin) {
        tfLogin.setText(getUsuari().getLogin());
        tfNom.setText(getUsuari().getNom());
        tfCognoms.setText(getUsuari().getCognoms());
        tfEmail.setText(getUsuari().getEmail());
        tfAdreca.setText(getUsuari().getAdreca());
        tfCodiPostal.setText(getUsuari().getCodiPostal());
        tfPoblacio.setText(getUsuari().getPoblacio());
        tfPais.setText(getUsuari().getPais());
        if (admin) {
            // Dades admin
            tfMaxIntents.setText(String.valueOf(Config.getInstancia().getMaxIntents()));
            tfMinCaracters.setText(String.valueOf(Config.getInstancia().getMinCaracters()));
            tfAssumpteBloqueig.setText(Config.getInstancia().getEmailSubject());
            tfEmailBloqueig.setText(Config.getInstancia().getEmailBody());
            cbProjeccioMapa.getSelectionModel().select(MapProjection.values()[Config.getInstancia().getMapProjection()]);
            // Servidor de Correu
            tfEmailSmtp.setText(Config.getInstancia().getEmailSmtp());
            tfEmailPort.setText(String.valueOf(Config.getInstancia().getEmailPort()));
            tfEmailUser.setText(Config.getInstancia().getEmailUser());
            tfEmailPasswd.setText(Config.getInstancia().getEmailPasswd());
            chkTls.setSelected(Config.getInstancia().isEmailTls());
            chkAuth.setSelected(Config.getInstancia().isEmailAuth());
            // Influx DB Config
            tfHost.setText(Config.getInstancia().getInfluxdbHost());
            tfPort.setText(String.valueOf(Config.getInstancia().getInfluxdbPort()));
            tfDataBase.setText(Config.getInstancia().getInfluxdbDatabase());
            tfUser.setText(Config.getInstancia().getInfluxdbUser());
            tfPassword.setText(Config.getInstancia().getInfluxdbPassword());
            chkSSL.setSelected(Config.getInstancia().isInfluxdbSSL());
        }
        tfNom.requestFocus();
    }

    private boolean checkMandatoryInfo(TabPane tabPane, Tab tab, AnchorPane pane) {
        boolean result = true;
        for (int i = 0; i < pane.getChildren().size(); i++) {
            if (pane.getChildren().get(i) instanceof TextField) {
                if (((TextField) pane.getChildren().get(i)).getText().isEmpty()) {
                    AlertHelper.alertInfo(String.format("El camp %s no pot ser buit", ((TextField) pane.getChildren().get(i)).getPromptText()));
                    tabPane.getSelectionModel().select(tab);
                    pane.getChildren().get(i).requestFocus();
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    private void guardarDades(Usuari usuari, boolean isAdmin) {
        if (checkMandatoryInfo(tpDades, tbDadesPersonals, apDadesPersonals)) {
            usuari.setLogin(tfLogin.getText());
            usuari.setNom(tfNom.getText());
            usuari.setCognoms(tfCognoms.getText());
            usuari.setEmail(tfEmail.getText());
            usuari.setAdreca(tfAdreca.getText());
            usuari.setCodiPostal(tfCodiPostal.getText());
            usuari.setPoblacio(tfPoblacio.getText());
            usuari.setPais(tfPais.getText());
            DAOHelper<Usuari> helper = new DAOHelper<>(this.getEmf(), Usuari.class);
            if (newRecord) {
                changePassword(usuari, Mode.ONLY_VALUE_MODE);
                if (usuari.getPassword() != null) {
                    helper.insert(usuari);
                } else {
                    //REVIEW No hauria de ser necessari al grava ho torna a mostrar
                    //AlertHelper.alertInfo("Ha d'introduir una contrasenya obligatòriament.");
                }
            } else
                helper.update(usuari);
        } else {
            return ;
        }

        if (isAdmin) {
            // Dades admin
            if (checkMandatoryInfo(tpDades, tbAdministracio, apDadesAdmin)) {
                // Config
                Config.getInstancia().setMaxIntents(Integer.parseInt(tfMaxIntents.getText()));
                Config.getInstancia().setMinCaracters(Integer.parseInt(tfMinCaracters.getText()));
                Config.getInstancia().setEmailSubject(tfAssumpteBloqueig.getText());
                Config.getInstancia().setEmailBody(tfEmailBloqueig.getText());
                Config.getInstancia().setMapProjection(MapProjection.valueOf(cbProjeccioMapa.getSelectionModel().getSelectedItem().toString()).getProjectionCode());
            } else
                return;

            // SMTP mail
            if (checkMandatoryInfo(tpDades, tbEmail, apDadesEmail)) {
                Config.getInstancia().setEmailSmtp(tfEmailSmtp.getText());
                Config.getInstancia().setEmailPort(Integer.parseInt(tfEmailPort.getText()));
                Config.getInstancia().setEmailUser(tfEmailUser.getText());
                Config.getInstancia().setEmailPasswd(SecurityHelper.getInstancia().encrypt(tfEmailPasswd.getText()));
                Config.getInstancia().setEmailTls(chkTls.isSelected());
                Config.getInstancia().setEmailAuth(chkAuth.isSelected());
            } else
                return;

            // InfluxDB
            if (checkMandatoryInfo(tpDades, tbInflux, apDadesInflux)) {
                Config.getInstancia().setInfluxdbHost(tfHost.getText());
                Config.getInstancia().setInfluxdbPort(Integer.parseInt(tfPort.getText()));
                Config.getInstancia().setInfluxdbDatabase(tfDataBase.getText());
                Config.getInstancia().setInfluxdbUser(tfUser.getText());
                Config.getInstancia().setInfluxdbPassword(SecurityHelper.getInstancia().encrypt(tfPassword.getText()));
                Config.getInstancia().setInfluxdbSSL(chkSSL.isSelected());
            }

            DAOHelper<Config> configDAOHelper = new DAOHelper<>(this.getEmf(), Config.class);
            configDAOHelper.update(Config.getInstancia());
        }
    }

    private void changePassword(Usuari usuari, Mode mode) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CanviaPassword.fxml"));
        Parent root = null;
        try {
            root = loader.load();
            CanviaPasswordController controller = loader.getController();
            controller.setEmf(this.getEmf());
            controller.setUsuari(usuari);
            controller.setMode(mode);
            controller.iniciDiferit();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.initStyle(StageStyle.DECORATED);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.setOnHidden(event -> {
                if ((controller.getUsuari().getPassword() != null) && (!controller.getUsuari().getPassword().isEmpty())) {
                    usuari.setPassword(controller.getUsuari().getPassword());
                }
            });
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void btPasswordOnAction(ActionEvent actionEvent) {
        // TODO: 19/03/2020 <RNB> Cal habilitar el canvi de contrasenya per l'usuari
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CanviaPassword.fxml"));
        Parent root = null;

        try {
            root = loader.load();
            CanviaPasswordController controller = loader.getController();
            controller.setEmf(this.getEmf());
            controller.setUsuari(this.getUsuari());
            if (this.getUsuari().isAdmin())
                controller.setMode(Mode.ADMIN_MODE);
            else
                controller.setMode(Mode.USER_MODE);
            controller.iniciDiferit();
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.initStyle(StageStyle.DECORATED);
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            AlertHelper.alertaError("No ha estat possible mostrar la pantalla de canvi de contrasenya\nAvisi a l'administrador/a.");
        }
    }

    @FXML
    public void btGuardarOnAction(ActionEvent actionEvent) {
        if (!newRecord)
            guardarDades(this.getUsuari(), this.getUsuari().isAdmin());
        else {
            guardarDades(newUser, false);
        }
    }

    @FXML
    public void btEnviarCorreuOnAction(ActionEvent actionEvent) {
        if (checkMandatoryInfo(tpDades, tbEmail, apDadesEmail)) {
            if (!tfEmail.getText().isEmpty()) {
                GMailHelper mailHelper = new GMailHelper(Config.getInstancia().getEmailSmtp(),
                                                         Config.getInstancia().getEmailPort(),
                                                         Config.getInstancia().getEmailUser(),
                                                         SecurityHelper.getInstancia().decrypt(Config.getInstancia().getEmailPasswd()),
                                                         Config.getInstancia().isEmailTls(),
                                                         Config.getInstancia().isEmailAuth());

                mailHelper.send(tfEmail.getText(), Constants.EMAIL_TEST_SUBJECT, Constants.EMAIL_TEST_BODY);
            }
        }
    }

    public boolean isNewRecord() {
        return newRecord;
    }

    public void setNewRecord(boolean newRecord) {
        this.newRecord = newRecord;
    }

    public Usuari getNewUser() {
        return newUser;
    }
}