package cat.innowine.desktop.classes;

import cat.innowine.desktop.utils.SecurityHelper;
import org.apache.commons.lang.StringUtils;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;

public class InfluxDbHelper {
    private static final String PROTOCOL_HTTPS = "https://";
    private static final String PROTOCOL_HTTP  = "http://";

    private static InfluxDB client;
    private String host;
    private int port;
    private String user;
    private String password;
    private boolean ssl;

    public InfluxDbHelper(String host, int port, String user, String password, boolean ssl) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
        this.ssl = ssl;
        client = createClient();
    }

    /**
     * Crea un nou client {@link InfluxDB} basat en la configuració del host, port, usuari i contrasenya.
     *
     * @return un client {@link InfluxDB}
     * @throws IllegalArgumentException es llançada si el client no pot ser creat
     */
    private InfluxDB createClient() throws IllegalArgumentException {
        if (StringUtils.isEmpty(host)) {
            throw new IllegalArgumentException("El nom del host no pot ser buit.");
        }
        if (port <= 0) {
            throw new IllegalArgumentException("Especifica un valor vàlid de port entre 1 i 65535.");
        }

        String protocol = ssl ? PROTOCOL_HTTPS : PROTOCOL_HTTP;
        String influxUrl = protocol + host + ":" + port;

        return InfluxDBFactory.connect(influxUrl, user, SecurityHelper.getInstancia().decrypt(password));
    }

    public void closeClient() {
        client.close();
    }

    public QueryResult query(Query query) {
        return client.query(query);
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }
}