package cat.innowine.desktop.model;


import javafx.beans.property.SimpleBooleanProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "tbl_usuari")
public class Usuari implements Serializable {

  private static final long serialVersionUID = 3671739520115868238L;

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "admin")
  private boolean admin;

  @Column(name = "login", length = 100, nullable = false)
  private String login;

  @Column(name = "password")
  private String password;

  @Column(name = "data")
  private Date data;

  @Column(name = "nom", nullable = false)
  private String nom;

  @Column(name = "cognoms", nullable = false)
  private String cognoms;

  @Column(name = "adreca")
  private String adreca;

  @Column(name = "codi_postal", length = 5)
  private String codiPostal;

  @Column(name = "poblacio")
  private String poblacio;

  @Column(name = "pais")
  private String pais;

  @Column(name = "email", nullable = false)
  private String email;

  @Column(name = "intents")
  private Integer intents;

  @Column(name = "bloquejat")
  private boolean bloquejat;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getCognoms() {
    return cognoms;
  }

  public void setCognoms(String cognoms) {
    this.cognoms = cognoms;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAdreca() {
    return adreca;
  }

  public void setAdreca(String adreca) {
    this.adreca = adreca;
  }

  public String getCodiPostal() {
    return codiPostal;
  }

  public void setCodiPostal(String codiPostal) {
    this.codiPostal = codiPostal;
  }

  public String getPoblacio() {
    return poblacio;
  }

  public void setPoblacio(String poblacio) {
    this.poblacio = poblacio;
  }

  public String getPais() {
    return pais;
  }

  public void setPais(String pais) {
    this.pais = pais;
  }

  public Date getData() {
    return data;
  }

  public void setData(Date data) {
    this.data = data;
  }

  public boolean isBloquejat() {
    return bloquejat;
  }

  public void setBloquejat(boolean bloquejat) {
    this.bloquejat = bloquejat;
  }

  // es necessita per omplir la casella bloquejat el formulari de cerca
  public SimpleBooleanProperty getBloquejatProperty() {
    return new SimpleBooleanProperty(bloquejat);
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Integer getIntents() {
    return intents;
  }

  public void setIntents(Integer intents) {
    this.intents = intents;
  }

  public boolean isAdmin() {
    return admin;
  }

  public void setAdmin(boolean admin) {
    this.admin = admin;
  }

  public String getNomComplet() {
    return this.nom + " " + this.cognoms;
  }

  @Override
  public String toString() {
    return "Usuari{" +
            "id=" + id +
            ", login='" + login + '\'' +
            ", nom='" + nom + '\'' +
            ", cognoms='" + cognoms + '\'' +
            ", email='" + email + '\'' +
            ", admin=" + admin +
            '}';
  }
}
