package cat.innowine.desktop.utils;

import cat.innowine.desktop.classes.Constants;
import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import org.jasypt.util.text.AES256TextEncryptor;

import java.util.logging.Level;
import java.util.logging.Logger;

public class SecurityHelper {
    private AES256TextEncryptor textEncryptor;
    //private Argon2 argon2;

    private static SecurityHelper instancia;

    public static SecurityHelper getInstancia(){
        if(instancia == null){
            instancia = new SecurityHelper();
        }
        return instancia;
    }
    /**
     * Constructor
     */
    private SecurityHelper(){
        //try{
            //this.argon2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2i, 32, 128);
            this.textEncryptor = new AES256TextEncryptor();
            this.textEncryptor.setPassword(Constants.ENCRYPTOR_PASSWORD);
       // }catch(Exception e){
       //     Logger.getLogger(SecurityHelper.class.getName()).log(Level.SEVERE, e.getLocalizedMessage(), e);
        //}
    }

    public String encrypt(String toEncrypt){
        try{
            return this.textEncryptor.encrypt(toEncrypt);
        }catch(Exception e){
            Logger.getLogger(SecurityHelper.class.getName()).log(Level.SEVERE, e.getLocalizedMessage(), e);
        }
        return "";
    }

    public String decrypt(String encryptedText){
        try{
            return this.textEncryptor.decrypt(encryptedText);
        }catch(Exception e){
            Logger.getLogger(SecurityHelper.class.getName()).log(Level.SEVERE, e.getLocalizedMessage(), e);
        }
        return "";
    }
}
