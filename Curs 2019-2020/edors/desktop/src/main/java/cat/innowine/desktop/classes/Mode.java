package cat.innowine.desktop.classes;

public enum Mode {
    ADMIN_MODE(0, "Ús del formulari com <Administrador>"),
    USER_MODE(1, "Ús del formulari com <Usuari>"),
    ONLY_VALUE_MODE(-1, "Solament retorna un valor");

    private int errorCode;
    private String message;

    Mode(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
