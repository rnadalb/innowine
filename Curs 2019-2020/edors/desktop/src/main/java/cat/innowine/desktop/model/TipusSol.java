package cat.innowine.desktop.model;

import javax.persistence.*;

@Entity
@Table (name = "tbl_tipus_sol")
public class TipusSol {
    @Id
    @Column (name = "id")
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "descripcio", length = 50, nullable = false)
    private String descripcio;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    @Override
    public String toString() {
        return descripcio;
    }
}
