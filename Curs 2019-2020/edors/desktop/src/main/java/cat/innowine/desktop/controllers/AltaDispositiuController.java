package cat.innowine.desktop.controllers;

import cat.innowine.desktop.classes.Constants;
import cat.innowine.desktop.classes.DispositiuHelper;
import cat.innowine.desktop.exceptions.AlertHelperExcepcion;
import cat.innowine.desktop.exceptions.AlertHelperExcepcionSelectedYes;
import cat.innowine.desktop.model.Dispositiu;
import cat.innowine.desktop.utils.AlertHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ResourceBundle;

public class AltaDispositiuController extends DefaultControllerDiferit implements Initializable {
    @FXML private TableView<Dispositiu> tvDispositius;
    @FXML private TableColumn<Dispositiu, String> colDescripcio;
    @FXML private TableColumn<Dispositiu, BigDecimal> colLat;
    @FXML private TableColumn<Dispositiu, BigDecimal> colLng;
    @FXML private TextField tfCerca;

    private DispositiuHelper<Dispositiu> helper;
    private ObservableList<Dispositiu> llistaDispositius;// sdasda

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setupEventHandlers();
        configuraColumnes();
    }

    @Override
    public void iniciDiferit() {
        helper = new DispositiuHelper<>(this.getEmf(), Dispositiu.class);
        actualitzaTaula(true);
        goTableItem(Constants.FIRST_ITEM);
    }

    /**
     * Inicialitza els esdeveniments
     */
    private void setupEventHandlers() {
        ContextMenu cm = new ContextMenu();
        MenuItem mi1 = new MenuItem("Modifica");
        MenuItem mi2 = new SeparatorMenuItem();
        MenuItem mi3 = new MenuItem("Elimina");

        cm.getItems().addAll(mi1, mi2, mi3);
        cm.setOnAction(event -> {
            if (event.getTarget().equals(mi1))
                modificaDispositiu(tvDispositius.getSelectionModel().getSelectedItem());
            else if (event.getTarget().equals(mi3)) {
                eliminaDispositiu(tvDispositius.getSelectionModel().getSelectedItem());
            }
        });

        tvDispositius.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getButton() == MouseButton.SECONDARY) {
                cm.show(tvDispositius, event.getScreenX(), event.getScreenY());
            }

            if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == Constants.DOUBLE_CLICK) {
                modificaDispositiu(tvDispositius.getSelectionModel().getSelectedItem());
            }
        });
    }

    private void modificaDispositiu(Dispositiu dispositiu) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/DadesDispositiu.fxml"));
        Parent root = null;
        int indexActual = tvDispositius.getSelectionModel().getSelectedIndex();
        try {
            root = loader.load();
            DadesDispositiuController controller = loader.getController();
            controller.setEmf(this.getEmf());
            controller.setUsuari(this.getUsuari());
            controller.setDispositiu(dispositiu);
            controller.iniciDiferit();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.initStyle(StageStyle.DECORATED);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.setOnHidden(event -> {
                Dispositiu d = controller.getDispositiu();
                if (d != null) {
                    if (d.getId() != null) { // No s'ha guardat a la BBDD per clau duplicada)
                        if (controller.isNewRecord())
                            llistaDispositius.add(d);
                        else {
                            llistaDispositius.set(indexActual, d);
                            //tvDispositius.getItems().add(d);
                        }
                    }
                    actualitzaTaula(false);
                    goTableItem(indexActual);
                }
            });

            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void eliminaDispositiu(Dispositiu d) {
        try {
            AlertHelper.alertaYesNo(Constants.APPNAME, "Estàs segur d'eliminar el dipositiu <" + d.getDescripcio() + ">");
        } catch (AlertHelperExcepcion excepcion) {
            if (excepcion instanceof AlertHelperExcepcionSelectedYes) {
                DispositiuHelper<Dispositiu> helper = new DispositiuHelper<>(this.getEmf(), Dispositiu.class);
                if (!this.getUsuari().isAdmin())
                    helper.delete(d.getId(), this.getUsuari().getId());
                else
                    helper.delete(d.getId(), d.getUsuari().getId());
            }
        }
        actualitzaTaula(true);
    }

    private void configuraColumnes() {
        // Alerta que els valors --------------------------------<>
        // Identifiquen el noms de les VARIABLES de la classe
        colDescripcio.setCellValueFactory(new PropertyValueFactory<>("descripcio"));
        colLat.setCellValueFactory(new PropertyValueFactory<>("latitud"));
        colLng.setCellValueFactory(new PropertyValueFactory<>("longitud"));

        colDescripcio.setSortable(false);
        colLat.setSortable(false);
        colLng.setSortable(false);
    }

    private void actualitzaTaula(boolean update) {
        if (update) {
            if (llistaDispositius != null)
                llistaDispositius.clear();
            if (this.getUsuari().isAdmin())
                llistaDispositius = FXCollections.observableArrayList(helper.getAll());
            else
                llistaDispositius = FXCollections.observableArrayList(helper.getDevices(this.getUsuari()));
            tvDispositius.getItems().clear();
            tvDispositius.setItems(llistaDispositius);
        } else
            tvDispositius.refresh();
        filtraTaula();
    }

    /**
     * Adaptació del filtratge per a la taula de Productes, pero en BBDD. És identic !!!
     *
     * Vigila el filtres que apliques, sobre tot el tipus (String, Integer, Float, ...)
     *
     */
    private void filtraTaula() {
        if (!tvDispositius.getItems().isEmpty())
            tvDispositius.getItems().removeAll();

        // 1.  Ajustar el ObservableList en una llista filtrada (inicialment mostra totes les dades).
        FilteredList<Dispositiu> filteredData = new FilteredList<>(tvDispositius.getItems(), p -> true);

        // 2. Estableix el Predicat del filtre cada vegada que camvia en filtre
        tfCerca.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(dispositiu -> {
                // Si el text del filtre està buit (null o empty), mostrar-ho tot
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                return dispositiu.getDescripcio().toLowerCase().indexOf(lowerCaseFilter) != -1; // El filtre coincideix amb la descripció del dispositiu
// No hi ha coincidència
            });
        });

        // 3. Embolica el FilteredList en una SortedList.
        SortedList<Dispositiu> sortedData = new SortedList<>(filteredData);

        // 4. Lliga el comparador de la SortedList cal del TableView
        // 	  En qualsevol altre cas, l'ordenació de la TableView no tindrà efecte
        sortedData.comparatorProperty().bind(tvDispositius.comparatorProperty());

        // 5. Afegeix la informació ordenada (i filtrada) a la taula
        tvDispositius.setItems(sortedData);
    }

    private void goTableItem(int row) {
        tvDispositius.requestFocus();
        tvDispositius.scrollTo(row);
        tvDispositius.getSelectionModel().select(row);
        tvDispositius.getFocusModel().focus(row);
    }

    @FXML
    public void btNewOnAction(ActionEvent actionEvent) {
        modificaDispositiu(null);
    }

    @FXML
    public void btUpdateOnAction(ActionEvent actionEvent) {
        if (tvDispositius.getSelectionModel().getSelectedItem() != null)
            modificaDispositiu(tvDispositius.getSelectionModel().getSelectedItem());
    }

    @FXML
    public void btDeleteOnAction(ActionEvent actionEvent) {
        if (tvDispositius.getSelectionModel().getSelectedItem() != null && !tvDispositius.getItems().isEmpty())
            eliminaDispositiu(tvDispositius.getSelectionModel().getSelectedItem());
    }
}