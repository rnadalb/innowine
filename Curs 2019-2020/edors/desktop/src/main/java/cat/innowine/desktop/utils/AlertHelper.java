package cat.innowine.desktop.utils;

import cat.innowine.desktop.classes.Constants;
import cat.innowine.desktop.exceptions.AlertHelperExcepcion;
import cat.innowine.desktop.exceptions.AlertHelperExcepcionSelectedNo;
import cat.innowine.desktop.exceptions.AlertHelperExcepcionSelectedYes;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class AlertHelper {
    private static final String TITLE = "iWine Desktop App";

    private static void alerta(String title, String header, String message, AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setTitle(message);
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private static void alerta(String title, String message, AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setTitle(message);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public static void alertaYesNo(String title, String message) throws AlertHelperExcepcion {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(Constants.MSG_CONFIRMAR);
        alert.setContentText(message);

        ButtonType btSi = new ButtonType(Constants.SI);
        ButtonType btNo = new ButtonType(Constants.NO);

        alert.getButtonTypes().setAll(btSi, btNo);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == btSi)
            throw new AlertHelperExcepcionSelectedYes();
        else
            throw new AlertHelperExcepcionSelectedNo();
    }

    public static void alertInfo(String message) {
        alerta(TITLE, message, AlertType.INFORMATION);
    }

    /**
     * Mostra una alerta de tipus Error
     *
     * @param missatge missatge a mostrar
     */
    public static void alertaError(String missatge) {
        alerta(TITLE, missatge, Alert.AlertType.ERROR);
    }

    /**
     * Mostra una alerta de tipus Warning
     *
     * @param missatge missatge a mostrar
     */
    public static void alertaWarning(String missatge) {
        alerta(TITLE, missatge, AlertType.WARNING);
    }
}
