package cat.innowine.desktop.classes;

public enum MapProjection {
    // Default projection -> MERCATOR
    MERCATOR (0, "MERCATOR"),
    WGS_84 (1, "WGS_84");

    private int projectionCode;
    private String projectionDescription;

    MapProjection(int projectionCode, String projectionDescription) {
        this.projectionCode = projectionCode;
        this.projectionDescription = projectionDescription;
    }

    public int getProjectionCode() {
        return projectionCode;
    }

    public void setProjectionCode(int projectionCode) {
        this.projectionCode = projectionCode;
    }

    public String getProjectionDescription() {
        return projectionDescription;
    }

    public void setProjectionDescription(String projectionDescription) {
        this.projectionDescription = projectionDescription;
    }
}
