package cat.innowine.desktop.junit;

import cat.innowine.desktop.Main;
import cat.innowine.desktop.utils.CipherHelper;
import cat.innowine.desktop.utils.FileUtils;
import org.junit.jupiter.api.*;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

@DisplayName ("Test Case: Encriptació")
@TestMethodOrder (MethodOrderer.OrderAnnotation.class)
class CipherHelperTest {

    @Test
    @DisplayName("El fitxer encriptat és el mateix desprès de desencriptar-lo]")
    @Order (1)
    void el_fitxer_encriptat_es_el_mateix_despres_de_desencriptarlo() {
        String clau = "contrasenya_encriptada";
        String enc = "app.properties_enc";
        URL clear = Main.class.getClassLoader().getResource("app.properties");
        try {
            String encFilePath = Paths.get(clear.toURI()).getParent().toString() + File.separator + enc;
            StringBuilder clearFile = FileUtils.readFile(clear.getFile());
            CipherHelper.fileEncrypt(clear.getFile(), encFilePath, clau);
            String decryptResult = CipherHelper.fileDecrypt(encFilePath, clau);
            Assertions.assertEquals(clearFile.toString(), decryptResult, "Falla");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}