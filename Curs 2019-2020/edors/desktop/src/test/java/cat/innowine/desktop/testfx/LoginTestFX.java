package cat.innowine.desktop.testfx;

import cat.innowine.desktop.Main;
import cat.innowine.desktop.classes.LoginState;
import cat.innowine.desktop.controllers.LoginController;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;
import org.testfx.api.FxAssert;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

@DisplayName ("Test Case: Login Test")
@TestMethodOrder (MethodOrderer.OrderAnnotation.class)
public class LoginTestFX extends ApplicationTest {
    @Start
    public void start(Stage primaryStage) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Login.fxml"));
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mariaDB");
        try {
            Parent root = loader.load();
            LoginController controller = loader.getController();
            controller.setEmf(emf);
            Scene scene = new Scene(root);
            //primaryStage.initStyle(StageStyle.DECORATED);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }

    @Order(1)
    @ParameterizedTest
    @DisplayName("Test d'usuaris amb la contrasenya correcta")
    @CsvSource ({ "bgates,pass", "emusk,pass", "pnorton,pass" })
    void introdueix_dades_correctament(ArgumentsAccessor argumentsAccessor) {
        String usuari = argumentsAccessor.getString(0);
        String password = argumentsAccessor.getString(1);
        clickOn("#tfUsuari"); // cerca per fx:id
        write(usuari);
        clickOn("#tfPassword");
        write(password);
        clickOn("#btSignIn");
        LoginState expected = LoginState.GRANTED;
        FxAssert.verifyThat("#lbErrors", LabeledMatchers.hasText(LoginState.GRANTED.getMessage()));
    }

    @After
    public void afterEachTest() throws TimeoutException {
        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }
}
