Feature: Avaluador de Login
  Com usuari del sistema
  Necessito entrar d'una manera segura
  Per poder treballar

  Scenario Outline: Entrada al sistema correcta
    Given Donat avaluador de Login
    When usuari introdueix les dades correctament <usuari> <passwd>
    Then usuari accedeix al sistema
    Examples:
    | usuari  | passwd |
    | pnorton | pass   |
    | bgates  | pass   |
    | emusk   | pass   |

  Scenario Outline: Entrada al sistema incorrecta
    Given Donat avaluador de Login
    When usuari introdueix les dades incorrectament <usuari> <passwd>
    Then usuari no accedeix al sistema
    Examples:
      | usuari  | passwd |
      | pnorton | fail   |
      | bgates  | fail   |
      | emusk   | fail   |
      | noexist | fail   |


  Scenario Outline: Ultima al sistema incorrecta
    Given Donat avaluador de Login
    When usuari introdueix les dades incorrectament <usuari> <passwd>
    Then usuari no accedeix al sistema i rep un correu electronic
    Examples:
      | usuari  | passwd |
      | pnorton | fail   |
      | bgates  | fail   |
      | emusk   | fail   |