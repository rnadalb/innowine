
> En procés d'actualització ...

# InnoWine

Resultat del projecte desenvolupat pels instituts [Rambla Prim](https://www.irp.cat/) (Barcelona) i [Eugeni d'Ors](https://www.ies-eugeni.cat) (Vilafranca del Penedès) amb el suport de la [Fundació Bankia per la Formació Dual](https://www.dualizabankia.com/es/) guardonat amb l'ajuda de la [III Convocatòria d'ajudes FPEmpresa - Dualiza Bankia](https://www.dualizabankia.com/es/que-hacemos/convocatoria-dualiza/#undefined).

![Dualiza Bankia](img/logo_dualiza_bankia.jpeg "Fundación Bankia por la Formación Dual")
![FP Empresa](img/logo_fpempresa.png "Asociación de Centros de Formación Profesional")



## Breu descripció del projecte

Desenvolupar i documentar un sistema viable econòmicament que sigui capaç d'integrar diferents tecnologies de sensorització i bigdata que faciliti la presa de decisions al productor i a la viticultura de precisió, així com investigar opcions de donar accés a la informació via web.

## Contextualització

Tradicionalment la presa de decisions en la gestió de les explotacions vitícoles es basa en la inspecció visual de les vinyes. És necessari millorar la pren decisions i fer que aquesta es basi en dades i informació objectiva. La sensorització de les vinyes mitjançant tecnologies basades en Internet Of Things (IoT) pot aportar una diferència per a aquells agricultors que no poden permetre's dispositius molt cars. Donant accessibilitat a la tecnologia i no sent un producte exclusiu de grans productors.

### Algunes imatges




