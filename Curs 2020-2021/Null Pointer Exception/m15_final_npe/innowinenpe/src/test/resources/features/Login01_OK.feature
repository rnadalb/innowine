Feature: Ingreso al sistema
  Como usuario del sistema
  Necesito entrar de una manera segura
  Para poder trabajar
  Background:
  El PO nos ha indicado unos criterios de aceptación para garantir la seguridad del ingreso al sistema
  El usuario introducirá su login(correo electrònico) i su contraseña
  Después le darà <ENTER> o hará clic al botón Login

  Scenario Outline: Entrada al sistema correcta
    Given Dado avaluador de Login
    When usuario intoduce los datos correctamente <usuario> i <passwd>
    Then usuario accede al sistema
    Examples:
      | usuario          | passwd |
      | daniel@offing.es | 1234   |