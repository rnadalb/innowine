Feature: Ingreso al sistema
  Como usuario del sistema
  Necesito entrar de una manera segura
  Para poder trabajar
  Background:
  El PO nos ha indicado unos criteria de aceptación para garantizar la seguridad del ingreso al sistema
  El usuario introducirá el login (correo electrónico) y su contraseña
  Después pulsará <ENTER> o hará clic en el botón signos

  Scenario Outline: Entrada al sistema incorrecta por X vez
    Given Dado evaluador de Login con x intentos fallidos
    When usuario introduce los datos mal por x vez <usuario> i <passwd>
    Then usuario no accede al sistema y se bloqueado
    Examples:
    | usuario          | passwd |
    | daniel@offing.es | 1234   |
