Feature: Preparar escenario de error al ingreso
  Como programador del sistema
  Necesito poder ajustar la BBDD
  Para poder ejecutar el test de login fallido correctamente
  Background:
  Hay que actualizar el número de intentos a 4

  Scenario: Preparar la BBDD (Login.FAILED)
    Given Una Base de datos
    When los datos no están preparadas para el siguiente test
    Then el DevTeam actualiza la bbdd