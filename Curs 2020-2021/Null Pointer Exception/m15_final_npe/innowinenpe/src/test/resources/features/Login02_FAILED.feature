Feature: Ingreso al sistema
  Como usuario del sistema
  Necesito entrar de una manera segura para poder trabajar
  Background:
  El PO nos ha indicado unos criteria de aceptación para garantizar la seguridad del ingreso al sistema
  El usuario introducirá el login (correo electrónico) y su contraseña
  Después pulsará <ENTER> o hará clic en el botón signos

  Scenario Outline: Entrada al sistema incorrecta
    Given Dado evaluador de Login preparado
    When usuario introduce los datos incorrectamente <usuario> i <passwd>
    Then usuario no accede al sistema
    Examples:
      | usuario           | passwd |
      | daniel@offing.es  | fail   |
