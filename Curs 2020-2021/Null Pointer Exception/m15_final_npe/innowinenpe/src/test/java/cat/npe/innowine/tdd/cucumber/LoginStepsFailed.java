package cat.npe.innowine.tdd.cucumber;

import cat.npe.innowine.states.LoginState;
import cat.npe.innowine.classes.UtilsDB;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.*;
import io.cucumber.java.en.*;
import org.junit.jupiter.api.Assertions;


import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class LoginStepsFailed {
    // Necesario porqué trabajamos en BBDD
    private static EntityManagerFactory emf;
    private static UtilsDB utilsDB;

    // Nos indicarà el resultado de la función a testear
    private LoginState result;

    @Before
    public void initDB() {
        emf = Persistence.createEntityManagerFactory( "mariaDBConnectionTest" );
    }

    @After
    public void closeDB(Scenario scenario){
        emf.close();
    }

    @Given("^Dado evaluador de Login preparado$")
    public void dadoAvaluadorDeLoginPreparado() {
        utilsDB = new UtilsDB(emf);
    }

    @When ("^usuario introduce los datos incorrectamente (.+) i (.+)$")
    public void usuarioIntroduceLosDatosIncorrectamenteUsuarioIPasswd(String usuario, String passwd) {
        result = utilsDB.checkPass(true,usuario, passwd);
    }

    @Then ("usuario no accede al sistema")
    public void usuarioNoAccedeAlSistema() {
        LoginState expected = LoginState.FAILED;
        Assertions.assertEquals(expected, result, "utilsDB.CheckPass() falla");
    }
}

