package cat.npe.innowine.tdd.junit;

import cat.npe.innowine.states.LoginState;
import cat.npe.innowine.classes.UtilsDB;
import cat.npe.innowine.controllers.helpers.SecurityHelper;
import cat.npe.innowine.controllers.helpers.UsuarioHelper;
import cat.npe.innowine.model.Usuario;
import org.junit.jupiter.api.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Test Case: Ingreso al sistema")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class LoginSteps {

    private static EntityManagerFactory emf;
    private static UtilsDB utilsDB;

    private static final String LOGIN = "oriol@offing.es";
    private static final String PASSWD_OK = "1234";
    private static final String PASSWD_FAIL = "0000";

    /**
     * Este mètodo se ejecuta solo 1 vez al principio
     */
    @BeforeAll
    static void setUp() {
        emf = Persistence.createEntityManagerFactory("mariaDBConnectionTest");

        utilsDB = new UtilsDB(emf);
        SecurityHelper sh = new SecurityHelper();


        // Preparem la BBDD per realitzar els tests
        UsuarioHelper<Usuario> helper = new UsuarioHelper<>(emf, Usuario.class);
        Usuario usuari = helper.findUserByEmail(LOGIN);
        usuari.setBloqueado(false);
        usuari.setIntentos(4);
        usuari.setPassword(sh.passwordEncrypt(PASSWD_OK));
        helper.update(usuari);

    }

    /**
     * Este mètodo se ejecuta al final del test
     */
    @AfterAll
    static void tearDown() {
        if (emf != null && emf.isOpen())
            emf.close();
    }

    @Test
    @DisplayName("El usuario introduce los datos correctamente")
    @Order(1)
    void usuarioIntoduceDatosCorrectamente() {
        LoginState esperat = LoginState.GRANTED;
        LoginState resultat = utilsDB.checkPass(true,LOGIN, PASSWD_OK);
        assertEquals(esperat, resultat, String.format("dBUtils.checkPassword(%s,%s) ha fallado!", LOGIN, PASSWD_OK));
    }

    @Test
    @DisplayName("El usuario introduce los datos mal")
    @Order(2)
    void usuarioIntroduceMalDatos() {
        LoginState esperat = LoginState.FAILED;
        LoginState resultat = utilsDB.checkPass(true,LOGIN, PASSWD_FAIL);
        assertEquals(esperat, resultat, String.format("dBUtils.checkPassword(%s,%s) ha fallado!", LOGIN, PASSWD_OK));
    }

    @Test
    @DisplayName("El usuario es bloqueado al 4to intento")
    @Order(3)
    void usuarioBloqeuado() {
        LoginState esperat = LoginState.BLOCKED;
        LoginState resultat = LoginState.GRANTED;
        for (int i = 4; i > 0; i--)
            resultat = utilsDB.checkPass(true, LOGIN, PASSWD_FAIL);
        assertEquals(esperat, resultat, String.format("dBUtils.checkPassword(%s,%s) ha fallado!", LOGIN, PASSWD_OK));
    }
}
