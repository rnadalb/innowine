package cat.npe.innowine.tdd.junit;

import cat.npe.innowine.controllers.MiPerfilController;
import cat.npe.innowine.controllers.helpers.UsuarioHelper;
import cat.npe.innowine.model.ConfiguracionSistema;
import cat.npe.innowine.model.Usuario;
import cat.npe.innowine.states.CambioPasswordState;
import com.jfoenix.controls.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.jupiter.api.*;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationTest;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.concurrent.TimeoutException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS) /*Metodos no estaticos, el emf no puede ser estatico*/
public class TestMiPerfilGUI extends ApplicationTest {

    static EntityManagerFactory emf;
    private String pass="$argon2i$v=19$m=1048576,t=4,p=8$pFi8GwwL0U5TUGSynv4GSTh9ChO4DCo7+XCU6mLUsEA$DEafzywUqgxqid7Qi23g1fqd8GQ/gEg/WZc28aFB8M2uSFiZqJW0DwcjBMf+fnlr+QTFbt5PY9N241/xIoeeqt+/U8NQB2kKytrnFr47DSl5jpyBzk45maOQmbBymbWUNI7CeByeLSVvV/P8cI29BW10k06fujPHUCuJtA04Vsg";

    JFXButton btnGuardar,btnCerrarInfo, btnSalirInfo, btnCerrarInfoX;
    Label lbError, lbSuccess;
    JFXTextField tfInputEmail, tfInputNom, tfInputCognom1, tfInputCognom2 ,tfInputTelefon, tfInputPoblacio, tfInputCodiPostal, tfInputPais;
    JFXTextArea tfDireccio;
    JFXRadioButton rbAvatar1, rbAvatar2, rbAvatar3, rbAvatar4, rbAvatar5, rbAvatar6, rbAvatar7, rbAvatar8, rbAvatar9, rbAvatar10, rbAvatar11, rbAvatar12;
    JFXPasswordField tfInputPassActual, tfInputPassNou, tfInputPassNou2;

    @BeforeAll
    public void setUpClass() throws Exception {
        System.out.println("Mètode setUpClass() ...");
        // emf = Persistence.createEntityManagerFactory("cat.dam2.m15.hibernate.connexio");
        emf = Persistence.createEntityManagerFactory("mariaDBConnectionTest");
    }

    /**
     * Cargar vista , instanciar usuario de pruebas de la bbdd, y toda aquella configuracion necesaria para
     * que la vista funcione correctamente
     * @param stage La escena
     * @throws Exception Error de la excepción
     */
    @Override
    public void start(Stage stage) throws Exception {
        System.out.println("Mètode Start() ...");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/MiPerfil.fxml"));
        Parent mainNode = (Parent) loader.load();
        MiPerfilController controller = loader.getController();
        controller.setEmf(this.emf);

        Usuario usuario = null;
        UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(this.emf, Usuario.class);
        usuario = daoHelper.findUserByEmail("dummy@test.es");

        if(usuario== null){
            System.out.println("No se ha encontrado el usuario de prueba: dummy@test.es");
            tearDownClass();
            System.exit(1);
        }else{
            usuario.setNombre("Dummy");
            usuario.setApellido1("Test");
            usuario.setApellido2("Test Test");
            usuario.setPassword(pass);
            usuario.setIntentos(0);
            usuario.setBloqueado(false);
            usuario.setCambioPassword(false);
            daoHelper.update(usuario);
            ConfiguracionSistema.getInstancia().setIntentos(4);
            ConfiguracionSistema.getInstancia().setMinimoPassword(6);
            CambioPasswordState cps = CambioPasswordState.NOTMINLENGTH;
            cps.setMessage("La longitud minima de la nova contrasenya ha de ser de "+ ConfiguracionSistema.getInstancia().getMinimoPassword().toString()+" caracters.");

        }

        controller.setUsuarioLogueado(usuario);
        controller.setFormData();

        Scene scene = new Scene(mainNode);
        stage.setScene(scene);
        stage.show();
        stage.toFront();
    }


    /**
     * Comprobar que Existan los botones ... antes de los test ...
     * @throws Exception Error de la excepción
     */
    @BeforeEach
    public void setUp() throws Exception {
        System.out.println("Mètode setUp() ...");
        // RNB. more help --> https://github.com/TestFX/TestFX/wiki/Queries

        tfInputEmail = find("#tfInputEmail");  // --> El valor fx:id
        tfInputNom = find("#tfInputNom");
        tfInputCognom1 = find("#tfInputCognom1");
        tfInputCognom2 = find("#tfInputCognom2");
        tfInputTelefon = find("#tfInputTelefon");
        tfInputPoblacio = find("#tfInputPoblacio");
        tfInputCodiPostal = find("#tfInputCodiPostal");
        tfInputPais = find("#tfInputPais");
        tfDireccio = find("#tfDireccio");

        rbAvatar1 = find("#rbAvatar1");
        rbAvatar2 = find("#rbAvatar2");
        rbAvatar3 = find("#rbAvatar3");
        rbAvatar4 = find("#rbAvatar4");
        rbAvatar5 = find("#rbAvatar5");
        rbAvatar6 = find("#rbAvatar6");
        rbAvatar7 = find("#rbAvatar7");
        rbAvatar8 = find("#rbAvatar8");
        rbAvatar9 = find("#rbAvatar9");
        rbAvatar10 = find("#rbAvatar10");
        rbAvatar11 = find("#rbAvatar11");
        rbAvatar12 = find("#rbAvatar12");

        tfInputPassActual = find("#tfInputPassActual");
        tfInputPassNou = find("#tfInputPassNou");
        tfInputPassNou2 = find("#tfInputPassNou2");

        lbError= find("#lbError");
        lbSuccess = find("#lbSuccess");

        btnGuardar = find("#btnGuardar");
        btnCerrarInfo = find("#btnCerrarInfo");
        btnSalirInfo = find("#btnSalirInfo");
        btnCerrarInfoX = find("#btnCerrarInfoX");


    }

    /**
     * Resetear buffers de teclado y ratón, para resetear posicion cursor y teclado ...
     * @throws TimeoutException Error de excepción de tiempo maximo de ejecución
     */
    @AfterEach
    public void tearDown() throws TimeoutException {
        System.out.println("Mètode tearDown() ...");
        FxToolkit.hideStage();

        // Resetear buffers de teclado y ratón, para resetear posicion cursor y teclado ...
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    /**
     * Cerrar la conexión con la base de datos al finalizar los tests
     * @throws TimeoutException Error de excepción de tiempo maximo de ejecución
     */
    @AfterAll
    public void tearDownClass() throws TimeoutException {
        System.out.println("Mètode tearDownClass() ...");
        if (emf != null && emf.isOpen())
            emf.close();
    }

    /**
     * Comprobar si tenemos acceso a todos los controles, inputs y botones de la vista
     */
    @Test
    @Order (1)
    @DisplayName("Comprovar si tenim accés als controls")
    public void testA00_WidgestsExist() {
        final String errMsg = "Algun dels widgets no pot obtenir-se";

        //assertNotNull(errMsg, btnGuardar);
        //assertNotNull(errMsg, tfInputNom);

        Assertions.assertNotNull(tfInputEmail, errMsg);
        Assertions.assertNotNull(tfInputNom, errMsg);
        Assertions.assertNotNull(tfInputCognom1, errMsg);
        Assertions.assertNotNull(tfInputCognom2, errMsg);


        Assertions.assertNotNull(btnGuardar, errMsg);
        Assertions.assertNotNull(btnCerrarInfo, errMsg);
        Assertions.assertNotNull(btnSalirInfo, errMsg);
        Assertions.assertNotNull(btnCerrarInfoX, errMsg);


    }

    /**
     * Guardar los datos actuales recogidos al cargar el formulario de mi perfil
     */
    @Test
    @Order (2)
        @DisplayName("Guardar dades actuals")
        public void testA01_tipusGuardarDadesActuals() {
            clickOn(btnGuardar);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clickOn(btnCerrarInfo);
        }

    /**
     * Mostrar los errores si alguno de los 3 campos obligatorios esta vacío
     */
    @Test
        @Order (3)
        @DisplayName("Guardar dades actuals amb camp buits obligatoris")
        public void testA01_tipusGuardarDadesCampsBuits() {
            clickOn(tfInputNom);
            write("");
            tfInputNom.setText("");
            clickOn(btnGuardar);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clickOn(btnCerrarInfo);

            clickOn(tfInputNom);
            write("NomNou");
            clickOn(tfInputCognom1);
            write("");
            tfInputCognom1.setText("");
            clickOn(btnGuardar);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clickOn(btnCerrarInfo);

            clickOn(tfInputCognom1);
            write("CogNom1Nou");
            clickOn(tfInputCognom2);
            write("");
            tfInputCognom2.setText("");
            clickOn(btnGuardar);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clickOn(btnCerrarInfo);

            clickOn(tfInputCognom2);
            write("CogNom2Nou");

            clickOn(btnGuardar);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clickOn(btnCerrarInfo);
        }

    /**
     * Mostrar todos los errores relacionados con los inputs vacios si se quiere cambiar la contraseña
     */
    @Test
        @Order (4)
        @DisplayName("Guardar dades actuals amb camps password buits")
        public void testA01_tipusGuardarDadesCampsPassWordBuits() {
            clickOn(tfInputPassActual);
            write("sdfsf");
            tfInputPassActual.setText("sdfsf");
            clickOn(btnGuardar);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clickOn(btnCerrarInfo);

            clickOn(tfInputPassNou);
            write("1234");
            tfInputPassNou.setText("1234");
            clickOn(btnGuardar);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clickOn(btnCerrarInfo);

            clickOn(tfInputPassNou2);
            write("4321");
            tfInputPassNou2.setText("4321");
            clickOn(btnGuardar);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clickOn(btnCerrarInfo);

        }

    /**
     * Mostrar error y correción si el password actual es erroneo, o la contraseña tiene una longitud
     * menor a la obligatoria.
     */
    @Test
    @Order (5)
    @DisplayName("Guardar dades actuals amb camps password actual erroni")
    public void testA01_tipusGuardarDadesCampPassActualErroni() {

        clickOn(tfInputPassActual);
        write("sdfsf");
        tfInputPassActual.setText("sdfsf");

        clickOn(tfInputPassNou);
        write("4321");
        tfInputPassNou.setText("4321");

        clickOn(tfInputPassNou2);
        write("4321");
        tfInputPassNou2.setText("4321");


        clickOn(btnGuardar);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);

        clickOn(tfInputPassActual);
        write("");
        tfInputPassActual.setText("");
        write("1234");
        tfInputPassActual.setText("1234");

        clickOn(btnGuardar);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clickOn(btnCerrarInfoX);

        clickOn(tfInputPassNou);
        write("");
        tfInputPassNou.setText("");
        write("123456");
        tfInputPassNou.setText("123456");

        clickOn(tfInputPassNou2);
        write("");
        tfInputPassNou2.setText("");
        write("123456");
        tfInputPassNou2.setText("123456");

        clickOn(btnGuardar);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clickOn(btnCerrarInfoX);


    }

    /* Per obtenir els controls de la GUI */

    /**
     * Para obtener los controles de la GUI
     * @param query Cadena de la query
     * @param <T> Clase genery
     * @return query
     */
    private <T extends Node> T find(final String query) {
        return lookup(query).query();
    }
}
