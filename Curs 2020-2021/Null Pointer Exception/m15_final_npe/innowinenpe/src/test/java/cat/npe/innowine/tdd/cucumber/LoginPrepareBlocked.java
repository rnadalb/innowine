package cat.npe.innowine.tdd.cucumber;

import cat.npe.innowine.classes.Constants;
import cat.npe.innowine.controllers.LoginController;
import cat.npe.innowine.model.Usuario;
import io.cucumber.java.en.*;
import org.junit.jupiter.api.Assertions;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.List;

public class LoginPrepareBlocked {
    // Necesario porque trabajamos en BBDD
    private static EntityManagerFactory emf;
    private static LoginController utilsDB;

    @Given("^Una Base de datos")
    public void unaBaseDeDatos() {
        emf = Persistence.createEntityManagerFactory( "mariaDBConnectionTest");
    }

    @When("^los datos no están preparadas para el siguiente test$")
    public void losDatosNoEstanPreparadosPorAlSiguienteTest() {
        // Equivalente a SELECT * FROM tbl_usuari WHERE passwd = :passwd
        CriteriaBuilder cb = emf.getCriteriaBuilder();
        EntityManager manager = emf.createEntityManager();

        CriteriaQuery<Usuario> cbQuery = cb.createQuery(Usuario.class);
        Root<Usuario> c = cbQuery.from(Usuario.class);
        cbQuery.select(c);
        cbQuery.where(cb.equal(c.get("password"), "passwd"));

        Query query = manager.createQuery(cbQuery);

        List<Usuario> llista = query.getResultList();

        int esperat = 0;
        int actual = llista.size();

        if (actual == 0)
            Assertions.assertEquals(esperat, actual, "Tots els usuaris tenen la passwd correcta (fail)");
    }

    @Then("^el DevTeam actualiza la bbdd$")
    public void elDevteamActualitzaLaBbdd() {
        // Equivalente a  UPDATE tbl_usuari set bloquear = :bloquejado and intentos = :intentos
        CriteriaBuilder cb = emf.getCriteriaBuilder();
        CriteriaUpdate<Usuario> update = cb.createCriteriaUpdate(Usuario.class);
        Root<Usuario> p = update.from(Usuario.class);
        update.set("bloqueado", true);
        update.set("intentos", Constants.DEFAULT_MAX_INTENTS);
        EntityManager manager = emf.createEntityManager();
        manager.getTransaction().begin();
        manager.createQuery(update).executeUpdate();
        manager.getTransaction().commit();
    }
}

