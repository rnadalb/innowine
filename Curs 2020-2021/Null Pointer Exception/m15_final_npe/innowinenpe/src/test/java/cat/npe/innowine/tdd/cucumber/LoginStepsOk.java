package cat.npe.innowine.tdd.cucumber;

import cat.npe.innowine.states.LoginState;
import cat.npe.innowine.classes.UtilsDB;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class LoginStepsOk {
    // Necesario porqué trabajamos en BBDD
    private static EntityManagerFactory emf;
    private static UtilsDB utilsDB;

    // Nos indicará el resultado de la función a testear
    private LoginState result;

    @Before
    public void initDB() {
        emf = Persistence.createEntityManagerFactory( "mariaDBConnectionTest" );
    }

    @After
    public void closeDB(Scenario scenario){
        emf.close();
    }

    @Given("^Dado avaluador de Login$")
    public void dadoAvaluadorDeLogin() {
        utilsDB = new UtilsDB(emf);
    }

    @When("^usuario intoduce los datos correctamente (.+) i (.+)$")
    public void usuarioIntroduceLosDatosCorrectamenteUsuarioIPasswd(String usuario, String passwd) {
        result = utilsDB.checkPass(true,usuario, passwd);
    }

    @Then("^usuario accede al sistema$")
    public void usuarioAccedeAlSistema() {
        LoginState expected = LoginState.GRANTED;
        Assertions.assertEquals(expected, result, "utilsDB.CheckPass() falla");
    }
}

