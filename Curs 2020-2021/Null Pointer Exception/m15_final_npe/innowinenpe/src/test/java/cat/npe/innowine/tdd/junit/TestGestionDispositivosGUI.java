package cat.npe.innowine.tdd.junit;

import cat.npe.innowine.controllers.GestionDispositivosController;
import cat.npe.innowine.controllers.GestionUsuariosController;
import cat.npe.innowine.controllers.helpers.DispositivoHelper;
import cat.npe.innowine.controllers.helpers.UsuarioHelper;
import cat.npe.innowine.model.ConfiguracionSistema;
import cat.npe.innowine.model.Dispositivo;
import cat.npe.innowine.model.Usuario;
import cat.npe.innowine.states.CambioPasswordState;
import com.jfoenix.controls.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.jupiter.api.*;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationTest;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.concurrent.TimeoutException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS) /*Metodos no estaticos, el emf no puede ser estatico*/
public class TestGestionDispositivosGUI extends ApplicationTest {

    static EntityManagerFactory emf;
    private String pass = "$argon2i$v=19$m=1048576,t=4,p=8$i3DVFDdD9cFdMARWKp38ObOn08+D3mDGOM+yw+HrLH4$EbeTngaI0p20FbUv/FlRAXkvr1mPJWYsO1cKTDk+7Ztzo/3LbrRYfs+tEqwp2iBnC+9a7m0ed3kLk9agcv+UaacepPY8ph+hsDj97GroWzUyvTfI0StTkVREmI5Uu5F3/SDOAAC14E88CYpcf+7NTIe/SK4AhFD4PHhS3o4ycu0";

    JFXButton btnNouDispositiu, btnCerrarInfoDisp, btnCerrarInfoDispX, btnEsborrarDispositiu;
    JFXTextField tfCercador;

    @BeforeAll
    public void setUpClass() throws Exception {
        System.out.println("Mètode setUpClass() ...");
        // emf = Persistence.createEntityManagerFactory("cat.dam2.m15.hibernate.connexio");
        emf = Persistence.createEntityManagerFactory("mariaDBConnectionTest");
    }


    /**
     * Cargar vista , instanciar usuario de pruebas de la bbdd, y toda aquella configuracion necesaria para
     * que la vista funcione correctamente
     *
     * @param stage La escena
     * @throws Exception Error de la excepción
     */
    @Override
    public void start(Stage stage) throws Exception {
        System.out.println("Mètode Start() ...");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/GestionDispositivos.fxml"));
        Parent mainNode = (Parent) loader.load();
        GestionDispositivosController controller = loader.getController();
        controller.setEmf(this.emf);

        Dispositivo dispositivo = null;
        DispositivoHelper<Dispositivo> daoHelper = new DispositivoHelper<>(this.emf, Dispositivo.class);
        dispositivo = daoHelper.getById(2);

        Usuario usuario = null;
        UsuarioHelper<Usuario> daoHelperUsuario = new UsuarioHelper<>(this.emf, Usuario.class);
        usuario = daoHelperUsuario.findUserByEmail("amourou@ies-eugeni.cat");


        if (dispositivo == null) {
            System.out.println("No se ha encontrado el disopitivo de prueba con id 1");
            tearDownClass();
            System.exit(1);
        } else {
            dispositivo.setIdDispositivo("100");
            dispositivo.setIdUsuario(dispositivo.getIdUsuario());
            dispositivo.setDescripcion("test2");
            dispositivo.setLatitud(43.214242);
            dispositivo.setLongitud(23.234234);
            daoHelper.update(dispositivo);
            usuario.setNombre("Admin");
            usuario.setApellido1("Super");
            usuario.setApellido2("Admin");
            usuario.setPassword(pass);
            usuario.setIntentos(0);
            usuario.setBloqueado(false);
            usuario.setCambioPassword(false);
            usuario.setAdmin(true);
            daoHelperUsuario.update(usuario);
            ConfiguracionSistema.getInstancia().setIntentos(4);
            ConfiguracionSistema.getInstancia().setMinimoPassword(6);
            ConfiguracionSistema.getInstancia().setDireccionSmtp("smtp.gmail.com");
            ConfiguracionSistema.getInstancia().setUsuarioSmtp("innowine.npe@gmail.com");
            ConfiguracionSistema.getInstancia().setPasswordSmtp("OK4J/Utfyz6h0r6xDPdO2N1l/ZR/Z4PiI0GxUl8JcFKWgABwUp7aG8Pn8i4M2ku59MxndTxlyDTTXmHALZL644iGLj06xHcvfUICmW7WrvE=");
            ConfiguracionSistema.getInstancia().setPuertoSmtp(587);
            ConfiguracionSistema.getInstancia().setProjection("WGS_84");

        }

        controller.setUsuarioLogueado(usuario);

        Scene scene = new Scene(mainNode);
        stage.setScene(scene);
        stage.show();
        stage.toFront();

    }

    /**
     * Comprobar que Existan los botones ... antes de los test ...
     *
     * @throws Exception Error de la excepción
     */
    @BeforeEach
    public void setUp() throws Exception {
        System.out.println("Mètode setUp() ...");
        // RNB. more help --> https://github.com/TestFX/TestFX/wiki/Queries

        btnNouDispositiu = find("#btnNouDispositiu");
        tfCercador = find("#tfCercador");

        btnEsborrarDispositiu = find("#btnEsborrarDispositiu");
        btnCerrarInfoDisp = find("#btnCerrarInfoDisp");
        btnCerrarInfoDispX = find("#btnCerrarInfoDispX");
    }

    /**
     * Resetear buffers de teclado y ratón, para resetear posicion cursor y teclado ...
     *
     * @throws TimeoutException Error de excepción de tiempo maximo de ejecución
     */
    @AfterEach
    public void tearDown() throws TimeoutException {
        System.out.println("Mètode tearDown() ...");
        FxToolkit.hideStage();

        // Resetear buffers de teclado y ratón, para resetear posicion cursor y teclado ...
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    /**
     * Cerrar la conexión con la base de datos al finalizar los tests
     *
     * @throws TimeoutException Error de excepción de tiempo maximo de ejecución
     */
    @AfterAll
    public void tearDownClass() throws TimeoutException {
        System.out.println("Mètode tearDownClass() ...");
        if (emf != null && emf.isOpen())
            emf.close();
    }

    /**
     * Comprobar si tenemos acceso a todos los controles, inputs y botones de la vista
     */
    @Test
    @Order(1)
    @DisplayName("Comprovar si tenim accés als controls")
    public void testA00_WidgestsExist() {
        final String errMsg = "Algun dels widgets no pot obtenir-se";

        Assertions.assertNotNull(btnNouDispositiu, errMsg);

        Assertions.assertNotNull(btnCerrarInfoDisp, errMsg);
        Assertions.assertNotNull(btnCerrarInfoDispX, errMsg);
        Assertions.assertNotNull(btnEsborrarDispositiu, errMsg);

        Assertions.assertNotNull(tfCercador, errMsg);

    }

    /**
     * Buscador de dispositivos por descripción
     */
    @Test
    @Order(2)
    @DisplayName("Cercador por descripción")
    public void testA01_tipusCercador() {
        clickOn(tfCercador);
        write("test2");
        tfCercador.setText("test2");
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clickOn(tfCercador);
        write("");
        tfCercador.setText("");
        clickOn(tfCercador);

        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(tfCercador);

    }

    /**
     * Buscador de dispositivos por id dispositivo
     */
    @Test
    @Order(3)
    @DisplayName("Cercador por id dispositivo")
    public void testA02_tipusCercador() {
        clickOn(tfCercador);
        write("100");
        tfCercador.setText("100");
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clickOn(tfCercador);
        write("");
        tfCercador.setText("");
        clickOn(tfCercador);

        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(tfCercador);
    }

    /**
     * Muestra pantalla nuevo dispositivo
     */
    @Test
    @Order(4)
    @DisplayName("Pantalla nuevo dispositivo")
    public void testA03_nuevoDispositivo() {
        clickOn(btnNouDispositiu);

        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Para obtener los controles de la GUI
     * @param query Cadena de la query
     * @param <T> Clase genery
     * @return query
     */
    private <T extends Node> T find(final String query) {
        return lookup(query).query();
    }

}
