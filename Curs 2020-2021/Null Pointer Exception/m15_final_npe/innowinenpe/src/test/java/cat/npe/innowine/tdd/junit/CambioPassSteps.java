package cat.npe.innowine.tdd.junit;

import cat.npe.innowine.states.LoginState;
import cat.npe.innowine.classes.UtilsDB;
import cat.npe.innowine.controllers.helpers.SecurityHelper;
import cat.npe.innowine.controllers.helpers.UsuarioHelper;
import cat.npe.innowine.model.Usuario;
import org.junit.jupiter.api.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;


@DisplayName("Test Case: Cambio de Password")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class CambioPassSteps {

    private static EntityManagerFactory emf;
    private static UtilsDB utilsDB;

    private static final String LOGIN = "oriol@offing.es";
    private static final  String PASSWD_ACTUALOK = "1234";
    private static final  String PASSWD_ACTUALFAIL = "0000";
    private static final  String PASSWD_NEWOK = "123";
    private static final  String PASSWD_NEWFAIL = "124";

    /**
     * Este mètodo se ejecuta solo 1 vez al principio
     */
    @BeforeAll
    static void setUp() {
        emf = Persistence.createEntityManagerFactory("mariaDBConnectionTest");
        utilsDB = new UtilsDB(emf);
        SecurityHelper sh = new SecurityHelper();
        // Preparem la BBDD per realitzar els tests
        UsuarioHelper<Usuario> helper = new UsuarioHelper<>(emf, Usuario.class);
        Usuario usuari = helper.findUserByEmail(LOGIN);
        usuari.setBloqueado(false);
        usuari.setIntentos(4);
        usuari.setPassword(sh.passwordEncrypt(PASSWD_ACTUALOK));
        helper.update(usuari);
        //utilsDB.setUsuarioLogueado(usuari);

    }

    /**
     * Este mètodo se ejecuta al final del test
     */
    @AfterAll
    static void tearDown() {
        if (emf != null && emf.isOpen())
            emf.close();
    }

    @Test
    @DisplayName("El usuario introduce mal el password actual")
    @Order(1)
    void passwordActualErroneo() {
        LoginState esperat = LoginState.FAILED;
        LoginState resultat = utilsDB.checkPass(true,LOGIN, PASSWD_ACTUALFAIL);
        assertEquals(esperat, resultat, String.format("dBUtils.checkPassword(%s) ha fallado!", PASSWD_ACTUALOK));
    }

    @Test
    @DisplayName("El usuario introduce mal el password repetido")
    @Order(2)
    void passwordRepetidoErroneo() {
        Boolean esperat = false;
        Boolean resultat = PASSWD_NEWOK.equals(PASSWD_NEWFAIL);
        assertEquals(esperat, resultat, String.format("dBUtils.checkPassword(%s,%s) ha fallado!", LOGIN, PASSWD_ACTUALOK));
    }


    @Test
    @DisplayName("El usuario introduce los datos correctamente")
    @Order(3)
    void usuarioIntoduceDatosCorrectamente() {
        LoginState esperat = LoginState.GRANTED;
        LoginState resultat = (utilsDB.checkPass(true, LOGIN, PASSWD_ACTUALOK)  == LoginState.GRANTED && PASSWD_NEWOK.equals(PASSWD_NEWOK)==true)?LoginState.GRANTED:LoginState.FAILED;
        assertEquals(esperat, resultat, String.format("dBUtils.checkPassword(%s,%s) ha fallado!", LOGIN, PASSWD_ACTUALOK));
    }
}
