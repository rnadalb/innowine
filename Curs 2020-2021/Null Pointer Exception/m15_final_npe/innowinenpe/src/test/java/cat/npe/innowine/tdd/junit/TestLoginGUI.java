package cat.npe.innowine.tdd.junit;

import cat.npe.innowine.controllers.LoginController;
import cat.npe.innowine.controllers.helpers.UsuarioHelper;
import cat.npe.innowine.model.ConfiguracionSistema;
import cat.npe.innowine.model.Usuario;
import cat.npe.innowine.states.CambioPasswordState;
import com.jfoenix.controls.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.jupiter.api.*;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationTest;
import sun.rmi.runtime.Log;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.concurrent.TimeoutException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS) /*Metodos no estaticos, el emf no puede ser estatico*/
public class TestLoginGUI extends ApplicationTest{

    static EntityManagerFactory emf;
    private String pass = "$argon2i$v=19$m=1048576,t=4,p=8$B7dbUAyb4Af2+jhwbyZGz2flYKooA3SyiaNgvoDzowo$zWcUzVXrAnTS1X6KL3NG7p+Cgzg1gK9k7i4XJQdsgKhqoIT8c00ptoexrEioJBFh4FyZn2mavOF1YIdLOEhvvNkhpwIkAmkEpGwCZaHGmqwreCm26g2gzrevjcW6bBDx/Tw9z6OmCUSTL5A/ZnqgVJiUD2+W4wl1VwfpfSFZUTo";

    JFXButton btnLogin, btnSalir;
    JFXTextField tfInputEmail;
    JFXPasswordField pfInputPassword;
    Label lError;

    @BeforeAll
    public void setUpClass() throws Exception {
        System.out.println("Mètode setUpClass() ...");
        // emf = Persistence.createEntityManagerFactory("cat.dam2.m15.hibernate.connexio");
        emf = Persistence.createEntityManagerFactory("mariaDBConnectionTest");
    }

    /**
     * Cargar vista , instanciar usuario de pruebas de la bbdd, y toda aquella configuracion necesaria para
     * que la vista funcione correctamente
     * @param stage La escena
     * @throws Exception Error de la excepción
     */
    @Override
    public void start(Stage stage) throws Exception {
        System.out.println("Mètode Start() ...");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/Login.fxml"));
        Parent mainNode = (Parent) loader.load();
        LoginController controller = loader.getController();
        controller.setEmf(this.emf);

        Usuario usuario = null;
        UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(this.emf, Usuario.class);
        usuario = daoHelper.findUserByEmail("amourou@ies-eugeni.cat");

        if(usuario== null){
            System.out.println("No se ha encontrado el usuario de prueba: dummy@test.es");
            tearDownClass();
            System.exit(1);
        }else{
            usuario.setNombre("Ayoub");
            usuario.setApellido1("Mourou");
            usuario.setApellido2("Bounab");
            usuario.setPassword(pass);
            usuario.setIntentos(0);
            usuario.setBloqueado(false);
            usuario.setCambioPassword(false);
            daoHelper.update(usuario);
            ConfiguracionSistema.getInstancia().setIntentos(4);
            ConfiguracionSistema.getInstancia().setMinimoPassword(6);
            ConfiguracionSistema.getInstancia().setAsuntoCorreo("Test");
            ConfiguracionSistema.getInstancia().setCuerpoCorreo("Test Grafico Login");
            ConfiguracionSistema.getInstancia().setDireccionSmtp("smtp.gmail.com");
            ConfiguracionSistema.getInstancia().setPasswordSmtp("OK4J/Utfyz6h0r6xDPdO2N1l/ZR/Z4PiI0GxUl8JcFKWgABwUp7aG8Pn8i4M2ku59MxndTxlyDTTXmHALZL644iGLj06xHcvfUICmW7WrvE=");
            ConfiguracionSistema.getInstancia().setUsuarioSmtp("innowine.npe@gmail.com");
            ConfiguracionSistema.getInstancia().setPuertoSmtp(587);
            ConfiguracionSistema.getInstancia().setProtocoloSmtp("SSL");

            CambioPasswordState cps = CambioPasswordState.NOTMINLENGTH;
            cps.setMessage("La longitud minima de la nova contrasenya ha de ser de "+ ConfiguracionSistema.getInstancia().getMinimoPassword().toString()+" caracters.");

        }
        controller.setUsuarioLogueado(usuario);

        Scene scene = new Scene(mainNode);
        stage.setScene(scene);
        stage.show();
        stage.toFront();
    }

    /**
     * Comprobar que Existan los botones ... antes de los test ...
     * @throws Exception Error de la excepción
     */
    @BeforeEach
    public void setUp() throws Exception {
        System.out.println("Mètode setUp() ...");
        // RNB. more help --> https://github.com/TestFX/TestFX/wiki/Queries

        tfInputEmail = find("#tfInputEmail");  // --> El valor fx:id

        pfInputPassword = find("#pfInputPassword");

        lError= find("#lError");

        btnLogin = find("#btnLogin");
        btnSalir = find("#btnSalir");

    }

    /**
     * Resetear buffers de teclado y ratón, para resetear posicion cursor y teclado ...
     * @throws TimeoutException Error de excepción de tiempo maximo de ejecución
     */
    @AfterEach
    public void tearDown() throws TimeoutException {
        System.out.println("Mètode tearDown() ...");
        FxToolkit.hideStage();

        // Resetear buffers de teclado y ratón, para resetear posicion cursor y teclado ...
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    /**
     * Comprobar si tenemos acceso a todos los controles, inputs y botones de la vista
     */
    @Test
    @Order (1)
    @DisplayName("Comprovar si tenim accés als controls")
    public void testA00_WidgestsExist() {
        final String errMsg = "Algun dels widgets no pot obtenir-se";

        //assertNotNull(errMsg, btnGuardar);
        //assertNotNull(errMsg, tfInputNom);

        Assertions.assertNotNull(tfInputEmail, errMsg);
        Assertions.assertNotNull(pfInputPassword, errMsg);

        Assertions.assertNotNull(lError, errMsg);

        Assertions.assertNotNull(btnLogin, errMsg);
        Assertions.assertNotNull(btnSalir, errMsg);

    }

    /**
     * Mostrar el error si alguno de los 2 campos obligatorios esta vacío
     */

    @Test
    @Order (2)
    @DisplayName("Mostrar error amb els camps obligatoris buits")
    public void testA01_mostrarErrorCamposVacios() {
        clickOn(btnLogin);

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clickOn(tfInputEmail);
        write("usuario@gmail.com");
        clickOn(btnLogin);

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        tfInputEmail.setText("");
        clickOn(pfInputPassword);
        write("1234");
        clickOn(btnLogin);

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Mostrar el error cuando los datos introducidos son incorrectos
     */

    @Test
    @Order (3)
    @DisplayName("Mostrar error con datos incorrectos")
    public void testA02_mostrarErrorDatosIncorrectos() {
        clickOn(tfInputEmail);
        write("usuario@gmail.com");
        clickOn(pfInputPassword);
        write("1234");
        clickOn(btnLogin);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Mostrar el error cuando los datos introducidos son incorrectos 4 veces y el usuario es bloqueado
     */

    @Test
    @Order (4)
    @DisplayName("Mostrar error al bloquear usuario")
    public void testA03_mostrarErrorBloqueo() {
        clickOn(tfInputEmail);
        write("amourou@ies-eugeni.cat");
        clickOn(pfInputPassword);
        write("1234");
        clickOn(btnLogin);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clickOn(btnLogin);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clickOn(btnLogin);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clickOn(btnLogin);

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Entar al menu princiapl al introducir los datos correctamente
     */

    @Test
    @Order (5)
    @DisplayName("Entar menu principal con datos correctos")
    public void testA04_EntrarMenuPrincipalDatosCorrectos() {
        clickOn(tfInputEmail);
        write("amourou@ies-eugeni.cat");
        clickOn(pfInputPassword);
        write("12345");
        clickOn(btnLogin);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    /**
     * Cerrar la conexión con la base de datos al finalizar los tests
     * @throws TimeoutException Error de excepción de tiempo maximo de ejecución
     */
    @AfterAll
    public void tearDownClass() throws TimeoutException {
        System.out.println("Mètode tearDownClass() ...");
        if (emf != null && emf.isOpen())
            emf.close();
    }

    /* Per obtenir els controls de la GUI */
    /**
     * Para obtener los controles de la GUI
     * @param query Cadena de la query
     * @param <T> Clase genery
     * @return query
     */
    private <T extends Node> T find(final String query) {
        return lookup(query).query();
    }

}
