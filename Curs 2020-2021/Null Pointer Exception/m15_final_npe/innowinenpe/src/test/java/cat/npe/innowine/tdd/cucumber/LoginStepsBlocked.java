package cat.npe.innowine.tdd.cucumber;

import cat.npe.innowine.states.LoginState;
import cat.npe.innowine.classes.UtilsDB;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.*;
import io.cucumber.java.en.*;
import org.junit.jupiter.api.Assertions;


import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class LoginStepsBlocked {
    // Necesario porque trabajamos en BBDD
    private static EntityManagerFactory emf;
    private static UtilsDB utilsDB;

    // Nos indicarà el resultado de la función a testear
    private LoginState result;

    @Before
    public void initDB() {
        emf = Persistence.createEntityManagerFactory( "mariaDBConnectionTest" );
    }

    @After
    public void closeDB(Scenario scenario){
        emf.close();
    }

    @Given("^Dado evaluador de Login con x intentos fallidos$")
    public void dodoAvaluadorDeLoginConIntentosFallidos() {
        utilsDB = new UtilsDB(emf);
    }

    @When("^usuario introduce los datos mal por x vez (.+) i (.+)$")
    public void usuarioIntroduceLosDatosMalPorTerceraVezUsuarioIPasswd(String usuari, String passwd) {
        result = utilsDB.checkPass(true,usuari, passwd);
    }

    @Then("^usuario no accede al sistema y se bloqueado")
    public void usuarioNoAccedeAlSistemaIEsBloqueado() {
        LoginState expected = LoginState.BLOCKED;
        Assertions.assertEquals(expected, result, "utilsDB.CheckPass() falla");
    }


}

