package cat.npe.innowine.tdd.junit;

import cat.npe.innowine.classes.DAOHelper;
import cat.npe.innowine.controllers.ConfiguracionSistemaController;
import cat.npe.innowine.controllers.MiPerfilController;
import cat.npe.innowine.controllers.helpers.UsuarioHelper;
import cat.npe.innowine.model.ConfiguracionSistema;
import cat.npe.innowine.model.Usuario;
import cat.npe.innowine.states.CambioPasswordState;
import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.jupiter.api.*;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationTest;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.concurrent.TimeoutException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS) /*Metodos no estaticos, el emf no puede ser estatico*/
public class TestConfigGUI extends ApplicationTest {

    static EntityManagerFactory emf;
    private String pass="$argon2i$v=19$m=1048576,t=4,p=8$pFi8GwwL0U5TUGSynv4GSTh9ChO4DCo7+XCU6mLUsEA$DEafzywUqgxqid7Qi23g1fqd8GQ/gEg/WZc28aFB8M2uSFiZqJW0DwcjBMf+fnlr+QTFbt5PY9N241/xIoeeqt+/U8NQB2kKytrnFr47DSl5jpyBzk45maOQmbBymbWUNI7CeByeLSVvV/P8cI29BW10k06fujPHUCuJtA04Vsg";

    JFXButton btnGuardarPassConf, btnGuardarInflux, btnGuardarEmail, btnCerrarInfo, btnCerrarInfoX;
    Label lbError, lbSuccess;
    JFXTextField tfIntents, tfLongitud, tfInfluxHost, tfInfluxPort, tfInfluxBBDD, tfInfluxUsuari, tfEmailSMTP, tfEmailPort, tfEmailUsuari;
    JFXTextArea taEmailSubject, taEmailBody;
    JFXPasswordField pfInfluxPassword, pfEmailPassword;

    @BeforeAll
    public void setUpClass() throws Exception {
        System.out.println("Mètode setUpClass() ...");
        // emf = Persistence.createEntityManagerFactory("cat.dam2.m15.hibernate.connexio");
        emf = Persistence.createEntityManagerFactory("mariaDBConnectionTest");
    }

    /**
     * Cargar vista , instanciar usuario de pruebas de la bbdd, y toda aquella configuracion necesaria para
     * que la vista funcione correctamente
     * @param stage La escena
     * @throws Exception Error de la excepción
     */
    @Override
    public void start(Stage stage) throws Exception {
        System.out.println("Mètode Start() ...");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/ConfiguracionSistema.fxml"));
        Parent mainNode = (Parent) loader.load();
        ConfiguracionSistemaController controller = loader.getController();
        controller.setEmf(this.emf);
        DAOHelper<ConfiguracionSistema> configDao = new DAOHelper<>(emf,ConfiguracionSistema.class);

        ConfiguracionSistema query = configDao.getById(1);
        if(query== null) {
            System.out.println("No se ha encontrado la configuracion del sistema");
            tearDownClass();
            System.exit(1);
        }else{
            ConfiguracionSistema.getInstancia().setId(query.getId());
            ConfiguracionSistema.getInstancia().setIntentos(query.getIntentos());
            ConfiguracionSistema.getInstancia().setMinimoPassword(query.getMinimoPassword());
            ConfiguracionSistema.getInstancia().setAsuntoCorreo(query.getAsuntoCorreo());
            ConfiguracionSistema.getInstancia().setCuerpoCorreo(query.getCuerpoCorreo());
            ConfiguracionSistema.getInstancia().setDireccionSmtp(query.getDireccionSmtp());
            ConfiguracionSistema.getInstancia().setPuertoSmtp(query.getPuertoSmtp());
            ConfiguracionSistema.getInstancia().setProtocoloSmtp(query.getProtocoloSmtp());
            ConfiguracionSistema.getInstancia().setUsuarioSmtp(query.getUsuarioSmtp());
            ConfiguracionSistema.getInstancia().setPasswordSmtp(query.getPasswordSmtp());
            ConfiguracionSistema.getInstancia().setHostInflux(query.getHostInflux());
            ConfiguracionSistema.getInstancia().setPuertoInflux(query.getPuertoInflux());
            ConfiguracionSistema.getInstancia().setBbddInflux(query.getBbddInflux());
            ConfiguracionSistema.getInstancia().setUsuarioInflux(query.getUsuarioInflux());
            ConfiguracionSistema.getInstancia().setPasswordInflux(query.getPasswordInflux());

        }

//        Usuario usuario = null;
//        UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(this.emf, Usuario.class);
//        usuario = daoHelper.findUserByEmail("dummy@test.es");
//
//        if(usuario== null){
//            System.out.println("No se ha encontrado el usuario de prueba: dummy@test.es");
//            tearDownClass();
//            System.exit(1);
//        }else{
//            usuario.setNombre("Dummy");
//            usuario.setApellido1("Test");
//            usuario.setApellido2("Test Test");
//            usuario.setPassword(pass);
//            usuario.setIntentos(0);
//            usuario.setBloqueado(false);
//            usuario.setCambioPassword(false);
//            daoHelper.update(usuario);
//            ConfiguracionSistema.getInstancia().setIntentos(4);
//            ConfiguracionSistema.getInstancia().setMinimoPassword(6);
//            CambioPasswordState cps = CambioPasswordState.NOTMINLENGTH;
//            cps.setMessage("La longitud minima de la nova contrasenya ha de ser de "+ ConfiguracionSistema.getInstancia().getMinimoPassword().toString()+" caracters.");
//
//        }

        controller.setUsuarioLogueado(null);
        controller.setFormData();

        Scene scene = new Scene(mainNode);
        stage.setScene(scene);
        stage.show();
        stage.toFront();
    }


    /**
     * Comprobar que Existan las variables ... antes de los test ...
     * @throws Exception Error de la excepción
     */
    @BeforeEach
    public void setUp() throws Exception {
        System.out.println("Mètode setUp() ...");
        // RNB. more help --> https://github.com/TestFX/TestFX/wiki/Queries

        tfIntents = find("#tfIntents");
        tfLongitud = find("#tfLongitud");
        tfInfluxHost = find("#tfInfluxHost");
        tfInfluxPort = find("#tfInfluxPort");
        tfInfluxBBDD = find("#tfInfluxBBDD");
        tfInfluxUsuari = find("#tfInfluxUsuari");
        tfEmailSMTP = find("#tfEmailSMTP");
        tfEmailPort = find("#tfEmailPort");
        tfEmailUsuari = find("#tfEmailUsuari");

        lbError= find("#lbError");
        lbSuccess = find("#lbSuccess");

        btnGuardarPassConf = find("#btnGuardarPassConf");
        btnGuardarInflux = find("#btnGuardarInflux");
        btnGuardarEmail = find("#btnGuardarEmail");
        btnCerrarInfo = find("#btnCerrarInfo");
        btnCerrarInfoX = find("#btnCerrarInfoX");


        taEmailSubject = find("#taEmailSubject");
        taEmailBody = find("#taEmailBody");

        pfInfluxPassword = find("#pfInfluxPassword");
        pfEmailPassword = find("#pfEmailPassword");
    }

    /**
     * Resetear buffers de teclado y ratón, para resetear posicion cursor y teclado ...
     * @throws TimeoutException Error de excepción de tiempo maximo de ejecución
     */
    @AfterEach
    public void tearDown() throws TimeoutException {
        System.out.println("Mètode tearDown() ...");
        FxToolkit.hideStage();

        // Resetear buffers de teclado y ratón, para resetear posicion cursor y teclado ...
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    /**
     * Cerrar la conexión con la base de datos al finalizar los tests
     * @throws TimeoutException Error de excepción de tiempo maximo de ejecución
     */
    @AfterAll
    public void tearDownClass() throws TimeoutException {
        System.out.println("Mètode tearDownClass() ...");
        if (emf != null && emf.isOpen())
            emf.close();
    }

    /**
     * Comprobar si tenemos acceso a todos los controles, inputs y botones de la vista
     */
    @Test
    @Order (1)
    @DisplayName("Comprovar si tenim accés als controls")
    public void testA00_WidgestsExist() {
        final String errMsg = "Algun dels widgets no pot obtenir-se";

        Assertions.assertNotNull(tfIntents, errMsg);
        Assertions.assertNotNull(tfLongitud, errMsg);
        Assertions.assertNotNull(tfInfluxHost, errMsg);
        Assertions.assertNotNull(tfInfluxPort, errMsg);
        Assertions.assertNotNull(tfInfluxBBDD, errMsg);
        Assertions.assertNotNull(tfInfluxUsuari, errMsg);
        Assertions.assertNotNull(tfEmailSMTP, errMsg);
        Assertions.assertNotNull(tfEmailPort, errMsg);
        Assertions.assertNotNull(tfEmailUsuari, errMsg);

        Assertions.assertNotNull(btnGuardarPassConf, errMsg);
        Assertions.assertNotNull(btnGuardarInflux, errMsg);
        Assertions.assertNotNull(btnGuardarEmail, errMsg);

        Assertions.assertNotNull(btnCerrarInfo, errMsg);
        Assertions.assertNotNull(btnCerrarInfoX, errMsg);

        Assertions.assertNotNull(taEmailSubject, errMsg);
        Assertions.assertNotNull(taEmailBody, errMsg);

        Assertions.assertNotNull(pfInfluxPassword, errMsg);
        Assertions.assertNotNull(pfEmailPassword, errMsg);

    }

    /**
     * Guardar los datos actuales recogidos al cargar el formulario de mi perfil
     */
    @Test
    @Order (2)
    @DisplayName("Guardar dades actuals passwords ")
    public void testA01_tipusGuardarDadesActualsPasswords() {
        clickOn(btnGuardarPassConf);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
    }
    @Test
    @Order (3)
    @DisplayName("Guardar dades actuals influx")
    public void testA02_tipusGuardarDadesActualsInflux() {
        clickOn(btnGuardarInflux);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
    }
    @Test
    @Order (4)
    @DisplayName("Guardar dades actuals email")
    public void testA03_tipusGuardarDadesActualsEmail() {
        clickOn(btnGuardarEmail);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
    }

    /**
     * Mostrar los errores si alguno de los 3 campos obligatorios esta vacío
     */
    @Test
    @Order (5)
    @DisplayName("Guardar dades actuals amb camps passwords buits obligatoris")
    public void testA01_tipusGuardarDadesCampsBuitsPassword() {
        clickOn(tfIntents);
        write("");
        tfIntents.setText("");
        clickOn(btnGuardarPassConf);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
        clickOn(tfIntents);
        write(ConfiguracionSistema.getInstancia().getIntentos().toString());
        tfIntents.setText(ConfiguracionSistema.getInstancia().getIntentos().toString());
        clickOn(tfLongitud);
        write("");
        tfLongitud.setText("");
        clickOn(btnGuardarPassConf);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
    }

    /**
     * Mostrar los errores si alguno de los 3 campos obligatorios esta vacío
     */
    @Test
    @Order (6)
    @DisplayName("Guardar dades actuals amb camps influx buits obligatoris")
    public void testA01_tipusGuardarDadesCampsBuitsInflux() {
        clickOn(tfInfluxHost);
        write("");
        tfInfluxHost.setText("");
        clickOn(btnGuardarInflux);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
        clickOn(tfInfluxHost);
        write(ConfiguracionSistema.getInstancia().getHostInflux());
        tfInfluxHost.setText(ConfiguracionSistema.getInstancia().getHostInflux());

        clickOn(tfInfluxPort);
        write("");
        tfInfluxPort.setText("");
        clickOn(btnGuardarInflux);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
        clickOn(tfInfluxPort);
        write(ConfiguracionSistema.getInstancia().getPuertoInflux().toString());
        tfInfluxPort.setText(ConfiguracionSistema.getInstancia().getPuertoInflux().toString());

        clickOn(tfInfluxBBDD);
        write("");
        tfInfluxBBDD.setText("");
        clickOn(btnGuardarInflux);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
        clickOn(tfInfluxBBDD);
        write(ConfiguracionSistema.getInstancia().getBbddInflux());
        tfInfluxBBDD.setText(ConfiguracionSistema.getInstancia().getBbddInflux());

        clickOn(tfInfluxUsuari);
        write("");
        tfInfluxUsuari.setText("");
        clickOn(btnGuardarInflux);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
        clickOn(tfInfluxUsuari);
        write(ConfiguracionSistema.getInstancia().getUsuarioInflux());
        tfInfluxUsuari.setText(ConfiguracionSistema.getInstancia().getUsuarioInflux());

        clickOn(pfInfluxPassword);
        write("");
        pfInfluxPassword.setText("");
        clickOn(btnGuardarInflux);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);

    }

    /**
     * Mostrar los errores si alguno de los 3 campos obligatorios esta vacío
     */
    @Test
    @Order (7)
    @DisplayName("Guardar dades actuals amb camps email buits obligatoris")
    public void testA01_tipusGuardarDadesCampsBuitsEmail() {
        clickOn(tfEmailSMTP);
        write("");
        tfEmailSMTP.setText("");
        clickOn(btnGuardarEmail);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
        clickOn(tfEmailSMTP);
        write(ConfiguracionSistema.getInstancia().getDireccionSmtp());
        tfEmailSMTP.setText(ConfiguracionSistema.getInstancia().getDireccionSmtp());

        clickOn(tfEmailPort);
        write("");
        tfEmailPort.setText("");
        clickOn(btnGuardarEmail);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
        clickOn(tfEmailPort);
        write(ConfiguracionSistema.getInstancia().getPuertoSmtp().toString());
        tfEmailPort.setText(ConfiguracionSistema.getInstancia().getPuertoSmtp().toString());

        clickOn(tfEmailUsuari);
        write("");
        tfEmailUsuari.setText("");
        clickOn(btnGuardarEmail);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
        clickOn(tfEmailUsuari);
        write(ConfiguracionSistema.getInstancia().getUsuarioSmtp());
        tfEmailUsuari.setText(ConfiguracionSistema.getInstancia().getUsuarioSmtp());

        clickOn(pfEmailPassword);
        write("");
        pfEmailPassword.setText("");
        clickOn(btnGuardarEmail);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
        clickOn(pfEmailPassword);
        write("password");
        pfEmailPassword.setText("password");

        clickOn(taEmailSubject);
        write("");
        taEmailSubject.setText("");
        clickOn(btnGuardarEmail);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
        clickOn(taEmailSubject);
        write(ConfiguracionSistema.getInstancia().getAsuntoCorreo());
        taEmailSubject.setText(ConfiguracionSistema.getInstancia().getAsuntoCorreo());

        clickOn(taEmailBody);
        write("");
        taEmailBody.setText("");
        clickOn(btnGuardarEmail);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickOn(btnCerrarInfo);
    }


    /* Per obtenir els controls de la GUI */

    /**
     * Para obtener los controles de la GUI
     * @param query Cadena de la query
     * @param <T> Clase genery
     * @return query
     */
    private <T extends Node> T find(final String query) {
        return lookup(query).query();
    }
}
