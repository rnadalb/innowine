package cat.npe.innowine.tdd.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * Carregarà les històries i les passarà a Junit per ser executades
 */
@RunWith (Cucumber.class)
@CucumberOptions (plugin = "pretty", monochrome =  false, strict = false, // monochrome = true -> colorins
        features = "src/test/resources/features",
        glue = "cat.npe.innowine.tdd.cucumber") // package d'on estan els steps
public class CucumberRunner {

}
