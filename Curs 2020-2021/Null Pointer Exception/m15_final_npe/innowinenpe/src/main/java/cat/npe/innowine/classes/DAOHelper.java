package cat.npe.innowine.classes;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Helper para realizar las consulas basicas en la base de datos: insert, update y delete
 * @param <T> Entidad que relaciona la clase con una tabla
 */
public class DAOHelper<T> {

    // <editor-fold defaultstate="collapsed" desc="Variables">

    /**
     * La conexión con la base de datos
     */
    protected EntityManagerFactory emf;

    /**
     * La clase con el modelo entidad
     */
    protected Class<T> parameterClass;

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Constructor">

    /**
     * Constructor
     * @param emf la conexión con la BBDD
     * @param parameterClass la entidad en forma de clase equivalente
     */
    public DAOHelper(EntityManagerFactory emf, Class<T> parameterClass) {
        this.emf = emf;
        this.parameterClass = parameterClass;
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Métodos">

    /**
     * Inserta un registro en la BBDD
     * @param t  Objeto instanciado de una clase que representa una entidad / tabla de la BBDD
     */
    public void insert(T t) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.persist(t);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    /**
     * Actualiza un registro en la BBDD
     * @param t  Objeto instanciado de una clase que representa una entidad / tabla de la BBDD
     */
    public void update(T t) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.merge(t);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    /**
     * Elimina en la BBDD
     * @param id  id de un objeto instanciado de una clase que representa una entidad / tabla dla BBDD
     */
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();
        T t = em.find((Class<T>) parameterClass, id);

        try {
            if (t != null) {
                em.getTransaction().begin();
                em.remove(t);
                em.getTransaction().commit();
            }
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    /**
     * Devuelve un objeto instanciado que representa un registro de la BBDD
     * @param id  Busca el registro por id en la BBDD
     * @return  Objeto instanciado con la respuesta si existe en la BBDD
     */

    public T getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        try {
            return em.find((Class<T>) parameterClass, id);
        } finally {
            em.close();
        }
    }


    /**
     * Devuelve en forma de lista todos los registros.
     * @return lista con los registros instanciados en objetos
     */
    public List<T> getAll() {
        CriteriaBuilder cb = this.emf.getCriteriaBuilder();
        EntityManager manager = this.emf.createEntityManager();

        CriteriaQuery<T> cbQuery = cb.createQuery((Class<T>) parameterClass);
        Root<T> c = cbQuery.from((Class<T>) parameterClass);
        cbQuery.select(c);

        Query query = manager.createQuery(cbQuery);

        return query.getResultList();
    }

    /**
     * Escribe en la consola todos los registros de la BBDD
     */
    public void printAll() {
        List<T> list = getAll();
        list.forEach(System.out::println);
    }

    // </editor-fold>
}