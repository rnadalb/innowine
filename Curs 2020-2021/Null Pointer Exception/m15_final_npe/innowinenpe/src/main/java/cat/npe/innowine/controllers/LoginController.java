package cat.npe.innowine.controllers;

import cat.npe.innowine.classes.Alerta;
import cat.npe.innowine.classes.UtilsDB;
import cat.npe.innowine.controllers.helpers.EmailHelper;
import cat.npe.innowine.controllers.helpers.SecurityHelper;
import cat.npe.innowine.controllers.helpers.UsuarioHelper;
import cat.npe.innowine.exceptions.ConfirmationDialogException;
import cat.npe.innowine.exceptions.SelectedYesConfirmationDialogException;
import cat.npe.innowine.model.ConfiguracionSistema;
import cat.npe.innowine.model.Usuario;
import cat.npe.innowine.states.LoginState;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.stage.Stage;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Clase Driver carga la primera vista de la aplicación, la pantalla de login
 */
public class LoginController extends DefaultController implements Initializable {

    // <editor-fold defaultstate="collapsed" desc="Variables">

    /**
     * Botón de Login ejecuta la comprobación para iniciar sesión en la aplicación.
     */
    @FXML
    private JFXButton btnLogin;

    /**
     * Botón de Salir ejecuta un alert con la opción de cerrar la aplicación.
     */
    @FXML
    private JFXButton btnSalir;

    /**
     * Campo a rellenar con el correo del usuario o administrador.
     */
    @FXML
    JFXTextField tfInputEmail;

    /**
     * Campo a rellenar con la contraseña del usuario o administrador.
     */
    @FXML
    JFXPasswordField pfInputPassword;

    /**
     * Muestra los mensajes de error que hace el usuario al introducir los datos.
     */
    @FXML
    Label lError;

    private Usuario usuario = null;
    private Calendar calendario = new GregorianCalendar();
    UtilsDB utilsDB;

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Métodos">

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tfInputEmail.requestFocus();
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Eventos">

    /**
     * Gestiona los eventos relacionados con los diferentes botones de la vista de Login
     *
     * @param actionEvent Info del botón pulsado en forma de evento
     */
    public void handleClicks(ActionEvent actionEvent) {
        if (actionEvent.getSource() == btnLogin) {
            String correo = tfInputEmail.getText();
            String contrasenya = pfInputPassword.getText();
            String mensajes = "";
           
            boolean valido = true;
            try {
                InternetAddress emailAddr = new InternetAddress(correo);
                emailAddr.validate();
            } catch (AddressException ex) {
                valido = false;
            }

            // Vemos si el usuario ha introducido los valores correctos para poder entrar.
            if (!correo.isEmpty() && !contrasenya.isEmpty()) {
                UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(this.getEmf(), Usuario.class);
                this.usuario = daoHelper.findUserByEmail(correo);

                if (valido) {
                    utilsDB = new UtilsDB(this.getEmf());
                    LoginState lg = utilsDB.checkPass(true,correo, contrasenya);
                    if(lg == LoginState.CHANGEPASSWORD){
                        try {
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/CambioPass.fxml"));
                            Stage stage = (Stage) btnLogin.getScene().getWindow();
                            Scene scene = new Scene(loader.load());
                            stage.setScene(scene);

                            // Cargamos la vista por defecto
                            DefaultController controller = loader.getController();

                            // Guardamos el usuario y pasamos emf
                            controller.setUsuarioLogueado(this.usuario);
                            controller.setEmf(getEmf());

                        } catch (Exception ex) {
                            System.out.println("test "+ex.getMessage());
                        }
                    }else if (lg == LoginState.GRANTED) {
                        try {
                            // Cargamos el marco: Menu
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/Menu.fxml"));
                            Stage stage = (Stage) btnLogin.getScene().getWindow();
                            Scene scene = new Scene(loader.load());
                            stage.setScene(scene);

                            // Cargamos la vista por defecto
                            MenuController controller = loader.getController();
                            controller.loadFxml("Main.fxml");

                            // Guardamos el usuario y pasamos emf
                            controller.setUsuarioLogueado(this.usuario);
                            controller.setEmf(getEmf());

                            controller.setShowUserLogued();

                        } catch (Exception ex) {
                            System.out.println(ex.getMessage());
                        }
                    } else if (lg == LoginState.FAILED) {
                        mensajes = mensajes.concat("Usuari i/o contrasenya no v\u00e0lid, si us plau recordi que t\u00E9 4 intents per a entrar al sistema, despr\u00E9s d'aix\u00F2 el seu usuari ser\u00e0 bloquejat.");

                    } else if (lg == LoginState.BLOCKED && !usuario.isAdmin()) {
                        mensajes = mensajes.concat("El seu usuari es troba bloquejat, per favor contacti amb l'administrador del sistema, perqu\u00E8 siguin restablerts els drets d'acc\u00E9s.");
                        enviarEmail();
                    }
                } else {
                    mensajes = mensajes.concat("El correu introduït no existeix.");
                }
            } else {
                mensajes = mensajes.concat("Omple els camps que est\u00e0n buits amb el format correcte.");
            }
            lError.setText(mensajes);
            tfInputEmail.requestFocus();
        }

        if (actionEvent.getSource() == btnSalir) {
            try {
                Alerta.alertaConfirmacio("App: Sortir", "Segur que vol sortir de l'App?","Trieu una opcio");
            } catch (ConfirmationDialogException e) {
                if (e instanceof SelectedYesConfirmationDialogException) {
                    Platform.exit(); // Atura l'App
                } else {
                    actionEvent.consume(); // Consumeix un esdeveniment
                }
            }
        }
    }

    /**
     * A partir de los datos de la tabla config, envia un email por smtp al usuario bloqueado para informarle de la situación actual de su cuenta.
     */
    private void enviarEmail() {

        // <editor-fold defaultstate="collapsed" desc="Variables">

        int hora = calendario.get(Calendar.HOUR_OF_DAY);
        int minutos = calendario.get(Calendar.MINUTE);
        int segundos = calendario.get(Calendar.SECOND);
        int any = calendario.get(Calendar.YEAR);
        int mes = calendario.get(Calendar.MONTH) + 1;
        int dia = calendario.get(Calendar.DAY_OF_MONTH);
        EmailHelper emailHelper;
        // </editor-fold">

        String dataCompleta = dia + " - " + mes + " - " + any + " : " + hora + ":" + minutos + ":" + segundos;

        Socket socket = new Socket();
        InetAddress localIP = null;
        try {
            socket.connect(new InetSocketAddress("google.com", 80));
            localIP=socket.getLocalAddress();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String cuerpoCorreo = ConfiguracionSistema.getInstancia().getCuerpoCorreo() + "\n\n@IP: " + localIP + "\n\nData: " + dataCompleta;

        emailHelper = new EmailHelper();
        emailHelper.sendEmail(tfInputEmail.getText(), ConfiguracionSistema.getInstancia().getAsuntoCorreo(),cuerpoCorreo);


    }

    // </editor-fold>
}
