package cat.npe.innowine.interfaces;

/**
 * Interface: IniciDiferit per modelar el comportament al cargar la primera ventana de la aplicación
 */
public interface IniciDiferit {
    /**
     * Per exemple:
     * Si la classe implementa aquest mètode s'executarà per poder fer
     * tasques desprès d'executar el mètode initialize (implements Initializable)
     * @return Retorna un booleano si ha podido o no conectar con la BBDD
     */
    boolean iniciDiferit();
}
