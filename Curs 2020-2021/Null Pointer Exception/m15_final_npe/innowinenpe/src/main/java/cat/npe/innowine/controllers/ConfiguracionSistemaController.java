package cat.npe.innowine.controllers;

import cat.npe.innowine.classes.DAOHelper;
import cat.npe.innowine.controllers.helpers.SecurityHelper;
import cat.npe.innowine.model.ConfiguracionSistema;
import cat.npe.innowine.model.Usuario;
import com.jfoenix.controls.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

/**
 * Clase que gestiona la vista de Configuración del Sistema
 */
public class ConfiguracionSistemaController extends DefaultController {

    /**
     * Botón para guardar la configuración de password
     */
    @FXML
    private JFXButton btnGuardarPassConf;

    /**
     * Botón para guardar la configuración de la base de datos de influxDB
     */
    @FXML
    private JFXButton btnGuardarInflux;

    /**
     * Botón para guardar la configuración del servidor smtp de correo
     */
    @FXML
    private JFXButton btnGuardarEmail;

    /**
     * Botón para cerrar el cuadro de dialogo informativo
     */
    @FXML
    private JFXButton btnCerrarInfo;

    /**
     * Botón con el aspa para cerrar el cuadro de dialogo informativo
     */
    @FXML
    private JFXButton btnCerrarInfoX;


    /**
     * Botón para salir del cuadro de dialogo informativo
     */
    @FXML
    private JFXButton btnSalirInfo;

    /**
     * Campo del formulario para guardar el número de intentos de login en la aplicación
     */
    @FXML
    private JFXTextField tfIntents;

    /**
     * Campo del formulario para guardar la longitud de la contraseña
     */
    @FXML
    private JFXTextField tfLongitud;

    /**
     * Campo del formulario para guardar la dirección o IP del host de la bbdd influx
     */
    @FXML
    private JFXTextField tfInfluxHost;

    /**
     * Campo para guardar el puerto de conexión a la bbdd influx
     */
    @FXML
    private JFXTextField tfInfluxPort;

    /**
     * Campo para guardar el nombre de la base de datos influx con la que la aplicación se conectará
     */
    @FXML
    private JFXTextField tfInfluxBBDD;

    /**
     * Campo para guardar el usuario de la bbdd influx
     */
    @FXML
    private JFXTextField tfInfluxUsuari;

    /**
     * Campo para guardar la dirección o IP del servidor smtp de correo que usará la aplicación
     */
    @FXML
    private JFXTextField tfEmailSMTP;

    /**
     * Campo para guardar el puerto de conexión al servidor smtp
     */
    @FXML
    private JFXTextField tfEmailPort;

    /**
     * Campo para guardar el nombre de usuario del servidor smtp
     */
    @FXML
    private JFXTextField tfEmailUsuari;

    /**
     * Campo con la contraseña de la BBDD de influx
     */
    @FXML
    private JFXPasswordField pfInfluxPassword;

    /**
     * Input con la contraseña del servidor de email
     */
    @FXML
    private JFXPasswordField pfEmailPassword;

    /**
     * Desplegable con los valores del protocolo del servidor de email
     */
    @FXML
    private JFXComboBox cbEmailProtocol;


    /**
     * Campos con el asunto del correo
     */
    @FXML
    private JFXTextArea taEmailSubject;

    /**
     * Campo con el cuerpo del correo
     */
    @FXML
    private JFXTextArea taEmailBody;

    /**
     * Etiqueta donde irá el texto de error
     */
    @FXML
    private Label lbError;

    /**
     * Etiqueta donde irá el  texto de exito
     */
    @FXML
    private Label lbSuccess;

    /**
     * Panel informativo del resultado de la operacion de guardar y si hay errores en el formulario
     */
    @FXML
    private Pane pInfo;

    /**
     * Variable para guardar un usuario
     */
    private Usuario user = null;

    /**
     * Gestiona los eventos relacionados con los diferentes botones de la vista de Login
     * @param actionEvent Info del botón pulsado en forma de evento
     */
    public void handleClicks(ActionEvent actionEvent) {
        if (actionEvent.getSource() == btnGuardarPassConf) {
            String intents = tfIntents.getText();
            String longitud = tfLongitud.getText();
            if(!intents.equals("") && !longitud.equals("")){
                if (isInteger(intents) && isInteger(longitud)) {
                    ConfiguracionSistema.getInstancia().setIntentos(Integer.parseInt(intents));
                    ConfiguracionSistema.getInstancia().setMinimoPassword(Integer.parseInt(longitud));
                    DAOHelper<ConfiguracionSistema> configDao = new DAOHelper<>(getEmf(),ConfiguracionSistema.class);
                    configDao.update(ConfiguracionSistema.getInstancia());
                    lbSuccess.setText("Canvis a la configuració de contrasenya aplicats correctament.");
                    lbSuccess.setVisible(true);
                } else {
                    lbError.setText("Si us plau, assegura't que el camp intents i la longitud de la contrasenya siguin números.\n");
                    lbError.setVisible(true);
                }
            }else{
                lbError.setText("Els camps no poden estar buits.\n");
                lbError.setVisible(true);
            }


            pInfo.setVisible(true);
            btnCerrarInfo.setVisible(true);
        }else if (actionEvent.getSource() == btnGuardarInflux) {
            SecurityHelper sh = new SecurityHelper();
            String host = tfInfluxHost.getText();
            String port = tfInfluxPort.getText();
            String bbdd = tfInfluxBBDD.getText();
            String usuari = tfInfluxUsuari.getText();
            String password = pfInfluxPassword.getText();
            if(!host.equals("") && !port.equals("") && !bbdd.equals("") && !usuari.equals("") && !password.equals("")){
                if(isInteger(port)){
                    ConfiguracionSistema.getInstancia().setHostInflux(host);
                    ConfiguracionSistema.getInstancia().setBbddInflux(bbdd);
                    ConfiguracionSistema.getInstancia().setUsuarioInflux(usuari);
                    ConfiguracionSistema.getInstancia().setPuertoInflux(Integer.parseInt(port));
                    if(!password.equals("password")){
                        ConfiguracionSistema.getInstancia().setPasswordInflux(sh.textEncrypt(password));
                    }
                    DAOHelper<ConfiguracionSistema> configDao = new DAOHelper<>(getEmf(),ConfiguracionSistema.class);

                    configDao.update(ConfiguracionSistema.getInstancia());
                    lbSuccess.setText("Canvis a la configuració de la base de dades InfluxDB aplicats correctament.");
                    lbSuccess.setVisible(true);
                }else{
                    lbError.setText("Si us plau, assegura't que el camp del port sigui un número.\n");
                    lbError.setVisible(true);
                }
            }else{
                lbError.setText("Els camps no poden estar buits.\n");
                lbError.setVisible(true);
            }

            pInfo.setVisible(true);
            btnCerrarInfo.setVisible(true);
        }else if (actionEvent.getSource() == btnGuardarEmail) {
            SecurityHelper sh = new SecurityHelper();
            String host = tfEmailSMTP.getText();
            String port = tfEmailPort.getText();
            String usuari = tfEmailUsuari.getText();
            String password = pfEmailPassword.getText();
            String subject = taEmailSubject.getText();
            String body = taEmailBody.getText();
            if(!host.equals("") && !port.equals("") && !usuari.equals("")
                    && !password.equals("") && !body.equals("") && !subject.equals("") && !cbEmailProtocol.getValue().toString().equals("") ){
                if(isInteger(port)){
                    ConfiguracionSistema.getInstancia().setDireccionSmtp(host);
                    ConfiguracionSistema.getInstancia().setPuertoSmtp(Integer.parseInt(port));
                    ConfiguracionSistema.getInstancia().setProtocoloSmtp(cbEmailProtocol.getValue().toString());
                    ConfiguracionSistema.getInstancia().setUsuarioSmtp(usuari);
                    ConfiguracionSistema.getInstancia().setAsuntoCorreo(subject);
                    ConfiguracionSistema.getInstancia().setCuerpoCorreo(body);
                    if(!password.equals("password")){
                        ConfiguracionSistema.getInstancia().setPasswordSmtp(sh.textEncrypt(password));
                    }
                    DAOHelper<ConfiguracionSistema> configDao = new DAOHelper<>(getEmf(),ConfiguracionSistema.class);

                    configDao.update(ConfiguracionSistema.getInstancia());
                    lbSuccess.setText("Canvis a la configuració del correu electrónic aplicats correctament.");
                    lbSuccess.setVisible(true);
                }else{
                    lbError.setText("Si us plau, assegura't que el camp del port sigui un número.\n");
                    lbError.setVisible(true);
                }
            }else{
                lbError.setText("Els camps no poden estar buits.\n");
                lbError.setVisible(true);
            }

            pInfo.setVisible(true);
            btnCerrarInfo.setVisible(true);
        }else if (actionEvent.getSource() == btnCerrarInfo || actionEvent.getSource() == btnCerrarInfoX) {
            btnCerrarInfo.setVisible(false);
            pInfo.setVisible(false);
            lbSuccess.setVisible(false);
            lbError.setVisible(false);
        }


    }

    /**
     * Comprueba si la cadena es un entero valido
     * @param s Cadena a comprobar
     * @return Booleano si es un entero: true, false en caso contrario
     */
    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }


    /**
     * Carga los inputs del formulario a partir de los datos de configuración del sistema, guardados
     * en una tabla de la BBDD
     */
    public void setFormData(){

        tfIntents.setText(ConfiguracionSistema.getInstancia().getIntentos().toString());
        tfLongitud.setText(ConfiguracionSistema.getInstancia().getMinimoPassword().toString());
        tfInfluxHost.setText(ConfiguracionSistema.getInstancia().getHostInflux());
        tfInfluxBBDD.setText(ConfiguracionSistema.getInstancia().getBbddInflux());
        tfInfluxPort.setText(ConfiguracionSistema.getInstancia().getPuertoInflux().toString());
        tfInfluxUsuari.setText(ConfiguracionSistema.getInstancia().getUsuarioInflux());
        pfInfluxPassword.setText("password");
        tfEmailSMTP.setText(ConfiguracionSistema.getInstancia().getDireccionSmtp());
        tfEmailPort.setText(ConfiguracionSistema.getInstancia().getPuertoSmtp().toString());
        cbEmailProtocol.setValue(ConfiguracionSistema.getInstancia().getProtocoloSmtp());
        tfEmailUsuari.setText(ConfiguracionSistema.getInstancia().getUsuarioSmtp());
        pfEmailPassword.setText("password");
        taEmailSubject.setText(ConfiguracionSistema.getInstancia().getAsuntoCorreo());
        taEmailBody.setText(ConfiguracionSistema.getInstancia().getCuerpoCorreo());
    }

}
