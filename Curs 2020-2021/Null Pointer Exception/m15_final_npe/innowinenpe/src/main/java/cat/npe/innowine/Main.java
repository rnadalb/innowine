package cat.npe.innowine;

import cat.npe.innowine.classes.Alerta;
import cat.npe.innowine.classes.DAOHelper;
import cat.npe.innowine.controllers.DefaultController;
import cat.npe.innowine.controllers.helpers.SecurityHelper;
import cat.npe.innowine.model.ConfiguracionSistema;
import cat.npe.innowine.states.CambioPasswordState;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase Driver inicia la aplicación y carga la primera vista
 */
public class Main extends Application {

    // <editor-fold defaultstate="collapsed" desc="Variables">

    /**
     * variable x para el eje de coordenadas X
     */
    private double x;

    /**
     * variable y para el eje de coordenadas Y
     */
    private double y;

    /**
     * variable que guardará la conexión com BBDD
     */
    private EntityManagerFactory emf;
    /**
     * variable que guardará la excepción si se produce al conectar con la BBDD
     */
    private Exception emfException;

    private Parameters a = getParameters();

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Metodos">

    /**
     * Carga de la primera ventana de la aplicación
     *
     * @param primaryStage La scene principal
     */
    @Override
    public void start(Stage primaryStage) {

        if (emf == null) {
            Alerta.alertaError("ERROR","BBDD error","ERROR: No se ha podido conectar\ncon la base de datos.");
        } else {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/Login.fxml"));
            Parent root;

            try {
                root = loader.load();

                // D'aquesta manera obtenim el controlador
                DefaultController controller = loader.getController();
                controller.setEmf(emf);
                // controller.setEmfException(emfException);

                // Com, sabem, el nostre Controlador implementa la interface IniciDiferit
                // executem el mètode


                primaryStage.setScene(new Scene(root));
                //set stage borderless
                primaryStage.initStyle(StageStyle.UNDECORATED);

                primaryStage.setTitle("InnoWine");

                //drag it here
                root.setOnMousePressed(event -> {
                    x = event.getSceneX();
                    y = event.getSceneY();
                });
                root.setOnMouseDragged(event -> {

                    primaryStage.setX(event.getScreenX() - x);
                    primaryStage.setY(event.getScreenY() - y);

                });


                primaryStage.getScene().getStylesheets().add("/css/styles.css");
                primaryStage.show();
            } catch (Exception ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                Platform.exit();
            }

        }
    }

    /**
     * Método que carga la ventana a la vez que la conexión con la BBDD
     *
     * @throws Exception Error de la excepción
     */
    @Override
    public void init() throws Exception {
        super.init();
        try {

            Map<String,String> map = new HashMap<>();
            SecurityHelper sc = new SecurityHelper();

            // Leemos fichero propertie: app.properties y desencriptamos valores

            Properties p = new Properties();
            p.load(Main.class.getClass().getResourceAsStream("/app.properties"));

            String url = p.getProperty("url");
            String user=p.getProperty("user");
            String pass=p.getProperty("password");

            map.put("javax.persistence.jdbc.url",sc.textDecrypt(url));
            map.put("javax.persistence.jdbc.user",sc.textDecrypt(user));
            map.put("javax.persistence.jdbc.password",sc.textDecrypt(pass));

            emf = Persistence.createEntityManagerFactory("mariaDBConnection",map);
            instanciarConfig(emf);

        } catch (Exception e) {
            e.printStackTrace();

            emfException = e;
        }

    }

    /**
     * Carga en la aplicación la configuración guardada en la BBDD
     * @param emf La conexión con la base de datos
     */

    private void instanciarConfig(EntityManagerFactory emf){
        DAOHelper<ConfiguracionSistema> configDao = new DAOHelper<>(emf,ConfiguracionSistema.class);

        ConfiguracionSistema query = configDao.getById(1);

        ConfiguracionSistema.getInstancia().setProjection(query.getProjection());
        ConfiguracionSistema.getInstancia().setId(query.getId());
        ConfiguracionSistema.getInstancia().setIntentos(query.getIntentos());
        ConfiguracionSistema.getInstancia().setMinimoPassword(query.getMinimoPassword());
        ConfiguracionSistema.getInstancia().setAsuntoCorreo(query.getAsuntoCorreo());
        ConfiguracionSistema.getInstancia().setCuerpoCorreo(query.getCuerpoCorreo());
        ConfiguracionSistema.getInstancia().setDireccionSmtp(query.getDireccionSmtp());
        ConfiguracionSistema.getInstancia().setPuertoSmtp(query.getPuertoSmtp());
        ConfiguracionSistema.getInstancia().setProtocoloSmtp(query.getProtocoloSmtp());
        ConfiguracionSistema.getInstancia().setUsuarioSmtp(query.getUsuarioSmtp());
        ConfiguracionSistema.getInstancia().setPasswordSmtp(query.getPasswordSmtp());
        ConfiguracionSistema.getInstancia().setHostInflux(query.getHostInflux());
        ConfiguracionSistema.getInstancia().setPuertoInflux(query.getPuertoInflux());
        ConfiguracionSistema.getInstancia().setBbddInflux(query.getBbddInflux());
        ConfiguracionSistema.getInstancia().setUsuarioInflux(query.getUsuarioInflux());
        ConfiguracionSistema.getInstancia().setPasswordInflux(query.getPasswordInflux());

        CambioPasswordState cps = CambioPasswordState.NOTMINLENGTH;
        cps.setMessage("La longitud minima de la nova contrasenya ha de ser de "+ ConfiguracionSistema.getInstancia().getMinimoPassword().toString()+" caracters.");

    }

    /**
     * Método que cierra la conexión con la BBDD y la aplicación
     *
     * @throws Exception Error de la excepción
     */
    @Override
    public void stop() throws Exception {
        if (emf != null && emf.isOpen())
            emf.close();

        super.stop();
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Main">

    /**
     * Lanza la aplicación
     *
     * @param args Argumentos de ejecución por linea de comandos
     */
    public static void main(String[] args) {
        launch(args);
    }

    // </editor-fold>
}