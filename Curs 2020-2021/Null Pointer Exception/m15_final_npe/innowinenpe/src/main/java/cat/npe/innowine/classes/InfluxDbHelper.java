package cat.npe.innowine.classes;

import cat.npe.innowine.controllers.helpers.SecurityHelper;
import cat.npe.innowine.model.ConfiguracionSistema;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;

public class InfluxDbHelper {
    //FIXME: Hauria d'estar en un fitxer .properties amb la contrasenya encriptada
    private static final String host = ConfiguracionSistema.getInstancia().getHostInflux();    // Nom de host o adreça IP
    private static final int port = ConfiguracionSistema.getInstancia().getPuertoInflux();                   // Port d'escolta de la BBDD
    private static final String user = ConfiguracionSistema.getInstancia().getUsuarioInflux();             // Nom de l'usuari
    private static final String password = ConfiguracionSistema.getInstancia().getPasswordInflux(); // Contrasenya de l'usuari de la BBDD
    private static final boolean sslEnabled = true;         // Si el servidor utilitza ssl true. En altre cas false

    private static InfluxDbHelper _instancia;
    private static InfluxDB client;

    private InfluxDbHelper() { client = createClient(); }

    /**
     * Crea una connexió
     * @return
     */
    private InfluxDB createClient() {
        String protocol = sslEnabled ? "https" : "http";
        String influxUrl = protocol + "://" + host + ":" + port;
        SecurityHelper sh = new SecurityHelper();
        return InfluxDBFactory.connect(influxUrl, user, sh.textDecrypt(password));
    }

    public static InfluxDbHelper getInstancia() {
        if (_instancia == null) {
            _instancia = new InfluxDbHelper();
        }
        return _instancia;
    }

    public void closeClient() {
        client.close();
    }

    public QueryResult query(Query query) {
        return client.query(query);
    }
}
