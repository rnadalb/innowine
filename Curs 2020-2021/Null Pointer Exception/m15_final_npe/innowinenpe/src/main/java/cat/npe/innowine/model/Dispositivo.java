package cat.npe.innowine.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entidad Usuario
 */

@Entity
@Table (name = "tbl_dispositivo")
public class Dispositivo implements Serializable {

  private static final long serialVersionUID = 6743369980306275042L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @ManyToOne
  @JoinColumn(name = "idUsuario", nullable = false)
  private Usuario idUsuario;

  @Column(name = "idDispositivo", nullable = false)
  private String idDispositivo;

  @Column(name = "descripcion", nullable = false)
  private String descripcion;

  @Column(name = "latitud")
  private double latitud;

  @Column(name = "longitud")
  private double longitud;

  public Dispositivo() {
  }

  public Integer getId() {
    return id;
  }

  public Usuario getIdUsuario() {
    return idUsuario;
  }

  public void setIdUsuario(Usuario idUsuario) {
    this.idUsuario = idUsuario;
  }


  public String getIdDispositivo() {
    return idDispositivo;
  }

  public void setIdDispositivo(String idDispositivo) {
    this.idDispositivo = idDispositivo;
  }


  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }


  public double getLatitud() {
    return latitud;
  }

  public void setLatitud(double latitud) {
    this.latitud = latitud;
  }


  public double getLongitud() {
    return longitud;
  }

  public void setLongitud(double longitud) {
    this.longitud = longitud;
  }

  @Override
  public String toString() {
    return "Dispositivo{" +
            "id=" + id +
            ", idUsuario=" + idUsuario +
            ", idDispositivo=" + idDispositivo +
            ", descripcion='" + descripcion + '\'' +
            ", latitud=" + latitud +
            ", longitud=" + longitud +
            '}';
  }
}
