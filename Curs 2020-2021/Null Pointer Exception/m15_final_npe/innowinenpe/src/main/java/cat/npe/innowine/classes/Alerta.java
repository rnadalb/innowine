package cat.npe.innowine.classes;

import cat.npe.innowine.exceptions.ConfirmationDialogException;
import cat.npe.innowine.exceptions.SelectedNoConfirmationDialogException;
import cat.npe.innowine.exceptions.SelectedYesConfirmationDialogException;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Clase que crea y muestra los cuadros de alerta al usuario: confirmación, de error, ...
 */
public class Alerta {
    private static final String TITLE = "MapJFX Desktop App";

    /**
     * Muestra un cuadro de dialogo con los botones de confirmación Si i No
     * @param title Titulo de la ventana
     * @param missatge Mensaje con la pregunta al usuario
     * @param contentText Mensaje informativo sobre la acción
     * @throws ConfirmationDialogException Excepcio que indicarà si s'ha escollit Si o No
     */
    public static void alertaConfirmacio(String title, String missatge, String contentText) throws ConfirmationDialogException {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(missatge);
        alert.setContentText(contentText);

        ButtonType btSi = new ButtonType(Constants.DEFAULT_BUTTON_YES);
        ButtonType btNo = new ButtonType(Constants.DEFAULT_BUTTON_NO);

        alert.getButtonTypes().setAll(btSi, btNo);
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == btSi)
            throw new SelectedYesConfirmationDialogException();
        else
            throw new SelectedNoConfirmationDialogException();

    }

    /**
     * Muestra un cuadro de texto con el mensaje de error y el botón aceptar
     * @param title Titulo de la ventana
     * @param missatge Mensaje con la pregunta al usuario
     * @param contentText Mensaje informativo sobre la acción
     */
    public static void alertaError(String title, String missatge, String contentText){

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(missatge);
        alert.setContentText(contentText);
        alert.showAndWait();

        Platform.exit();

    }

    private static void alerta(String title, String message, Alert.AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setTitle(message);
        alert.setContentText(message);
        alert.showAndWait();
    }


    private static void alertInfo(String title, String header, String message) {
        alertaError(title, header, message);
    }

    public static void alertInfo(String message) {
        alerta(TITLE, message, Alert.AlertType.INFORMATION);
    }

}
