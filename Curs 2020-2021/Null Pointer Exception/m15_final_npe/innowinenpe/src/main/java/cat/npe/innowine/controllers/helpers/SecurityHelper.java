package cat.npe.innowine.controllers.helpers;

import cat.npe.innowine.model.ConfiguracionSistema;
import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import org.jasypt.util.text.AES256TextEncryptor;

import java.util.Random;

/**
 * Clase Helper con los métodos de encriptacion y hash
 */
public class SecurityHelper  {
    private AES256TextEncryptor textEncryptor;
    private Argon2 argon2;

    /**
     * Constructor
     */
    public SecurityHelper(){
        try{
            this.argon2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2i, 32, 128);
            this.textEncryptor = new AES256TextEncryptor();
            this.textEncryptor.setPassword("QNFfojoCMnSPsXYEgzSR96tLPbuyvV");

        }catch(Exception e){
            System.out.println("Hubo un error a la hora de crear el Helper%nError: " + e.getLocalizedMessage());
        }
    }

    /**
     * Este método se encarga de encriptar el password que le pasen por parámetro y así devolver el password resumido
     * @param password Password en texto plano
     * @return Devuelve una string que es el password resumido
     */
    public String passwordEncrypt(String password){
        try{
            return this.argon2.hash(4, 1024 * 1024, 8, password.toCharArray());
        }catch(Exception e){
            System.out.println("Hubo un error a la hora de encriptar la contraseña%nError: " + e.getLocalizedMessage());
        }
        return "";
    }

    /**
     * Este método se encarga de comprobar que el resumen de la contraseña que le envias pertenece a la contraseña en texto plano
     * @param password Password en texto plano
     * @param pwdHashed Password encriptado
     * @return Boolean true si el resumen pertenece al password en texto plano o no
     */
    public boolean passwordCheck(String password, String pwdHashed){
        try{
            return this.argon2.verify(pwdHashed, password.toCharArray());
        }catch(Exception e){
            System.out.println("Hubo un error a la hora de desencriptar la contraseña%nError: " + e.getLocalizedMessage());
        }
        return false;
    }

    /**
     * Encripta el texto que le pasas con un salt, de manera que se pueda desencriptar después sin necesidad de poner el texto plano.
     * @param aEncriptar String del texto plano a ser encriptado
     * @return Devuelve la String pasada por parámetro encriptada
     */
    public String textEncrypt(String aEncriptar){
        try{
            return this.textEncryptor.encrypt(aEncriptar);
        }catch(Exception e){
            System.out.println("Hubo un error a la hora de encriptar el texto%nError: " + e.getLocalizedMessage());
        }
        return "";
    }

    /**
     * Desencripta el texto que se le pasa por parámetro
     * @param encriptado Texto encriptado a desencriptar
     * @return Devuelve el texto pasado por parámetro desencriptado
     */
    public String textDecrypt(String encriptado){
        try{
            return this.textEncryptor.decrypt(encriptado);
        }catch(Exception e){
            System.out.println("Hubo un error a la hora de desencriptar el texto%nError: " + e.getLocalizedMessage());
        }
        return "";
    }
    
    public String generateRandomPass(){

        // https://www.baeldung.com/java-random-string

        Integer targetStringLength= ConfiguracionSistema.getInstancia().getMinimoPassword()+2;
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'

        Random random = new Random();

        String generatePass = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return generatePass;
    }
}
