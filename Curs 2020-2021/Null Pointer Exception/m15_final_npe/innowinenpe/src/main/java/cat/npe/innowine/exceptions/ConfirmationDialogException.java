package cat.npe.innowine.exceptions;

/**
 * Excepción que nos permite controlar las acciones a seguir según el usuario pulse si o no
 * en los cuadros de alerta
 */
public class ConfirmationDialogException extends Exception {

}
