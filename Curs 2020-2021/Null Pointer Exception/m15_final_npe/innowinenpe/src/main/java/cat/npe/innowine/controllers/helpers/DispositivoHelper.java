package cat.npe.innowine.controllers.helpers;

import cat.npe.innowine.classes.DAOHelper;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Clase Helper que contiene metodos y consultas personalizados para la clase dispositivos
 * @param <Dispositivo> La instancia del dispositivo a crear, modificar, consultar ... en la bbdd
 */
public class DispositivoHelper<Dispositivo> extends DAOHelper<Dispositivo> {
    /**
     * Constructor
     *
     * @param emf            la conexión con la BBDD
     * @param parameterClass la entidad en forma de clase equivalente
     */
    public DispositivoHelper(EntityManagerFactory emf, Class<Dispositivo> parameterClass) {
        super(emf, parameterClass);
    }

    /**
     * Devuelve una lista de dispositivos a partir del texto introducido en el buscador
     * @param cerca El texto o palabra a buscar por id de dispositivo o descripción
     * @return Lista de dispositivos encontrados o null si no encuentra ninguno
     */
    public List<Dispositivo> findDispositiuBySearchString(String cerca) {

        EntityManager em = this.emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Dispositivo> cbQuery = cb.createQuery(parameterClass);
        Root<Dispositivo> c = cbQuery.from(parameterClass);
        cbQuery.select(c).where(cb.or(cb.like(cb.lower(c.get("idDispositivo")), "%"+cerca.toLowerCase()+"%"),
                cb.like(cb.lower(c.get("descripcion")), "%"+cerca.toLowerCase()+"%")));

        Query query = em.createQuery(cbQuery);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    /**
     * Devuelve una lista de dispositivos pertenecientes a un usuario en concreto
     * @param idUser El id del usuario
     * @return Lista de dispositivos o null si el usuario no tiene ninguno
     */
    public List<Dispositivo> findByIdUser(Integer idUser) {

        EntityManager em = this.emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Dispositivo> cbQuery = cb.createQuery(parameterClass);
        Root<Dispositivo> c = cbQuery.from(parameterClass);
        cbQuery.select(c).where(cb.equal(c.get("idUsuario"), idUser));

        Query query = em.createQuery(cbQuery);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
