package cat.npe.innowine.exceptions;

/**
 *  * Excepción que nos ayuda para ejecutar la opción del cuadro de alerta cuando el botón pulsado es NO
 */
public class SelectedNoConfirmationDialogException extends ConfirmationDialogException {
}
