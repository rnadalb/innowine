package cat.npe.innowine.model;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entidad Usuario
 */

@Entity
@Table (name = "tbl_usuario")
public class Usuario implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="Variables">

    private static final long serialVersionUID = 640048819861368633L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "apellido1", nullable = false)
    private String apellido1;

    @Column(name = "apellido2", nullable = false)
    private String apellido2;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "avatar")
    @ColumnDefault(value="1")
    private String avatar;

    @Column(name = "telefono")
    private String telefono;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "poblacion")
    private String poblacion;

    @Column(name = "codpostal")
    private String codigoPostal;

    @Column(name = "pais")
    private String pais;

    @Column(name = "admin", nullable = false)
    private boolean esAdmin;

    @Column(name = "intentos", nullable = false)
    private Integer intentos;

    @Column(name = "bloqueado",nullable = false)
    private boolean bloqueado;

    @Column(name = "cambiopass",nullable = false)
    private boolean cambioPassword;

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Constructores">

    public Usuario() {
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters / Getters">

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatar() { return avatar; }

    public void setAvatar(String avatar) { this.avatar = avatar; }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public boolean isAdmin() {
        return esAdmin;
    }

    public void setAdmin(boolean esAdmin) {
        this.esAdmin = esAdmin;
    }

    public boolean isBloqueado() {
        return bloqueado;
    }

    public void setBloqueado(boolean bloqueado) {
        this.bloqueado = bloqueado;
    }

    public boolean getCambioPassword() {
        return cambioPassword;
    }

    public void setCambioPassword(boolean cambioPassword) {
        this.cambioPassword = cambioPassword;
    }

    public Integer getIntentos() {
        return intentos;
    }

    public void setIntentos(Integer intentos) {
        this.intentos = intentos;
    }
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Métodos">

    @Override
    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido1=" + apellido1 +
                ", apellido2=" + apellido2 +
                ", bloqueado=" + bloqueado +
                ", cambioPassword=" + cambioPassword +
                '}';
    }


    // </editor-fold>
}