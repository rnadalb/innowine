package cat.npe.innowine.classes;

import cat.npe.innowine.controllers.helpers.SecurityHelper;
import cat.npe.innowine.controllers.helpers.UsuarioHelper;
import cat.npe.innowine.model.ConfiguracionSistema;
import cat.npe.innowine.model.Usuario;
import cat.npe.innowine.states.CambioPasswordState;
import cat.npe.innowine.states.LoginState;

import javax.persistence.EntityManagerFactory;

/**
 * Contiene métodos y funciones reutilizables utiles en todos los programas
 */
public class UtilsDB {

    /**
     * Instancia que nos permitirá encriptar y desencriptar o realizar hash's de datos y contraseñas
     */
    private SecurityHelper sh = new SecurityHelper();

    /**
     * Instancia de tipo usuario
     */
    private Usuario usuario;

    /**
     * La conexión con la base de datos
     */
    private final EntityManagerFactory emf;

    /**
     * Constructor
     * @param emf Recibe la conexión con la base de datos
     */
    public UtilsDB(EntityManagerFactory emf) {
        this.emf = emf;
    }

    /**
     * Valida que el correo existe en la base de datos y que la contraseña que entra el usuario
     * sea valido con con el nombre del usuario.
     *
     * @param bloquearUser Indica si se debe bloquear al usuario, si se ejecuta desde mi perfil y se equivoca de password, no se bloqueara, aunque supere el número de intentos
     * @param Email Correo que introduce el usuario
     * @param pass  Contraseña que introduce el usuario
     * @return Nos devuelve un LoginState que depende de la validez de los datos que nos da el usuario
     */

    public LoginState checkPass(boolean bloquearUser, String Email, String pass) {
        LoginState resultat = LoginState.BLOCKED;
        UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(this.emf, Usuario.class);
        this.usuario = daoHelper.findUserByEmail(Email);

        if (this.usuario != null) {
            if (!usuario.isBloqueado() || usuario.getCambioPassword()) {
                if (sh.passwordCheck(pass,usuario.getPassword())) {
                    if(usuario.getCambioPassword()){
                        return LoginState.CHANGEPASSWORD;
                    }
                    resultat = LoginState.GRANTED;
                    usuario.setIntentos(0);
                } else {
                    if (usuario.getIntentos() <= ConfiguracionSistema.getInstancia().getIntentos()) {
                        if(!usuario.isAdmin()) usuario.setIntentos(usuario.getIntentos() + 1);
                        resultat = LoginState.FAILED;
                        if (!usuario.isAdmin() && (usuario.getIntentos() >= ConfiguracionSistema.getInstancia().getIntentos())) {
                            if (bloquearUser) {
                                usuario.setBloqueado(true);
                                resultat = LoginState.BLOCKED;
                            } else {
                                resultat = LoginState.GRANTED;
                            }
                        }

                    }
                    daoHelper.update(usuario);
                }
            }else{
                if(usuario.isAdmin()){
                    resultat = LoginState.GRANTED;
                }else{
                    resultat = LoginState.BLOCKED;
                }
            }
        } else {
            resultat = LoginState.FAILED;
        }
        return resultat;
    }

    /**
     * Metodo que controla los diversos estados al cambiar de contraseña
     * @param cambiarPass Variable que indica si se debe guardar en BBDD la nueva contraseña
     * @param user  El usuario que quiere o al que se le va a cambiar la contraseña
     * @param passActual La contraseña actual
     * @param passNuevo La nueva contraseña
     * @param passNuevoRep La nueva contraseña repetida
     * @return Devuelve un estado informando del resultado
     */
    public CambioPasswordState changePassword(boolean cambiarPass,Usuario user, String passActual, String passNuevo, String passNuevoRep){
        this.usuario = user;
        LoginState lg = this.checkPass(false, this.usuario.getEmail(), passActual);

        // Vemos si el usuario ha introducido los valores correctos para poder entrar.
        if (!passActual.isEmpty() && !passNuevo.isEmpty() && !passNuevoRep.isEmpty()) {
            if(passNuevo.equals(passNuevoRep)){
                if(passActual.equals(passNuevo)){
                    return CambioPasswordState.EQUALPASSWORDS;
                }else{
                    if (lg == LoginState.GRANTED || lg == LoginState.CHANGEPASSWORD) {

                        if(passNuevo.length()< ConfiguracionSistema.getInstancia().getMinimoPassword()){
                            return CambioPasswordState.NOTMINLENGTH;
                        }

                        if(cambiarPass) { // Para poder reaprovechar función en mi perfil
                            UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(emf, Usuario.class);

                            String newPassword = this.sh.passwordEncrypt(passNuevo);
                            this.usuario.setPassword(newPassword);
                            this.usuario.setCambioPassword(false);
                            this.usuario.setIntentos(0);
                            daoHelper.update(this.usuario);

                            try {
                                return CambioPasswordState.OK;
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }else{
                            return CambioPasswordState.OK;
                        }

                    } else if (lg == LoginState.FAILED) {
                        return CambioPasswordState.ACTUALWRONG;
                    }
                }

            }else{
                return CambioPasswordState.NOTMATCHING;
            }
        }
        return CambioPasswordState.EMPTY;
    }

    /**
     * Metodo que controla si es correcto que un administrador cambie una contraseña
     *
     * @param userEdit el usuario
     * @param passNuevo pass nuevo
     * @return Devuelve un estado informando del resultado
     */
    public CambioPasswordState changePasswordByAdmin(Usuario userEdit, String passNuevo){

        // Vemos si el usuario ha introducido los valores correctos para poder entrar.
        if (!passNuevo.isEmpty()) {

            if(passNuevo.length()< ConfiguracionSistema.getInstancia().getMinimoPassword()){
                return CambioPasswordState.NOTMINLENGTH;
            }
            else{
                if(userEdit.getEmail()!=null) {
                    UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(this.emf, Usuario.class);
                    this.usuario = daoHelper.findUserByEmail(userEdit.getEmail());

                    if (passNuevo.equals(usuario.getPassword())) {
                        return CambioPasswordState.EQUALPASSWORDS;
                    } else {
                        return CambioPasswordState.OK;
                    }
                }
                return CambioPasswordState.OK;

            }

        }else {
            return CambioPasswordState.EMPTY;
        }

    }


}
