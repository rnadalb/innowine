package cat.npe.innowine.controllers;

import cat.npe.innowine.classes.Alerta;
import cat.npe.innowine.classes.UtilsDB;
import cat.npe.innowine.controllers.helpers.SecurityHelper;
import cat.npe.innowine.controllers.helpers.UsuarioHelper;
import cat.npe.innowine.exceptions.ConfirmationDialogException;
import cat.npe.innowine.exceptions.SelectedYesConfirmationDialogException;
import cat.npe.innowine.model.Usuario;
import cat.npe.innowine.states.CambioPasswordState;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

public class CambioPassController extends DefaultController {


    /**
     * Botón de Cambio de Password ejecuta la comprobación para cambiar el password del usuario.
     */
    @FXML
    private JFXButton btnCambiarPass;

    @FXML
    private JFXButton btnGoLogin;

    /**
     * Botón de Salir ejecuta un alert con la opción de cerrar la aplicación
     */
    @FXML
    private JFXButton btnSalir;

    /**
     * Botón de salir alternativo
     */
    @FXML
    private JFXButton btnSalir2;

    /**
     * Input con el pass Actual
     */
    @FXML
    private JFXPasswordField pwdActual;

    /**
     * Input con el nuevo pass
     */
    @FXML
    private JFXPasswordField pwdNew;

    /**
     * Input con el nuevo pass repetido
     */
    @FXML
    private JFXPasswordField pwdNewRepeat;

    /**
     * Pane que contiene los inputs en la vista de cambio de contraseña
     */
    @FXML
    private AnchorPane panePassword;

    /**
     * AnchorPane con el mensaje de exito de cambio de contraseña
     */
    @FXML
    private AnchorPane paneSuccess;

    /**
     * Label donde aparece el mensaje de error
     */
    @FXML
    private Label lbError;

    /**
     * ProgressIndicator para indicar al usuario que espere mientras el programa realiza una acción
     */
    @FXML
    private ProgressIndicator piCarga;

    private Usuario user = null;

    /**
     * Gestiona los eventos relacionados con los diferentes botones de la vista de Login
     * @param actionEvent Info del botón pulsado en forma de evento
     * @throws IOException Error de entrada / salida
     */
    public void handleClicks(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == btnCambiarPass) {

            String passActual = pwdActual.getText();
            String passNuevo = pwdNew.getText();
            String passNuevoRep = pwdNewRepeat.getText();

            user = this.getUsuarioLogueado();
            UtilsDB utilsDB = new UtilsDB(getEmf());

            CambioPasswordState estado = utilsDB.changePassword(true,user, passActual, passNuevo, passNuevoRep );

            if(estado == CambioPasswordState.OK){
                panePassword.setVisible(false);
                paneSuccess.setVisible(true);
            }else{
                lbError.setText(estado.getMessage());
                pwdActual.requestFocus();
                piCarga.setVisible(false);

            }

        }

        if (actionEvent.getSource() == btnSalir || actionEvent.getSource() == btnSalir2 ) {

            try {
                Alerta.alertaConfirmacio("App: Sortir", "Segur que vol sortir de l'App?","Trieu una opcio");
            } catch (ConfirmationDialogException e) {
                if (e instanceof SelectedYesConfirmationDialogException)
                    Platform.exit(); // Atura l'App
                else
                    actionEvent.consume(); // Consumeix un esdeveniment
            }
        }else if(actionEvent.getSource() == btnGoLogin){
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/Login.fxml"));
            Stage stage = (Stage) btnGoLogin.getScene().getWindow();
            Scene scene = new Scene(loader.load());

            DefaultController controller = loader.getController();
            controller.setEmf(getEmf());
            controller.setUsuarioLogueado(null);
            stage.setScene(scene);
        }
    }

}
