package cat.npe.innowine.controllers.helpers;

import cat.npe.innowine.model.ConfiguracionSistema;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Clase de ayuda con la confiracion y envio de emails
 */
public class EmailHelper {

    /**
     * Para encriptar la contraseña
     */
    private SecurityHelper sh = new SecurityHelper();

    /**
     * Email origen
     */
    private String remitente = ConfiguracionSistema.getInstancia().getUsuarioSmtp();

    /**
     * Direccio IP o dominio del SMTP
     */
    private String host = ConfiguracionSistema.getInstancia().getDireccionSmtp();

    /**
     * Constraseña del servidor SMTP
     */
    private String pass = sh.textDecrypt(ConfiguracionSistema.getInstancia().getPasswordSmtp());

    /**
     * Puerto del servidor SMTP
     */
    private Integer port = ConfiguracionSistema.getInstancia().getPuertoSmtp();

    /**
     * Sesion de la conexion
     */
    private Session session;

    /**
     * Formato mime para crear el email
     */
    private MimeMessage message;

    /**
     * Constructor que prepara la sesion y el mensaje a enviar con la configuracion del SMTP
     */
    public EmailHelper(){

        Properties props = System.getProperties();
        props.put("mail.smtp.host", host);  //El servidor SMTP de Google
        props.put("mail.smtp.user", remitente);
        props.put("mail.smtp.clave", pass);    //La clave de la cuenta
        props.put("mail.smtp.auth", "true");    //Usar autenticación mediante usuario y clave
        props.put("mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", port); //El puerto SMTP seguro de Google

        this.session = Session.getDefaultInstance(props);
        this.message = new MimeMessage(session);

    }

    /**
     * Envia mail segun la configuracion del smtp
     * @param emailTo Email del Destinatario
     * @param subject Asunto del email
     * @param msgBody Mensaje del email
     */
    public void sendEmail(String emailTo, String subject, String msgBody){

        try {
            message.setFrom(new InternetAddress(remitente));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));   //Se podrían añadir varios de la misma manera
            message.setSubject(subject);
            message.setText(msgBody);

            Transport transport = session.getTransport("smtp");
            transport.connect(host, remitente, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (MessagingException me) {
            me.printStackTrace();   //Si se produce un error
        }
    }

    /**
     * Informa al destinatario que el Administrador ha creado una cuenta en la aplicación.
     * También de que deberá obligatoriamente cambiar la contraseña antes de poder entrar en la aplicación.
     * @param emailTo Dirección email de quien recibira el mensaje
     * @param nuevoPass La nueva contraseña en texto plano
     */
    public void sendWelcomeEmail(String emailTo, String nuevoPass){

        String asunto = "INNOWINE: Compte creat";
        String cuerpo="L'Administrador ha creat un compte per a vostè en la nostra aplicació.\n\n"+
                "Inicií sessió amb aquest email: " + emailTo+ " i aquesta contrasenya: " + nuevoPass +
                "\nper establir una nova contrasenya.\nGràcies per la vostra atenció.";

        sendEmail(emailTo,asunto,cuerpo);
    }

    /**
     * Envia email informando al usuairo del nuevo pass que el administrador ha creado,
     * y que deberá cambiarlo cuando inicie sesión, ya que automaticamente le llevará a la pantalla de cambio de pass
     * sin hacer el login
     * @param emailTo Dirección email de quien recibira el mensaje
     * @param nuevoPass La nueva contraseña en texto plano
     */
    public void sendAdminChangePass(String emailTo, String nuevoPass){

        String asunto = "INNOWINE: Constrasenya canviada";
        String cuerpo="L'Administrador ha canviat la seva contrasenya en la nostra aplicació.\n\n"+
                "Inicií sessió amb l'email del seu compte: " + emailTo+ " i la nova contrasenya: " + nuevoPass +
                "\nper establir una nova contrasenya.\nGràcies per la vostra atenció.";

        sendEmail(emailTo,asunto,cuerpo);

    }

    /**
     * Envia email informando al usuairo que su cuenta ha sido desbloqueada,
     * y que deberá cambiar su contraseña cuando inicie sesión, ya que automaticamente le llevará a la pantalla de cambio de pass
     * sin hacer el login
     * @param emailTo Dirección email de quien recibira el mensaje
     * @param nuevoPass La nueva contraseña en texto plano
     */
    public void sendAdminUnblockUser(String emailTo, String nuevoPass){

        String asunto = "INNOWINE: Usuari Desbloquejat";
        String cuerpo="L'Administrador ha desbloquejat el seu compte en la nostra aplicació.\n\n"+
                "Inicií sessió amb l'email del seu compte: " + emailTo+ " amb aquesta contrasenya: " + nuevoPass +
                "\nper establir una nova.\nGràcies per la vostra atenció.";

        sendEmail(emailTo,asunto,cuerpo);

    }
}
