package cat.npe.innowine.controllers;

import cat.npe.innowine.classes.Alerta;
import cat.npe.innowine.classes.Patterns;
import cat.npe.innowine.controllers.helpers.DispositivoHelper;
import cat.npe.innowine.model.ConfiguracionSistema;
import cat.npe.innowine.model.Dispositivo;
import cat.npe.innowine.model.Usuario;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.sothawo.mapjfx.*;
import com.sothawo.mapjfx.event.MapLabelEvent;
import com.sothawo.mapjfx.event.MapViewEvent;
import com.sothawo.mapjfx.event.MarkerEvent;
import com.sothawo.mapjfx.offline.OfflineCache;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Clase que controla la gestión de Dispositivos.
 */
public class GestionDispositivosController extends DefaultController {

    // <editor-fold defaultstate="collapsed" desc="Variables">
    /**
     * Panel con el dialogo de confirmación de advertencia de borrar un dispositivo.
     */
    @FXML
    private Pane pInfoDisp;

    /**
     * Panel con la tabla de los dispositivos del sistema
     */
    @FXML
    private Pane pDispositius;

    /**
     * Componente donde se creará la lista de dispositivos del sistema
     */
    @FXML
    private ScrollPane scrollDispositius;


    /**
     * Botón para crear un nuevo dispositivo: desoculta formulario de creación.
     */
    @FXML
    private JFXButton btnNouDispositiu;

    /**
     * Btn de confirmación de borrar usuario
     */
    @FXML
    private JFXButton btnCerrarInfoDispX;

    /**
     * Botón para cerrar el panel de información de la operacion de borrar
     */
    @FXML
    private JFXButton btnEsborrarDispositiu;

    /**
     * Botón cuadrado con un aspa para cerrar el dialogo de confirmación de borrar usuario
     */
    @FXML
    private JFXButton btnCerrarInfoDisp;

    /**
     * Input del buscador de dispositivos
     */
    @FXML
    private JFXTextField tfCercador;

    /**
     * PanelAncla con un Hijo Pane, llamado mainPanel que sirve para ir cargando las vistas
     * de los diferentes apartedos de la aplicación. En formato FXML
     */
    @FXML
    private AnchorPane mainAnchor;

    /**
     * Hijo de mainAnchor, Pane donde cargaremos las diferentes vistas con el método
     * loadFMXL
     */
    @FXML
    private Pane mainPanel;

    /**
     * Componente a modo de tabla donde insertaremos los elementos HBox a modo de filas con la información de cada usuario.
     */
    @FXML
    private VBox pnItems;

    /**
     * Inputs para escribir longitud y latitud manualmente.
     */
    @FXML
    private JFXTextField tfLatitud, tfLongitud;

    /**
     * Botones para hacer zoom in y zoom out.
     */
    @FXML
    private JFXButton btZoomPlus, btZoomMinus;

    /**
     * Botón para cerrar la pantalla de alta/modificacion dispositivos.
     */
    @FXML
    private JFXButton btSortir;

    /**
     * Botón para buscar por longitud y latitud en el mapa.
     */
    @FXML
    private JFXButton btnCercar;

    /**
     * Input para escribir id dispositivo manualmente.
     */
    @FXML
    private JFXTextField tfId;

    /**
     * Input para escribir descripción dispositivo manualmente.
     */
    @FXML
    private JFXTextField tfDesc;

    @FXML
    private MapView mapView;

    /**
     * Botones para elegir tipo de vista del mapa.
     */
    @FXML
    private JFXButton btOSM, btWSM, btXYZ; // Botones por estilo del mapa OSM, WSM, XYZ

    /**
     * Slider para ajustar el tamaño del zoom.
     */
    @FXML
    private Slider sdZoom;

    private MapType mapType;

    /**
     * Indica si estamos creando o editando un dispositivo
     */
    private boolean newDispositivo;

    /**
     * Guardamos el id del dispositivo que estamos modificando
     */
    private Integer idDispositivoEdit;

    /**
     * Guardamos el dispositivo que estamos creando o editando
     */
    private Dispositivo dispositivoEdit;

    /**
     * Panel con los inputs del formulario
     */
    @FXML
    private Pane pForm;

    /** Localitzación por defecto, Vilafranca del Penedés */
    private static final double DEFAULT_LAT= 41.3461265;
    private static final double DEFAULT_LONG= 1.697939799999972;
    private static final Coordinate coordVilafrancaPenedes = new Coordinate(DEFAULT_LAT, DEFAULT_LONG);

    /** zoom por defecto **/
    private static final int ZOOM_DEFAULT = 14;
    private static final int ZOOM_LOCATED = 17;
    private static final Marker.Provided DEFAULT_MARKER_COLOR = Marker.Provided.GREEN;
    private static IntegerProperty zoom_value = new SimpleIntegerProperty(ZOOM_DEFAULT);

    /** Parámetros para el WMS server. */
    private WMSParam wmsParam = new WMSParam()
            .setUrl("http://ows.terrestris.de/osm/service?")
            .addParam("layers", "OSM-WMS");

    private XYZParam xyzParams = new XYZParam()
            .withUrl("https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x})")
            .withAttributions("'Tiles &copy; <a href=\"https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer\">ArcGIS</a>'");

    @FXML
    private AnchorPane apData;

    /**
     * Botón DondeEstoy: Guarda el botón actual de la vista, para poder ir indicando al usuario en que apartado del menu se encuenta.
     */
    @FXML
    private Button btnDondeEstoy;

    //private EntityManagerFactory emf = getEmf();

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Métodos">

    /**
     * Recupera la información del usuario y la pone en el campo correspondiente del formulario
     * Asigna eventos de onBlur, al perder el foco, para comprobar si esta vacío y mostrar un error visual
     * Reformatea la entrada del usuario al equivalente text-tranform:capitalize;
     */
    public void setFormData(){

        tfId.setText(String.valueOf(dispositivoEdit.getId()));
        tfDesc.setText(dispositivoEdit.getDescripcion());
        tfLatitud.setText(String.valueOf(dispositivoEdit.getLatitud()));
        tfLongitud.setText(String.valueOf(dispositivoEdit.getLongitud()));

        addListenerFocused(tfId);
        addListenerFocused(tfDesc);
        addListenerFocused(tfLatitud);
        addListenerFocused(tfLongitud);

        // https://docs.oracle.com/javafx/2/ui_controls/radio-button.htm

    }

    /**
     * Añade al JFXTextField pasado por parametro un evento blur para controlar si hay errores
     * modificando el input de forma visual
     * @param tfInput El JFXTextField a aplicarle el evento de escucha
     */

    public void addListenerFocused(JFXTextField tfInput){

         /* No dejaba poner espacios
        tfInput.textProperty().addListener((obs, oldText, newText) -> {
            tfInput.setText(capitalizeWord(newText));
        });*/

        tfInput.focusedProperty().addListener((obs, oldVal, newVal) -> {

            if(!newVal){ // Si no hay nuevo valor, es que ha perdido el foco -> blur
                String value=tfInput.getText();

                if(value.length()==0){
                    tfInput.setUnFocusColor(Color.web("red"));
                }
                else{
                    tfInput.setText(capitalizeWord(value));
                    tfInput.setUnFocusColor(Color.web("#030303"));
                }
            }
        });
    }

    /**
     * Devuelve una cadena donde la primera letra de cada palabra se ha convetido a maýusculas
     * @param str Cadena de texto a transformar
     * @return La cadena transformada
     */

    public String capitalizeWord(String str){

        if(str.length()!=0) {
            String words[] = str.split("\\s");
            String capitalizeWord = "";
            for (String w : words) {
                String first = w.substring(0, 1);
                String afterfirst = w.substring(1);
                capitalizeWord += first.toUpperCase() + afterfirst.toLowerCase() + " ";
            }
            return capitalizeWord.trim();
        }else{
            return new String("");
        }
    }

    public void initMapAndControls(Projection projection) {
        // inicialitza MapView-Cache
        final OfflineCache offlineCache = mapView.getOfflineCache();
        final String cacheDir = System.getProperty("java.io.tmpdir") + "/mapjfx-cache";

        // escolta (monitortiza) la propietat que indica que el mapa ha finalitzat la seva inicialització
        mapView.initializedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                afterMapIsInitialized();
            }
        });

        // controla els botons que gestionen els diferents tipus de mapes
        mapType = MapType.WMS;

        // Associament de la propietat del zoom als controls slider i button corresponents
        sdZoom.valueProperty().bindBidirectional(mapView.zoomProperty());
        zoom_value.bindBidirectional(mapView.zoomProperty());

        setupEventHandlers();

        mapView.initialize(Configuration.builder()
                .projection(projection)
                .showZoomControls(false)
                .build());
    }

    /**
     * Inicializa los listeners de gestión del mapa i la GUI
     */
    private void setupEventHandlers() {

        // Gestió del zoom pels botons
        btZoomPlus.setOnAction(event -> {
            event.consume();
            mapView.setZoom(mapView.getZoom() + 1);
        });

        btZoomMinus.setOnAction(event -> {
            event.consume();
            mapView.setZoom(mapView.getZoom() - 1);
        });

        // Gestiona l'esdeveniment de fer clic en el mapa
        mapView.addEventHandler(MapViewEvent.MAP_CLICKED, event -> {
            event.consume();
            final Coordinate newPosition = event.getCoordinate().normalize();
            Alerta.alertInfo(String.format("[MAP_CLICKED] --> Posició: [%s, %s]", newPosition.getLatitude(), newPosition.getLongitude()));
        });

        // Gestiona l'esdeveniment de fer clic amb el botó dret en el mapa
        mapView.addEventHandler(MapViewEvent.MAP_RIGHTCLICKED, event -> {
            event.consume();
            Alerta.alertInfo(String.format("[MAP_RIGHTCLICKED] --> Posició: [%s, %s]", event.getCoordinate().getLatitude(), event.getCoordinate().getLongitude()));
        });

        // Gestiona quan s'ha fet clic a un marcador
        mapView.addEventHandler(MarkerEvent.MARKER_CLICKED, event -> {
            event.consume();
            Alerta.alertInfo(String.format("[MARKER_CLICKED] --> Id del marcador: [%s]", event.getMarker().getId()));
        });

        // Gestiona quan s'ha fet clic amb el botó dret a un marcador
        mapView.addEventHandler(MarkerEvent.MARKER_RIGHTCLICKED, event -> {
            event.consume();
            Alerta.alertInfo(String.format("[MARKER_RIGHTCLICKED] --> Id del marcador: [%s]", event.getMarker().getId()));
        });

        // Gestiona quan s'ha fer clic en una etiqueta al mapa
        mapView.addEventHandler(MapLabelEvent.MAPLABEL_CLICKED, event -> {
            event.consume();
            Alerta.alertInfo(String.format("[MAPLABEL_CLICKED] --> Id del marcador: [%s]", event.getMapLabel().getId()));
        });

        // Gestiona quan s'ha fer clic amb el botó dret en una etiqueta al mapa
        mapView.addEventHandler(MapLabelEvent.MAPLABEL_RIGHTCLICKED, event -> {
            event.consume();
            Alerta.alertInfo(String.format("[MAPLABEL_RIGHTCLICKED] --> Id del marcador: [%s]", event.getMapLabel().getId()));
        });

        // Gestiona el punter al mapa
        mapView.addEventHandler(MapViewEvent.MAP_POINTER_MOVED, event -> {
            System.out.println("el punter està en " + event.getCoordinate());
        });
    }

    /**
     * Finaliza la configuración después que el mapa este inicializado
     */
    private void afterMapIsInitialized() {
        mapView.setZoom(ZOOM_DEFAULT);
        mapView.setCenter(coordVilafrancaPenedes);
        addMarker(true);
        btWSM.fire();

    }

    /**
     * Añade un marcador
     *
     * @param center si és true centra en el mapa la nueva posición
     * @return booleano
     */
    private boolean addMarker (boolean center) {
        boolean result = false;
        if (!tfLatitud.getText().isEmpty() && !tfLongitud.getText().isEmpty()) {
            if (Pattern.matches(Patterns.fpRegex, tfLatitud.getText())) {
                Double lat = Double.valueOf(tfLatitud.getText());
                if (Pattern.matches(Patterns.fpRegex, tfLongitud.getText())) {
                    Double lng = Double.valueOf(tfLongitud.getText());
                    Coordinate c = new Coordinate(lat,lng);
                    addMarker(c, DEFAULT_MARKER_COLOR, center);
                    result = true;
                } else {
                    Alerta.alertInfo("El valor de la <longitud> no és correcte");
                }
            } else {
                Alerta.alertInfo("El valor de la <latitud> no és correcte");
            }
        }
        return result;
    }

    private void addMarker (Coordinate coordinate, Marker.Provided color, boolean center) {
        Marker marker = Marker.createProvided(color).setPosition(coordinate).setVisible(true);
        String info = String.format("Posició [%s,%s]", coordinate.getLatitude().toString(), coordinate.getLongitude().toString());
        MapLabel lblInfo = new MapLabel(info, 10, -10).setVisible(false).setCssClass("orange-label");
        marker.attachLabel(lblInfo);
        mapView.addMarker(marker);
        if (center)
            mapView.setCenter(coordinate);
        mapView.setZoom(ZOOM_LOCATED);
    }

    @FXML
    public void btSearchOnAction(ActionEvent actionEvent) {
        addMarker(true);
    }

    /**
     * Método que aplica una clase CSS para señalar en el menu lateral donde se encuentra los botones
     * donde eliges el tipo de mapa
     * @param btn Botón a marcar visualmente
     */
    public void marcaBtnActivo(Button btn){

        if (btnDondeEstoy == null) { // Se ha cargado la vista por primera vez, el btnInicio tiene la clase aplicada por defecto

            btnDondeEstoy=btWSM;
            btnDondeEstoy.getStyleClass().remove("menu-btn-clicked");
            btnDondeEstoy=btn;
            btnDondeEstoy.getStyleClass().add("menu-btn-clicked");
        } else{
            btnDondeEstoy.getStyleClass().remove("menu-btn-clicked");
            btnDondeEstoy=btn;
            btnDondeEstoy.getStyleClass().add("menu-btn-clicked");
        }

    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Eventos">

    /**
     * Método que a partir de un input de texto busca los dispositivos por su id o nombre
     */
    public void cercarDispositius(){

        DispositivoHelper<Dispositivo> daoHelper = new DispositivoHelper<>(this.getEmf(), Dispositivo.class);
        List<Dispositivo> dispositivos = null;

        if (tfCercador.getLength()==0) {
            dispositivos = daoHelper.getAll();
        } else {
            dispositivos = daoHelper.findDispositiuBySearchString(tfCercador.getText());
        }

        pnItems.getChildren().clear();

        // Si hay usuarios

        if( dispositivos!=null && !dispositivos.isEmpty()){

            Node[] nodes = new Node[dispositivos.size()];

            int contadorNodos=0;

            for (int i = 0; i < nodes.length; i++) {
                try {

                    if(dispositivos.get(i).getIdUsuario().getId() == getUsuarioLogueado().getId() || getUsuarioLogueado().isAdmin()) {

                    final int j = i;
                    nodes[i] = FXMLLoader.load(getClass().getResource("/views/DispositivoItem.fxml"));

                    //give the items some effect
                    nodes[i].setOnMouseEntered(event -> {
                        nodes[j].setStyle("-fx-background-color : #f8b500");
                    });

                    nodes[i].setOnMouseExited(event -> {
                        nodes[j].setStyle("-fx-background-color : transparent");
                    });

                    int idUsuarioDisp = dispositivos.get(i).getIdUsuario().getId();

                        nodes[i].getProperties().put("emf", this.getEmf());
                        nodes[i].getProperties().put("getUsuarioLogueado", this.getUsuarioLogueado());
                        nodes[i].getProperties().put("mainAnchor", pDispositius.getParent());
                        nodes[i].getProperties().put("mainPanel", pDispositius);
                        nodes[i].getProperties().put("tfCercador", tfCercador);
                        nodes[i].getProperties().put("pInfoDisp", pInfoDisp);
                        nodes[i].getProperties().put("btnEsborrarDispositiu", btnEsborrarDispositiu);

                        ObservableList<Node> hijos = ((HBox) nodes[i]).getChildren();

                        ((Label) hijos.get(0)).setText("Ayoub Mourou");
                        ((Label) hijos.get(1)).setText(dispositivos.get(i).getIdDispositivo());
                        ((Label) hijos.get(2)).setText(dispositivos.get(i).getDescripcion());
                        ((Label) hijos.get(3)).setText(String.valueOf(dispositivos.get(i).getLatitud()));
                        ((Label) hijos.get(4)).setText(String.valueOf(dispositivos.get(i).getLongitud()));
                        hijos.get(5).setId(String.valueOf(dispositivos.get(i).getId()));
                        ((Button) hijos.get(5)).setStyle("-fx-background-image: url('images/icons8-edit-48.png');");
                        hijos.get(6).setId(String.valueOf(dispositivos.get(i).getId()));
                        ((Button) hijos.get(6)).setStyle("-fx-background-image: url('images/icons8-trash-52.png');");

                        pnItems.getChildren().add(nodes[i]);
                        contadorNodos++;

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Gestiona los eventos relacionados con los diferentes botones de la vista de GestionDispositivos
     * @param actionEvent Info del botón pulsado en forma de evento
     * @throws IOException La excepción
     */
    public void handleClicksBotones(ActionEvent actionEvent) {
        if (actionEvent.getSource() == btWSM) {
            marcaBtnActivo(btWSM);
            mapView.setWMSParam(wmsParam);
            mapType = MapType.WMS;
        }

        if (actionEvent.getSource() == btOSM) {
            marcaBtnActivo(btOSM);
            mapType = MapType.OSM;
        }

        if (actionEvent.getSource() == btXYZ) {
            marcaBtnActivo(btXYZ);
            mapView.setXYZParam(xyzParams);
            mapType = MapType.XYZ;
        }

        mapView.setMapType(mapType);
    }

    /**
     * Gestiona los eventos relacionados con los diferentes botones de la vista de GestionDispositivos
     * @param actionEvent Info del botón pulsado en forma de evento
     * @throws IOException La excepción
     */
    public void handleClicks(ActionEvent actionEvent) throws IOException {
        if(actionEvent.getSource() == btnNouDispositiu){
            dispositivoEdit = new Dispositivo();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/GestionDispositivos.fxml"));

            Pane newPane = null;
            try {
                newPane = loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Cargamos la vista por defecto
            GestionDispositivosController gestDisp = loader.getController();
            final Projection projection = Projection.valueOf(ConfiguracionSistema.getInstancia().getProjection());// : Projection.WEB_MERCATOR;
            gestDisp.initMapAndControls(projection);

            //Guardamos el usuario y pasamos el emf
            gestDisp.setUsuarioLogueado(getUsuarioLogueado());
            gestDisp.setEmf(getEmf());

            gestDisp.setTfCercadorText(tfCercador.getText());

            // get children of parent of secPane (the AnchorPanel)
            List<Node> parentChildren = mainAnchor.getChildren();

            // replace the child that contained the old secPane
            parentChildren.set(parentChildren.indexOf(pForm), newPane);

            // store the new pane in the secPane field to allow replacing it the same way later
            pForm = newPane;

            gestDisp.setDispEdit(dispositivoEdit);
            gestDisp.setNewDisp(true);
            gestDisp.nouDisp(true);

        }

        if (actionEvent.getSource() == btnCerrarInfoDisp) {
            pInfoDisp.setVisible(false);
            cercarDispositius();
        }

        if (actionEvent.getSource() == btnCerrarInfoDispX) {
            pInfoDisp.setVisible(false);
            cercarDispositius();
        }

        if (actionEvent.getSource() == btSortir) {
            pForm.setVisible(false);
            pDispositius.setVisible(true);
            cercarDispositius();
        }
    }

    /**
     * Metodo que muestra el formulario de crear / editar dispositivo
     * @param isNewDisp Indica si es un nuevo dispositivo o se está editando un dispositivo
     */
    public void nouDisp(boolean isNewDisp){

        if(isNewDisp){
            afterMapIsInitialized();
            setNewDisp(true);
        } else {
            setFormData();
            setNewDisp(false);
        }

        pDispositius.setVisible(false);
        pForm.setVisible(true);
    }

    /**
     * Metodo que asigna y guarda el dispositivo que estamos creando o editando, es publica porque
     * necesitamos acceder al realizar los test graficos, pero y mejor que sea privada
     * @param d El dispositivo que vamos a crear / editar
     */
    public void setDispEdit (Dispositivo d){
        if (d == null){
            this.dispositivoEdit=new Dispositivo();
            setNewDisp(true);
        }
        else{
            this.dispositivoEdit=d;
            setNewDisp(false);
        }
    }

    /**
     * Metodo que asigna e indica si estamos creando un nuevo dispositivo isNew: true o editando un dispositivo: false
     * @param edDisp Booleanso que indica si es o no un nuevo dispositivo
     */
    private void setNewDisp(boolean edDisp){
        this.newDispositivo=edDisp;
    }

    /**
     * Metodo que asigna al buscador la cadena recibida, usado para comunicar los items
     * de la lista con las funciones de sus iconos
     * @param tfCercador Cadena de texto con el texto a buscar
     */
    private void setTfCercadorText(String tfCercador) {
        this.tfCercador.setText(tfCercador);
    }

    /**
     * Método que a partir del boton pulsado en la lista de dispositivos, carga sus datos en el formulario,
     * y gestiona su guardado con su control de errores.
     * @param actionEvent El evento clic del botón de la tabla de dispositivos pulsado
     */
    public void editarDisp(ActionEvent actionEvent) {

        idDispositivoEdit=Integer.parseInt(((Button) actionEvent.getSource()).getId());

        EntityManagerFactory emf= (EntityManagerFactory) ((Button) actionEvent.getSource()).getParent().getProperties().get("emf");
        Usuario getUsuarioLogueado = (Usuario) ((Button) actionEvent.getSource()).getParent().getProperties().get("getUsuarioLogueado");
        AnchorPane mainAnchor = (AnchorPane) ((Button) actionEvent.getSource()).getParent().getProperties().get("mainAnchor");
        Pane mainPanel = (Pane) ((Button) actionEvent.getSource()).getParent().getProperties().get("mainPanel");
        JFXTextField tfCercador = (JFXTextField) ((Button) actionEvent.getSource()).getParent().getProperties().get("tfCercador");

        DispositivoHelper<Dispositivo> daoHelper = new DispositivoHelper<>(emf, Dispositivo.class);
        dispositivoEdit = daoHelper.getById(idDispositivoEdit);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/GestionDispositivos.fxml"));

        Pane newPane = null;
        try {
            newPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Cargamos la vista por defecto
        GestionDispositivosController gestDisp = loader.getController();
        final Projection projection = Projection.valueOf(ConfiguracionSistema.getInstancia().getProjection());// : Projection.WEB_MERCATOR;
        gestDisp.initMapAndControls(projection);

        //Guardamos el usuario y pasamos el emf
        gestDisp.setUsuarioLogueado(getUsuarioLogueado);
        gestDisp.setEmf(emf);

        gestDisp.setTfCercadorText(tfCercador.getText());
        gestDisp.cercarDispositius();

        // get children of parent of secPane (the AnchorPanel)
        List<Node> parentChildren = mainAnchor.getChildren();

        // replace the child that contained the old secPane
        parentChildren.set(parentChildren.indexOf(mainPanel), newPane);

        // store the new pane in the secPane field to allow replacing it the same way later
        mainPanel = newPane;

        gestDisp.setDispEdit(dispositivoEdit);
        gestDisp.setNewDisp(false);
        gestDisp.nouDisp(false);

    }

    public void deleteDispositivo(ActionEvent actionEvent){

        idDispositivoEdit = Integer.valueOf(((Button) actionEvent.getSource()).getId());

        EntityManagerFactory emf= (EntityManagerFactory) ((Button) actionEvent.getSource()).getParent().getProperties().get("emf");
        Usuario getUsuarioLogueado = (Usuario) ((Button) actionEvent.getSource()).getParent().getProperties().get("getUsuarioLogueado");

        Pane pInfoDisp = (Pane) ((Button) actionEvent.getSource()).getParent().getProperties().get("pInfoDisp");
        JFXButton btnEsborrarDispositiu = (JFXButton) ((Button) actionEvent.getSource()).getParent().getProperties().get("btnEsborrarDispositiu");

        DispositivoHelper<Dispositivo> daoHelper = new DispositivoHelper<>(emf, Dispositivo.class);
        dispositivoEdit = daoHelper.getById(idDispositivoEdit);

        btnEsborrarDispositiu.setId(String.valueOf(idDispositivoEdit));
        btnEsborrarDispositiu.getProperties().put("emf", emf);
        btnEsborrarDispositiu.getProperties().put("getUsuarioLogueado", getUsuarioLogueado);

        pInfoDisp.setVisible(true);

    }

    public void esborrarDispositiu(ActionEvent actionEvent){

        idDispositivoEdit=Integer.parseInt(((JFXButton) actionEvent.getSource()).getId());

        EntityManagerFactory emf= (EntityManagerFactory) ((JFXButton) actionEvent.getSource()).getProperties().get("emf");

        DispositivoHelper<Dispositivo> daoHelper = new DispositivoHelper<>(emf, Dispositivo.class);
        dispositivoEdit = daoHelper.getById(idDispositivoEdit);

        daoHelper.delete(idDispositivoEdit);

        // Escondemos el panel que contiene el cuadro de dialogo de borrar usuario y recargamos lista
        ((JFXButton) actionEvent.getSource()).getParent().getParent().setVisible(false);
        cercarDispositius();

    }

    // </editor-fold>
}

