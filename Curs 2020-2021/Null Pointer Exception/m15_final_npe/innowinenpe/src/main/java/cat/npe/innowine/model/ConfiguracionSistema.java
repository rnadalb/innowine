package cat.npe.innowine.model;

import javafx.application.Application;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entidad Configuracion del Sistema
 */

@Entity
@Table (name = "tbl_config")
public class ConfiguracionSistema implements Serializable{
    private static final long serialVersionUID = 3047991830584132442L;

    // <editor-fold defaultstate="collapsed" desc="Variables">

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column (name = "intentos")
    private Integer intentos;

    @Column (name = "minimoPassword")
    private Integer minimoPassword;

    @Column (name = "asuntoCorreo")
    private String asuntoCorreo;

    @Column (name = "cuerpoCorreo")
    private String cuerpoCorreo;

    @Column (name = "direccionSmtp")
    private String direccionSmtp;

    @Column (name = "puertoSmtp")
    private Integer puertoSmtp;

    @Column (name = "protocoloSmtp")
    private String protocoloSmtp;

    @Column (name = "usuarioSmtp")
    private String usuarioSmtp;

    @Column (name = "passwordSMTP")
    private String passwordSmtp;

    @Column (name = "hostInflux")
    private String hostInflux;

    @Column (name = "puertoInflux")
    private Integer puertoInflux;

    @Column (name = "bbddInflux")
    private String bbddInflux;

    @Column (name = "usuarioInflux")
    private String usuarioInflux;

    @Column (name = "passwordInflux")
    private String passwordInflux;

    private String projection;

    private static ConfiguracionSistema instancia;

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Constructores">


    public ConfiguracionSistema() {
    }

    public static ConfiguracionSistema getInstancia(){
        if(instancia == null){
            instancia = new ConfiguracionSistema();
        }
        return instancia;
    }


    // </editor-fold">

    // <editor-fold defaultstate="collapsed" desc="Setters / Getters">

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getIntentos() {
        return intentos;
    }

    public void setIntentos(Integer intentos) {
        this.intentos = intentos;
    }


    public Integer getMinimoPassword() {
        return minimoPassword;
    }

    public void setMinimoPassword(Integer minimoPassword) {
        this.minimoPassword = minimoPassword;
    }


    public String getAsuntoCorreo() {
        return asuntoCorreo;
    }

    public void setAsuntoCorreo(String asuntoCorreo) {
        this.asuntoCorreo = asuntoCorreo;
    }


    public String getCuerpoCorreo() {
        return cuerpoCorreo;
    }

    public void setCuerpoCorreo(String cuerpoCorreo) {
        this.cuerpoCorreo = cuerpoCorreo;
    }


    public String getDireccionSmtp() {
        return direccionSmtp;
    }

    public void setDireccionSmtp(String direccionSmtp) {
        this.direccionSmtp = direccionSmtp;
    }


    public Integer getPuertoSmtp() {
        return puertoSmtp;
    }

    public void setPuertoSmtp(Integer puertoSmtp) {
        this.puertoSmtp = puertoSmtp;
    }


    public String getProtocoloSmtp() {
        return protocoloSmtp;
    }

    public void setProtocoloSmtp(String protocoloSmtp) {
        this.protocoloSmtp = protocoloSmtp;
    }


    public String getUsuarioSmtp() {
        return usuarioSmtp;
    }

    public void setUsuarioSmtp(String usuarioSmtp) {
        this.usuarioSmtp = usuarioSmtp;
    }


    public String getPasswordSmtp() {
        return passwordSmtp;
    }

    public void setPasswordSmtp(String passwordSmtp) {
        this.passwordSmtp = passwordSmtp;
    }


    public String getHostInflux() {
        return hostInflux;
    }

    public void setHostInflux(String hostInflux) {
        this.hostInflux = hostInflux;
    }


    public Integer getPuertoInflux() {
        return puertoInflux;
    }

    public void setPuertoInflux(Integer puertoInflux) {
        this.puertoInflux = puertoInflux;
    }


    public String getBbddInflux() {
        return bbddInflux;
    }

    public void setBbddInflux(String bbddInflux) {
        this.bbddInflux = bbddInflux;
    }


    public String getUsuarioInflux() {
        return usuarioInflux;
    }

    public void setUsuarioInflux(String usuarioInflux) {
        this.usuarioInflux = usuarioInflux;
    }


    public String getPasswordInflux() {
        return passwordInflux;
    }

    public void setPasswordInflux(String passwordInflux) {
        this.passwordInflux = passwordInflux;
    }

    public String getProjection() {
        return projection;
    }

    public void setProjection(String projection) {
        this.projection = projection;
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Métodos">

    @Override
    public String toString() {
        return "ConfiguracionSistema{" +
                "id=" + id +
                ", intentos=" + intentos +
                ", minimoPassword=" + minimoPassword +
                ", asuntoCorreo='" + asuntoCorreo + '\'' +
                ", cuerpoCorreo='" + cuerpoCorreo + '\'' +
                ", direccionSmtp='" + direccionSmtp + '\'' +
                ", puertoSmtp=" + puertoSmtp +
                ", protocoloSmtp='" + protocoloSmtp + '\'' +
                ", usuarioSmtp='" + usuarioSmtp + '\'' +
                ", passwordSmtp='" + passwordSmtp + '\'' +
                ", hostInflux='" + hostInflux + '\'' +
                ", puertoInflux=" + puertoInflux +
                ", bbddInflux='" + bbddInflux + '\'' +
                ", usuarioInflux='" + usuarioInflux + '\'' +
                ", passwordInflux='" + passwordInflux + '\'' +
                '}';
    }
    // </editor-fold">


}
