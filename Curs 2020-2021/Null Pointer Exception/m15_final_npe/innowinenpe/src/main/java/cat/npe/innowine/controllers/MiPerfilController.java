package cat.npe.innowine.controllers;

import cat.npe.innowine.classes.Alerta;
import cat.npe.innowine.classes.UtilsDB;
import cat.npe.innowine.controllers.helpers.SecurityHelper;
import cat.npe.innowine.controllers.helpers.UsuarioHelper;
import cat.npe.innowine.exceptions.ConfirmationDialogException;
import cat.npe.innowine.exceptions.SelectedYesConfirmationDialogException;
import cat.npe.innowine.model.Usuario;
import cat.npe.innowine.states.CambioPasswordState;
import com.jfoenix.controls.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * Clase que gestiona la vista de mi perfil
 */
public class MiPerfilController extends DefaultController {

    // <editor-fold defaultstate="collapsed" desc="Variables">

    /**
     * Panel con los inputs del formulario
     */
    @FXML
    private Pane pForm;
    /**
     * Campo indicativo del correo electronico, no modificable.
     */
    @FXML
    JFXTextField tfInputEmail;

    /**
     * Campo indicativo del nombre del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputNom;

    /**
     * Campo indicativo del primer apellido del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputCognom1;

    /**
     * Campo indicativo del segundo apellido del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputCognom2;

    /**
     * Campo indicativo del telefono del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputTelefon;

    /**
     * Text Area para introducir la dirección del usuario, mdificable
     */
    @FXML
    private JFXTextArea tfDireccio;

    /**
     * Campo indicativo de la población del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputPoblacio;

    /**
     * Campo indicativo del código postal del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputCodiPostal;

    /**
     * Campo indicativo del Pais del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputPais;

    /**
     * Agrupa los radiobuttons para que sean mutuamente excluyentes,al elegir la imagen del avatar
     */
    final ToggleGroup avatarRadioButtonGroup = new ToggleGroup();

    /**
     * Variables de los 12 radioButtons
     */
    @FXML
    private JFXRadioButton rbAvatar1, rbAvatar2, rbAvatar3, rbAvatar4, rbAvatar5, rbAvatar6, rbAvatar7, rbAvatar8, rbAvatar9, rbAvatar10, rbAvatar11, rbAvatar12;

    /**
     * Input para introducir el pass actual
     */
    @FXML
    private JFXPasswordField tfInputPassActual;

    /**
     * Input para introducir el nuevo pass
     */
    @FXML
    private JFXPasswordField tfInputPassNou;

    /**
     * Input para repetir el nuevo pass
     */
    @FXML
    private JFXPasswordField tfInputPassNou2;

    /**
     * Indica si al guardar datos del usuario, se debe guardar el nuevo password
     */
    boolean cambiarPass = false;

    /**
     * Botón de Guardar la información del usuario.
     */
    @FXML
    private JFXButton btnGuardar;

    /**
     * Panel informativo del resultado de la operacion de guardar y si hay errores en el formulario
     */
    @FXML
    private Pane pInfo;

    /**
     * Label para mostrar errores del formulario y resultado de la operación
     */
    @FXML
    private Label lbError;

    /**
     * Label para mostrar exito del resultado de la operación
     */
    @FXML
    private Label lbSuccess;

    /**
     * Indicador de progreso infinito
     */
    @FXML
    private ProgressIndicator pgIndicador;

    /**
     * Botón para cerrar el panel de información de la operacion de guardar
     */
    @FXML
    private JFXButton btnCerrarInfo;

    /**
     * Botón para salir de la aplicación en el panel informativo del resultado de la operación
     */
    @FXML
    private JFXButton btnSalirInfo;

    /**
     * Botón para cerrar el panel de informacion, situaldo en la esquina derecha en forma de X (aspa)
     */
    @FXML
    private JFXButton btnCerrarInfoX;

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Métodos">

    /**
     * Recupera la información del usuario y la pone en el campo correspondiente del formulario
     * Asigna eventos de onBlur, al perder el foco, para comprobar si esta vacío y mostrar un error visual
     * Reformatea la entrada del usuario al equivalente text-tranform:capitalize;
     */

    public void setFormData(){

        tfInputEmail.setText(getUsuarioLogueado().getEmail());
        tfInputNom.setText(getUsuarioLogueado().getNombre());
        tfInputCognom1.setText(getUsuarioLogueado().getApellido1());
        tfInputCognom2.setText(getUsuarioLogueado().getApellido2());
        tfInputTelefon.setText(getUsuarioLogueado().getTelefono());
        tfDireccio.setText(getUsuarioLogueado().getDireccion());
        tfInputCodiPostal.setText(getUsuarioLogueado().getCodigoPostal());
        tfInputPoblacio.setText(getUsuarioLogueado().getPoblacion());
        tfInputPais.setText(getUsuarioLogueado().getPais());

        addListenerFocused(tfInputNom);
        addListenerFocused(tfInputCognom1);
        addListenerFocused(tfInputCognom2);

        // https://docs.oracle.com/javafx/2/ui_controls/radio-button.htm

        rbAvatar1.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar2.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar3.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar4.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar5.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar6.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar7.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar8.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar9.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar10.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar11.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar12.setToggleGroup(avatarRadioButtonGroup);

        switch (getUsuarioLogueado().getAvatar()){
            case "1":  rbAvatar1.setSelected(true); break;
            case "2":  rbAvatar2.setSelected(true); break;
            case "3":  rbAvatar3.setSelected(true); break;
            case "4":  rbAvatar4.setSelected(true); break;
            case "5":  rbAvatar5.setSelected(true); break;
            case "6":  rbAvatar6.setSelected(true); break;
            case "7":  rbAvatar7.setSelected(true); break;
            case "8":  rbAvatar8.setSelected(true); break;
            case "9":  rbAvatar9.setSelected(true); break;
            case "10": rbAvatar10.setSelected(true); break;
            case "11": rbAvatar11.setSelected(true); break;
            case "12": rbAvatar12.setSelected(true); break;
        }

        tfInputNom.requestFocus();

    }

    /**
     * Añade al JFXTextField pasado por parametro un evento blur para controlar si hay errores
     * modificando el input de forma visual
     * @param tfInput El JFXTextField a aplicarle el evento de escucha
     */

    public void addListenerFocused(JFXTextField tfInput){

         /* No dejaba poner espacios
        tfInput.textProperty().addListener((obs, oldText, newText) -> {
            tfInput.setText(capitalizeWord(newText));
        });*/

        tfInput.focusedProperty().addListener((obs, oldVal, newVal) -> {

            if(!newVal){ // Si no hay nuevo valor, es que ha perdido el foco -> blur
                String value=tfInput.getText();

                if(value.length()==0){
                    tfInput.setUnFocusColor(Color.web("red"));
                }
                else{
                    tfInput.setText(capitalizeWord(value));
                    tfInput.setUnFocusColor(Color.web("#030303"));
                }
            }
        });
    }

    /**
     * Método que gestiona el panel informativo y en caso de que el formulario este correcto guarda
     * el usuario y en caso de error: los muestra.
     */
    public void guardar(){

        boolean valido=true;
        btnGuardar.setDisable(true);
        btnCerrarInfo.setVisible(false);
        btnSalirInfo.setVisible(false);
        lbError.setVisible(false);
        lbSuccess.setVisible(false);
        pgIndicador.setVisible(true);
        pInfo.setVisible(true);

        btnCerrarInfo.requestFocus();

            valido=verificarFormulario();

        if(valido){
            updateUsuario();
            pgIndicador.setVisible(false);
            lbSuccess.setText("Operaci\u00f3 realitzada correctament.\nEls canvis al men\u00fa lateral es veuran reflectits en tornar a iniciar sessió.");
            lbSuccess.setVisible(true);
        }
        else{
            pgIndicador.setVisible(false);
            lbError.setVisible(true);
        }

        btnCerrarInfo.setVisible(true);
        btnSalirInfo.setVisible(true);
        btnGuardar.setDisable(false);

    }

    /**
     * Verifica que los campos obligatorios del formulario esten completados, sino cambia el input de estilo
     * para resaltar donde están los errores. Si el campo passActual esta escrito, comprobará los inputs relacionados
     * con el cambio de contraseña.
     * @return Indica si el formulario ha pasado las validaciones
     */
    public boolean verificarFormulario(){

        boolean valido = true;
        String msg = "";
        boolean errorInputVacio=false;

        /* Como solo son 3 campos obligatorios, y si rellena pass entonces compruebo esos 3 adicionales
         * No uso este bucle

        for (Node node : pForm.getChildren()) {
            if (node instanceof JFXTextField) {
                ((JFXTextField)node).setUnFocusColor(Color.web("#030303"));
                if(((JFXTextField)node).getText().length()==0){
                    ((JFXTextField)node).setUnFocusColor(Color.web("red"));
                    valido=false;
                    errorInputVacio=true;
                }
            }
        }
        * */

        tfInputNom.setUnFocusColor(Color.web("#030303"));
        tfInputCognom1.setUnFocusColor(Color.web("#030303"));
        tfInputCognom2.setUnFocusColor(Color.web("#030303"));

        if(tfInputNom.getText().length()==0){
            tfInputNom.setUnFocusColor(Color.web("red"));
            valido=false;
            errorInputVacio=true;
        }

        if(tfInputCognom1.getText().length()==0){
            tfInputCognom1.setUnFocusColor(Color.web("red"));
            valido=false;
            errorInputVacio=true;
        }

        if(tfInputCognom2.getText().length()==0){
            tfInputCognom2.setUnFocusColor(Color.web("red"));
            valido=false;
            errorInputVacio=true;
        }

        if(errorInputVacio){
            msg=msg.concat("Els camps obligatoris no poden estar buits.\n");
        }

        tfInputPassActual.setUnFocusColor(Color.web("#030303"));
        tfInputPassNou.setUnFocusColor(Color.web("#030303"));
        tfInputPassNou2.setUnFocusColor(Color.web("#030303"));

        if(tfInputPassActual.getText().length()!=0 || tfInputPassNou.getText().length()!=0 || tfInputPassNou2.getText().length()!=0 ) {
            UtilsDB utilsDB = new UtilsDB(getEmf());

            CambioPasswordState estado = utilsDB.changePassword(false,getUsuarioLogueado(), tfInputPassActual.getText(), tfInputPassNou.getText(), tfInputPassNou2.getText());

            if (estado != CambioPasswordState.OK) {

                msg = msg.concat(estado.getMessage());
                tfInputPassActual.setUnFocusColor(Color.web("red"));
                tfInputPassNou.setUnFocusColor(Color.web("red"));
                tfInputPassNou2.setUnFocusColor(Color.web("red"));
                valido=false;
                cambiarPass=false;
            }
            else{
                cambiarPass=true;
            }
        }

        lbError.setText(msg);

        return valido;
    }

    /**
     * En caso de que el formulario sea valido, instancia el usuario logueado para modificar sus datos, y
     * también guarda en base de datos los datos modificados.
     */

    public void updateUsuario(){

        Usuario usuario = this.getUsuarioLogueado();

        usuario.setNombre(capitalizeWord(tfInputNom.getText()));
        usuario.setApellido1(capitalizeWord(tfInputCognom1.getText()));
        usuario.setApellido2(capitalizeWord(tfInputCognom2.getText()));
        usuario.setTelefono(tfInputTelefon.getText());
        usuario.setDireccion(tfDireccio.getText());

        if(tfInputPoblacio.getText()!=null) {
            usuario.setPoblacion(capitalizeWord(tfInputPoblacio.getText()));
        }
        else{
            usuario.setPoblacion(tfInputPoblacio.getText());
        }
        usuario.setCodigoPostal(tfInputCodiPostal.getText());

        if(tfInputPais.getText()!=null) {
            usuario.setPais(capitalizeWord(tfInputPais.getText()));
        }
        else{
            usuario.setPoblacion(tfInputPais.getText());
        }

        String idRadioButton="-1";
        for (Node node : pForm.getChildren()) {
            if (node instanceof JFXRadioButton) {
                if(((JFXRadioButton)node).isSelected()){
                    idRadioButton=((JFXRadioButton)node).getId();
                    idRadioButton=idRadioButton.replace("rbAvatar","");
                }

            }
        }

        if(idRadioButton.equals("-1")){
            usuario.setAvatar("1");
        }
        else{
            usuario.setAvatar(idRadioButton);
        }

        if(cambiarPass){
            SecurityHelper sh = new SecurityHelper();
            usuario.setPassword(sh.passwordEncrypt(tfInputPassNou.getText()));
        }


        UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(this.getEmf(), Usuario.class);

        daoHelper.update(usuario);
        setUsuarioLogueado(usuario);

        if(cambiarPass){
            tfInputPassActual.setText("");
            tfInputPassNou.setText("");
            tfInputPassNou2.setText("");
        }

    }

    /**
     * Devuelve una cadena donde la primera letra de cada palabra se ha convetido a maýusculas
     * @param str Cadena de texto a transformar
     * @return La cadena transformada
     */

    public String capitalizeWord(String str){

        if(str.length()!=0) {
            String words[] = str.split("\\s");
            String capitalizeWord = "";
            for (String w : words) {
                String first = w.substring(0, 1);
                String afterfirst = w.substring(1);
                capitalizeWord += first.toUpperCase() + afterfirst.toLowerCase() + " ";
            }
            return capitalizeWord.trim();
        }else{
            return new String("");
        }
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Eventos">

    /**
     * Gestiona los eventos relacionados con los diferentes botones de la vista de MiPerfil
     *
     * @param actionEvent Info del botón pulsado en forma de evento
     */
    public void handleClicks(ActionEvent actionEvent) {

        if (actionEvent.getSource() == btnGuardar) {
            /* Como siempre al dar enter detecta que es el btnGuardar, compruebo si es el
            * panel informativo el que esta visible y en ese caso lo cierro, de lo contrario es el
            * botón guardar y guardo el formulario. Se ha podido hacer esto porque solo hay 2 botones
            * en esta vista */
            if(pInfo.isVisible()){
                btnCerrarInfo.setVisible(false);
                btnSalirInfo.setVisible(false);
                pInfo.setVisible(false);
            }
            else{
                guardar();
            }

        }

        if (actionEvent.getSource() == btnCerrarInfo) {
            btnCerrarInfo.setVisible(false);
            btnSalirInfo.setVisible(false);
            pInfo.setVisible(false);
        }

        if (actionEvent.getSource() == btnCerrarInfoX) {
            btnCerrarInfo.setVisible(false);
            btnSalirInfo.setVisible(false);
            pInfo.setVisible(false);
        }

        if(actionEvent.getSource() == btnSalirInfo){
            try {
                Alerta.alertaConfirmacio("App: Sortir", "Segur que vol sortir de l'App?","Trieu una opcio");
            } catch (ConfirmationDialogException e) {
                if (e instanceof SelectedYesConfirmationDialogException) {
                    Platform.exit(); // Atura l'App
                } else {
                    actionEvent.consume(); // Consumeix un esdeveniment
                }
            }
        }
    }

    /* No ha hecho falta usarla
    public void handleKeys(KeyEvent kevent){

        if(kevent.getCode()== KeyCode.ENTER){
                        guardar();
                    }

    }
    */

    // </editor-fold>
}
