package cat.npe.innowine.controllers;

import cat.npe.innowine.classes.UtilsDB;
import cat.npe.innowine.controllers.helpers.DispositivoHelper;
import cat.npe.innowine.controllers.helpers.EmailHelper;
import cat.npe.innowine.controllers.helpers.SecurityHelper;
import cat.npe.innowine.controllers.helpers.UsuarioHelper;
import cat.npe.innowine.model.Dispositivo;
import cat.npe.innowine.model.Usuario;
import cat.npe.innowine.states.CambioPasswordState;
import com.jfoenix.controls.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.util.List;

/**
 * Clase que gestiona la vista de gestión de usuarios
 */
public class GestionUsuariosController extends DefaultController {

    // <editor-fold defaultstate="collapsed" desc="Variables">

    /**
     * Componente donde se creará la lista de usuarios del sistema
     */
    @FXML
    private ScrollPane scrollUsers;

    /**
     * Panel con los inputs del formulario
     */
    @FXML
    private Pane pForm;

    /**
     * Panel con la tabla de los usuarios del sistema
     */
    @FXML
    private Pane pUsers;

    /**
     * Panel con el dialogo de confirmación de advertencia de borrar un usuario y todos sus datos asociados: dispositivos, ...
     */
    @FXML
    private Pane pInfoUser;

    /**
     * Btn de confirmación de borrar usuario
     */
    @FXML
    private JFXButton btnEsborrarUsuari;


    /**
     * Botón para cerrar el panel de información de la operacion de borrar
     */
    @FXML
    private JFXButton btnCerrarInfoUser;

    /**
     * Botón cuadrado con un aspa para cerrar el dialogo de confirmación de borrar usuario
     */
    @FXML
    private JFXButton btnCerrarInfoUserX;


    /**
     * Botón para crear un nuevo usuario: desoculta formulario de creación.
     */
    @FXML
    private JFXButton btnNouUsuari;


    /**
     * Input del buscador de usuarios
     */
    @FXML
    private JFXTextField tfCercador;


    /**
     * Componente a modo de tabla donde insertaremos los elementos HBox a modo de filas con la información de cada usuario.
     */
    @FXML
    private VBox pnItems;


    /**
     * Guardamos el id del usuario que estamos modificando
     */
    private Integer idUserEdit;

    /**
     * Guardamos el usuario que estamos creando o editando
     */
    private Usuario userEdit;

    /**
     * Indica si estamos creando o editando un usuario
     */

    private boolean newUser;

    /**
     * Texto que muestra titulo de la vista: será dinamica ya que mostrará nombre del usuario
     */
    @FXML
    private Text tTextPerfil;

    /**
     * Etiqueta email obligatorio
     */
    @FXML
    Label lbEmailObligatorio;


    /**
     * Campo indicativo del correo electronico, no modificable.
     */
    @FXML
    JFXTextField tfInputEmail;

    /**
     * Campo indentificativo si el usuario esta bloqueado o no
     */
    @FXML
    JFXCheckBox cbBloquejat;

    /**
     * Campo indicativo del nombre del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputNom;

    /**
     * Campo indicativo del primer apellido del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputCognom1;

    /**
     * Campo indicativo del segundo apellido del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputCognom2;

    /**
     * Campo indicativo del telefono del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputTelefon;

    /**
     * Text Area para introducir la dirección del usuario, mdificable
     */
    @FXML
    private JFXTextArea tfDireccio;

    /**
     * Campo indicativo de la población del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputPoblacio;

    /**
     * Campo indicativo del código postal del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputCodiPostal;

    /**
     * Campo indicativo del Pais del usuario, modificable.
     */
    @FXML
    private JFXTextField tfInputPais;

    /**
     * Agrupa los radiobuttons para que sean mutuamente excluyentes,al elegir la imagen del avatar
     */
    final ToggleGroup avatarRadioButtonGroup = new ToggleGroup();

    /**
     * Variables de los 12 radioButtons
     */
    @FXML
    private JFXRadioButton rbAvatar1, rbAvatar2, rbAvatar3, rbAvatar4, rbAvatar5, rbAvatar6, rbAvatar7, rbAvatar8, rbAvatar9, rbAvatar10, rbAvatar11, rbAvatar12;

    /**
     * Input para introducir el nuevo pass
     */
    @FXML
    private JFXPasswordField tfInputPassNou;


    /**
     * Indica si al guardar datos del usuario, se debe guardar el nuevo password
     */
    boolean cambiarPass = false;

    /**
     * Botón de Guardar la información del usuario.
     */
    @FXML
    private JFXButton btnGuardar;

    /**
     * Boton en forma de aspa para cerrar la vista de usuario
     */
    @FXML
    private JFXButton btnCerrarUserX;

    /**
     * Panel informativo del resultado de la operacion de guardar y si hay errores en el formulario
     */
    @FXML
    private Pane pInfo;

    /**
     * Label para mostrar errores del formulario y resultado de la operación
     */
    @FXML
    private Label lbError;

    /**
     * Label para mostrar exito del resultado de la operación
     */
    @FXML
    private Label lbSuccess;

    /**
     * Indicador de progreso infinito
     */
    @FXML
    private ProgressIndicator pgIndicador;

    /**
     * Botón para cerrar el panel de información de la operacion de guardar
     */
    @FXML
    private JFXButton btnCerrarInfo;

    /**
     * Botón para cerrar el panel de informacion, situaldo en la esquina derecha en forma de X (aspa)
     */
    @FXML
    private JFXButton btnCerrarInfoX;

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Métodos">

    /**
     * Recupera la información del usuario y la pone en el campo correspondiente del formulario
     * Asigna eventos de onBlur, al perder el foco, para comprobar si esta vacío y mostrar un error visual
     * Reformatea la entrada del usuario al equivalente text-tranform:capitalize;
     */

    public void setFormData(){

        if(newUser){
            tTextPerfil.setText("Perfil de Nou Usuari");
        }
        else{
            tTextPerfil.setText("Perfil de " + userEdit.getNombre() + " " + userEdit.getApellido1());
        }

        tfInputEmail.setText(userEdit.getEmail());

        if(userEdit.isBloqueado()){
            cbBloquejat.setSelected(true);
        } else{
            cbBloquejat.setSelected(false);
        }

        tfInputNom.setText(userEdit.getNombre());
        tfInputCognom1.setText(userEdit.getApellido1());
        tfInputCognom2.setText(userEdit.getApellido2());
        tfInputTelefon.setText(userEdit.getTelefono());
        tfDireccio.setText(userEdit.getDireccion());
        tfInputCodiPostal.setText(userEdit.getCodigoPostal());
        tfInputPoblacio.setText(userEdit.getPoblacion());
        tfInputPais.setText(userEdit.getPais());

        addListenerFocused(tfInputNom);
        addListenerFocused(tfInputCognom1);
        addListenerFocused(tfInputCognom2);

        // https://docs.oracle.com/javafx/2/ui_controls/radio-button.htm

        rbAvatar1.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar2.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar3.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar4.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar5.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar6.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar7.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar8.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar9.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar10.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar11.setToggleGroup(avatarRadioButtonGroup);
        rbAvatar12.setToggleGroup(avatarRadioButtonGroup);

        if(userEdit.getAvatar() == null) userEdit.setAvatar("1");

        switch (userEdit.getAvatar()){
            case "1":  rbAvatar1.setSelected(true); break;
            case "2":  rbAvatar2.setSelected(true); break;
            case "3":  rbAvatar3.setSelected(true); break;
            case "4":  rbAvatar4.setSelected(true); break;
            case "5":  rbAvatar5.setSelected(true); break;
            case "6":  rbAvatar6.setSelected(true); break;
            case "7":  rbAvatar7.setSelected(true); break;
            case "8":  rbAvatar8.setSelected(true); break;
            case "9":  rbAvatar9.setSelected(true); break;
            case "10": rbAvatar10.setSelected(true); break;
            case "11": rbAvatar11.setSelected(true); break;
            case "12": rbAvatar12.setSelected(true); break;
        }

        tfInputNom.requestFocus();

    }

    /**
     * Añade al JFXTextField pasado por parametro un evento blur para controlar si hay errores
     * modificando el input de forma visual
     * @param tfInput El JFXTextField a aplicarle el evento de escucha
     */

    public void addListenerFocused(JFXTextField tfInput){

         /* No dejaba poner espacios
        tfInput.textProperty().addListener((obs, oldText, newText) -> {
            tfInput.setText(capitalizeWord(newText));
        });*/

        tfInput.focusedProperty().addListener((obs, oldVal, newVal) -> {

            if(!newVal){ // Si no hay nuevo valor, es que ha perdido el foco -> blur
                String value=tfInput.getText();

                if(value.length()==0){
                    tfInput.setUnFocusColor(Color.web("red"));
                }
                else{
                    tfInput.setText(capitalizeWord(value));
                    tfInput.setUnFocusColor(Color.web("#030303"));
                }
            }
        });
    }

    /**
     * Método que gestiona el panel informativo y en caso de que el formulario este correcto guarda
     * el usuario y en caso de error: los muestra.
     */
    public void guardar(){

        boolean valido=true;
        btnGuardar.setDisable(true);
        btnCerrarInfo.setVisible(false);
        lbError.setVisible(false);
        lbSuccess.setVisible(false);
        pgIndicador.setVisible(true);
        pInfo.setVisible(true);

        btnCerrarInfo.requestFocus();

        valido=verificarFormulario();

        if(valido){
            updateUsuario();
            pgIndicador.setVisible(false);
            lbSuccess.setText("Operaci\u00f3 realitzada correctament.\n");
            lbSuccess.setVisible(true);
        }
        else{
            pgIndicador.setVisible(false);
            lbError.setVisible(true);
        }

        btnCerrarInfo.setVisible(true);
        btnGuardar.setDisable(false);

    }

    /**
     * Verifica que los campos obligatorios del formulario esten completados, sino cambia el input de estilo
     * para resaltar donde están los errores. Si el campo passActual esta escrito, comprobará los inputs relacionados
     * con el cambio de contraseña.
     * @return Indica si el formulario ha pasado las validaciones
     */
    public boolean verificarFormulario(){

        boolean valido = true;
        String msg = "";
        boolean errorInputVacio=false;

        tfInputEmail.setUnFocusColor(Color.web("#030303"));
        tfInputNom.setUnFocusColor(Color.web("#030303"));
        tfInputCognom1.setUnFocusColor(Color.web("#030303"));
        tfInputCognom2.setUnFocusColor(Color.web("#030303"));

        if(newUser){
            if(tfInputEmail.getText()==null || tfInputEmail.getText().length()==0){
                tfInputEmail.setUnFocusColor(Color.web("red"));
                valido=false;
                errorInputVacio=true;
            }
        }

        if(tfInputNom.getText()==null || tfInputNom.getText().length()==0){
            tfInputNom.setUnFocusColor(Color.web("red"));
            valido=false;
            errorInputVacio=true;
        }

        if(tfInputCognom1.getText()==null || tfInputCognom1.getText().length()==0){
            tfInputCognom1.setUnFocusColor(Color.web("red"));
            valido=false;
            errorInputVacio=true;
        }

        if(tfInputCognom2.getText()==null || tfInputCognom2.getText().length()==0){
            tfInputCognom2.setUnFocusColor(Color.web("red"));
            valido=false;
            errorInputVacio=true;
        }

        tfInputPassNou.setUnFocusColor(Color.web("#030303"));

        if(newUser){
            if(tfInputPassNou.getText()==null || tfInputPassNou.getText().length()==0) {
                tfInputPassNou.setUnFocusColor(Color.web("red"));
                valido=false;
                errorInputVacio=true;
            }
        }

        if(errorInputVacio){
            msg=msg.concat("Els camps obligatoris no poden estar buits.\n");
        }

        if(valido)
        {
            if(newUser){

                try {
                    InternetAddress emailAddr = new InternetAddress(tfInputEmail.getText());
                    emailAddr.validate();


                } catch (AddressException ex) {
                    msg = msg.concat("El camp email no t\u00e9 el format correcte.\n");
                    valido = false;
                }

                // Comprobamos que el email no exista
                UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(this.getEmf(), Usuario.class);
                Usuario existeUser = daoHelper.findUserByEmail(tfInputEmail.getText());

                if(existeUser!=null){
                    msg = msg.concat("Aquest email pertany a un usuari del sistema. Posi un altre ...\n");
                    valido = false;
                }

            }

            if(tfInputPassNou.getText()==null || tfInputPassNou.getText().length()!=0) {

                UtilsDB utilsDB = new UtilsDB(getEmf());
                CambioPasswordState estado;

                estado = utilsDB.changePasswordByAdmin(userEdit,tfInputPassNou.getText());

                if (estado != CambioPasswordState.OK) {

                    msg = msg.concat(estado.getMessage());
                    tfInputPassNou.setUnFocusColor(Color.web("red"));

                    valido=false;
                    cambiarPass=false;
                }
                else{
                    cambiarPass=true;
                }
            }
        }

        lbError.setText(msg);

        return valido;
    }

    /**
     * En caso de que el formulario sea valido, instancia el usuario logueado para modificar sus datos, y
     * también guarda en base de datos los datos modificados.
     */

    public void updateUsuario(){

        if(newUser){
            userEdit.setEmail(tfInputEmail.getText().toLowerCase());
            userEdit.setIntentos(0);
            userEdit.setCambioPassword(true);
        }

        userEdit.setNombre(capitalizeWord(tfInputNom.getText()));
        userEdit.setApellido1(capitalizeWord(tfInputCognom1.getText()));
        userEdit.setApellido2(capitalizeWord(tfInputCognom2.getText()));
        userEdit.setTelefono(tfInputTelefon.getText());
        userEdit.setDireccion(tfDireccio.getText());

        if(tfInputPoblacio.getText()!=null) {
            userEdit.setPoblacion(capitalizeWord(tfInputPoblacio.getText()));
        }
        else{
            userEdit.setPoblacion(tfInputPoblacio.getText());
        }
        userEdit.setCodigoPostal(tfInputCodiPostal.getText());

        if(tfInputPais.getText()!=null) {
            userEdit.setPais(capitalizeWord(tfInputPais.getText()));
        }
        else{
            userEdit.setPoblacion(tfInputPais.getText());
        }

        String idRadioButton="-1";
        for (Node node : pForm.getChildren()) {
            if (node instanceof JFXRadioButton) {
                if(((JFXRadioButton)node).isSelected()){
                    idRadioButton=((JFXRadioButton)node).getId();
                    idRadioButton=idRadioButton.replace("rbAvatar","");
                }

            }
        }

        if(idRadioButton.equals("-1")){
            userEdit.setAvatar("1");
        }
        else{
            userEdit.setAvatar(idRadioButton);
        }

        if(cbBloquejat.isSelected()){
            userEdit.setBloqueado(true);
        }else{
            userEdit.setBloqueado(false);
        }

        if(cambiarPass){
            SecurityHelper sh = new SecurityHelper();
            userEdit.setPassword(sh.passwordEncrypt(tfInputPassNou.getText()));
        }

        UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(this.getEmf(), Usuario.class);
        EmailHelper emailHelper = new EmailHelper();

        if(newUser){
            daoHelper.insert(userEdit);
            emailHelper.sendWelcomeEmail(userEdit.getEmail(),tfInputPassNou.getText());
        }else{

            if(cambiarPass){
                userEdit.setCambioPassword(true);
                userEdit.setBloqueado(true);
                daoHelper.update(userEdit);
                emailHelper.sendAdminChangePass(userEdit.getEmail(),tfInputPassNou.getText());
            }else{
                daoHelper.update(userEdit);
            }
        }

        if(cambiarPass){

            tfInputPassNou.setText("");
        }

    }

    /**
     * Devuelve una cadena donde la primera letra de cada palabra se ha convetido a maýusculas
     * @param str Cadena de texto a transformar
     * @return La cadena transformada
     */

    public String capitalizeWord(String str){

        if(str.length()!=0) {
            String words[] = str.split("\\s");
            String capitalizeWord = "";
            for (String w : words) {
                String first = w.substring(0, 1);
                String afterfirst = w.substring(1);
                capitalizeWord += first.toUpperCase() + afterfirst.toLowerCase() + " ";
            }
            return capitalizeWord.trim();
        }else{
            return new String("");
        }
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Eventos">

    /**
     * Gestiona los eventos relacionados con los diferentes botones de la vista de MiPerfil
     *
     * @param actionEvent Info del botón pulsado en forma de evento
     */
    public void handleClicks(ActionEvent actionEvent) {

        if(actionEvent.getSource() == btnNouUsuari){

            userEdit= new Usuario();
            setUserEdit(userEdit);
            setNewUser(true);
            nouUser(true);

        }


        if (actionEvent.getSource() == btnGuardar) {
            /* Como siempre al dar enter detecta que es el btnGuardar, compruebo si es el
            * panel informativo el que esta visible y en ese caso lo cierro, de lo contrario es el
            * botón guardar y guardo el formulario. Se ha podido hacer esto porque solo hay 2 botones
            * en esta vista */
            if(pInfo.isVisible()){
                btnCerrarInfo.setVisible(false);
                pInfo.setVisible(false);
            }
            else{
                guardar();
                cercarUsuaris();
            }

        }

        if (actionEvent.getSource() == btnCerrarUserX) {
            btnCerrarInfo.setVisible(false);
            pInfo.setVisible(false);
            pForm.setVisible(false);
            pUsers.setVisible(true);
        }

        if (actionEvent.getSource() == btnCerrarInfo) {
            btnCerrarInfo.setVisible(false);
            pInfo.setVisible(false);
        }

        if (actionEvent.getSource() == btnCerrarInfoX) {
            btnCerrarInfo.setVisible(false);
            pInfo.setVisible(false);
        }

        if (actionEvent.getSource() == btnCerrarInfoUser) {
            pInfoUser.setVisible(false);
        }

        if (actionEvent.getSource() == btnCerrarInfoUserX) {
            pInfoUser.setVisible(false);
        }
    }

    /**
     * Método que busca a partir de un imput los usuarios del sistema y los carga en un ScrollPane
     */

    public void cercarUsuaris(){

        UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(this.getEmf(), Usuario.class);
        List<Usuario> usuarios = null;

        if(tfCercador.getLength()==0) {

            usuarios = daoHelper.getAll();
        }
        else{

            usuarios = daoHelper.findUserBySearchString(tfCercador.getText());
        }

        pnItems.getChildren().clear();

        // Si hay usuarios

        if( usuarios!=null && !usuarios.isEmpty()){

            Node[] nodes = new Node[usuarios.size()];

            int contadorNodos=0;

            for (int i = 0; i < nodes.length; i++) {
                try {

                    if(usuarios.get(i).getId() == getUsuarioLogueado().getId() || usuarios.get(i).isAdmin()) continue; // No mostramos a el mismo enla lista

                    final int j = i;
                    nodes[i] = FXMLLoader.load(getClass().getResource("/views/UserItem.fxml"));

                    //give the items some effect

                    nodes[i].setOnMouseEntered(event -> {
                        nodes[j].setStyle("-fx-background-color : #f8b500");

                    });

                    nodes[i].getProperties().put("emf", this.getEmf());
                    nodes[i].getProperties().put("getUsuarioLogueado", this.getUsuarioLogueado());
                    nodes[i].getProperties().put("mainAnchor", pUsers.getParent());
                    nodes[i].getProperties().put("mainPanel", pUsers);
                    nodes[i].getProperties().put("tfCercador", tfCercador);
                    nodes[i].getProperties().put("pInfoUser", pInfoUser);
                    nodes[i].getProperties().put("btnEsborrarUsuari", btnEsborrarUsuari);

                    ObservableList<Node> hijos = ((HBox) nodes[i]).getChildren();

                    ((Label) hijos.get(0)).setText(usuarios.get(i).getEmail());
                    ((Label) hijos.get(1)).setText(usuarios.get(i).getNombre());
                    ((Label) hijos.get(2)).setText(usuarios.get(i).getApellido1() + " " +  usuarios.get(i).getApellido2());
                    ((Label) hijos.get(3)).setText(usuarios.get(i).getTelefono());

                    if(usuarios.get(i).isBloqueado()){
                        ((Button) hijos.get(4)).setStyle("-fx-background-image: url('images/icons8-delete-48.png')");
                        nodes[i].setStyle("-fx-background-color : #f76565");
                        nodes[i].setOnMouseExited(event -> {
                            nodes[j].setStyle("-fx-background-color : #f76565");
                        });
                    }
                    else{
                        ((Button) hijos.get(4)).setStyle("-fx-background-image: url('images/icons8-checkmark-52.png');");
                        nodes[i].setOnMouseExited(event -> {
                            nodes[j].setStyle("-fx-background-color : transparent");
                        });
                    }

                    hijos.get(4).setId(String.valueOf(usuarios.get(i).getId()));
                    hijos.get(5).setId(String.valueOf(usuarios.get(i).getId()));
                    ((Button) hijos.get(5)).setStyle("-fx-background-image: url('images/icons8-edit-48.png');");
                    hijos.get(6).setId(String.valueOf(usuarios.get(i).getId()));
                    ((Button) hijos.get(6)).setStyle("-fx-background-image: url('images/icons8-trash-52.png');");
                    
                    pnItems.getChildren().add(nodes[i]);

                    contadorNodos++;

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Metodo que muestra el formulario de crear / editar user y en función de ello, deja modificar campo email
     * @param isNewUser Indica si es un nuevo usuario o se está editando un usuario
     */
    public void nouUser(boolean isNewUser){

        setFormData();

        if(isNewUser){
            lbEmailObligatorio.setVisible(true);
            tfInputEmail.setDisable(false);
            setNewUser(true);
        }
        else{
            lbEmailObligatorio.setVisible(false);
            tfInputEmail.setDisable(true);
            setNewUser(false);
        }

        pUsers.setVisible(false);
        pForm.setVisible(true);

    }


    /**
     * Metodo que a partir del id del boton del evento, bloquea a un usuario especifico o lo desbloquea si
     * estaba bloqueando, mandando un email con la nueva contraseña generada automaticamente.
     * @param actionEvent evento, en este caso clic el ratón en un botón
     */
    public void bloquejarUser(ActionEvent actionEvent) {

        idUserEdit=Integer.parseInt(((Button) actionEvent.getSource()).getId());

        EntityManagerFactory emf= (EntityManagerFactory) ((Button) actionEvent.getSource()).getParent().getProperties().get("emf");
        Usuario getUsuarioLogueado = (Usuario) ((Button) actionEvent.getSource()).getParent().getProperties().get("getUsuarioLogueado");
        AnchorPane mainAnchor = (AnchorPane) ((Button) actionEvent.getSource()).getParent().getProperties().get("mainAnchor");
        Pane mainPanel = (Pane) ((Button) actionEvent.getSource()).getParent().getProperties().get("mainPanel");
        JFXTextField tfCercador = (JFXTextField) ((Button) actionEvent.getSource()).getParent().getProperties().get("tfCercador");

        UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(emf, Usuario.class);
        userEdit = daoHelper.getById(idUserEdit);

        // Comprobacion redundante pero por seguridad si en un futuro puede haber más de un admin
        if(!userEdit.isAdmin()) {
            if (userEdit.isBloqueado()) {
                userEdit.setBloqueado(false);
                userEdit.setCambioPassword(true);

                SecurityHelper sh = new SecurityHelper();
                String nouPass= sh.generateRandomPass();

                userEdit.setPassword(sh.passwordEncrypt(nouPass));

                EmailHelper emailHelper = new EmailHelper();
                emailHelper.sendAdminUnblockUser(userEdit.getEmail(),nouPass);
            }
            else userEdit.setBloqueado(true);
        }

        daoHelper.update(userEdit);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/GestionUsuarios.fxml"));

        Pane newPane = null;
        try {
            newPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }


        // Cargamos la vista por defecto
        GestionUsuariosController controller = loader.getController();

        //Guardamos el usuario y pasamos el emf
        controller.setUsuarioLogueado(getUsuarioLogueado);
        controller.setEmf(emf);

        GestionUsuariosController gestUsers = loader.getController();
        gestUsers.setTfCercadorText(tfCercador.getText());
        gestUsers.cercarUsuaris();

        // get children of parent of secPane (the AnchorPanel)
        List<Node> parentChildren = mainAnchor.getChildren();

        // replace the child that contained the old secPane
        parentChildren.set(parentChildren.indexOf(mainPanel), newPane);

        // store the new pane in the secPane field to allow replacing it the same way later
        mainPanel = newPane;


    }

    /**
     * Metodo que asigna al buscador la cadena recibida, usado para comunicar los items
     * de la lista con las funciones de sus iconos
     * @param tfCercador Cadena de texto con el texto a buscar
     */
    private void setTfCercadorText(String tfCercador) {
        this.tfCercador.setText(tfCercador);
    }

    // Esta función es publica porque la necesitamos para hacer el test de editar usuario
    /**
     * Metodo que asigna y guarda el usuario que estamos creando o editando, es publica porque
     * necesitamos acceder al realizar los test graficos, pero y mejor que sea privada
     * @param user El usuario que vamos a crear / editar
     */
    public void setUserEdit (Usuario user){
        if (user==null){
            this.userEdit=new Usuario();
            this.lbEmailObligatorio.setVisible(true);
            this.tfInputEmail.setDisable(false);
            setNewUser(true);
        }
        else{
            this.userEdit=user;
            this.lbEmailObligatorio.setVisible(false);
            this.tfInputEmail.setDisable(true);
            setNewUser(false);
        }
    }

    /**
     * Metodo que asigna e indica si estamos creando un nuevo usuario isNew: true o editando un usuario: false
     * @param edUser Booleanso que indica si es o no un nuevo usuario
     */
    private void setNewUser(boolean edUser){
        this.newUser=edUser;
    }

    /**
     * Método que a partir del boton pulsado en la lista de usuarios, carga sus datos en el formulario,
     * y gestiona su guardado con su control de errores.
     * @param actionEvent El evento clic del botón de la tabla de usuarios pulsado
     */
    public void editarUser(ActionEvent actionEvent) {

        idUserEdit=Integer.parseInt(((Button) actionEvent.getSource()).getId());

        EntityManagerFactory emf= (EntityManagerFactory) ((Button) actionEvent.getSource()).getParent().getProperties().get("emf");
        Usuario getUsuarioLogueado = (Usuario) ((Button) actionEvent.getSource()).getParent().getProperties().get("getUsuarioLogueado");
        AnchorPane mainAnchor = (AnchorPane) ((Button) actionEvent.getSource()).getParent().getProperties().get("mainAnchor");
        Pane mainPanel = (Pane) ((Button) actionEvent.getSource()).getParent().getProperties().get("mainPanel");
        JFXTextField tfCercador = (JFXTextField) ((Button) actionEvent.getSource()).getParent().getProperties().get("tfCercador");

        UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(emf, Usuario.class);
        userEdit = daoHelper.getById(idUserEdit);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/GestionUsuarios.fxml"));

        Pane newPane = null;
        try {
            newPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Cargamos la vista por defecto
        GestionUsuariosController gestUsers = loader.getController();

        //Guardamos el usuario y pasamos el emf
        gestUsers.setUsuarioLogueado(getUsuarioLogueado);
        gestUsers.setEmf(emf);

        gestUsers.setTfCercadorText(tfCercador.getText());
        gestUsers.cercarUsuaris();

        // get children of parent of secPane (the AnchorPanel)
        List<Node> parentChildren = mainAnchor.getChildren();

        // replace the child that contained the old secPane
        parentChildren.set(parentChildren.indexOf(mainPanel), newPane);

        // store the new pane in the secPane field to allow replacing it the same way later
        mainPanel = newPane;

        gestUsers.setUserEdit(userEdit);
        gestUsers.setNewUser(false);
        gestUsers.nouUser(false);

    }

    /**
     * Método que a partir del boton pulsado en la lista de usuarios, muestra un dialogo de confirmación
     * cargando los datos del usuario
     * @param actionEvent El evento clic del botón de la tabla de usuarios pulsado
     */
    public void deleteUser(ActionEvent actionEvent){

        idUserEdit=Integer.parseInt(((Button) actionEvent.getSource()).getId());

        EntityManagerFactory emf= (EntityManagerFactory) ((Button) actionEvent.getSource()).getParent().getProperties().get("emf");
        Usuario getUsuarioLogueado = (Usuario) ((Button) actionEvent.getSource()).getParent().getProperties().get("getUsuarioLogueado");
        AnchorPane mainAnchor = (AnchorPane) ((Button) actionEvent.getSource()).getParent().getProperties().get("mainAnchor");
        Pane mainPanel = (Pane) ((Button) actionEvent.getSource()).getParent().getProperties().get("mainPanel");
        JFXTextField tfCercador = (JFXTextField) ((Button) actionEvent.getSource()).getParent().getProperties().get("tfCercador");

        Pane pInfoUser = (Pane) ((Button) actionEvent.getSource()).getParent().getProperties().get("pInfoUser");
        JFXButton btnEsborrarUsuari = (JFXButton) ((Button) actionEvent.getSource()).getParent().getProperties().get("btnEsborrarUsuari");

        UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(emf, Usuario.class);
        userEdit = daoHelper.getById(idUserEdit);

        btnEsborrarUsuari.setId(String.valueOf(idUserEdit));
        btnEsborrarUsuari.getProperties().put("emf", emf);
        btnEsborrarUsuari.getProperties().put("getUsuarioLogueado", getUsuarioLogueado);

        pInfoUser.setVisible(true);

    }

    /**
     * Método que elimina un usuario y todos sus dispositivos
     * @param actionEvent envento click del botón
     */
    public void esborrarUsuari(ActionEvent actionEvent){

        idUserEdit=Integer.parseInt(((JFXButton) actionEvent.getSource()).getId());

        EntityManagerFactory emf= (EntityManagerFactory) ((JFXButton) actionEvent.getSource()).getProperties().get("emf");
        Usuario getUsuarioLogueado = (Usuario) ((JFXButton) actionEvent.getSource()).getProperties().get("getUsuarioLogueado");

        UsuarioHelper<Usuario> daoHelper = new UsuarioHelper<>(emf, Usuario.class);
        userEdit = daoHelper.getById(idUserEdit);

        //TODO Check Esborrar tots els dispositius
        DispositivoHelper<Dispositivo> dispositivo = new DispositivoHelper<>(emf, Dispositivo.class);
        List<Dispositivo> dispositivos = dispositivo.findByIdUser(userEdit.getId());

        if(dispositivos.size()!=0){

            for( Dispositivo dpo : dispositivos){
                dispositivo.delete(dpo.getId());
            }
        }

        daoHelper.delete(userEdit.getId());

         // Escondemos el panel que contiene el cuadro de dialogo de borrar usuario y recargamos lista
        ((JFXButton) actionEvent.getSource()).getParent().getParent().setVisible(false);
        cercarUsuaris();

    }

    // </editor-fold>
}
