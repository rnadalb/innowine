package cat.npe.innowine.states;

public enum CambioPasswordState {

    EMPTY (-1, "Omple els camps que est\u00e0n buits amb el format correcte."),
    OK(0, "Contrasenya canviada."),
    ACTUALWRONG (1, "La contrasenya no coincideix amb la actual, torna-ho a intentar."),
    NOTMATCHING (2, "Si us plau, assegura't de que les noves contrasenyes son iguals."),
    EQUALPASSWORDS (3, "La contrasenya nova i l'antiga son iguals, han de ser diferents."),
    NOTMINLENGTH(4,"La longitud minima de la contrasenya ha de ser de ... ");

    private int errorCode;
    private String message;

    CambioPasswordState(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
