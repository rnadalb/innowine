package cat.npe.innowine.model;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

import java.io.Serializable;
import java.time.Instant;

//@Measurement(name = "dummyData", database = "iwinedb", timeUnit = TimeUnit.SECONDS)
@Measurement(name = "dummyData", database = "iwinedb")
public class InfluxData implements Serializable {
    private static final long serialVersionUID = -1497034550418278038L;
    @Column(name = "time")
    private Instant time;

    @Column(name = "Hamb")
    private Double hAmb;

    @Column(name = "Pamb")
    private Double pAmb;

    @Column(name = "Tamb")
    private Double tAmb;

    @Column(name = "airtimeT1")
    private Double airtimeT1;

    @Column(name = "bootC")
    private Double bootC;

    @Column(name = "counterT1")
    private Long comptadorT1;

    @Column(name = "humiditatT1")
    private Double humitatT1;

    @Column(name = "humiditatT2")
    private Double humitatT2;

    @Column(name = "humiditatT3")
    private Double humitatT3;

    @Column(name = "id")
    private Integer id;

    @Column(name = "portT1")
    private Double portT1;

    @Column(name = "temperaturaT1")
    private Double temperaturaT1;

    @Column(name = "temperaturaT2")
    private Double temperaturaT2;

    @Column(name = "temperaturaT3")
    private Double temperaturaT3;

    @Column(name = "vBattery")
    private Double vBattery;

    public Instant getTime() {
        return time;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public Double gethAmb() {
        return hAmb;
    }

    public void sethAmb(Double hAmb) {
        this.hAmb = hAmb;
    }

    public Double getpAmb() {
        return pAmb;
    }

    public void setpAmb(Double pAmb) {
        this.pAmb = pAmb;
    }

    public Double gettAmb() {
        return tAmb;
    }

    public void settAmb(Double tAmb) {
        this.tAmb = tAmb;
    }

    public Double getAirtimeT1() {
        return airtimeT1;
    }

    public void setAirtimeT1(Double airtimeT1) {
        this.airtimeT1 = airtimeT1;
    }

    public Double getBootC() {
        return bootC;
    }

    public void setBootC(Double bootC) {
        this.bootC = bootC;
    }

    public Long getComptadorT1() {
        return comptadorT1;
    }

    public void setComptadorT1(Long comptadorT1) {
        this.comptadorT1 = comptadorT1;
    }

    public Double getHumitatT1() {
        return humitatT1;
    }

    public void setHumitatT1(Double humitatT1) {
        this.humitatT1 = humitatT1;
    }

    public Double getHumitatT2() {
        return humitatT2;
    }

    public void setHumitatT2(Double humitatT2) {
        this.humitatT2 = humitatT2;
    }

    public Double getHumitatT3() {
        return humitatT3;
    }

    public void setHumitatT3(Double humitatT3) {
        this.humitatT3 = humitatT3;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPortT1() {
        return portT1;
    }

    public void setPortT1(Double portT1) {
        this.portT1 = portT1;
    }

    public Double getTemperaturaT1() {
        return temperaturaT1;
    }

    public void setTemperaturaT1(Double temperaturaT1) {
        this.temperaturaT1 = temperaturaT1;
    }

    public Double getTemperaturaT2() {
        return temperaturaT2;
    }

    public void setTemperaturaT2(Double temperaturaT2) {
        this.temperaturaT2 = temperaturaT2;
    }

    public Double getTemperaturaT3() {
        return temperaturaT3;
    }

    public void setTemperaturaT3(Double temperaturaT3) {
        this.temperaturaT3 = temperaturaT3;
    }

    public Double getvBattery() {
        return vBattery;
    }

    public void setvBattery(Double vBattery) {
        this.vBattery = vBattery;
    }

    @Override
    public String toString() {
        return "DummyData{" +
                "time=" + time +
                ", hAmb=" + hAmb +
                ", pAmb=" + pAmb +
                ", tAmb=" + tAmb +
                ", airtimeT1=" + airtimeT1 +
                ", bootC=" + bootC +
                ", comptadorT1=" + comptadorT1 +
                ", humitatT1=" + humitatT1 +
                ", humitatT2=" + humitatT2 +
                ", humitatT3=" + humitatT3 +
                ", id=" + id +
                ", portT1=" + portT1 +
                ", temperaturaT1=" + temperaturaT1 +
                ", temperaturaT2=" + temperaturaT2 +
                ", temperaturaT3=" + temperaturaT3 +
                ", vBattery=" + vBattery +
                '}';
    }
}
