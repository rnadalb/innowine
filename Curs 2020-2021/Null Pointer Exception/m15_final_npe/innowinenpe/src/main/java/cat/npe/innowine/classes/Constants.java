package cat.npe.innowine.classes;

/**
 * Clase con las constantes del programa
 */
public class Constants {
    public static final Integer DEFAULT_MAX_INTENTS = 6;
    public static final String DEFAULT_BUTTON_YES = "Si";
    public static final String DEFAULT_BUTTON_NO  = "No";
}
