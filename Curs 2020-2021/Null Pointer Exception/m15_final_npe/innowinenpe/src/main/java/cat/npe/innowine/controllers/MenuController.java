package cat.npe.innowine.controllers;

import cat.npe.innowine.classes.Alerta;
import cat.npe.innowine.exceptions.ConfirmationDialogException;
import cat.npe.innowine.exceptions.SelectedYesConfirmationDialogException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.List;


/**
 * Clase que controla el menú de la aplicación
 */
public class MenuController extends DefaultController  {

    // <editor-fold defaultstate="collapsed" desc="Variables">

    /**
     * PanelAncla con un Hijo Pane, llamado mainPanel que sirve para ir cargando las vistas
     * de los diferentes apartedos de la aplicación. En formato FXML
     */
    @FXML
    private AnchorPane mainAnchor;

    /**
     * Hijo de mainAnchor, Pane donde cargaremos las diferentes vistas con el método
     * loadFMXL
     */
    @FXML
    private Pane mainPanel;

    /**
     * Donde se encuentra la imagen del avatar, el nombre y los botones del menú de la aplicacion
     */
    @FXML
    private VBox vboxMenu;

    /**
     * Botón de Inicio: nos muestra una vista con un mensaje de bienvendida.
     */
    @FXML
    private Button btnInicio;

    /**
     * Botón de Gestión de Usuarios: nos muestra la vista para gestionar usuarios de la aplicación.
     */
    @FXML
    private Button btnGestionUsuarios;

    /**
     * Botón de Gestión de Dispositivos: nos muestra la vista para gestionar dispositivos de la aplicación.
     */
    @FXML
    private Button btnGestionDispositivos;

    /**
     * Botón de Mi Perfil: nos muestra la vista para modificar datos del usuario actual que ha iniciado
     * sesión en la aplicación.
     */
    @FXML
    private Button btnMiPerfil;

    /**
     * Botón de Configuración: permite modificar los parámetros de configuración de la aplicación.
     */
    @FXML
    private Button btnConfiguracion;

    /**
     * Botón de Ajuda: muestra una vista con información Acerca De la aplicación.
     */
    @FXML
    private Button btnAyuda;

    /**
     * Botón Salir: permite salir de la aplicación, cerrando la ventana.
     */
    @FXML
    private Button btnSalir;

    /**
     * Botón DondeEstoy: Guarda el botón actual de la vista, para poder ir indicando al usuario en que apartado del menu se encuenta.
     */
    @FXML
    private Button btnDondeEstoy;

    /**
     * Label con el nombre y apellidos del usuario
     */
    @FXML
    public Label lbNomAvatar;

    /**
     * Imagen del avatar del usuario logueado
     */
    @FXML
    public ImageView ivAvatar;


    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Métodos">

    /**
     * Método que carga las vistas en el mainPanel
     * @param vista La vista a cargar en el mainPanel
     * @throws IOException Error de entrda / salida
     */
    public void loadFxml (String vista) throws IOException {

        String ruta="/views/";
        ruta=ruta.concat(vista);

        // load new pane
        FXMLLoader loader = new FXMLLoader(getClass().getResource(ruta));

        Pane newPane = loader.load();


        // Cargamos la vista por defecto
        DefaultController controller = loader.getController();

        //Guardamos el usuario y pasamos el emf
        controller.setUsuarioLogueado(getUsuarioLogueado());
        controller.setEmf(getEmf());

        // Si es el controlador de mi perfil cargamos datos del form

        if(loader.getController().getClass().getSimpleName().equals("MiPerfilController")){
            MiPerfilController mPerfil = loader.getController();
            mPerfil.setFormData();

        }else if(loader.getController().getClass().getSimpleName().equals("GestionUsuariosController")){
            GestionUsuariosController gestUsers = loader.getController();
            gestUsers.cercarUsuaris();
        }
        /*else if(loader.getController().getClass().getSimpleName().equals("GestionDispositivosController")){
            GestionDispositivosController gestDisp = loader.getController();
            gestDisp.cercarDispositius();
        }*/else if(loader.getController().getClass().getSimpleName().equals("ConfiguracionSistemaController")) {
            ConfiguracionSistemaController configSistema = loader.getController();
            configSistema.setFormData();
        }

        // get children of parent of secPane (the AnchorPanel)
        List<Node> parentChildren = mainAnchor.getChildren();

        // replace the child that contained the old secPane
        parentChildren.set(parentChildren.indexOf(mainPanel), newPane);

        // store the new pane in the secPane field to allow replacing it the same way later
        mainPanel = newPane;
    }

    /**
     * Método que aplica una clase CSS para señalar en el menu lateral donde se encuentra el usuario
     * @param btn Botón a marcar visualmente
     */
    public void marcaBtnActivo(Button btn){

        if (btnDondeEstoy == null) { // Se ha cargado la vista por primera vez, el btnInicio tiene la clase aplicada por defecto

            btnDondeEstoy=btnInicio;
            btnDondeEstoy.getStyleClass().remove("menu-btn-clicked");
            btnDondeEstoy=btn;
            btnDondeEstoy.getStyleClass().add("menu-btn-clicked");
        } else{
            btnDondeEstoy.getStyleClass().remove("menu-btn-clicked");
            btnDondeEstoy=btn;
            btnDondeEstoy.getStyleClass().add("menu-btn-clicked");
        }

    }

    /**
     * Metodo que a partir del usuarioLogueado muestra su nombre, imagen de avatar en el menu lateral
     * También esconde aquellas funciones propias de un administrador si es un usuario
     */
    public void setShowUserLogued(){

        lbNomAvatar.setText(String.format("%s %s %s",getUsuarioLogueado().getNombre(),getUsuarioLogueado().getApellido1(),getUsuarioLogueado().getApellido2()));
        Image imageObject = new Image("/avatar/avatar"+getUsuarioLogueado().getAvatar()+".png");
        ivAvatar.setImage(imageObject);

        if(!getUsuarioLogueado().isAdmin()){
            btnGestionUsuarios.setVisible(false);
            btnGestionUsuarios.managedProperty().bind(btnGestionUsuarios.visibleProperty());
            btnConfiguracion.setVisible(false);
            btnConfiguracion.managedProperty().bind(btnConfiguracion.visibleProperty());
        }

    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Eventos">

    /**
     * Gestiona los eventos relacionados con los diferentes botones del menu de la aplicación
     * Carga la vista adecuada según el botón pulsado
     * @param actionEvent Info del botón pulsado en forma de evento
     * @throws IOException Error de entrda / salida
     */
    public void handleClicks(ActionEvent actionEvent) throws IOException {

        if (actionEvent.getSource() == btnInicio) {
            loadFxml("Main.fxml");
            marcaBtnActivo(btnInicio);
            return;
        }

        if (actionEvent.getSource() == btnMiPerfil) {
            loadFxml("MiPerfil.fxml");
            marcaBtnActivo(btnMiPerfil);
            return;
        }

        if (actionEvent.getSource() == btnGestionUsuarios) {
            loadFxml("GestionUsuarios.fxml");
            marcaBtnActivo(btnGestionUsuarios);
            return;
        }

        if (actionEvent.getSource() == btnGestionDispositivos) {
            //loadFxml("GestionDispositivos.fxml");
            loadFxml("EnDesenvolupament.fxml");
            marcaBtnActivo(btnGestionDispositivos);
            return;
        }

        if (actionEvent.getSource() == btnConfiguracion) {
            loadFxml("ConfiguracionSistema.fxml");
            marcaBtnActivo(btnConfiguracion);
            return;
        }


        if (actionEvent.getSource() == btnAyuda) {
            loadFxml("SobreNosotros.fxml");
            marcaBtnActivo(btnAyuda);
            return;
        }

        if (actionEvent.getSource()==btnSalir) {

            marcaBtnActivo(btnSalir);

            try {
                Alerta.alertaConfirmacio("App: Sortir", "Segur que vol sortir de l'App?","Trieu una opcio");
            } catch (ConfirmationDialogException e) {
                if (e instanceof SelectedYesConfirmationDialogException)
                    Platform.exit(); // Atura l'App
                else
                    actionEvent.consume(); // Consumeix un esdeveniment
            }
        }
    }

    // </editor-fold>
}
