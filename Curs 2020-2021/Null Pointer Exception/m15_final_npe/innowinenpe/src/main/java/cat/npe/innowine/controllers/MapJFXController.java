package cat.npe.innowine.controllers;

import cat.npe.innowine.classes.Patterns;
import cat.npe.innowine.classes.Alerta;
import cat.npe.innowine.exceptions.ConfirmationDialogException;
import cat.npe.innowine.exceptions.SelectedYesConfirmationDialogException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.sothawo.mapjfx.*;
import com.sothawo.mapjfx.event.MapLabelEvent;
import com.sothawo.mapjfx.event.MapViewEvent;
import com.sothawo.mapjfx.event.MarkerEvent;
import com.sothawo.mapjfx.offline.OfflineCache;
import io.cucumber.java.bs.A;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.regex.Pattern;

/**
 * Clase que gestiona la vista de alta y edición de dispositivos
 */
public class MapJFXController {

    /** Localitzación por defecto, Vilafranca del Penedés */
    private static final double DEFAULT_LAT= 41.3461265;
    private static final double DEFAULT_LONG= 1.697939799999972;
    private static final Coordinate coordVilafrancaPenedes = new Coordinate(DEFAULT_LAT, DEFAULT_LONG);

    /** zoom por defecto **/
    private static final int ZOOM_DEFAULT = 14;
    private static final int ZOOM_LOCATED = 17;
    private static final Marker.Provided DEFAULT_MARKER_COLOR = Marker.Provided.BLUE;
    private static IntegerProperty zoom_value = new SimpleIntegerProperty(ZOOM_DEFAULT);

    /** Parámetros para el WMS server. */
    private WMSParam wmsParam = new WMSParam()
            .setUrl("http://ows.terrestris.de/osm/service?")
            .addParam("layers", "OSM-WMS");

    private XYZParam xyzParams = new XYZParam()
            .withUrl("https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x})")
            .withAttributions("'Tiles &copy; <a href=\"https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer\">ArcGIS</a>'");

    @FXML
    private AnchorPane apData;

    /**
     * Botón DondeEstoy: Guarda el botón actual de la vista, para poder ir indicando al usuario en que apartado del menu se encuenta.
     */
    @FXML
    private Button btnDondeEstoy;

    /**
     * Inputs para escribir longitud y latitud manualmente.
     */
    @FXML
    private JFXTextField tfLatitud, tfLongitud;

    /**
     * Botones para hacer zoom in y zoom out.
     */
    @FXML
    private JFXButton btZoomPlus, btZoomMinus;

    /**
     * Botón para cerrar la pantalla de alta/modificacion dispositivos.
     */
    @FXML
    private JFXButton btSortir;

    /**
     * Botón para buscar por longitud y latitud en el mapa.
     */
    @FXML
    private JFXButton btnCercar;

    /**
     * Input para escribir id dispositivo manualmente.
     */
    @FXML
    private JFXTextField tfId;

    /**
     * Input para escribir descripción dispositivo manualmente.
     */
    @FXML
    private JFXTextField tfDesc;

    @FXML
    private MapView mapView;

    /**
     * Botones para elegir tipo de vista del mapa.
     */
    @FXML
    private JFXButton btOSM, btWSM, btXYZ; // Botons prr estil de mapa OSM, WSM, XYZ

    /**
     * Slider para ajustar el tamaño del zoom.
     */
    @FXML
    private Slider sdZoom;

    private MapType mapType;

    public void initMapAndControls(Projection projection) {
        // inicialitza MapView-Cache
        final OfflineCache offlineCache = mapView.getOfflineCache();
        final String cacheDir = System.getProperty("java.io.tmpdir") + "/mapjfx-cache";

        // escolta (monitortiza) la propietat que indica que el mapa ha finalitzat la seva inicialització
        mapView.initializedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                afterMapIsInitialized();
            }
        });

        // controla los botones que gestionan los diferentes tipos de mapas
        mapType = MapType.OSM;

        // Associament de la propietat del zoom als controls slider i button corresponents
        sdZoom.valueProperty().bindBidirectional(mapView.zoomProperty());
        zoom_value.bindBidirectional(mapView.zoomProperty());

        setupEventHandlers();

        mapView.initialize(Configuration.builder()
                .projection(projection)
                .showZoomControls(false)
                .build());
    }

    /**
     * Inicializa los listeners de gestión del mapa i la GUI
     */
    private void setupEventHandlers() {

        // Gestió del zoom pels botons
        btZoomPlus.setOnAction(event -> {
            event.consume();
            mapView.setZoom(mapView.getZoom() + 1);
        });

        btZoomMinus.setOnAction(event -> {
            event.consume();
            mapView.setZoom(mapView.getZoom() - 1);
        });

        // Gestiona l'esdeveniment de fer clic en el mapa
        mapView.addEventHandler(MapViewEvent.MAP_CLICKED, event -> {
            event.consume();
            final Coordinate newPosition = event.getCoordinate().normalize();
            Alerta.alertInfo(String.format("[MAP_CLICKED] --> Posició: [%s, %s]", newPosition.getLatitude(), newPosition.getLongitude()));
        });

        // Gestiona l'esdeveniment de fer clic amb el botó dret en el mapa
        mapView.addEventHandler(MapViewEvent.MAP_RIGHTCLICKED, event -> {
            event.consume();
            Alerta.alertInfo(String.format("[MAP_RIGHTCLICKED] --> Posició: [%s, %s]", event.getCoordinate().getLatitude(), event.getCoordinate().getLongitude()));
        });

        // Gestiona quan s'ha fet clic a un marcador
        mapView.addEventHandler(MarkerEvent.MARKER_CLICKED, event -> {
            event.consume();
            Alerta.alertInfo(String.format("[MARKER_CLICKED] --> Id del marcador: [%s]", event.getMarker().getId()));
        });

        // Gestiona quan s'ha fet clic amb el botó dret a un marcador
        mapView.addEventHandler(MarkerEvent.MARKER_RIGHTCLICKED, event -> {
            event.consume();
            Alerta.alertInfo(String.format("[MARKER_RIGHTCLICKED] --> Id del marcador: [%s]", event.getMarker().getId()));
        });

        // Gestiona quan s'ha fer clic en una etiqueta al mapa
        mapView.addEventHandler(MapLabelEvent.MAPLABEL_CLICKED, event -> {
            event.consume();
            Alerta.alertInfo(String.format("[MAPLABEL_CLICKED] --> Id del marcador: [%s]", event.getMapLabel().getId()));
        });

        // Gestiona quan s'ha fer clic amb el botó dret en una etiqueta al mapa
        mapView.addEventHandler(MapLabelEvent.MAPLABEL_RIGHTCLICKED, event -> {
            event.consume();
            Alerta.alertInfo(String.format("[MAPLABEL_RIGHTCLICKED] --> Id del marcador: [%s]", event.getMapLabel().getId()));
        });

        // Gestiona el punter al mapa
        mapView.addEventHandler(MapViewEvent.MAP_POINTER_MOVED, event -> {
            System.out.println("el punter està en " + event.getCoordinate());
        });
    }

    /**
     * Finaliza la configuración después que el mapa este inicializado
     */
    private void afterMapIsInitialized() {
        mapView.setZoom(ZOOM_DEFAULT);
        mapView.setCenter(coordVilafrancaPenedes);
        addMarker(true);
    }

    /**
     * Añade un marcador
     *
     * @param center si és true centra en el mapa la nueva posición
     * @return booleano
     */
    private boolean addMarker (boolean center) {
        boolean result = false;
        if (!tfLatitud.getText().isEmpty() && !tfLongitud.getText().isEmpty()) {
            if (Pattern.matches(Patterns.fpRegex, tfLatitud.getText())) {
                Double lat = Double.valueOf(tfLatitud.getText());
                if (Pattern.matches(Patterns.fpRegex, tfLongitud.getText())) {
                    Double lng = Double.valueOf(tfLongitud.getText());
                    Coordinate c = new Coordinate(lat,lng);
                    addMarker(c, DEFAULT_MARKER_COLOR, center);
                    result = true;
                } else {
                    Alerta.alertInfo("El valor de la <longitud> no és correcte");
                }
            } else {
                Alerta.alertInfo("El valor de la <latitud> no és correcte");
            }
        }
        return result;
    }

    private void addMarker (Coordinate coordinate, Marker.Provided color, boolean center) {
        Marker marker = Marker.createProvided(color).setPosition(coordinate).setVisible(true);
        String info = String.format("Posició [%s,%s]", coordinate.getLatitude().toString(), coordinate.getLongitude().toString());
        MapLabel lblInfo = new MapLabel(info, 10, -10).setVisible(false).setCssClass("orange-label");
        marker.attachLabel(lblInfo);
        mapView.addMarker(marker);
        if (center)
            mapView.setCenter(coordinate);
        mapView.setZoom(ZOOM_LOCATED);
    }

    @FXML
    public void btSearchOnAction(ActionEvent actionEvent) {
        addMarker(true);
    }

    @FXML
    public void btExitOnAction(ActionEvent actionEvent) {
        ((Stage)apData.getScene().getWindow()).close();
    }

    /**
     * Método que aplica una clase CSS para señalar en el menu lateral donde se encuentra el usuario
     * @param btn Botón a marcar visualmente
     */
    public void marcaBtnActivo(Button btn){

        if (btnDondeEstoy == null) { // Se ha cargado la vista por primera vez, el btnInicio tiene la clase aplicada por defecto

            btnDondeEstoy=btOSM;
            btnDondeEstoy.getStyleClass().remove("menu-btn-clicked");
            btnDondeEstoy=btn;
            btnDondeEstoy.getStyleClass().add("menu-btn-clicked");
        } else{
            btnDondeEstoy.getStyleClass().remove("menu-btn-clicked");
            btnDondeEstoy=btn;
            btnDondeEstoy.getStyleClass().add("menu-btn-clicked");
        }

    }

    /**
     * Gestiona los eventos relacionados con los diferentes botones del tipo de mapas
     * Carga la vista adecuada del mapa según el botón pulsado
     * @param actionEvent Info del botón pulsado en forma de evento
     */
    public void handleClicks(ActionEvent actionEvent) {

        if (actionEvent.getSource() == btOSM) {
            marcaBtnActivo(btOSM);
            mapType = MapType.OSM;
        }

        if (actionEvent.getSource() == btWSM) {
            marcaBtnActivo(btWSM);
            mapView.setWMSParam(wmsParam);
            mapType = MapType.WMS;
        }

        if (actionEvent.getSource() == btXYZ) {
            marcaBtnActivo(btXYZ);
            mapView.setXYZParam(xyzParams);
            mapType = MapType.XYZ;
        }

        mapView.setMapType(mapType);
    }


}
