package cat.npe.innowine.exceptions;

/**
 * Excepción que nos ayuda para ejecutar la opción del cuadro de alerta cuando el botón pulsado es SI
 */
public class SelectedYesConfirmationDialogException extends ConfirmationDialogException{
}
