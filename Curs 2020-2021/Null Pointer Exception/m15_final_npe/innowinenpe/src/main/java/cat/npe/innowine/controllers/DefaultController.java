package cat.npe.innowine.controllers;

import cat.npe.innowine.model.Usuario;

import javax.persistence.EntityManagerFactory;

/**
 * Patron Singleton para la conexión con la BBDD con Hibernate
 */
public abstract class DefaultController {

    // <editor-fold defaultstate="collapsed" desc="Variables">
    /**
     * Conexión con la BBDD
     */
    private EntityManagerFactory emf;

    /**
     * Usuario que ha hecho login en la aplicación, se deja la contrasña encriptada para evitar que un ataque de RAM muestre el password sin encriptar
     */
    private Usuario usuarioLogueado;

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters / Getters">

    /**
     * Guarda la conexión con la BBDD
     * @param emf la conexión con la base de datos
     */

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    /**
     * Devuelve la conexión con la BBDD
     * @return la conexión con la base de datos
     */
    public EntityManagerFactory getEmf() {
        return emf;
    }

    public Usuario getUsuarioLogueado() {
        return this.usuarioLogueado;
    }

    public void setUsuarioLogueado(Usuario usuarioLogueado) {
        this.usuarioLogueado = usuarioLogueado;

    }

    // </editor-fold>
}
