package cat.npe.innowine.controllers.helpers;

import cat.npe.innowine.classes.DAOHelper;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Clase Helper que contiene metodos y consultas personalizados para la clase usuario
 * @param <Usuario> La instancia del usuario a crear, modificar, consultar ... en la bbdd
 */
public class UsuarioHelper<Usuario> extends DAOHelper<Usuario> {
    /**
     * Constructor
     *
     * @param emf            la conexión con la BBDD
     * @param parameterClass la entidad en forma de clase equivalente
     */
    public UsuarioHelper(EntityManagerFactory emf, Class<Usuario> parameterClass) {
        super(emf, parameterClass);
    }

    /**
     * Método que nos permite buscar a un usuario por su email
     * @param email Email del usuario
     * @return Devuelve el usuario encontrado o null
     */
    public Usuario findUserByEmail(String email) {
        // RNB. Equivalent a SELECT * FROM tbl_usuario WHERE email = :email

        EntityManager em = this.emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Usuario> cbQuery = cb.createQuery(parameterClass);
        Root<Usuario> c = cbQuery.from(parameterClass);
        cbQuery.select(c).where(cb.equal(c.get("email"), email));

        Query query = em.createQuery(cbQuery);

        try {
            Usuario user = (Usuario) query.getSingleResult();
            return user;
        } catch (NoResultException nre) {
            return null;
        }
    }

    public List<Usuario> findUserBySearchString(String cerca) {

        EntityManager em = this.emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Usuario> cbQuery = cb.createQuery(parameterClass);
        Root<Usuario> c = cbQuery.from(parameterClass);
        cbQuery.select(c).where(cb.or(cb.like(cb.lower(c.get("email")), "%"+cerca.toLowerCase()+"%"),
                cb.like(cb.lower(c.get("nombre")), "%"+cerca.toLowerCase()+"%")));

        Query query = em.createQuery(cbQuery);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }


}
