package cat.npe.innowine.states;

public enum LoginState {

    GRANTED(0, "Accés garantit"),
    FAILED (-1, "Accés erroni"),
    BLOCKED (1, "Compte Bloquejat"),
    CHANGEPASSWORD (2, "S'ha de canviar el password");

    private int errorCode;
    private String message;

    LoginState(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
