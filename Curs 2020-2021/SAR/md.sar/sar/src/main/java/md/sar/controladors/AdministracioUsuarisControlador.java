package md.sar.controladors;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import md.sar.classes.*;
import md.sar.interfaces.IniciDiferit;
import md.sar.models.ConfiguracioSistema;
import md.sar.models.DadesUsuari;
import md.sar.classes.DAOHelper;
import md.sar.classes.DefaultControllerUser;

import java.net.URL;
import java.util.ResourceBundle;

public class AdministracioUsuarisControlador extends DefaultControllerUser implements Initializable, IniciDiferit {

    // <editor-fold defaultstate="collapsed" desc="Components FXML">
    @FXML
    private TextField tfBuscar;
    @FXML
    private TableView<DadesUsuari> tvUsuaris;
    @FXML
    private TableColumn<DadesUsuari, String> loginColumn;
    @FXML
    private TableColumn<DadesUsuari, String> nomColumn;
    @FXML
    private TableColumn<DadesUsuari, String> cognomColumn;
    @FXML
    private TableColumn<DadesUsuari, String> telColumn;
    @FXML
    private TableColumn<DadesUsuari, Boolean> bloqColumn;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Variables globals">
    private static final int DOBLE_CLICK = 2;
    private static final int FIRST_ITEM = 0;
    private DAOHelper<DadesUsuari> helper;
    private ObservableList<DadesUsuari> llistaUsuaris;
    private ConfiguracioSistema config;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Metodes">
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ContextMenu cm = new ContextMenu();
        MenuItem mi1 = new MenuItem("Canvia contrasenya");
        MenuItem mi2 = new MenuItem("Bloqueja");
        MenuItem mi3 = new MenuItem("Desbloqueja");
        MenuItem mi4 = new MenuItem("Envia contrasenya");
        MenuItem mi5 = new MenuItem("Personalitzacio de Dades");

        cm.getItems().add(mi1);
        cm.getItems().add(mi2);
        cm.getItems().add(mi3);
        cm.getItems().add(mi4);
        cm.getItems().add(mi5);

        mi1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                canviaContrasenya(tvUsuaris.getSelectionModel().getSelectedItem());
            }
        });
        mi2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                bloquejaUsuari(tvUsuaris.getSelectionModel().getSelectedItem());
            }
        });
        mi3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                desbloquejaUsuari(tvUsuaris.getSelectionModel().getSelectedItem());
            }
        });
        mi4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //creaienviaContrasenya(tvUsuaris.getSelectionModel().getSelectedItem());
            }
        });
        mi5.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                personalitzacioDades(tvUsuaris.getSelectionModel().getSelectedItem());
            }
        });

        tvUsuaris.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getButton() == MouseButton.SECONDARY) {
                    cm.show(tvUsuaris, event.getScreenX(), event.getScreenY());
                }
            }
        });
        // doble click a la taula
        tvUsuaris.setRowFactory(tv -> {
            TableRow<DadesUsuari> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == DOBLE_CLICK && (!row.isEmpty())) {
                    //si la finestra es la de baixa, al fer dos clicks dona el camp Propi
                    DadesUsuari p = tvUsuaris.getSelectionModel().getSelectedItem();
                    //esta en el altre model d'usuari...
                    //p.setBlocked(!p.isBlocked()); //Obté el valor booleà contrari.
                    helper.update(p);
                    tvUsuaris.refresh();
                    personalitzacioDades(tvUsuaris.getSelectionModel().getSelectedItem());
                }
            });
            return row;
        });
        configuraColumnes();
    }

    private void personalitzacioDades(DadesUsuari usuari) {
        obrir_vista("/vistes/Usuari.fxml", getEmf(), usuari.getUsuari(), getConfig(), ModeStates.ALTA_USUARIS);
    }

    private void canviaContrasenya(DadesUsuari usuari) {
        obrir_vista("/vistes/CanviContra.fxml", getEmf(), usuari.getUsuari(), getConfig(), ModeStates.CANVIA_CONNTRA_ADMIN);
    }

    private void bloquejaUsuari(DadesUsuari usuari) {
        if (!usuari.getUsuari().isBlocked() && !usuari.getUsuari().isAdmin()) {
            usuari.getUsuari().setBlocked(true);
            helper.update(usuari);
            actualitzaTaula();
            mostraAlertaInformation(
                    "Fet",
                    "",
                    "Usuari " + usuari.getUsuari().getLogin() + " bloquejat");
        } else {
            mostraAlertaWarning(
                    "Avis",
                    "L'usuari " + usuari.getUsuari().getLogin() + " ja esta bloquejat o es administrador",
                    "");

        }
    }

    private void desbloquejaUsuari(DadesUsuari usuari) {
        if (usuari.getUsuari().isBlocked()) {
            usuari.getUsuari().setBlocked(false);
            usuari.getUsuari().setIntents(0);
            helper.update(usuari);
            actualitzaTaula();
            mostraAlertaInformation(
                    "Fet",
                    "",
                    "Usuari " + usuari.getUsuari().getLogin() + " desbloquejat");
        } else {
            mostraAlertaWarning(
                    "Avis",
                    "L'usuari " + usuari.getUsuari().getLogin() + " ja esta desbloquejat",
                    "");

        }
    }

    private void creaienviaContrasenya(DadesUsuari usuari) {
        String pass = PasswordUtils.getPassword(6);
        EncriptacioSAR.setKey(pass);
        //Enviar mail(amb la contrasenya(pass)no xifrada)...
        // EnviarCorreu.EnviarMail(usuari.getEmail(), config);


        //encriptar pass
        pass = EncriptacioSAR.encrypt(pass, EncriptacioSAR.secretKey);
        assert pass != null;
        byte[] passHash = new byte[0];
        try {
            passHash = EncriptacioSAR.computeHash(pass);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String passFinal = EncriptacioSAR.byteArrayToHexString(passHash);
        usuari.getUsuari().setPasswd(passFinal);


    }

    @Override
    public void iniciDiferit() {
        helper = new DAOHelper<>(getEmf(), DadesUsuari.class);
        actualitzaTaula();
        goTableItem(FIRST_ITEM);
    }

    private void actualitzaTaula() {
        llistaUsuaris = getUsuaris();
        tvUsuaris.setItems(llistaUsuaris);
        filtraTaula();
    }

    private void filtraTaula() {
        //if (!tvUsuaris.getItems().isEmpty())
        //tvUsuaris.getItems().removeAll();

        // 1.  Ajustar el ObservableList en una llista filtrada (inicialment mostra totes les dades).
        FilteredList<DadesUsuari> filteredData = new FilteredList<>(tvUsuaris.getItems(), p -> true);

        // 2. Estableix el Predicat del filtre cada vegada que camvia en filtre
        tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(busq -> {
                // Si el text del filtre està buit (null o empty), mostrar-ho tot
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (busq.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true; // El filtre coincideix amb el nom del usuari
                } else if ((busq.getUsuari().getLogin().toLowerCase().indexOf(lowerCaseFilter) != -1)) {
                    return true; // El filtre conincideix amb el login del usuari
                }
                return false; // No hi ha coincidència
            });
        });

        // 3. Embolica el FilteredList en una SortedList.
        SortedList<DadesUsuari> sortedData = new SortedList<>(filteredData);

        // 4. Lliga el comparador de la SortedList cal del TableView
        // 	  En qualsevol altre cas, l'ordenació de la TableView no tindrà efecte
        sortedData.comparatorProperty().bind(tvUsuaris.comparatorProperty());

        // 5. Afegeix la informació ordenada (i filtrada) a la taula
        tvUsuaris.setItems(sortedData);
    }

    private void configuraColumnes() {
        // Alerta que els valors --------------------------------<>
        // Identifiquen el noms de les VARIABLES de la classe

        loginColumn.setCellValueFactory(cellData -> cellData.getValue().getUsuari().getLoginProperty());
        nomColumn.setCellValueFactory(new PropertyValueFactory<DadesUsuari, String>("nom"));
        cognomColumn.setCellValueFactory(new PropertyValueFactory<DadesUsuari, String>("cognom"));
        telColumn.setCellValueFactory(new PropertyValueFactory<DadesUsuari, String>("telefon"));
        // Per mostrar un CheckBox en un camp Boolean
        bloqColumn.setCellValueFactory(new PropertyValueFactory<DadesUsuari, Boolean>("blocked"));
        bloqColumn.setCellFactory(column -> new CheckBoxTableCell<>());
        bloqColumn.setCellValueFactory(cellData -> cellData.getValue().getUsuari().getBlockedProperty());
    }

    private ObservableList<DadesUsuari> getUsuaris() {
        return FXCollections.observableArrayList(helper.getAll());
    }

    private void goTableItem(int row) {
        tvUsuaris.requestFocus();
        tvUsuaris.scrollTo(row);
        tvUsuaris.getSelectionModel().select(row);
        tvUsuaris.getFocusModel().focus(row);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Alertes">

    /**
     * Crea una alerta de tipus WARNING
     *
     * @param TitleText   el text que sortirà al títol de la alerta
     * @param HeaderText  el text que sortirà al header de la alerta
     * @param ContentText el contingut de la alerta
     */
    private void mostraAlertaWarning(String TitleText, String HeaderText, String ContentText) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(TitleText);
        alert.setHeaderText(HeaderText);
        alert.setContentText(ContentText);
        alert.showAndWait();
    }

    /**
     * Crea una alerta de tipus INFORMATION
     *
     * @param TitleText   el text que sortirà al títol de la alerta
     * @param ContentText el contingut de la alerta
     */
    private void mostraAlertaInformation(String TitleText, String HeaderText, String ContentText) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(TitleText);
        alert.setHeaderText(HeaderText);
        alert.setContentText(ContentText);
        alert.showAndWait();
    }
    // </editor-fold>
}
