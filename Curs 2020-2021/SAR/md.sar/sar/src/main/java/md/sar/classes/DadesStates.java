package md.sar.classes;

public enum DadesStates {
    CORRECTES(0, "Dades correctes !!!"),
    INCORRECTES(-1,  "Dades incorrectes");

    private int errorCode;
    private String message;

    DadesStates(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
