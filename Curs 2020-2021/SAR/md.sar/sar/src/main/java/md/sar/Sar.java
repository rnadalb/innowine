package md.sar;

import com.sothawo.mapjfx.Projection;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.stage.StageStyle;
import md.sar.classes.AppPropertiesHelper;
import md.sar.classes.DefaultController;
import md.sar.controladors.MapControlador;
import md.sar.exceptions.PropertiesHelperException;
import md.sar.models.ConfiguracioSistema;
import org.apache.commons.configuration.ConfigurationException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Sar extends Application {

    private EntityManagerFactory emf;
    private ConfiguracioSistema config;


    /**
     * El següent mètode permet entrar al login
     *
     * @param primaryStage indica la finestra del login
     */
    @Override
    public void start(Stage primaryStage) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistes/Login.fxml"));
        Parent root;

        try {
            root = loader.load();
            DefaultController controller = loader.getController();
            controller.setEmf(emf);
            controller.setConfig(config);
            Scene scene = new Scene(root);
            primaryStage.initStyle(StageStyle.DECORATED);
            primaryStage.setScene(scene);
            primaryStage.show();
            primaryStage.setResizable(false);
        } catch (IOException ex) {
            Logger.getLogger(Sar.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * El següent mètode inicialitza la connexió per poder accedir a la base de dades i carregará
     * la configuracio
     * @throws Exception
     */
   @Override
    public void init() throws Exception {
        super.init();
        String passwd = getDecryptedPropsFilePassword();
        Map<String,String> mapprop = new HashMap<>();
        mapprop.put("javax.persistence.jdbc.password",passwd);
        try{
            emf = Persistence.createEntityManagerFactory( "mariaDBConnection",mapprop);
            EntityManager em = emf.createEntityManager();
            config = new ConfiguracioSistema();
            config = em.find(config.getClass(),1);
            em.close();
        }catch(Exception ignored){

        }
    }
    private static String getDecryptedPropsFilePassword() throws PropertiesHelperException, ConfigurationException {
        AppPropertiesHelper helper = new AppPropertiesHelper("app.properties", "password", "enc");
        return helper.getDecryptedUserPassword();
    }
    @Override
    public void stop() throws Exception {
        if (emf != null && emf.isOpen())
            emf.close();

        super.stop();
    }
}

