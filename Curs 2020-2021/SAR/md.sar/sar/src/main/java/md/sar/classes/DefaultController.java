package md.sar.classes;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import md.sar.models.ConfiguracioSistema;
import md.sar.models.Usuari;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DefaultController {
    private ModeStates mode;

    private EntityManagerFactory emf;
    private ConfiguracioSistema config;

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }
    public EntityManagerFactory getEmf() {
        return emf;
    }
    public ConfiguracioSistema getConfig() {
        return config;
    }
    public void setConfig(ConfiguracioSistema config) {
        this.config = config;
    }

    public ModeStates getMode() {
        return mode;
    }

    public void setMode(ModeStates mode) {
        this.mode = mode;
    }

    public void obrir_vista(String vista, EntityManagerFactory emf, Usuari user, ConfiguracioSistema config, ModeStates mode){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource(vista));

            Parent root1 = fxmlLoader.load();
            DefaultControllerUser controlador = fxmlLoader.getController();
            controlador.setEmf(emf);
            controlador.setUser(user);
            controlador.setConfig(config);
            controlador.setMode(mode);
            controlador.obtenirDades();
            controlador.iniciDiferit();
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Usuari - " + user.getLogin());
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (IOException e) {
            //Missatge d'error al no poder obrir correctament la Stage
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }
}
