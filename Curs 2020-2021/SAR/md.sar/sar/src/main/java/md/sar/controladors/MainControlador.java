package md.sar.controladors;

import com.sothawo.mapjfx.Projection;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.StageStyle;
import md.sar.classes.DefaultControllerUser;
import md.sar.classes.ModeStates;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;


public class MainControlador extends DefaultControllerUser {


    @FXML
    private MenuItem MIAdiministracioUsuaris;

    @FXML
    private MenuItem MIConfiguracio;


    /**
     * El següent metode obra la vista Administrador de Dispositius
     */
    @FXML
    private void MIadministracioDispositiusOnAction() {
        obrir_vista("/vistes/Mapa.fxml", getEmf(), getUser(), getConfig(), ModeStates.MAPA);
    }

    /**
     * El següent metode obra la vista d'administració d'usuaris
     */
    @FXML
    private void MIadministracioUsuarisOnAction() {

        obrir_vista("/vistes/AdministracioUsuaris.fxml", getEmf(), getUser(), getConfig(), ModeStates.ADMIN_USUARIS);

    }

    /**
     * El següent metode obra la vista de la configuració
     */
    @FXML
    private void MIconfiguracioOnAction() {
        obrir_vista("/vistes/ConfiguracioSistema.fxml", getEmf(), getUser(), getConfig(), ModeStates.CONFIGURACIO);
    }

    /**
     * El següent metode obra la vista de personalització de dades
     */
    @FXML
    private void MIpersonalitzacioOnAction() {
        obrir_vista("/vistes/Usuari.fxml", getEmf(), getUser(), getConfig(), ModeStates.PERSONALITZACIO_DADES);
    }

    /**
     * El següent metode obra la vista sobre
     */
    @FXML
    private void MIsobreOnAction() {
        obrir_vista("/vistes/Sobre.fxml", getEmf(), getUser(), getConfig(), ModeStates.SOBRE);
    }

    /**
     * El següent metode obra l'alerta per la sortida del programa
     */
    @FXML
    private void MIsurtOnAction() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Sortir");
        alert.setContentText("Segur que vols sortir?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            Platform.exit();
        }
    }

    @Override
    public void obtenirDades() {
        MIAdiministracioUsuaris.setVisible(getUser().isAdmin());
        MIConfiguracio.setVisible(getUser().isAdmin());
    }

    private void mostraAlertaWarning(String TitleText, String HeaderText, String ContextText) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(TitleText);
        alert.setHeaderText(HeaderText);
        alert.setContentText(ContextText);
        alert.showAndWait();
    }
}