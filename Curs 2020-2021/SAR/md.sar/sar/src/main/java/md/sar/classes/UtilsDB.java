package md.sar.classes;

import md.sar.models.ConfiguracioSistema;
import md.sar.models.DadesUsuari;
import md.sar.models.Usuari;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class UtilsDB extends DefaultControllerUser {

    // Connexió a la BBDD per obtenir l'usuari
    private final EntityManagerFactory emf;
    private ConfiguracioSistema config;

    public UtilsDB(EntityManagerFactory emf) {
        this.emf = emf;
    }

    /**
     * Comprova si la contrasenya de l'usuari és correcta
     *
     * @param login    Identificador d'usuari
     * @param password Contrasenya sense xifrar
     * @return Tres possibles valors (GRANTED, FAILED, BLOCKED)
     */
    public LoginStates checkPassword(String login, String password) throws Exception {
        LoginStates loginStates = LoginStates.BLOCKED;
        UsuariHelper<Usuari> helper = new UsuariHelper<>(this.emf, Usuari.class);
        Usuari u = helper.getUsuariByLogin(login);
        ConfiguracioSistema config = new ConfiguracioSistema();
        EntityManager em = emf.createEntityManager();
        try {
            config = em.find(config.getClass(), 1);
            System.out.println(config);
        } finally {
            em.close();
        }
        if (!u.isBlocked()) {
            //FIXIT: Comparació de contrasenyes en clar
            //Encripta la contrasenya
            EncriptacioSAR.setKey(password);
            String intentContrasenya = EncriptacioSAR.encrypt(password, EncriptacioSAR.secretKey);
            //Hash a la contrasenya encriptada
            byte[] intentContrasenyaHash = EncriptacioSAR.computeHash(intentContrasenya);
            String hex = EncriptacioSAR.byteArrayToHexString(intentContrasenyaHash);
            if (u.getLogin().equals(login) && u.getPasswd().equals(hex)) {
                if (u.isReset()) {
                    loginStates = LoginStates.RESET;
                } else {
                    loginStates = LoginStates.GRANTED;
                }
            } else {
                u.setIntents(u.getIntents() + 1);
                if (u.getIntents() >= config.getPassMaxIntents() && !u.isAdmin()) {
                    u.setBlocked(true);
                    DadesUsuari userInfo = new DadesUsuari();
                    DAOHelper daoHelper2 = new DAOHelper(emf, userInfo.getClass());
                    userInfo = (DadesUsuari) daoHelper2.getById(u.getId());
                    EnviarCorreu.EnviarMail(userInfo.getEmail(), config);
                }
                loginStates = LoginStates.FAILED;
                helper.update(u);
            }
        }
        return loginStates;
    }

    /**
     * Comprova que les dades d'uasuari siguin les correctes
     *
     * @param login      Identificador d'usuari
     * @param password   Contrasenya sense xifrar
     * @param nom        Nom d'usuari
     * @param cognom     Cognom usuari
     * @param email      email usuari
     * @param telefon    telefon usuari
     * @param adreca     adreça usuari
     * @param postalCode codi postal
     * @param poblacio   poblacio
     * @param pais       pais
     * @return Dos possibles valors (CORRECTES, INCORRECTES)
     */
    public DadesStates checkDades(String login, String password, String nom, String cognom, String email, String telefon, String adreca, String postalCode, String poblacio, String pais) throws Exception {
        DadesStates dadesStates = DadesStates.INCORRECTES;
        UsuariHelper<Usuari> helper = new UsuariHelper<>(this.emf, Usuari.class);
        Usuari u = helper.getUsuariByLogin(login);
        ConfiguracioSistema config = new ConfiguracioSistema();
        EntityManager em = emf.createEntityManager();
        try {
            config = em.find(config.getClass(), 1);
        } finally {
            em.close();
        }
        if (!u.isBlocked()) {
            //FIXIT: Comparació de contrasenyes en clar
            //Encripta la contrasenya
            EncriptacioSAR.setKey(password);
            String intentContrasenya = EncriptacioSAR.encrypt(password, EncriptacioSAR.secretKey);
            //Hash a la contrasenya encriptada
            byte[] intentContrasenyaHash = EncriptacioSAR.computeHash(intentContrasenya);
            String hex = EncriptacioSAR.byteArrayToHexString(intentContrasenyaHash);
            if (u.getLogin().equals(login) && u.getPasswd().equals(hex)) {
                if (u.getDadesUsuari().getNom().equals(nom)
                        && u.getDadesUsuari().getCognom().equals(cognom)
                        && u.getDadesUsuari().getEmail().equals(email)
                        && u.getDadesUsuari().getTelefon().equals(telefon)
                        && u.getDadesUsuari().getAdeca().equals(adreca)
                        && u.getDadesUsuari().getCodi_postal().equals(postalCode)
                        && u.getDadesUsuari().getPoblacio().equals(poblacio)
                        && u.getDadesUsuari().getPais().equals(pais)) {
                    dadesStates = DadesStates.CORRECTES;
                } else {
                    dadesStates = DadesStates.INCORRECTES;
                }
            }
        }
        return dadesStates;
    }

    /**
     * Actualitza les dades d'usuari
     *
     * @param login      Identificador d'usuari
     * @param password   Contrasenya sense xifrar
     * @param nom        Nom d'usuari
     * @param cognom     Cognom usuari
     * @param email      email usuari
     * @param telefon    telefon usuari
     * @param adreca     adreça usuari
     * @param postalCode codi postal
     * @param poblacio   poblacio
     * @param pais       pais
     * @return Dos possibles valors (CORRECTES, INCORRECTES)
     */
    public DadesStates updateDades(String login, String password, String nom, String cognom, String email, String telefon, String adreca, String postalCode, String poblacio, String pais) throws Exception {
        DadesStates dadesStates = DadesStates.INCORRECTES;
        ConfiguracioSistema config = new ConfiguracioSistema();
        EntityManager em = emf.createEntityManager();
        UsuariHelper<Usuari> helper = new UsuariHelper<>(this.emf, Usuari.class);
        Usuari u = helper.getUsuariByLogin(login);
        try {
            config = em.find(config.getClass(), 1);
        } finally {
            em.close();
        }
        if (!u.isBlocked()) {
            //FIXIT: Comparació de contrasenyes en clar
            //Encripta la contrasenya
            EncriptacioSAR.setKey(password);
            String intentContrasenya = EncriptacioSAR.encrypt(password, EncriptacioSAR.secretKey);
            //Hash a la contrasenya encriptada
            byte[] intentContrasenyaHash = EncriptacioSAR.computeHash(intentContrasenya);
            String hex = EncriptacioSAR.byteArrayToHexString(intentContrasenyaHash);
            if (u.getLogin().equals(login) && u.getPasswd().equals(hex)) {
                UsuariHelper<DadesUsuari> helper2 = new UsuariHelper(this.emf, DadesUsuari.class);
                DadesUsuari u2 = helper2.getById(4);
                u2.setNom(nom);
                helper2.update(u2);
                u2.setCognom(cognom);
                helper2.update(u2);
                u2.setEmail(email);
                helper2.update(u2);
                u2.setTelefon(telefon);
                helper2.update(u2);
                u2.setAdeca(adreca);
                helper2.update(u2);
                u2.setCodi_postal(postalCode);
                helper2.update(u2);
                u2.setPoblacio(poblacio);
                helper2.update(u2);
                u2.setPais(pais);
                helper2.update(u2);
                dadesStates = DadesStates.CORRECTES;
            }
        }
        return dadesStates;
    }
}
