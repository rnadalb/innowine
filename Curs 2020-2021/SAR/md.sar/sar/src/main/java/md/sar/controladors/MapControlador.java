package md.sar.controladors;

import com.sothawo.mapjfx.*;
import com.sothawo.mapjfx.event.MapLabelEvent;
import com.sothawo.mapjfx.event.MapViewEvent;
import com.sothawo.mapjfx.event.MarkerEvent;
import com.sothawo.mapjfx.offline.OfflineCache;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import md.sar.classes.AlertHelper;
import md.sar.classes.DefaultControllerUser;
import md.sar.classes.Patterns;

import java.util.regex.Pattern;

public class MapControlador extends DefaultControllerUser {
    /**
     * Localització per defecte, Vilafranca del Penedès
     */
    private static final double DEFAULT_LAT = 41.3461265;
    private static final double DEFAULT_LONG = 1.697939799999972;
    private static final Coordinate coordVilafrancaPenedes = new Coordinate(DEFAULT_LAT, DEFAULT_LONG);

    /**
     * zoom per defecte
     **/
    private static final int ZOOM_DEFAULT = 14;
    private static final int ZOOM_LOCATED = 17;
    private static final Marker.Provided DEFAULT_MARKER_COLOR = Marker.Provided.BLUE;
    private static IntegerProperty zoom_value = new SimpleIntegerProperty(ZOOM_DEFAULT);

    /**
     * Paràmetres per al WMS server.
     */
    private WMSParam wmsParam = new WMSParam()
            .setUrl("http://ows.terrestris.de/osm/service?")
            .addParam("layers", "OSM-WMS");

    private XYZParam xyzParams = new XYZParam()
            .withUrl("https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x})")
            .withAttributions("'Tiles &copy; <a href=\"https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer\">ArcGIS</a>'");

    @FXML
    private AnchorPane apMap;
    @FXML
    private Accordion leftControls;
    @FXML
    private TitledPane optionsMapType;
    @FXML
    private RadioButton radioMsOSM;
    @FXML
    private ToggleGroup mapTypeGroup;
    @FXML
    private RadioButton radioMsWMS;
    @FXML
    private RadioButton radioMsXYZ;
    @FXML
    private MapView mapView;
    @FXML
    private Button btZoomPlus;
    @FXML
    private Button btZoomMinus;
    @FXML
    private Slider sdZoom;
    @FXML
    private AnchorPane apData;
    @FXML
    private TextField tfLatitud;
    @FXML
    private TextField tfLongitud;
    @FXML
    private TextField tfId;
    @FXML
    private TextField tfdesc;
    @FXML
    private Button btnEdit;
    @FXML
    private Button btnSave;


    @Override
    public void iniciDiferit() {
        super.iniciDiferit();
        initMapAndControls(Projection.WEB_MERCATOR);

    }

    private void initMapAndControls(Projection projection) {
        // inicialitza MapView-Cache
        final OfflineCache offlineCache = mapView.getOfflineCache();
        final String cacheDir = System.getProperty("java.io.tmpdir") + "/mapjfx-cache";

        // estil CSS del mapa
        mapView.setCustomMapviewCssURL(getClass().getResource("/css/map.css"));

        // escolta (monitortiza) la propietat que indica que el mapa ha finalitzat la seva inicialització
        mapView.initializedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                afterMapIsInitialized();
            }
        });

        // controla els radio btuuons que gestionen els tipus de mapes
        mapTypeGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            MapType mapType = MapType.OSM;
            if (newValue == radioMsOSM) {
                mapType = MapType.OSM;
            } else if (newValue == radioMsWMS) {
                mapView.setWMSParam(wmsParam);
                mapType = MapType.WMS;
            } else if (newValue == radioMsXYZ) {
                mapView.setXYZParam(xyzParams);
                mapType = MapType.XYZ;
            }
            mapView.setMapType(mapType);
        });
        mapTypeGroup.selectToggle(radioMsOSM);

        // Associament de la propietat del zoom als controls slider i button corresponents
        sdZoom.valueProperty().bindBidirectional(mapView.zoomProperty());
        zoom_value.bindBidirectional(mapView.zoomProperty());

        setupEventHandlers();

        mapView.initialize(Configuration.builder()
                .projection(projection)
                .showZoomControls(false)
                .build());
    }

    /**
     * Inicialitza els listeners de gestió del mapa i la GUI
     */
    private void setupEventHandlers() {

        // Gestió del zoom pels botons
        btZoomPlus.setOnAction(event -> {
            event.consume();
            mapView.setZoom(mapView.getZoom() + 1);
        });

        btZoomMinus.setOnAction(event -> {
            event.consume();
            mapView.setZoom(mapView.getZoom() - 1);
        });

        // Gestiona l'esdeveniment de fer clic en el mapa
        mapView.addEventHandler(MapViewEvent.MAP_CLICKED, event -> {
            event.consume();
            final Coordinate newPosition = event.getCoordinate().normalize();
            AlertHelper.alertInfo(String.format("[MAP_CLICKED] --> Posicio: [%s, %s]", newPosition.getLatitude(), newPosition.getLongitude()));
        });

        // Gestiona l'esdeveniment de fer clic amb el botó dret en el mapa
        mapView.addEventHandler(MapViewEvent.MAP_RIGHTCLICKED, event -> {
            event.consume();
            AlertHelper.alertInfo(String.format("[MAP_RIGHTCLICKED] --> Posicio: [%s, %s]", event.getCoordinate().getLatitude(), event.getCoordinate().getLongitude()));
        });

        // Gestiona quan s'ha fet clic a un marcador
        mapView.addEventHandler(MarkerEvent.MARKER_CLICKED, event -> {
            event.consume();
            AlertHelper.alertInfo(String.format("[MARKER_CLICKED] --> Id del marcador: [%s]", event.getMarker().getId()));
        });

        // Gestiona quan s'ha fet clic amb el botó dret a un marcador
        mapView.addEventHandler(MarkerEvent.MARKER_RIGHTCLICKED, event -> {
            event.consume();
            AlertHelper.alertInfo(String.format("[MARKER_RIGHTCLICKED] --> Id del marcador: [%s]", event.getMarker().getId()));
        });

        // Gestiona quan s'ha fer clic en una etiqueta al mapa
        mapView.addEventHandler(MapLabelEvent.MAPLABEL_CLICKED, event -> {
            event.consume();
            AlertHelper.alertInfo(String.format("[MAPLABEL_CLICKED] --> Id del marcador: [%s]", event.getMapLabel().getId()));
        });

        // Gestiona quan s'ha fer clic amb el botó dret en una etiqueta al mapa
        mapView.addEventHandler(MapLabelEvent.MAPLABEL_RIGHTCLICKED, event -> {
            event.consume();
            AlertHelper.alertInfo(String.format("[MAPLABEL_RIGHTCLICKED] --> Id del marcador: [%s]", event.getMapLabel().getId()));
        });

        // Gestiona el punter al mapa
        mapView.addEventHandler(MapViewEvent.MAP_POINTER_MOVED, event -> {
            System.out.println("el punter esta en " + event.getCoordinate());
        });
    }

    /**
     * Finalitza la configuració desprès que el mapa estigui inicialitzat
     */
    private void afterMapIsInitialized() {
        mapView.setZoom(ZOOM_DEFAULT);
        mapView.setCenter(coordVilafrancaPenedes);
        addMarker(true);
    }

    /**
     * Afegeix un marcador
     *
     * @param center si és true centra en el mapa la nova posició
     * @return
     */
    private boolean addMarker(boolean center) {
        boolean result = false;
        if (!tfLatitud.getText().isEmpty() && !tfLongitud.getText().isEmpty()) {
            if (Pattern.matches(Patterns.fpRegex, tfLatitud.getText())) {
                Double lat = Double.valueOf(tfLatitud.getText());
                if (Pattern.matches(Patterns.fpRegex, tfLongitud.getText())) {
                    Double lng = Double.valueOf(tfLongitud.getText());
                    Coordinate c = new Coordinate(lat, lng);
                    addMarker(c, DEFAULT_MARKER_COLOR, center);
                    result = true;
                } else {
                    AlertHelper.alertInfo("El valor de la <longitud> no es correcte");
                }
            } else {
                AlertHelper.alertInfo("El valor de la <latitud> no es correcte");
            }
        }
        return result;
    }

    private void addMarker(Coordinate coordinate, Marker.Provided color, boolean center) {
        Marker marker = Marker.createProvided(color).setPosition(coordinate).setVisible(true);
        String info = String.format("Posicio [%s,%s]", coordinate.getLatitude().toString(), coordinate.getLongitude().toString());
        MapLabel lblInfo = new MapLabel(info, 10, -10).setVisible(false).setCssClass("orange-label");
        marker.attachLabel(lblInfo);
        mapView.addMarker(marker);
        if (center)
            mapView.setCenter(coordinate);
        mapView.setZoom(ZOOM_LOCATED);
    }

    @FXML
    public void btSearchOnAction(ActionEvent actionEvent) {
        addMarker(true);
    }

    @FXML
    public void btEditOnAction(ActionEvent actionEvent) {
        tfId.setEditable(true);
        tfdesc.setEditable(true);
        btnEdit.setVisible(false);
        btnSave.setVisible(true);
    }

    @FXML
    public void btSaveOnAction(ActionEvent actionEvent) {
        Window owner = btnSave.getScene().getWindow();
        if (tfId.getText().equals("")){
            AlertHelper.showAlert(Alert.AlertType.ERROR,owner,"Error","El camp ID es obligatori");
            return;
        }
        tfId.setEditable(false);
        tfdesc.setEditable(false);
        btnEdit.setVisible(true);
        btnSave.setVisible(false);
    }

    @FXML
    public void btExitOnAction(ActionEvent actionEvent) {
        ((Stage) apData.getScene().getWindow()).close();
    }
}
