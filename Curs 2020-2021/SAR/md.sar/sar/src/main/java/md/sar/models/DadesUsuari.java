package md.sar.models;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "tbl_dades_usuari")
/**
 * Clase que representa a l'Usuari en la base de dades
 */

public class DadesUsuari implements Serializable {
    private static final long serialVersionUID = -8525273910151342973L;

    @Id
    @Column(name = "usuari_id", unique = true)
    private int idUsuari;

    @OneToOne(mappedBy = "dadesUsuari", cascade = CascadeType.ALL)
    //@MapsId
    //@JoinColumn(name = "usuari_id", updatable = false, nullable = false)
    private Usuari usuari;

    @Column(name = "nom")
    private String nom;

    @Column(name = "cognoms")
    private String cognom;

    @Column(name = "email")
    private String email;

    @Column(name = "telefon")
    private String telefon;

    @Column(name = "adreca")
    private String adeca;

    @Column(name = "codi_postal")
    private String codi_postal;

    @Column(name = "poblacio")
    private String poblacio;

    @Column(name = "pais")
    private String pais;

    public DadesUsuari() {
    }

    public int getIdUsuari() {
        return idUsuari;
    }

    public void setIdUsuari(int idUsuari) {
        this.idUsuari = idUsuari;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getAdeca() {
        return adeca;
    }

    public void setAdeca(String adeca) {
        this.adeca = adeca;
    }

    public String getCodi_postal() {
        return codi_postal;
    }

    public void setCodi_postal(String codi_postal) {
        this.codi_postal = codi_postal;
    }

    public String getPoblacio() {
        return poblacio;
    }

    public void setPoblacio(String poblacio) {
        this.poblacio = poblacio;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Usuari getUsuari() {
        return usuari;
    }

    public void setUsuari(Usuari usuari) {
        this.usuari = usuari;
    }
}
