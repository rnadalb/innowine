package md.sar.models;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

import javax.persistence.*;
import java.io.Serializable;



@Entity
@Table (name = "tbl_usuari")
/**
 * Clase que representa a l'Usuari en la base de dades
 */
public class Usuari implements Serializable{

    private static final long serialVersionUID = -8525273910151342973L;

    @Id
    @Column(name = "id", unique = true)
    private int id;

    @Column(name = "login")
    private String login;

    @Column(name = "passwd")
    private String passwd;

    @Column(name = "blocked")
    private boolean blocked;

    @Column(name = "reset")
    private boolean reset;

    @Column(name = "admin")
    private boolean admin;

    @Column(name = "intents")
    private int intents;

    @OneToOne
    @JoinColumn(name = "id", updatable = false, nullable = false)
    private DadesUsuari dadesUsuari;

    public DadesUsuari getDadesUsuari() {
        return dadesUsuari;
    }

    public void setDadesUsuari(DadesUsuari dadesUsuari) {
        this.dadesUsuari = dadesUsuari;
    }

    //Constructors
    public Usuari() {
    }

    public Usuari(int id, String login, String passwd, boolean blocked, boolean admin, boolean reset, int intents) {
        this.id = id;
        this.login = login;
        this.passwd = passwd;
        this.blocked = blocked;
        this.admin = admin;
        this.reset = reset;
        this.intents = intents;
    }

    //Getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isReset() {
        return reset;
    }

    public void setReset(boolean reset) {
        this.reset = reset;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public int getIntents() {
        return intents;
    }

    public void setIntents(int intents) {
        this.intents = intents;
    }

    // es necessita per omplir la casella propi el formulari de cerca
    public SimpleBooleanProperty getBlockedProperty() {
        return new SimpleBooleanProperty(blocked);
    }
    public SimpleStringProperty getLoginProperty() {
        return new SimpleStringProperty(login);
    }

    @Override
    public String toString() {
        return "Usuari{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", passwd='" + passwd + '\'' +
                ", admin=" + admin +
                ", blocked=" + blocked +
                ", reset=" + reset +
                ", intents=" + intents +
                '}';
    }


}
