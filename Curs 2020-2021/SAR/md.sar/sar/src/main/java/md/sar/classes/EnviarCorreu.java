package md.sar.classes;

import md.sar.models.ConfiguracioSistema;

import javax.mail.*;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;


public class EnviarCorreu{

    /**
     * Mètode que envia un correo a un compte de gmail
     */
    public static void EnviarMail(String destinatario, ConfiguracioSistema config)  {


        String asunto = config.getEmailAbout();
        String cuerpo = config.getEmailBody();

        // Això es el que va davant de @gmail.com en la teva compte de correu. Es el remitent també.
        String remitente = config.getEmailUser();  //Per la direcció nomcuenta@gmail.com
        String clave= config.getEmailContrasenya();     //La contrasenya del tu correo
        Properties props = System.getProperties();
        props.put("mail.smtp.host", config.getEmailSMPT());  //El servidor SMTP de Google
        props.put("mail.smtp.user", remitente);
        props.put("mail.smtp.clave", clave);    //La clau de la cuenta
        props.put("mail.smtp.auth", "true");    //Usar autenticació a partir de usuari i clau
        props.put("mail.smtp.starttls.enable", "true"); //Per conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", "587"); //El puerto SMTP segur de Google

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(remitente));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));   //Es podria afegir de la mateixa manera
            message.setSubject(asunto);
            message.setText(cuerpo);
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", remitente, clave);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (MessagingException me) {
            me.printStackTrace();   //Si es produeix un error
        }

    }
}
