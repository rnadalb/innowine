package md.sar.controladors;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import md.sar.classes.DAOHelper;
import md.sar.classes.DefaultControllerUser;
import md.sar.classes.EncriptacioSAR;
import md.sar.classes.ModeStates;
import md.sar.models.DadesUsuari;
import md.sar.models.Usuari;

public class CanviContraControlador extends DefaultControllerUser {

    // <editor-fold defaultstate="collapsed" desc="Components FXML">
    @FXML
    private Button BTcanviar;
    @FXML
    private Button BTcancelar;
    @FXML
    private TextField TFactualPass;
    @FXML
    private TextField TFnewPass;
    @FXML
    private TextField TFconfirmPass;
    @FXML
    private Label lActualPass;
    @FXML
    private Label lConfirmPass;
    // </editor-fold>


    /**
     * A l'apretar el botó "canviar agafa el valor introduït en el textfields i fa una seqüència de condicions comprovant
     * que el usuari introduït en el textfield es igual al de la base de dades, el mateix amb la contrasenya i comprova que
     * la contrasenya compleix els criteris de complexitat"
     *
     */

    @FXML
    void BTcanviarOnAction() throws Exception {
        // <editor-fold defaultstate="collapsed" desc="Definir les variables, crear helper i user">
        String actualPass = TFactualPass.getText();
        String newPass = TFnewPass.getText();
        String confirmPass = TFconfirmPass.getText();
        String novapassEncriptada;
        String antigapassEncriptada;
        Usuari user = getUser();
        DadesUsuari userInfo = new DadesUsuari();
        DAOHelper daoHelper = new DAOHelper(getEmf(), userInfo.getClass());
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Encriptació de la nova contrasenya">
        //Per comprovar en les condicions si coincideix amb la contrasenya encriptada de la base de dades
        EncriptacioSAR.setKey(actualPass);
        antigapassEncriptada = EncriptacioSAR.encrypt(actualPass, EncriptacioSAR.secretKey);
        assert antigapassEncriptada != null;
        byte[] intentContrasenyaHash = EncriptacioSAR.computeHash(antigapassEncriptada);
        String hexActual = EncriptacioSAR.byteArrayToHexString(intentContrasenyaHash);

        //Nova contrasenya
        EncriptacioSAR.setKey(newPass);
        novapassEncriptada = EncriptacioSAR.encrypt(newPass, EncriptacioSAR.secretKey);
        assert novapassEncriptada != null;
        byte[] novapassEncriptadaHash = EncriptacioSAR.computeHash(novapassEncriptada);
        String hexNew = EncriptacioSAR.byteArrayToHexString(novapassEncriptadaHash);
        // </editor-fold>


        // <editor-fold defaultstate="collapsed" desc="Condicions">
        if (getMode().equals(ModeStates.CANVIA_CONNTRA_ADMIN)) {
            canviContra(newPass, confirmPass, user, daoHelper, hexNew);
        }
        if (!hexActual.equals(hexNew) && getMode().equals(ModeStates.CANVIA_CONTRA)) {
            if (hexActual.equals(user.getPasswd())) {
                if (comprovaReqPass(newPass)) {
                    canviContra(newPass, confirmPass, user, daoHelper, hexNew);
                } else {
                    mostraAlertaWarning(
                            "Comprova els requisits",
                            "La contrasenya tindra al menys 6 caracters,al menys una majus,al menys un digit"
                    );
                }
            } else {
                mostraAlertaWarning(
                        "",
                        "Error en la contrasenya actual"
                );
            }
        } else if (getMode().equals(ModeStates.CANVIA_CONTRA)){
            mostraAlertaWarning(
                    "Mateixa contrasenya",
                    "Has de introduir una contrasenya diferent"
            );
        }
        // </editor-fold>
    }

    private void canviContra(String newPass, String confirmPass, Usuari user, DAOHelper daoHelper, String hexNew) {
        //Si coincideix amb la confirmacio o si el usuari es admin
        if (confirmPass.equals(newPass) || getMode().equals(ModeStates.CANVIA_CONNTRA_ADMIN)) {
            user.setPasswd(hexNew);
            user.setReset(false);
            daoHelper.update(user);
            mostraAlertaInformation(
            );
            //Tanquem la stage
            Stage stage = (Stage) BTcanviar.getScene().getWindow();
            stage.close();
        }else {
            mostraAlertaWarning(
                    "No coincideix",
                    "La confirmació de la contrasenya no coincideix"
            );
        }
    }

    /**
     * Tanca la vista
     *
     */
    @FXML
    void BTcancelarOnAction() {

        Stage stage = (Stage) BTcancelar.getScene().getWindow();
        stage.close();
    }

    // <editor-fold defaultstate="collapsed" desc="Metodes">

    /**
     * Comprova els requeriments de complexitat de la contrasenya,La contrasenya tindra al menys 6 caracters,
     * ha de contenir al menys una lletra i al menys un digit.
     *
     * @param password valor que agafem del textfield TFnewPass
     */
    private static boolean comprovaReqPass(String password) {
        return (password.length() >= 6) &&
                (password.matches(".*[a-z]+.*")) &&
                (password.matches(".*[A-Z]+.*")) &&
                (password.matches(".*[0-9]+.*"));
    }

    public void obtenirDades() {
        if (getMode().equals(ModeStates.CANVIA_CONNTRA_ADMIN)) {
            TFactualPass.setVisible(false);
            lActualPass.setVisible(false);
            TFconfirmPass.setVisible(false);
            lConfirmPass.setVisible(false);
        }
    }
    @FXML
    public void onEnter(KeyEvent e) throws Exception {
        if(e.getCode().toString().equals("ENTER"))
        {
            BTcanviarOnAction();
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Alertes">

    /**
     * Crea una alerta de tipus WARNING
     *  @param TitleText   el text que sortirà al títol de la alerta
     * @param HeaderText  el text que sortirà al header de la alerta
     */
    private void mostraAlertaWarning(String TitleText, String HeaderText) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(TitleText);
        alert.setHeaderText(HeaderText);
        alert.setContentText("Torna a intentar-ho");
        alert.showAndWait();
    }

    /**
     * Crea una alerta de tipus INFORMATION
     *
     */
    private void mostraAlertaInformation() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Fet");
        alert.setHeaderText("");
        alert.setContentText("Contrasenya actualitzada");
        alert.showAndWait();
    }
    // </editor-fold>
}