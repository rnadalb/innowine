package md.sar.controladors;


import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import md.sar.classes.*;
import md.sar.models.Usuari;

public class LoginControlador extends DefaultControllerUser {

    @FXML
    private TextField TFUsuari;

    @FXML
    private PasswordField PFContrasenya;

    @FXML
    private Button BEntrar;

    //Obrim el fxml principal al apretar el boto d'entrar en el Login.fxml
    @FXML
    void onActionEntrar() throws Exception {

        UtilsDB utilsDB = new UtilsDB(getEmf());

        if(TFUsuari.getText() != null) {

            UsuariHelper<Usuari> helper = new UsuariHelper<>(getEmf(), Usuari.class);
            try {
                setUser(helper.getUsuariByLogin(TFUsuari.getText()));
            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error conexió");
                alert.setContentText("No es pot conectar a la base de dades, comprovi la seva conexio a internet");
                alert.showAndWait();
            }

            //Evalua l'intent d'entrar al sistema
            LoginStates loginStates = utilsDB.checkPassword(TFUsuari.getText(),PFContrasenya.getText());

            if(loginStates == LoginStates.GRANTED){

                Stage stage2 = (Stage) BEntrar.getScene().getWindow();
                stage2.close();
                obrir_vista("/vistes/Main.fxml",getEmf(),getUser(),getConfig(),ModeStates.MAIN);

            }else if(loginStates == LoginStates.RESET){

                Stage stage2 = (Stage) BEntrar.getScene().getWindow();
                stage2.close();
                obrir_vista("/vistes/CanviContra.fxml",getEmf(),getUser(),getConfig(),ModeStates.CANVIA_CONTRA);

            }else if(loginStates == LoginStates.FAILED){

                setUser(helper.getUsuariByLogin(getUser().getLogin()));
                int intentsRestants = getConfig().getPassMaxIntents() - getUser().getIntents();
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error login");
                if(!(intentsRestants == 0)){
                    alert.setContentText("Usuari i/o contrasenya no valid, si us plau recordi que te "+
                            intentsRestants+
                            " intents per a entrar al sistema, despres el seu usuari sera bloquejat.");
                }else{
                    alert.setContentText("Usuari i/o contrasenya no valid, 0 intents restants, usuari bloquejat");
                }
                alert.showAndWait();

            }else if((loginStates == LoginStates.BLOCKED)){

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error login");
                alert.setContentText("Usuari bloquejat! Posat en contacte amb el teu administrador"+
                        " i/o consulta el teu correu electornic asignat a la conta");
                alert.showAndWait();

            }
        }
    }

    @FXML
    public void onEnter(KeyEvent e) throws Exception {
        if(e.getCode().toString().equals("ENTER"))
        {
            onActionEntrar();
        }
    }
}
