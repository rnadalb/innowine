package md.sar.classes;

import javafx.scene.control.Alert;
import javafx.stage.Window;

public class AlertHelper {
    private static final String TITLE = "MapJFX Desktop App";

    public static void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }
    private static void alerta(String title, String header, String message, Alert.AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setTitle(message);
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private static void alerta(String title, String message, Alert.AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setTitle(message);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private static void alertInfo(String title, String header, String message) {
        alerta(title, header, message, Alert.AlertType.INFORMATION);
    }

    public static void alertInfo(String message) {
        alerta(TITLE, message, Alert.AlertType.INFORMATION);
    }

}
