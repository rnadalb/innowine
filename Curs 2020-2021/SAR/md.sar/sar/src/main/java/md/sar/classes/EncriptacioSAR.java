package md.sar.classes;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

public class EncriptacioSAR {
    public static SecretKeySpec secretKey;

    /**
     * Agafa els Bytes de la clau en format String introduida per parametre i la assigna a la variable global secretKey
     *
     * @param myKey Variable que s'utilitza en el metode de encriptar asignant el valor de la variable global secreyKey
     */
    public static void setKey(String myKey) {
        MessageDigest sha;
        try {
            byte[] key = myKey.getBytes(StandardCharsets.UTF_8);
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * Fa hash a una String rebuda per parametre i retorna una array de bytes
     *
     * @param x El text al que li farem el hash
     */
    public static byte[] computeHash(String x) throws Exception {
        MessageDigest d;
        d = MessageDigest.getInstance("SHA-1");
        d.reset();
        d.update(x.getBytes());
        return d.digest();
    }

    /**
     * Transforma una Array de bytes a una String hexadecimal
     *
     * @param b la array a transformar
     */
    public static String byteArrayToHexString(byte[] b) {
        StringBuilder sb = new StringBuilder(b.length * 2);
        for (byte value : b) {
            int v = value & 0xff;
            if (v < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(v));
        }
        return sb.toString().toUpperCase();
    }

    /**
     * Encripta la variable String amb el algoritme AES i el retorna codificat en Base64
     *
     * @param strToEncrypt El text a encriptar
     * @param secret       La clau generada amb el metode setKey
     */
    public static String encrypt(String strToEncrypt, SecretKeySpec secret) {
        try {
            setKey(String.valueOf(secret));
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }
}