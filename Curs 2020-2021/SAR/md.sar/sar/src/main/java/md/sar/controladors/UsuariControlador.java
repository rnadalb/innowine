package md.sar.controladors;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import md.sar.classes.*;
import md.sar.models.DadesUsuari;
import md.sar.models.Usuari;

import javax.management.Notification;


public class UsuariControlador extends DefaultControllerUser {
    /**
     * Variables de la vista Usuari.fxml
     */

    @FXML
    private Button beditar;
    @FXML
    private Button bguardar;
    @FXML
    private TextField tfusuari;
    @FXML
    private TextField tfcorreu;
    @FXML
    private TextField tfnom;
    @FXML
    private TextField tfcognom;
    @FXML
    private TextField tftelefon;
    @FXML
    private TextField tfadreca;
    @FXML
    private TextField tfcodipostal;
    @FXML
    private TextField tfpoblacio;
    @FXML
    private TextField tfpais;
    @FXML
    private Text lobligatorinom;
    @FXML
    private Text lobligatoricognom;
    @FXML
    private Text lobligatoricorreu;
    @FXML
    private Text lphonerror;
    @FXML
    private Button bcanviar;
    @FXML
    private CheckBox chkbBloquejat;
    @FXML
    private PasswordField tfpassword;
    @FXML
    private ImageView imgCandau;
    @FXML
    private Text lmissatge;


    /**
     * El següent mètode permet l'edició de tots els camps de la vista personalització
     *
     */
    @FXML
    void Beditar() {
        if (getMode() == ModeStates.ALTA_USUARIS) {
            tfpassword.setEditable(true);
            chkbBloquejat.setDisable(false);
            imgCandau.setDisable(false);
        }
        //Habilitem tots els camps per ser editables
        tfnom.setEditable(true);
        tfcognom.setEditable(true);
        tfcorreu.setEditable(true);
        tftelefon.setEditable(true);
        tfadreca.setEditable(true);
        tfcodipostal.setEditable(true);
        tfpoblacio.setEditable(true);
        tfpais.setEditable(true);
        //Deshabilitem el boto editar i habilitem el de guardar
        beditar.setDisable(true);
        bguardar.setDisable(false);
        bcanviar.setDisable(false);
    }

    /**
     * El següent mètode permet guardar les dades en variables
     *
     */
    @FXML
    void Bguardar() throws Exception {
        //Informació necessaria per actualitzar la BBDD
        DadesUsuari userInfo = new DadesUsuari();
        DAOHelper daoHelper = new DAOHelper(getEmf(), userInfo.getClass());
        userInfo.setIdUsuari(getUser().getId());
        Usuari user = getUser();

        //Comprovem els camps que estiguin correctament colocats
        String nom = tfnom.getText();
        String cognom = tfcognom.getText();
        String correu = tfcorreu.getText();
        String phone = tftelefon.getText();
        if (nom.equals("")) {
            lobligatorinom.setVisible(true);
            bguardar.setDisable(false);
            beditar.setDisable(true);
            return;
        } else {
            lobligatorinom.setVisible(false);
        }
        if (cognom.equals("")) {
            lobligatoricognom.setVisible(true);
            bguardar.setDisable(false);
            beditar.setDisable(true);
            return;
        } else {
            lobligatoricognom.setVisible(false);
        }
        if (correu.equals("")) {
            lobligatoricorreu.setVisible(true);
            bguardar.setDisable(false);
            beditar.setDisable(true);
            return;
        } else if (!isValidEmail(correu)) {
            lobligatoricorreu.setText("Correu invalid");
            lobligatoricorreu.setVisible(true);
            bguardar.setDisable(false);
            beditar.setDisable(true);
            return;
        } else {
            lobligatoricorreu.setVisible(false);
        }
        if (!phone.equals("") && !isValidPhone(phone)) {
            lphonerror.setVisible(true);
            bguardar.setDisable(false);
            beditar.setDisable(true);
            return;
        } else {
            lphonerror.setVisible(false);
        }

        //Activem el boto editar i deshabilitem el boto guardar
        bguardar.setDisable(true);
        beditar.setDisable(false);
        if (getMode() == ModeStates.ALTA_USUARIS) {
            //Deshabilitem els TF corresponents a alta usuaris
            tfpassword.setEditable(false);
            chkbBloquejat.setDisable(true);
            imgCandau.setDisable(true);
        }
        //Deshabilitem tots els TF per impedir editar-los
        tfnom.setEditable(false);
        tfcognom.setEditable(false);
        tfcorreu.setEditable(false);
        tftelefon.setEditable(false);
        tfadreca.setEditable(false);
        tfcodipostal.setEditable(false);
        tfpoblacio.setEditable(false);
        tfpais.setEditable(false);
        bcanviar.setDisable(true);

        //Actualitzar informacio a la BBDD
        userInfo.setNom(tfnom.getText());
        userInfo.setCognom(tfcognom.getText());
        userInfo.setEmail(tfcorreu.getText());
        userInfo.setTelefon(tftelefon.getText());
        userInfo.setAdeca(tfadreca.getText());
        userInfo.setCodi_postal(tfcodipostal.getText());
        userInfo.setPoblacio(tfpoblacio.getText());
        userInfo.setPais(tfpais.getText());
        daoHelper.update(userInfo);
        if (getMode() == ModeStates.ALTA_USUARIS) {
            //Deshabilitem els TF corresponents a alta usuaris
            user.setBlocked(chkbBloquejat.isSelected());
            if (!tfpassword.getText().equals("")) {
                String newPass = tfpassword.getText();
                EncriptacioSAR.setKey(newPass);
                String novapassEncriptada = EncriptacioSAR.encrypt(newPass, EncriptacioSAR.secretKey);
                assert novapassEncriptada != null;
                byte[] novapassEncriptadaHash = EncriptacioSAR.computeHash(novapassEncriptada);
                String hexNew = EncriptacioSAR.byteArrayToHexString(novapassEncriptadaHash);
                user.setPasswd(hexNew);
            }
            daoHelper.update(user);


        }
        Stage stage = (Stage) bguardar.getScene().getWindow();
        showPopupMessage("Dades guardades amb exit",stage);

    }

    public static Popup createPopup(final String message) {
        final Popup popup = new Popup();
        popup.setAutoFix(true);
        popup.setAutoHide(true);
        popup.setHideOnEscape(true);
        Label label = new Label(message);
        label.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                popup.hide();
            }
        });
        label.setTextFill(Color.web("#36ff33"));
        label.setId("lmissatgeexit");
        label.getStyleClass().add("popup");
        popup.getContent().add(label);
        return popup;
    }
    public static void showPopupMessage(final String message, final Stage stage) {
        final Popup popup = createPopup(message);
        popup.setOnShown(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent e) {
                popup.setX((stage.getX() + stage.getWidth()/2 - popup.getWidth()/2)-50);
                popup.setY((stage.getY() + stage.getHeight()/2 - popup.getHeight()/2)-240);
                System.out.println(popup.getY());
            }
        });
        popup.show(stage);
    }
    /**
     * El següent mètode permet comporvar el email
     *
     * @param email paràmetre que llença l'event per tal de poder guardar les dades
     */
    static boolean isValidEmail(String email) {
        String regex = "^[\\w-_.+]*[\\w-_.]@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    static boolean isValidPhone(String phone) {
        String regex = "^\\d{9}$";
        return phone.matches(regex);
    }

    /**
     * El següent mètode permet obrir la vista de canviar contrasenya
     *
     * @param event paràmetre que llença l'event per tal de poder obrir la vista
     */
    @FXML
    void canvi_contrasenya_img(MouseEvent event) {

        if (event.getButton() == MouseButton.PRIMARY) {
            obrir_vista("/vistes/CanviContra.fxml", getEmf(), getUser(), getConfig(), ModeStates.CANVIA_CONNTRA_ADMIN);
        }
    }

    @FXML
    void canvi_contrasenya() {
        Stage stage = (Stage) bguardar.getScene().getWindow();
        stage.close();
        obrir_vista("/vistes/CanviContra.fxml", getEmf(), getUser(), getConfig(), ModeStates.CANVIA_CONTRA);
    }

    @Override
    public void obtenirDades() {
        tfnom.setText(this.getUser().getDadesUsuari().getNom());
        tfusuari.setText(this.getUser().getLogin());
        tfnom.setText(this.getUser().getDadesUsuari().getNom());
        tfcognom.setText(this.getUser().getDadesUsuari().getCognom());
        tfcorreu.setText(this.getUser().getDadesUsuari().getEmail());
        tftelefon.setText(this.getUser().getDadesUsuari().getTelefon());
        tfadreca.setText(this.getUser().getDadesUsuari().getAdeca());
        tfcodipostal.setText(this.getUser().getDadesUsuari().getCodi_postal());
        tfpoblacio.setText(this.getUser().getDadesUsuari().getPoblacio());
        tfpais.setText(this.getUser().getDadesUsuari().getPais());

        if (getMode() == ModeStates.PERSONALITZACIO_DADES) {
            tfpassword.setVisible(false);
            chkbBloquejat.setVisible(false);
            imgCandau.setVisible(false);
        } else if (getMode() == ModeStates.ALTA_USUARIS) {
            imgCandau.setVisible(true);
            imgCandau.setDisable(true);
            bcanviar.setVisible(false);
            tfpassword.setVisible(true);
            chkbBloquejat.setDisable(true);
            tfpassword.setEditable(false);
            if (getUser().isBlocked()) {
                chkbBloquejat.setSelected(true);
            }
        }
    }
}