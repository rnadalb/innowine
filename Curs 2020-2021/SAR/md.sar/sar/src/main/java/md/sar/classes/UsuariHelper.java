package md.sar.classes;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class UsuariHelper<Usuari> extends DAOHelper<Usuari>{

    public UsuariHelper(EntityManagerFactory emf, Class<Usuari> parameterClass) {
        super(emf, parameterClass);
    }

    /**
     * Permet obtenir un usuari a partir del seu login
     * @param login
     * @return
     */
    public Usuari getUsuariByLogin(String login) {
        EntityManager em = this.emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Usuari> cbQuery = (CriteriaQuery<Usuari>) cb.createQuery(parameterClass);
        Root<Usuari> c = (Root<Usuari>) cbQuery.from(parameterClass);
        cbQuery.select(c).where(cb.equal(c.get("login"), login));

        Query query = em.createQuery(cbQuery);
        return (Usuari) query.getSingleResult();
    }

}
