package md.sar.classes;


import md.sar.interfaces.Dades;
import md.sar.interfaces.IniciDiferit;
import md.sar.models.Usuari;


public class DefaultControllerUser extends DefaultController implements Dades, IniciDiferit {

    private Usuari user;
    public void setUser(Usuari user) {
        this.user = user;
    }
    public Usuari getUser() {
        return user;
    }

    @Override
    public void obtenirDades() {
    }

    @Override
    public void iniciDiferit() {

    }
}
