package md.sar.controladors;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import md.sar.classes.DefaultControllerUser;

public class SobreControlador extends DefaultControllerUser {
    //Declaracio variables del fxml de Sobre
    @FXML
    private Button btnTanca;

    /**
     * El seüent mètode permet tancar la vista de sobre al donar-li a sortir i permet que puguis retornar al main
     */
    //Metode que s'activa al clicar Tancar en Sobre.fxml per tancar la Satge i retornar al Main.fxml
    @FXML
    void btnTancaOnAction() {

        Stage stage = (Stage)btnTanca.getScene().getWindow();
        stage.close();

    }

}