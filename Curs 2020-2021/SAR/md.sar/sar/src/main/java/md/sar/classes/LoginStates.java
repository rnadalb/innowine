package md.sar.classes;

public enum LoginStates {
    GRANTED(0, "Accés garantit !!!"),
    FAILED(-1,  "Accés erroni"),
    BLOCKED(1,  "Compte bloquejat"),
    RESET(2,  "S'ha de canviat la contrasenya");

    private int errorCode;
    private String message;

    LoginStates(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
