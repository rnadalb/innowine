package md.sar.controladors;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.Window;
import md.sar.classes.AlertHelper;
import md.sar.classes.DAOHelper;
import md.sar.classes.DefaultControllerUser;
import md.sar.classes.EncriptacioSAR;
import md.sar.models.ConfiguracioSistema;

import java.util.Optional;

public class ConfiguracioSistemaControlador extends DefaultControllerUser {

    //Declaracio variables del fxml de configuració sistema
    // <editor-fold defaultstate="collapsed" desc="Components FMXL">
    @FXML
    Button bGuardar;
    @FXML
    Button bTancar;
    @FXML
    Button bNextTab1;
    @FXML
    Button bNextTab2;
    @FXML
    TabPane tabPane;
    @FXML
    Tab tabContrasenya, tabCorreu, tabBBDD;
    @FXML
    TextField tfNCaracters, tfNIntents, tfAssumpte,
            tfAdreca,tfUsuari,tfAdrecaHost,tfPort,tfNomBBDD,tfUsuari2,tfContrasenya2;
    @FXML
    TextArea tfCos;
    @FXML
    PasswordField tfContrasenya;

    // </editor-fold>

    ConfiguracioSistema config;

    @Override
    public void obtenirDades() {
        super.obtenirDades();
        DAOHelper<ConfiguracioSistema> daoHelper = new DAOHelper(getEmf(),ConfiguracioSistema.class);

        config = daoHelper.getAll().get(0);

        //Agafar els valors de la base de dades
        tfNCaracters.setText(String.valueOf(config.getPassMinCaracters()));
        tfNIntents.setText(String.valueOf(config.getPassMaxIntents()));
        tfAssumpte.setText(String.valueOf(config.getEmailAbout()));
        tfCos.setText(String.valueOf(config.getEmailBody()));
        tfAdreca.setText(String.valueOf(config.getEmailSMPT()));
        tfUsuari.setText(String.valueOf(config.getEmailUser()));
        tfContrasenya.setText(String.valueOf(config.getEmailContrasenya()));
        tfAdrecaHost.setText(String.valueOf(config.getInfluxdbHostAdress()));
        tfPort.setText(String.valueOf(config.getInfluxdbPort()));
        tfNomBBDD.setText(String.valueOf(config.getInfluxdbNomBBDD()));
        tfUsuari2.setText(String.valueOf(config.getInfluxdbUsuari()));
        tfContrasenya2.setText(String.valueOf(config.getInfluxdbContrasenya()));
    }

    // <editor-fold defaultstate="collapsed" desc="Buttons Tab">
    public void bNextTabOnAction(ActionEvent event) {
        Window owner = bNextTab1.getScene().getWindow();

        if (!tfNCaracters.getText().isEmpty() && !tfNIntents.getText().isEmpty()) {
            int valorCaracters = -1;
            int valorIntents = -1;
            try {
                valorCaracters = Integer.parseInt(tfNCaracters.getText());
                valorIntents = Integer.parseInt(tfNIntents.getText());

            } catch (NumberFormatException e) {

                AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error",
                        "Els camps han de ser un numero ");
                return;
            }
            if (valorCaracters < 6) {
                AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error",
                        "El nombre minim de caracters es 6, torna-ho a intentar! ");
                tfNCaracters.requestFocus();
                tfNCaracters.selectAll();
            }
            else if (valorIntents < 4) {
                AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error",
                        "El nombre minim d'intents es 4, torna-ho a intentar! ");
                tfNIntents.requestFocus();
                tfNIntents.selectAll();

            } else {
                tabPane.getSelectionModel().select(tabCorreu);
            }
        } else {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error",
                    "El camp es obligatori!");
            seleccionarError();
        }
    }

    public void seleccionarError(){
        if (tfNIntents.getText().isEmpty() ) {
            tfNIntents.requestFocus();
            tfNIntents.selectAll();
        }if (tfNCaracters.getText().isEmpty()) {
            tfNCaracters.requestFocus();
            tfNCaracters.selectAll();
        }
    }
    public void bNextTabOnAction1(ActionEvent event) {
        Window owner1 = bNextTab2.getScene().getWindow();

        if (!tfAssumpte.getText().isEmpty() && !tfCos.getText().isEmpty() && !tfAdreca.getText().isEmpty() &&
                !tfUsuari.getText().isEmpty() && !tfContrasenya.getText().isEmpty()) {

            tabPane.getSelectionModel().select(tabBBDD);

        }
        else {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner1, "Error",
                    "Omple tots els camps obligatoris!");

            if (tfAssumpte.getText().isEmpty()){
                tfAssumpte.requestFocus();
                tfAssumpte.selectAll();
            }
            if (tfCos.getText().isEmpty()){
                tfCos.requestFocus();
                tfCos.selectAll();
            }
            if (tfAdreca.getText().isEmpty()){
                tfAdreca.requestFocus();
                tfAdreca.selectAll();
            }

            if (tfUsuari.getText().isEmpty()){
                tfUsuari.requestFocus();
                tfUsuari.selectAll();
            }
            if (tfContrasenya.getText().isEmpty()){
                tfContrasenya.requestFocus();
                tfContrasenya.selectAll();
            }
        }
    }

    public void bPreviousTabOnAction(ActionEvent event) {

        tabPane.getSelectionModel().select(tabContrasenya);
    }

    public void bPreviousTabOnAction1(ActionEvent event) {

        tabPane.getSelectionModel().select(tabCorreu);
    }
    // </editor-fold>

    public void btGuardarOnAction(ActionEvent event) throws Exception {

        Window owner = bNextTab1.getScene().getWindow();

        if (!tfAdrecaHost.getText().isEmpty() && !tfPort.getText().isEmpty() && !tfNomBBDD.getText().isEmpty()
                && !tfUsuari2.getText().isEmpty() && !tfContrasenya2.getText().isEmpty()) {

            //Actualitzar informacio a la BBDD
            DAOHelper daoHelper = new DAOHelper(getEmf(),config.getClass());
            config.setPassMinCaracters(Integer.parseInt(tfNCaracters.getText()));
            config.setPassMaxIntents(Integer.parseInt(tfNIntents.getText()));
            config.setEmailAbout(tfAssumpte.getText());
            config.setEmailBody(tfCos.getText());
            config.setEmailSMPT(tfAdreca.getText());
            config.setEmailUser(tfUsuari.getText());


            config.setEmailContrasenya(encriptarContra(tfContrasenya.getText()));
            config.setInfluxdbHostAdress(tfAdrecaHost.getText());
            config.setInfluxdbPort(tfPort.getText());
            config.setInfluxdbNomBBDD(tfNomBBDD.getText());
            config.setInfluxdbUsuari(tfUsuari2.getText());
            config.setInfluxdbContrasenya(encriptarContra(tfContrasenya2.getText()));

            daoHelper.update(config);


            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Guardar");
            alert.setHeaderText("GUARDAR");
            alert.setContentText("Les dades s'han guardat correctament!");

            ButtonType buttonTypeOne = new ButtonType("Aceptar");


            alert.getButtonTypes().setAll(buttonTypeOne);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeOne){
                Stage stage = (Stage) bGuardar.getScene().getWindow();
                stage.close();
            }

        }
        else {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error",
                    "Omple tots els camps obligatoris!");

            if (tfAdrecaHost.getText().isEmpty()){
                tfAdrecaHost.requestFocus();
                tfAdrecaHost.selectAll();
            }
            if (tfPort.getText().isEmpty()){
                tfPort.requestFocus();
                tfPort.selectAll();
            }
            if (tfNomBBDD.getText().isEmpty()){
                tfNomBBDD.requestFocus();
                tfNomBBDD.selectAll();
            }

            if (tfUsuari2.getText().isEmpty()){
                tfUsuari2.requestFocus();
                tfUsuari2.selectAll();
            }
            if (tfContrasenya2.getText().isEmpty()){
                tfContrasenya2.requestFocus();
                tfContrasenya2.selectAll();
            }
        }
    }

    public void bTancarOnAction(ActionEvent event) {
        Stage stage = (Stage) bTancar.getScene().getWindow();
        stage.close();
    }

    public String encriptarContra(String pass) throws Exception {

        EncriptacioSAR.setKey(pass);
        String novapassEncriptada = EncriptacioSAR.encrypt(pass, EncriptacioSAR.secretKey);
        assert novapassEncriptada != null;
        byte[] novapassEncriptadaHash = EncriptacioSAR.computeHash(novapassEncriptada);
        String hexNew = EncriptacioSAR.byteArrayToHexString(novapassEncriptadaHash);

        return hexNew;
    }

}