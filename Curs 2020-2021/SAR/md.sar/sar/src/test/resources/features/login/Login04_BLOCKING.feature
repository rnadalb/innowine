Feature: Ingrés al sistema
  Com usuari del sistema
  Necessito entrar d'una manera segura
  Per poder treballar
  Background:
    El PO ens ha indicat uns criteria d'acceptació per garantir la seguretat de l'ingrès al sistema
    L'usuari introduirà el se login(correu electrònic) i la seva contrasenya
    Desprès polsarà <ENTER> o farà clic al botó SignIn

  Scenario Outline: Entrada al sistema incorrecta per X vegada
    Given Donat avaluador de Login amb x intents fallits
    When usuari introdueix les dades malament per x vegada <usuari> i <passwd>
    Then usuari no accedeix al sistema i es bloquejat
    Examples:
      | usuari     | passwd         |
      | sergiga    | fail           |
      | johntitor  | fail           |
      | admin      | admin          |