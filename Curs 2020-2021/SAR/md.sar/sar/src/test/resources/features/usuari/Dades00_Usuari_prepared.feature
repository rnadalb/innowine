Feature: DadesUsuari
  Com usuari del sistema
  Necessito entrar d'una manera segura
  Per poder treballar i visualitzar les meves dades
  Background:
    El PO ens ha indicat uns criteris d'acceptació per garantir la seguretat de l'ingrès al sistema
    Per la realització dels tests, es necessita preparar la BBDD

  Scenario Outline: Entrada al sistema correcta
    Given Donat usuari de Login
    When usuari fa login i preparem les seves dades <usuari> i <passwd> <nom> <cognom> <email> <telefon> <adreca> <postalCode> <poblacio> <pais>
    Then usuari accedeix a personalitzacio de dades amb les seves dades desde zero
    Examples:
      | usuari                      |  passwd  ||    nom   ||    cognom   ||        email        ||   telefon   ||   adreca   ||   postalCode   ||   poblacio    ||   pais   |
      | OnlyCanBeTouchedByTestos    |   test   ||    nom   ||    cognom   ||    email@gmail.com  || 7436593485  ||   adress   ||   postalCode   ||   poblacio    ||   pais   |