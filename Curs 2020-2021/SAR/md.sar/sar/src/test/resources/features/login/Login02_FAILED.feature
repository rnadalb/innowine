Feature: Ingrés al sistema
  Com usuari del sistema
  Necessito entrar d'una manera segura
  Per poder treballar
  Background:
    El PO ens ha indicat uns criteria d'acceptació per garantir la seguretat de l'ingrès al sistema
    L'usuari introduirà el se login(correu electrònic) i la seva contrasenya
    Desprès polsarà <ENTER> o farà clic al botó SignIn

  Scenario Outline: Entrada al sistema incorrecta
    Given Donat avaluador de Login preparat
    When usuari introdueix les dades incorrectament <usuari> i <passwd>
    Then usuari no accedeix al sistema
    Examples:
      | usuari       | passwd |
      | sergiga      | fail   |
      | johntitor    | fail   |