Feature: Preparar escenari de errada a l'ingrès
  Com programador del sistema
  Necessito poder ajustar la BBDD
  Per poder executar el test de login fallit correctament
  Background:
    Cal actualitzar el nombre d'intents a X-1

  Scenario: Preparar la BBDD (Login.FAILED)
    Given Una Base de dades
    When les dades no estan preparades per al seguent test
    Then el devteam actualitza la bbdd
