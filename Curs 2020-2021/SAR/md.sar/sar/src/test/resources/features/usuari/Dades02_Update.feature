Feature: DadesUsuari
  Com usuari del sistema
  Necessito entrar d'una manera segura
  Per poder treballar i visualitzar les meves dades
  Background:
    El PO ens ha indicat uns criteris d'acceptació per garantir la seguretat de l'ingrès al sistema
    L'usuari introduirà el seu login(nom d'usuari) i la seva contrasenya
    Desprès polsarà <ENTER> o farà clic al botó SignIn, tot seguit anirà al menú i clicarà en personalització on podrà veure les seves dades.
    Per actualitzar les dades, clicarà editar, odificarà les dades i donarà a guardar.

  Scenario Outline: Entrada al sistema correcta
    Given Donat usuari de Login i visualitza les seves dades
    When usuari clica guardar  <usuari> i <passwd> <nom> <cognom> <email> <telefon> <adreca> <postalCode> <poblacio> <pais>
    Then les dades queden modificades a la BBDD
    Examples:
      | usuari                      |  passwd   ||    nom   ||    cognom   ||        email        ||   telefon   ||   adreca   ||   postalCode   ||   poblacio    ||   pais   |
      | OnlyCanBeTouchedByTestos    |   test    ||updateName||updateSurname||updateEmail@gmail.com||   875655    ||updateAdress||   updaPCode    ||updatePoblation||updatePais|