Feature: DadesUsuari
  Com usuari del sistema
  Necessito entrar d'una manera segura
  Per poder treballar i visualitzar les meves dades
  Background:
    El PO ens ha indicat uns criteris d'acceptació per garantir la seguretat de l'ingrès al sistema
    L'usuari introduirà el seu login(nom d'usuari) i la seva contrasenya
    Desprès polsarà <ENTER> o farà clic al botó SignIn, tot seguit anirà al menú i clicarà en personalització on podrà veure les seves dades

  Scenario Outline: Entrada al sistema correcta
    Given Donat usuari de Login Administrador
    When usuari fa login i clica a personalitzacio de dades <usuari> i <passwd> <nom> <cognom> <email> <telefon> <adreca> <postalCode> <poblacio> <pais>
    Then usuari accedeix a personalitzacio de dades amb les seves dades
    Examples:
      | usuari                      | passwd  ||    nom   ||    cognom   ||        email        ||   telefon   ||   adreca   ||   postalCode   ||   poblacio    ||   pais   |
      | OnlyCanBeTouchedByTestos    |  test   ||    nom   ||    cognom   ||    email@gmail.com  || 7436593485  ||   adress   ||   postalCode   ||   poblacio    ||   pais   |