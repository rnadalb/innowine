Feature: Preparar escenari d'exit a l'ingrès
  Com programador del sistema
  Necessito poder ajustar la BBDD
  Per poder executar el test de login exitós correctament
  Background:
  Cal actualitzar el nombre d'intents a 0 i el desbloquejar els usuaris

  Scenario: Preparar la BBDD (Login.OK)
    Given Una Base de dades sense tractar
    When les dades no estan preparades per al test
    Then el devteam desbloqueja tots els usuaris a la bbdd