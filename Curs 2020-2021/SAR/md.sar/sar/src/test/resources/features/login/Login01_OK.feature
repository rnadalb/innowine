Feature: Ingrés al sistema
  Com usuari del sistema
  Necessito entrar d'una manera segura
  Per poder treballar
  Background:
    El PO ens ha indicat uns criteria d'acceptació per garantir la seguretat de l'ingrès al sistema
    L'usuari introduirà el se login(nom d'usuari) i la seva contrasenya
    Desprès polsarà <ENTER> o farà clic al botó SignIn

  Scenario Outline: Entrada al sistema correcta
    Given Donat avaluador de Login
    When usuari introdueix les dades correctament <usuari> i <passwd>
    Then usuari accedeix al sistema
    Examples:
      | usuari     | passwd         |
      | sergiga    | Covid20        |
      | johntitor  | steinsgate20   |