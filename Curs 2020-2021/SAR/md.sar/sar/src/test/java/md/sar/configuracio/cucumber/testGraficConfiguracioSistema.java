package md.sar.configuracio.cucumber;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Modality;
import javafx.stage.Stage;
import md.sar.classes.AppPropertiesHelper;
import md.sar.classes.DefaultControllerUser;
import md.sar.classes.ModeStates;
import md.sar.classes.UsuariHelper;
import md.sar.controladors.ConfiguracioSistemaControlador;
import md.sar.exceptions.PropertiesHelperException;
import md.sar.models.ConfiguracioSistema;
import md.sar.models.Usuari;
import org.apache.commons.configuration.ConfigurationException;
import org.junit.jupiter.api.*;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.matcher.base.NodeMatchers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.testfx.api.FxAssert.verifyThat;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class testGraficConfiguracioSistema extends ApplicationTest {

    private EntityManagerFactory emf;
    private ConfiguracioSistema config;

    /* Per obtenir els controls de la GUI */
    private <T extends Node> T find(final String query) {
        return lookup(query).query();
    }


    Button bGuardar,bPreviousTab2;
    Button bTancar;
    Button bNextTab1;
    Button bNextTab2;
    Button bPreviousTab1;
    TabPane tabPane;
    Node tabContrasenya;
    Node tabCorreu;
    Node tabBBDD;
    TextField tfNCaracters, tfNIntents, tfAssumpte,
            tfAdreca, tfUsuari, tfAdrecaHost, tfPort, tfNomBBDD, tfUsuari2, tfContrasenya2;
    TextArea tfCos;
    PasswordField tfContrasenya;


    //obra la vista de Configuracio de sistema i la posa davant de tot
    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/vistes/ConfiguracioSistema.fxml"));
            Parent root1 = fxmlLoader.load();
            DefaultControllerUser controlador = fxmlLoader.getController();
            controlador.setEmf(emf);
            UsuariHelper<Usuari> helper = new UsuariHelper<>(emf, Usuari.class);
            controlador.setUser(helper.getUsuariByLogin("admin"));
            controlador.setConfig(config);
            controlador.setMode(ModeStates.CONFIGURACIO);
            controlador.obtenirDades();
            controlador.iniciDiferit();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Usuari - admin");
            stage.setScene(new Scene(root1));
            stage.show();
            stage.toFront();
        } catch (IOException e) {
            //Missatge d'error al no poder obrir correctament la Stage
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }

    }

    @Override
    public void init() throws Exception {
        super.init();
        String passwd = getDecryptedPropsFilePassword();
        Map<String, String> mapprop = new HashMap<>();
        mapprop.put("javax.persistence.jdbc.password", passwd);
        try {
            emf = Persistence.createEntityManagerFactory("mariaDBConnection", mapprop);
            EntityManager em = emf.createEntityManager();
            config = new ConfiguracioSistema();
            config = em.find(config.getClass(), 1);
            em.close();
        } catch (Exception ignored) {

        }
    }

    //Comprova que no hi hagi hagut cap canvi de nom en cap variable
    @BeforeEach
    public void setUp() throws Exception {
        System.out.println("Mètode setUp() ...");
        // RNB. more help --> https://github.com/TestFX/TestFX/wiki/Queries
        //Buttons
        bGuardar = find("#bGuardar");
        bTancar = find("#bTancar");
        bNextTab1 = find("#bNextTab1");
        bNextTab2 = find("#bNextTab2");
        bPreviousTab1 = find("#bPreviousTab1");

        //Tabs
        tabPane = find("#tabPane");
        tabContrasenya = find("#tabContrasenya");
        tabCorreu = find("#tabCorreu");
        tabBBDD = find("#tabBBDD");
        //TextFields
        tfNCaracters = find("#tfNCaracters");
        tfNIntents = find("#tfNIntents");
        tfAssumpte = find("#tfAssumpte");
        tfAdreca = find("#tfAdreca");
        tfUsuari = find("#tfUsuari");
        tfAdrecaHost = find("#tfAdrecaHost");
        tfPort = find("#tfPort");
        tfNomBBDD = find("#tfNomBBDD");
        tfUsuari2 = find("#tfUsuari2");
        tfContrasenya2 = find("#tfContrasenya2");
        //TextArea
        tfCos = find("#tfCos");
        //PasswordField
        tfContrasenya = find("#tfContrasenya");
        bPreviousTab2 = find("#bPreviousTab2");
    }

    @AfterEach
    public void tearDown() throws Exception {
        System.out.println("Mètode tearDown() ...");
        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    @AfterAll
    public void tearDownClass() {
        System.out.println("Mètode tearDown() ...");
        emf.close();
    }

    private static String getDecryptedPropsFilePassword() throws PropertiesHelperException, ConfigurationException {
        AppPropertiesHelper helper = new AppPropertiesHelper("app.properties", "password", "enc");
        return helper.getDecryptedUserPassword();
    }

    @Test
    @Order(1)
    @DisplayName("Comprovacio de camps")
    public void comprovacioCamps() {
        Assertions.assertFalse(bGuardar.isDisabled());
        Assertions.assertFalse(bTancar.isDisabled());
        Assertions.assertFalse(bNextTab1.isDisabled());
        Assertions.assertFalse(bNextTab2.isDisabled());
        Assertions.assertFalse(bPreviousTab1.isDisabled());
        Assertions.assertFalse(tabPane.isDisabled());
        Assertions.assertFalse(tabContrasenya.isDisabled());
        Assertions.assertFalse(tabCorreu.isDisabled());
        Assertions.assertFalse(tabBBDD.isDisabled());
        Assertions.assertTrue(tfNCaracters.isEditable());
        Assertions.assertTrue(tfNIntents.isEditable());
        Assertions.assertTrue(tfAssumpte.isEditable());
        Assertions.assertTrue(tfAdreca.isEditable());
        Assertions.assertTrue(tfUsuari.isEditable());
        Assertions.assertTrue(tfAdrecaHost.isEditable());
        Assertions.assertTrue(tfPort.isEditable());
        Assertions.assertTrue(tfNomBBDD.isEditable());
        Assertions.assertTrue(tfUsuari2.isEditable());
        Assertions.assertTrue(tfContrasenya2.isEditable());
        Assertions.assertTrue(tfCos.isEditable());
        Assertions.assertTrue(tfContrasenya.isEditable());

    }

    @Test
    @Order(2)
    @DisplayName("Omplir camps del Tab contrasenya correctes")
    public void omplirCampsContrasenyaCorrectes() {

        //Omplir camps correctes
        tfNCaracters.clear();
        clickOn(tfNCaracters);
        write("6");
        tfNIntents.clear();
        clickOn(tfNIntents);
        write("4");
        clickOn(bNextTab1);
    }

    @Test
    @Order(3)
    @DisplayName("Omplir camps del Tab contrasenya no valids")
    public void omplirCampsContrasenyaIncorrectes() {
        //Omplir camps incorrectes
        tfNCaracters.clear();
        clickOn(tfNCaracters);
        write("4");
        tfNIntents.clear();
        clickOn(tfNIntents);
        write("3");
        clickOn(bNextTab1);
        verifyThat("Aceptar", NodeMatchers.isVisible());
    }

    @Test
    @Order(4)
    @DisplayName("Omplir camps del Tab contrasenya incorrectes amb lletres")
    public void omplirCampsContrasenyaIncorrectesL() {
        //Omplir camps incorrectes
        tfNCaracters.clear();
        clickOn(tfNCaracters);
        write("a");
        tfNIntents.clear();
        clickOn(tfNIntents);
        write("b");
        clickOn(bNextTab1);
        verifyThat("Aceptar", NodeMatchers.isVisible());
    }

    @Test
    @Order(5)
    @DisplayName("Camps del Tab contrasenya buits")
    public void campsContrasenyaBuits() {
        //Omplir camps incorrectes
        tfNCaracters.clear();
        clickOn(tfNCaracters);
        write("");
        tfNIntents.clear();
        clickOn(tfNIntents);
        write("");
        clickOn(bNextTab1);
        verifyThat("Aceptar", NodeMatchers.isVisible());
    }

    @Test
    @Order(6)
    @DisplayName("Omplir camps Correu correctament")
    public void omplirCampsCorreuCorrectament() {
        clickOn(tabCorreu);
        tfAssumpte.clear();
        clickOn(tfAssumpte);
        write("BLOQUEIG");
        tfCos.clear();
        clickOn(tfCos);
        write("Bloquejat per superar el numero d'intents");
        tfAdreca.clear();
        clickOn(tfAdreca);
        write("8080");
        tfUsuari.clear();
        clickOn(tfUsuari);
        write("raimon");
        tfContrasenya.clear();
        clickOn(tfContrasenya);
        write("locamontura");
        clickOn(bNextTab2);
    }

    @Test
    @Order(7)
    @DisplayName("camps Correu buits")
    public void campsCorreubuits() {
        clickOn(tabCorreu);
        tfAssumpte.clear();
        clickOn(tfAssumpte);
        write("");
        tfCos.clear();
        clickOn(tfCos);
        write("");
        tfAdreca.clear();
        clickOn(tfAdreca);
        write("");
        tfUsuari.clear();
        clickOn(tfUsuari);
        write("");
        tfContrasenya.clear();
        clickOn(tfContrasenya);
        write("");
        clickOn(bNextTab2);
        verifyThat("Aceptar", NodeMatchers.isVisible());
    }

    @Test
    @Order(8)
    @DisplayName("Boto retroces")
    public void botoRetroces(){
        clickOn(tabCorreu);
        clickOn(bPreviousTab1);

    }
    @Test
    @Order(9)
    @DisplayName("Comprovar camp adreca host buit")
    public void checkCampsInBlankAdress() {
        clickOn(tabBBDD);
        tfAdrecaHost.clear();
        clickOn(tfAdrecaHost);
        write("");
        clickOn(bGuardar);
        verifyThat("Aceptar", NodeMatchers.isVisible());
    }
    @Test
    @Order(10)
    @DisplayName("Comprovar camp port buit")
    public void checkCampsInBlankPort() {
        clickOn(tabBBDD);
        tfPort.clear();
        clickOn(tfPort);
        write("");
        clickOn(bGuardar);
        verifyThat("Aceptar", NodeMatchers.isVisible());
    }
    @Test
    @Order(11)
    @DisplayName("Comprovar camp nom BBDD buit")
    public void checkCampsInBlankBBDD() {
        clickOn(tabBBDD);
        tfNomBBDD.clear();
        clickOn(tfNomBBDD);
        write("");
        clickOn(bGuardar);
        verifyThat("Aceptar", NodeMatchers.isVisible());
    }
    @Test
    @Order(12)
    @DisplayName("Comprovar camp usuari buit")
    public void checkCampsInBlankUser() {
        clickOn(tabBBDD);
        tfUsuari2.clear();
        clickOn(tfUsuari2);
        write("");
        clickOn(bGuardar);
        verifyThat("Aceptar", NodeMatchers.isVisible());
    }
    @Test
    @Order(13)
    @DisplayName("Comprovar camp password buit")
    public void checkCampsInBlankPassword() {
        clickOn(tabBBDD);
        tfContrasenya2.clear();
        clickOn(tfContrasenya2);
        write("");
        clickOn(bGuardar);
        verifyThat("Aceptar", NodeMatchers.isVisible());
    }

    @Test
    @Order(14)
    @DisplayName("Comprovar camp port correcte")
    public void checkCampPort() {
        clickOn(tabBBDD);
        tfPort.clear();
        clickOn(tfPort);
        write("9090");
        clickOn(bGuardar);
        clickOn("Aceptar");
        //verifyThat("Aceptar", NodeMatchers.isVisible());
    }
    @Test
    @Order(15)
    @DisplayName("Comprovar camp nom BBDD correcte")
    public void checkCampBBDD() {
        clickOn(tabBBDD);
        tfNomBBDD.clear();
        clickOn(tfNomBBDD);
        write("clientbbdd");
        clickOn(bGuardar);
        clickOn("Aceptar");
        //verifyThat("Aceptar", NodeMatchers.isVisible());
    }
    @Test
    @Order(16)
    @DisplayName("Comprovar camp usuari correcte")
    public void checkCampUser() {
        clickOn(tabBBDD);
        tfUsuari2.clear();
        clickOn(tfUsuari2);
        write("ruben");
        clickOn(bGuardar);
        clickOn("Aceptar");
        //verifyThat("Aceptar", NodeMatchers.isVisible());
    }
    @Test
    @Order(17)
    @DisplayName("Comprovar camp password correcte")
    public void checkCampPassword() {
        clickOn(tabBBDD);
        tfContrasenya2.clear();
        clickOn(tfContrasenya2);
        write("gruposar");
        clickOn(bGuardar);
        clickOn("Aceptar");
        //verifyThat("Aceptar", NodeMatchers.isVisible());

    }
}
