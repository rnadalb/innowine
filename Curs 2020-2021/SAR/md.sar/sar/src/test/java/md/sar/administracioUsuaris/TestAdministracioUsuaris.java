package md.sar.administracioUsuaris;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import md.sar.classes.AppPropertiesHelper;
import md.sar.classes.DefaultControllerUser;
import md.sar.classes.ModeStates;
import md.sar.classes.UsuariHelper;
import md.sar.exceptions.PropertiesHelperException;
import md.sar.models.ConfiguracioSistema;
import md.sar.models.Usuari;
import org.apache.commons.configuration.ConfigurationException;
import org.junit.jupiter.api.*;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.base.NodeMatchers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.testfx.api.FxAssert.verifyThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestAdministracioUsuaris extends ApplicationTest {
    private EntityManagerFactory emf;
    private ConfiguracioSistema config;

    @Start
    public void start(Stage primaryStage) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/vistes/AdministracioUsuaris.fxml"));

            Parent root1 = fxmlLoader.load();
            DefaultControllerUser controlador = fxmlLoader.getController();
            controlador.setEmf(emf);
            UsuariHelper<Usuari> helper = new UsuariHelper<>(emf, Usuari.class);
            controlador.setUser(helper.getUsuariByLogin("admin"));
            controlador.setConfig(config);
            controlador.setMode(ModeStates.ADMIN_USUARIS);
            controlador.obtenirDades();
            controlador.iniciDiferit();
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Usuari - admin");
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (IOException e) {
            //Missatge d'error al no poder obrir correctament la Stage
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }

    }


    @Override
    public void init() throws Exception {
        super.init();
        String passwd = getDecryptedPropsFilePassword();
        Map<String, String> mapprop = new HashMap<>();
        mapprop.put("javax.persistence.jdbc.password", passwd);
        try {
            emf = Persistence.createEntityManagerFactory("mariaDBConnection", mapprop);
            EntityManager em = emf.createEntityManager();
            config = new ConfiguracioSistema();
            config = em.find(config.getClass(), 1);
            em.close();
        } catch (Exception ignored) {

        }
    }

    private static String getDecryptedPropsFilePassword() throws PropertiesHelperException, ConfigurationException {
        AppPropertiesHelper helper = new AppPropertiesHelper("app.properties", "password", "enc");
        return helper.getDecryptedUserPassword();
    }

    @AfterEach
    public void tearDown() throws Exception {
        System.out.println("Mètode tearDown() ...");
        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    @AfterAll
    public void tearDownClass() {
        System.out.println("Mètode tearDown() ...");
        emf.close();
    }

    TextField tfBuscar;


    // .............................. Test ......................................
    @BeforeEach
    public void login() {
        tfBuscar = find("#tfBuscar");
    }

    @Test
    @Order(1)
    @DisplayName("Comprovacio de camps")
    public void comprovacioCamps() {
        Assertions.assertTrue(tfBuscar.isEditable());

    }
    @Test
    @Order(2)
    @DisplayName("Comprovacio obeetura correcte personalitzacio de dades en doble click")
    public void comprovacioPersonalizacioDades() {
        doubleClickOn("OnlyCanBeTouchedByTestos");
        TextField tfpassword,tfusuari;
        tfpassword=find("#tfpassword");
        tfusuari=find("#tfusuari");
        Assertions.assertTrue(tfpassword.isVisible());
        Assertions.assertEquals(tfusuari.getText(),"OnlyCanBeTouchedByTestos");



    }
    @Test
    @Order(3)
    @DisplayName("Comprovacio obeetura correcte personalitzacio de dades en click dret")
    public void comprovacioPersonalizacioDadesClickDret() {
        rightClickOn("OnlyCanBeTouchedByTestos");
        clickOn("Personalitzacio de Dades");
        TextField tfpassword,tfusuari;
        tfpassword=find("#tfpassword");
        tfusuari=find("#tfusuari");
        Assertions.assertTrue(tfpassword.isVisible());
        Assertions.assertEquals(tfusuari.getText(),"OnlyCanBeTouchedByTestos");
    }
    @Test
    @Order(4)
    @DisplayName("Comprovacio obetura correcte canvi contrasenya")
    public void comprovacioCanviContra() {
        rightClickOn("OnlyCanBeTouchedByTestos");
        clickOn("Canvia contrasenya");
        TextField TFactualPass;
        TFactualPass=find("#TFactualPass");
        Assertions.assertFalse(TFactualPass.isVisible());
    }
    @Test
    @Order(5)
    @DisplayName("Comprovacio bloqueig usuari")
    public void comprovacioBlockUser() {
        rightClickOn("OnlyCanBeTouchedByTestos");
        clickOn("Bloqueja");
        verifyThat("Usuari OnlyCanBeTouchedByTestos bloquejat", NodeMatchers.isVisible());

    }
    @Test
    @Order(6)
    @DisplayName("Comprovacio no poder bloquejar usuari bloquejat")
    public void comprovacioDuplicateBloqued() {
        rightClickOn("OnlyCanBeTouchedByTestos");
        clickOn("Bloqueja");
        verifyThat("L'usuari OnlyCanBeTouchedByTestos ja esta bloquejat o es administrador", NodeMatchers.isVisible());

    }
    @Test
    @Order(7)
    @DisplayName("Comprovacio desbloquejar usuari")
    public void comprovacioUnblockUser() {
        rightClickOn("OnlyCanBeTouchedByTestos");
        clickOn("Desbloqueja");
        verifyThat("Usuari OnlyCanBeTouchedByTestos desbloquejat", NodeMatchers.isVisible());

    }
    @Test
    @Order(8)
    @DisplayName("Comprovacio desbloquejar usuari desbloquejat")
    public void comprovacioDuplicatedUnblockUser() {
        rightClickOn("OnlyCanBeTouchedByTestos");
        clickOn("Desbloqueja");
        verifyThat("L'usuari OnlyCanBeTouchedByTestos ja esta desbloquejat", NodeMatchers.isVisible());

    }
/*    @Test
    @Order(9)
    @DisplayName("Comprovacio enviar contrasenya")
    public void comprovacioSendPassword() {
        rightClickOn("OnlyCanBeTouchedByTestos");
        clickOn("Enviar Contrasenya");
        //verifyThat("L'usuari OnlyCanBeTouchedByTestos ja esta desbloquejat", NodeMatchers.isVisible());

    }*/
    @Test
    @Order(10)
    @DisplayName("Comprovacio buscador")
    public void comprovacioSendPassword() {
        clickOn(tfBuscar);
        write("OnlyCanBeTouchedByTestos");
        verifyThat("OnlyCanBeTouchedByTestos", NodeMatchers.isVisible());
    }

    private <T extends Node> T find(final String query) {
        return lookup(query).query();
    }

}
