package md.sar.usuari.cucumber;


import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import md.sar.classes.DadesStates;
import md.sar.classes.UtilsDB;
import org.junit.jupiter.api.Assertions;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DadesCheck {
    // Necessari perquè treballem en BBDD
    private static EntityManagerFactory emf;
    private static UtilsDB utilsDB;

    // Ens indicarà el resultat de la funció a testejar
    private DadesStates result;

    @Before
    public void initDB() {
        emf = Persistence.createEntityManagerFactory("mariaDBConnectionTest");
    }

    @After
    public void closeDB(Scenario scenario){
        emf.close();
    }

    @Given("Donat usuari de Login Administrador")
    public void donatUsuariDeLoginAdministrador() {
        utilsDB = new UtilsDB(emf);
    }

    @When("usuari fa login i clica a personalitzacio de dades (.+) i (.+) (.+) (.+) (.+) (.+) (.+) (.+) (.+) (.+)")
    public void usuariFaLoginIClicaAPersonalitzacioDeDadesUsuariIPasswd(String usuari, String passwd, String nom, String cognom, String email, String telefon, String adreca, String postalCode, String poblacio, String pais) throws Exception {
        result = utilsDB.checkDades(usuari, passwd, nom, cognom, email, telefon, adreca, postalCode, poblacio, pais);
    }

    @Then("usuari accedeix a personalitzacio de dades amb les seves dades")
    public void usuariAccedeixAPersonalitzacioDeDadesAmbLesSevesDades() {
        DadesStates expected = DadesStates.CORRECTES;
        Assertions.assertEquals(expected, result, "utilsDB.dadesCheck() falla");
    }


}
