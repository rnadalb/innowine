package md.sar.login.cucumber;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import md.sar.classes.LoginStates;
import md.sar.classes.UtilsDB;
import org.junit.jupiter.api.Assertions;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class LoginStepsBlocking {
    // Necessari perquè treballem en BBDD
    private static EntityManagerFactory emf;
    private static UtilsDB utilsDB;

    // Ens indicarà el resultat de la funció a testejar
    private LoginStates result;

    @Before
    public void initDB() {
        emf = Persistence.createEntityManagerFactory( "mariaDBConnectionTest");
    }

    @After
    public void closeDB(Scenario scenario){
        emf.close();
    }

    @Given ("^Donat avaluador de Login amb x intents fallits$")
    public void donatAvaluadorDeLoginAmbIntentsFallits() {
        utilsDB = new UtilsDB(emf);
    }

    @When ("^usuari introdueix les dades malament per x vegada (.+) i (.+)$")
    public void usuariIntrodueixLesDadesMalamentPerTerceraVegadaUsuariIPasswd(String usuari, String passwd) throws Exception {
        result = utilsDB.checkPassword(usuari, passwd);
    }

    @Then ("^usuari no accedeix al sistema i es bloquejat")
    public void usuariNoAccedeixAlSistemaIEsBloquejat() {
        LoginStates expected = LoginStates.FAILED;
        Assertions.assertEquals(expected, result, "utilsDB.checkPassword() falla");
    }
}
