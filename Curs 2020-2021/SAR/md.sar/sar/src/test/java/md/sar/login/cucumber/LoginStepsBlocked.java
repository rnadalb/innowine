package md.sar.login.cucumber;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import md.sar.classes.LoginStates;
import md.sar.classes.UtilsDB;
import org.junit.jupiter.api.Assertions;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class LoginStepsBlocked {
    // Necessari perquè treballem en BBDD
    private static EntityManagerFactory emf;
    private static UtilsDB utilsDB;

    // Ens indicarà el resultat de la funció a testejar
    private LoginStates result;

    @Before
    public void initDB() {
        emf = Persistence.createEntityManagerFactory( "mariaDBConnectionTest");
    }

    @After
    public void closeDB(Scenario scenario){
        emf.close();
    }

    @Given ("^Donat avaluador de Login despres de bloquejar els usuaris$")
    public void donatAvaluadorDeLoginAmbUsuarisBloquejats() {
        utilsDB = new UtilsDB(emf);
    }

    @When ("^usuari bloquejat introdueix les dades (.+) i (.+)$")
    public void usuariIntrodueixLesDadesPeroEstaBloquejat(String usuari, String passwd) throws Exception {
        result = utilsDB.checkPassword(usuari, passwd);
    }
    @Then ("^usuari esta bloquejat")
    public void usuariNoAccedeixAlSistemaPerqueEstaBloquejat() {
        LoginStates expected = LoginStates.BLOCKED;
        Assertions.assertEquals(expected, result, "utilsDB.checkPassword() falla");
    }
}
