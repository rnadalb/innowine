package md.sar.canvicontra.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * Carregarà les històries i les passarà a Junit per ser executades
 */
@RunWith (Cucumber.class)
@CucumberOptions (plugin = "pretty", monochrome =  false, strict = false, // monochrome = true -> colorins
        //features = "src/test/resources/features",
        features = {"src/test/resources/features/Dades01_Ok.feature"},
        glue = "md.sar.canvicontra.cucumber") // package d'on estan els steps

//@CucumberOptions (plugin = "pretty", monochrome =  false, strict = true,
//        features = {"src/test/resources/features/Dades01_Ok.feature",
//                    "src/test/resources/features/Login03_Prepara.feature",
//                    "src/test/resources/features/Login02_FAILED.feature"},
//        glue = "cat.dam2.m15.tdd.login.cucumber") // package d'on estan els steps
public class CucumberRunner {

}