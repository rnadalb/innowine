package md.sar.login.cucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import md.sar.classes.UtilsDB;
import md.sar.models.ConfiguracioSistema;
import md.sar.models.Usuari;
import org.junit.jupiter.api.Assertions;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.List;

public class LoginPrepareOkScenario {
    // Necessari perquè treballem en BBDD
    private static EntityManagerFactory emf;
    private static UtilsDB utilsDB;

    @Given("^Una Base de dades sense tractar$")
    public void unaBaseDeDades() {
        emf = Persistence.createEntityManagerFactory( "mariaDBConnectionTest");
    }

    @When("^les dades no estan preparades per al test$")
    public void lesDadesNoEstanPreparadesPerAlSeguentTest() {
        // Equivalent a SELECT * FROM tbl_usuari WHERE blocked = true
        CriteriaBuilder cb = emf.getCriteriaBuilder();
        EntityManager manager = emf.createEntityManager();

        CriteriaQuery<Usuari> cbQuery = cb.createQuery(Usuari.class);
        Root<Usuari> c = cbQuery.from(Usuari.class);
        cbQuery.select(c);
        cbQuery.where(cb.equal(c.get("blocked"), true));

        Query query = manager.createQuery(cbQuery);

        List<Usuari> llista = query.getResultList();

        int esperat = 0;
        int actual = llista.size();

        if (actual == 0)
            Assertions.assertEquals(esperat, actual, "Tots els usuaris están desbloquejats");
    }

    @Then("^el devteam desbloqueja tots els usuaris a la bbdd$")
    public void elDevteamActualitzaLaBbdd() {
        // Equivalent a  UPDATE tbl_usuari set bloquejar = false and intents = 0
        CriteriaBuilder cb = emf.getCriteriaBuilder();
        CriteriaUpdate<Usuari> update = cb.createCriteriaUpdate(Usuari.class);
        Root<Usuari> p = update.from(Usuari.class);
        update.set("blocked", false);
        update.set("intents", 0);
        EntityManager manager = emf.createEntityManager();
        manager.getTransaction().begin();
        manager.createQuery(update).executeUpdate();
        manager.getTransaction().commit();
    }
}