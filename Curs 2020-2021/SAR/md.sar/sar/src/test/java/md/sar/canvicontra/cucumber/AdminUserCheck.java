package md.sar.canvicontra.cucumber;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import md.sar.classes.UtilsDB;
import md.sar.models.ConfiguracioSistema;
import md.sar.models.Usuari;
import org.junit.jupiter.api.Assertions;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.List;

public class AdminUserCheck {
    // Necessari perquè treballem en BBDD
    private static EntityManagerFactory emf;
    private static UtilsDB utilsDB;

    @Given ("^Una Base de dades$")
    public void unaBaseDeDades() {
        emf = Persistence.createEntityManagerFactory( "mariaDBConnectionTest");
    }

    @When ("^les dades no estan preparades per al seguent test$")
    public void lesDadesNoEstanPreparadesPerAlSeguentTest() {
    }

    @Then ("^el devteam actualitza la bbdd$")
    public void elDevteamActualitzaLaBbdd() {
        ConfiguracioSistema config = new ConfiguracioSistema();
        EntityManager em = emf.createEntityManager();
        try{
            config = em.find(config.getClass(),1);
        }finally{
            em.close();
        }
        // Equivalent a  UPDATE tbl_usuari set bloquejar = :bloquejat and intents = :intents
        CriteriaBuilder cb = emf.getCriteriaBuilder();
        CriteriaUpdate<Usuari> update = cb.createCriteriaUpdate(Usuari.class);
        Root<Usuari> p = update.from(Usuari.class);
        update.set("blocked", false);
        update.set("intents", config.getPassMaxIntents()-1);
        EntityManager manager = emf.createEntityManager();
        manager.getTransaction().begin();
        manager.createQuery(update).executeUpdate();
        manager.getTransaction().commit();
    }
}
