package md.sar.usuari.cucumber;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import md.sar.classes.DadesStates;
import md.sar.classes.UtilsDB;
import org.junit.jupiter.api.Assertions;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DadesUpdate {
    // Necessari perquè treballem en BBDD
    private static EntityManagerFactory emf;
    private static UtilsDB utilsDB;

    // Ens indicarà el resultat de la funció a testejar
    private DadesStates result;

    @Before
    public void initDB() {
        emf = Persistence.createEntityManagerFactory("mariaDBConnectionTest");
    }
    @Given("Donat usuari de Login i visualitza les seves dades")
    public void donatUsuariDeLoginIVisualitzaLesSevesDades() {
        utilsDB = new UtilsDB(emf);
    }

    @When("usuari clica guardar  (.+) i (.+) (.+) (.+) (.+) (.+) (.+) (.+) (.+) (.+)")
    public void usuariClicaGuardarUsuariIPasswd(String usuari, String passwd, String nom, String cognom, String email, String telefon, String adreca, String postalCode, String poblacio, String pais ) throws Exception {
       result = utilsDB.updateDades(usuari, passwd, nom, cognom, email, telefon, adreca, postalCode, poblacio, pais);

    }

    @Then("les dades queden modificades a la BBDD")
    public void lesDadesQuedenModificadesALaBBDD() {
        DadesStates expected = DadesStates.CORRECTES;
        Assertions.assertEquals(expected, result, "utilsDB.updateDades() falla");

    }
}
