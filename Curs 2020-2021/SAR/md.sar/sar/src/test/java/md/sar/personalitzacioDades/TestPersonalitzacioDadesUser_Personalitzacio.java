package md.sar.personalitzacioDades;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import md.sar.classes.AppPropertiesHelper;
import md.sar.classes.DefaultControllerUser;
import md.sar.classes.ModeStates;
import md.sar.classes.UsuariHelper;
import md.sar.exceptions.PropertiesHelperException;
import md.sar.models.ConfiguracioSistema;
import md.sar.models.Usuari;
import org.apache.commons.configuration.ConfigurationException;
import org.junit.jupiter.api.*;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.framework.junit5.Start;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestPersonalitzacioDadesUser_Personalitzacio extends ApplicationTest {
    private EntityManagerFactory emf;
    private ConfiguracioSistema config;

    @Start
    public void start(Stage primaryStage) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/vistes/Usuari.fxml"));

            Parent root1 = fxmlLoader.load();
            DefaultControllerUser controlador = fxmlLoader.getController();
            controlador.setEmf(emf);
            UsuariHelper<Usuari> helper = new UsuariHelper<>(emf, Usuari.class);
            controlador.setUser(helper.getUsuariByLogin("OnlyCanBeTouchedByTestos"));
            controlador.setConfig(config);
            controlador.setMode(ModeStates.PERSONALITZACIO_DADES);
            controlador.obtenirDades();
            controlador.iniciDiferit();
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Usuari - admin");
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (IOException e) {
            //Missatge d'error al no poder obrir correctament la Stage
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }

    }


    @Override
    public void init() throws Exception {
        super.init();
        String passwd = getDecryptedPropsFilePassword();
        Map<String, String> mapprop = new HashMap<>();
        mapprop.put("javax.persistence.jdbc.password", passwd);
        try {
            emf = Persistence.createEntityManagerFactory("mariaDBConnection", mapprop);
            EntityManager em = emf.createEntityManager();
            config = new ConfiguracioSistema();
            config = em.find(config.getClass(), 1);
            em.close();
        } catch (Exception ignored) {

        }
    }

    private static String getDecryptedPropsFilePassword() throws PropertiesHelperException, ConfigurationException {
        AppPropertiesHelper helper = new AppPropertiesHelper("app.properties", "password", "enc");
        return helper.getDecryptedUserPassword();
    }

    @AfterEach
    public void tearDown() throws Exception {
        System.out.println("Mètode tearDown() ...");
        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    @AfterAll
    public void tearDownClass() {
        System.out.println("Mètode tearDown() ...");
        emf.close();
    }

    Button bguardar, beditar,bcanviar;
    TextField tfusuari, tfnom, tfcognom, tfcorreu, tftelefon, tfadreca, tfcodipostal, tfpoblacio, tfpais, TFactualPass,TFnewPass,TFconfirmPass;
    CheckBox chkbBloquejat;
    Text lobligatorinom, lobligatoricognom, lobligatoricorreu, lphonerror;
    ImageView imgCandau;
    Label lmissatgeexit;


    // .............................. Test ......................................
    @BeforeEach
    public void login() {
        bguardar = find("#bguardar");
        beditar = find("#beditar");
        tfusuari = find("#tfusuari");
        tfnom = find("#tfnom");
        tfcognom = find("#tfcognom");
        tfcorreu = find("#tfcorreu");
        tftelefon = find("#tftelefon");
        bcanviar = find("#bcanviar");
        tfadreca = find("#tfadreca");
        tfcodipostal = find("#tfcodipostal");
        tfpoblacio = find("#tfpoblacio");
        tfpais = find("#tfpais");
        lobligatorinom = find("#lobligatorinom");
        lobligatoricognom = find("#lobligatoricognom");
        lobligatoricorreu = find("#lobligatoricorreu");
        lphonerror = find("#lphonerror");
        imgCandau=find("#imgCandau");
    }

    @Test
    @Order(1)
    @DisplayName("Comprovacio de camps")
    public void comprovacioCamps() {
        Assertions.assertFalse(tfusuari.isEditable());
        Assertions.assertFalse(tfnom.isEditable());
        Assertions.assertFalse(tfcognom.isEditable());
        Assertions.assertFalse(tfcorreu.isEditable());
        Assertions.assertFalse(tftelefon.isEditable());
        Assertions.assertTrue(bcanviar.isDisabled());
        Assertions.assertFalse(tfadreca.isEditable());
        Assertions.assertFalse(tfcodipostal.isEditable());
        Assertions.assertFalse(tfpoblacio.isEditable());
        Assertions.assertFalse(tfpais.isEditable());
        Assertions.assertFalse(beditar.isDisabled());
        Assertions.assertTrue(bguardar.isDisabled());
        Assertions.assertFalse(lobligatorinom.isDisabled());
        Assertions.assertFalse(lobligatoricognom.isDisabled());
        Assertions.assertFalse(lobligatoricorreu.isDisabled());
        Assertions.assertFalse(lphonerror.isDisabled());
        Assertions.assertTrue(imgCandau.isDisabled());

    }

    @Test
    @Order(2)
    @DisplayName("Click editar dades i comprovar les noves configuracions")
    public void clickEditar() {
        clickOn("#beditar");
        Assertions.assertFalse(tfusuari.isEditable());
        Assertions.assertTrue(tfnom.isEditable());
        Assertions.assertTrue(tfcognom.isEditable());
        Assertions.assertTrue(tfcorreu.isEditable());
        Assertions.assertTrue(tftelefon.isEditable());
        Assertions.assertFalse(bcanviar.isDisabled());
        Assertions.assertTrue(tfadreca.isEditable());
        Assertions.assertTrue(tfcodipostal.isEditable());
        Assertions.assertTrue(tfpoblacio.isEditable());
        Assertions.assertTrue(tfpais.isEditable());
        Assertions.assertTrue(beditar.isDisabled());
        Assertions.assertFalse(bguardar.isDisabled());
        Assertions.assertFalse(lobligatorinom.isDisabled());
        Assertions.assertFalse(lobligatoricognom.isDisabled());
        Assertions.assertFalse(lobligatoricorreu.isDisabled());
        Assertions.assertFalse(lphonerror.isDisabled());
        Assertions.assertFalse(imgCandau.isVisible());

    }
    @Test
    @Order(3)
    @DisplayName("Click guardar dades i comprovar les noves configuracions")
    public void clickGuardar() {
        clickOn("#beditar");
        clickOn("#bguardar");
        Assertions.assertFalse(tfusuari.isEditable());
        Assertions.assertFalse(tfnom.isEditable());
        Assertions.assertFalse(tfcognom.isEditable());
        Assertions.assertFalse(tfcorreu.isEditable());
        Assertions.assertFalse(tftelefon.isEditable());
        Assertions.assertTrue(bcanviar.isDisabled());
        Assertions.assertFalse(tfadreca.isEditable());
        Assertions.assertFalse(tfcodipostal.isEditable());
        Assertions.assertFalse(tfpoblacio.isEditable());
        Assertions.assertFalse(tfpais.isEditable());
        Assertions.assertFalse(beditar.isDisabled());
        Assertions.assertTrue(bguardar.isDisabled());
        Assertions.assertFalse(lobligatorinom.isDisabled());
        Assertions.assertFalse(lobligatoricognom.isDisabled());
        Assertions.assertFalse(lobligatoricorreu.isDisabled());
        Assertions.assertFalse(lphonerror.isDisabled());
        Assertions.assertTrue(imgCandau.isDisabled());
        lmissatgeexit=find("#lmissatgeexit");
        Assertions.assertEquals(lmissatgeexit.getText(),"Dades guardades amb exit");


    }
    @Test
    @Order(4)
    @DisplayName("Comprovar les validacions del camp nom")
    public void checkCampNom() {
        clickOn("#beditar");
        tfnom.clear();
        clickOn(tfnom);
        write("");
        clickOn(bguardar);
        Assertions.assertFalse(lobligatorinom.isDisabled());
        Assertions.assertEquals(lobligatorinom.getText(),"*Aquest camp es obligatori");
    }
    @Test
    @Order(5)
    @DisplayName("Comprovar les validacions del camp cognom")
    public void checkCampCognom() {
        clickOn("#beditar");
        tfcognom.clear();
        clickOn(tfcognom);
        write("");
        clickOn(bguardar);
        Assertions.assertFalse(lobligatoricognom.isDisabled());
        Assertions.assertEquals(lobligatoricognom.getText(),"*Aquest camp es obligatori");

    }
    @Test
    @Order(6)
    @DisplayName("Comprovar les validacions del camp correu")
    public void checkCampCorreu() {
        clickOn("#beditar");
        tfcorreu.clear();
        clickOn(tfcorreu);
        write("");
        clickOn(bguardar);
        Assertions.assertFalse(lobligatoricorreu.isDisabled());
        Assertions.assertEquals(lobligatoricorreu.getText(),"*Aquest camp es obligatori");

        tfcorreu.clear();
        clickOn(tfcorreu);
        write("a");
        clickOn(bguardar);
        Assertions.assertFalse(lobligatoricorreu.isDisabled());
        Assertions.assertEquals(lobligatoricorreu.getText(),"Correu invalid");

        tfcorreu.clear();
        clickOn(tfcorreu);
        write("a@");
        clickOn(bguardar);
        Assertions.assertFalse(lobligatoricorreu.isDisabled());
        Assertions.assertEquals(lobligatoricorreu.getText(),"Correu invalid");

        tfcorreu.clear();
        clickOn(tfcorreu);
        write("a@g");
        clickOn(bguardar);
        Assertions.assertFalse(lobligatoricorreu.isDisabled());
        Assertions.assertEquals(lobligatoricorreu.getText(),"Correu invalid");

        tfcorreu.clear();
        clickOn(tfcorreu);
        write("a@gmail.");
        clickOn(bguardar);
        Assertions.assertFalse(lobligatoricorreu.isDisabled());
        Assertions.assertEquals(lobligatoricorreu.getText(),"Correu invalid");

        tfcorreu.clear();
        clickOn(tfcorreu);
        write("a@gmail.c");
        clickOn(bguardar);
        Assertions.assertFalse(lobligatoricorreu.isDisabled());
        Assertions.assertEquals(lobligatoricorreu.getText(),"Correu invalid");

    }
    @Test
    @Order(7)
    @DisplayName("Comprovar les validacions del camp telefon")
    public void checkCampTelefon() {
        clickOn("#beditar");
        tftelefon.clear();
        clickOn(tftelefon);
        write("1");
        clickOn(bguardar);
        Assertions.assertFalse(lphonerror.isDisabled());
        Assertions.assertEquals(lphonerror.getText(),"*Numero incorrecte (9 digits)");
    }
    @Test
    @Order(8)
    @DisplayName("Comprovar l'obertura correcte de canvi contrasenya")
    public void checkChangePasswordButton() {
        clickOn("#beditar");
        clickOn("#bcanviar");
        TFactualPass=find("#TFactualPass");
        TFnewPass=find("#TFnewPass");
        TFconfirmPass=find("#TFconfirmPass");
        Assertions.assertTrue(TFactualPass.isVisible());
        Assertions.assertTrue(TFnewPass.isVisible());
        Assertions.assertTrue(TFconfirmPass.isVisible());


    }


    private <T extends Node> T find(final String query) {
        return lookup(query).query();
    }

}
